#pragma once
#include <string>
using std::string;

class dimensions
{
public:
	static int nx;
	static int ny;
	static int sub_nx;
	static int sub_ny;

	static int triangle_limit;
	static double dh;


	static void setNum(int, int);
	static void setNumsub(int, int);
	static void setNumparam(int, double);

};


class atomistic
{
public:
	static double cut_off;
	static double r_eq;
	static void setNum(double,double);
};

class BLK1
{
public:
	//CG (it means conjugate gradient) or anything else for homogenous problem
	static string  model;
	static void setMethod(string);

	static double  beta_Var;
	static double theta;
	static double theta_init;

	static void setBeta(double);
	static void setTheta(double,double);

	//number of parallel threads
	static int num_threads;
	static void setThread(int);

	//initial strain 
	//    double load=0.01 *ny;
	static double dt;
	static void setTimestep(double);

	//statique or dynamic or analysis
	static string  method;
	static void setAnalysis(string);


	//pbc or free
	static string  BC;

	//wiener, uniform, defect or none
	static string  IC;
	static void setBCandIC(string, string);

	static double load;
	static void setNum(double);

	static double initial_strain, strain_rate, initial_strain2;
	static void setISandSR(double, double, double);

	static double total_iteration;
	static void setITER(int);

	static string  macro_def;
	static void setMacro_def(string);

	static string  crystal_symmetry;
	static double symmetry_constantx, symmetry_constanty;
	static void setCrystal_symmetry(string);

	static double  loading_limit;
	static void setloading_limit(double);


	static string  bc_x, bc_y, bc_mode;
	static void setBCnew(string, string, string);

	static string  first;
	static void setPoly(string);

	static string   config, config_u;
	static int memory,memory_number;
	static void setMemory(int, int, string, string);

	static int triangle;
	static void setTriangle(int);

	static string grid_creator;
	static void setGrid_creator(string);

	static int cg_save,cg_freq;
	static void setCg_save(int,int);
	
	static string  scalar;

	static void setScalar(string);


	static double  cutting_angle;

	static void setCutting_angle(double);

	static string  constraint_min;

	static void setConstraint_min(string);
	static double scale;
	static void setScale(double);

	static double precision;
	static void setPrecision(double);

	static double d1,d2,d3;
	static void setDisorder(double,double,double);

	static string  guess_method;
	static void   setGuess_method(string);
	
	static int  req;
	static void   setEigenvalues(int);


};

class reading
{
	//    string const filename  = "/Users/salman/Desktop/cluster/dir1497905502/dir_config/config_10176"   ;
	//    string const filename2 = "/Users/salman/Desktop/cluster/dir1497905502/dir_config_u/config_10176" ;
public: 
	// MJ ------------------------------------
	//static string filename = BLK1::config;
	//static string filename2 = BLK1::config_u;
	// ---------------------------------------
	static string filename;
	static string filename2;
	static void set() { filename = BLK1::config; filename2 = BLK1::config_u;}
};