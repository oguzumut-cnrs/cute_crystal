#include "atomistic_grid.h"

void atomistic_grid(coordinates& c){
	int n  = -1;
	double d1 = 1.0;
	double d2 = sqrt(3.0)/2.0;
	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	double xx,yy;
	double  dec_x;
	
	double dc  = atomistic::r_eq;


	c.x.resize(0);
	c.y.resize(0);

	for(int j=0;j<ny;j++)
	{
	
		for(int i=0;i<nx;i++)
		{	


			if (j % 2 == 0)
			{
				xx = dc*i*1.0;
				yy=  dc*j*1.0*sqrt(3.) / 2.;
// 			     xx = d1*(double)i + dec_x;
// 			     yy = d2*(double)j;


				n += 1;
				c.x.push_back(xx);// = xx; 
				c.y.push_back(yy);// = yy;



			}


			if (j % 2 != 0)
			{

				xx= dc*(i*1.0 + 0.5);
				yy= dc*j*1.0*sqrt(3) / 2;;
				
// 				xx = d1*(double)i + dec_x;
// 			    yy = d2*(double)j;
				n += 1;
				c.x.push_back(xx);// = xx; 
				c.y.push_back(yy);// = yy;


			}




			
// 			n += 1;
// 			c.x.push_back(xx);// = xx; 
// 			c.y.push_back(yy);// = yy;

		}
	}

// 	int n_atom = n;
// 	c.set_values (n_atom+1);
	// const matrix_exp::type max 
	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.x_range = x_range;
	c.y_range = y_range;

	c.length0_x = *max_element(c.x.begin(), c.x.end()) + d1/2.0;
	c.length0_y = *max_element(c.y.begin(), c.y.end()) + d2;


	cout<<"x_range = "<<c.x_range<<"\n";
	cout<<"y_range = "<<c.y_range<<"\n";

	cout<<"x_length = "<<c.length0_x<<"\n";
	cout<<"y_length = "<<c.length0_y<<"\n";
	
	cout<<"VERBOSE:: NUMBER OF ATOMS in SUB-GRID"<<c.x.size()<<endl;



 
}

void atomistic_grid_square(coordinates& c)
{
	int n  = -1;
	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	c.x.resize(0);
	c.y.resize(0);
	double xx,yy;
	double  dec_x,dec_y;
	double specialx =0.87606;
	double specialy =0.883982;
	double gamma = sqrt(2.)/pow(3,0.25);

	
	if(BLK1::crystal_symmetry.compare("hex")==0)
		specialx =sqrt(atomistic::r_eq*atomistic::r_eq/(gamma*gamma));
// 		specialx = 0.921518;

	else if(BLK1::crystal_symmetry.compare("square")==0)
		specialx =atomistic::r_eq;		
// 		specialx =1.06619;		
	
	cout<<"special"<<specialx<<endl;
	specialy = specialx;
	
	for(int j=0;j<ny;j++){

		dec_x = 0.;
		dec_y = 0.;
	
		for(int i=0;i<nx;i++){	
			dec_y=0;
// 			xx = 1.06619*(double)i;
// 			yy = 1.06619*(double)j ;

			xx = specialx*(double)i;
			yy = specialx*(double)j;

			n += 1;
			c.x.push_back(xx);// = xx; 
			c.y.push_back(yy);// = yy;
		}
	}

	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.x_range = x_range;
	c.y_range = y_range;

	c.length0_x = *max_element(c.x.begin(), c.x.end()) +  specialx;
	c.length0_y = *max_element(c.y.begin(), c.y.end()) +  specialy;


	cout<<"x_range = "<<c.x_range<<"\n";
	cout<<"y_range = "<<c.y_range<<"\n";

	cout<<"x_length = "<<c.length0_x<<"\n";
	cout<<"y_length = "<<c.length0_y<<"\n";

 	cout<<"VERBOSE:: NUMBER OF ATOMS in SUB-GRID"<<c.x.size()<<endl;

}


void transform_to_tilde_coordinates( coordinates&  c,coordinates&  tilde_c, struct def_grad& F ){

    int n_atom = c.x.size();
	matrix2D h;
	cout<<"NUMBER OF ATOMS in SUB-GRID"<<n_atom<<endl;
	
	h(0,0) = F.f11*c.length0_x;
	h(1,0) = F.f21*c.length0_x;
	h(0,1) = F.f12*c.length0_y;
	h(1,1) = F.f22*c.length0_y;




	matrix2D h_inv;
	h_inv=inv(h);

	if(tilde_c.x.size() ==0){
		tilde_c.x.assign (n_atom,0);
		tilde_c.y.assign (n_atom,0);	
	}

	for (int n=0;n<n_atom;n++){
		tilde_c.x[n]=(h_inv(0,0)*c.x[n] + h_inv(0,1)*c.y[n]) ;
		tilde_c.y[n]=(h_inv(1,0)*c.x[n] + h_inv(1,1)*c.y[n]) ;

	}
	
	cout<<"maxval_x "<<maxval(tilde_c.x)<<endl;
	cout<<"minval_x "<<minval(tilde_c.x)<<endl;

	cout<<"maxval_y "<<maxval(tilde_c.y)<<endl;
	cout<<"minval_y "<<minval(tilde_c.y)<<endl;


}






void transform_to_original_coordinates( coordinates&  c,coordinates&  tilde_c, struct def_grad& F )
{

    int n_atom = c.x.size();
	matrix2D h;

	h(0,0) = F.f11*c.length0_x;
	h(1,0) = F.f21*c.length0_x;
	h(0,1) = F.f12*c.length0_y;
	h(1,1) = F.f22*c.length0_y;

// 	h(:,1) = grad_def(:,1)*length0_x
// 	h(:,2) = grad_def(:,2)*length0_y
// 	h(:,3) = grad_def(:,3)*length0_z
// 
// 
//    x(n) = h(1,1)*x_tilde(n) + h(1,2)*y_tilde(n) + h(1,3)*z_tilde(n)
//    y(n) = h(2,1)*x_tilde(n) + h(2,2)*y_tilde(n) + h(2,3)*z_tilde(n)
//    z(n) = h(3,1)*x_tilde(n) + h(3,2)*y_tilde(n) + h(3,3)*z_tilde(n)


	for (int n=0;n<n_atom;n++)
	{
// 	c.x.push_back(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]);
// 	c.y.push_back(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]);
	c.x[n]=(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]) ;
	c.y[n]=(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]) ;

	}

	cout<<"origin maxval_x "<<maxval(c.x)<<endl;
	cout<<"origin minval_x "<<minval(c.x)<<endl;

	cout<<"origin maxval_y "<<maxval(c.y)<<endl;
	cout<<"origin minval_y "<<minval(c.y)<<endl;



}


void coarse_grain(coordinates& tilde_c,coordinates& c, double angle)
{

	matrix2D grad_def;
	matrix<double> eigenvalues(2,1);
	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;

	double start =0.8;
	double alpha=start;
	double theta_d = angle*dlib::pi/180.;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	struct matrix_stru F;
	double rho =  0;
	double drho =  0;
	coordinates c3;
// 	tilde_coordinates  tilde_c2, c_dx_tilde;
// 	int k=3;

//      
	string  filename="alpha_energy_angle_" + IntToStr(angle) +".dat";
	string  filename2="alpha_energy_derivative_angle_" + IntToStr(angle) +".dat";

	string  filename3="alpha_stress_c11_" + IntToStr(angle) +".dat";
	string  filename4="alpha_stress_c22_" + IntToStr(angle) +".dat";
	string  filename5="alpha_stress_c12_" + IntToStr(angle) +".dat";

	string  filename6="alpha_piola" + IntToStr(angle) +".dat";


// 	
// 	
	double length0_x = c.length0_x;
	double length0_y = c.length0_y; 

    fstream energy_stream;
	energy_stream.open (filename, fstream::in | fstream::out | fstream::app);
    fstream stress_stream;
	stress_stream.open (filename2, fstream::in | fstream::out | fstream::app);


    fstream c11_stream;
	c11_stream.open (filename3, fstream::in | fstream::out | fstream::app);
    fstream c22_stream;
	c22_stream.open (filename4, fstream::in | fstream::out | fstream::app);
    fstream c12_stream;
	c12_stream.open (filename5, fstream::in | fstream::out | fstream::app);

    fstream piola_stream;
	piola_stream.open (filename6, fstream::in | fstream::out | fstream::app);


// 	struct shearing_vectors base1= create_vectors(c,angle);

	
// 	for(int k=0;k<10000;++k){
	cout<<"entering with alpha "<<alpha<<endl;
	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	double detc=1;
	while(detc<1.5){
	

		base_stru real_vec  = shearing_vectors(alpha,angle,0);
	
		base_stru vec       = shearing_vectors(alpha,angle,1);

		struct matrix_stru m2 = find_integer_matrix(real_vec, vec);
// 		cout<<"lagrange _reduction m"<<endl;
// 		m2.print();

		F.m11=(vec.e1[0]);
		F.m21=(vec.e1[1]);
		F.m12=(vec.e2[0]);
		F.m22=(vec.e2[1]);	
// 	    cout<<"printing reducing matrix"<<endl;
// 		m2.print();
		struct cella_stru metric = faicella(vec);
		
// 		cout<<alpha<<endl;

		
// 		transform_to_original_coordinates(c,   tilde_c,  F );		
		
		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=ny*nx/2+nx/2 -1;i<ny*nx/2+nx/2 ;i++){
// 		for(int i=0;i<1;i++){
				
		//find \barrho		

			
			

			for(int j=0;j<tilde_c.x.size();j++){
				
				if(i==j)
					continue;
				


				
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	

				double dx2 = length0_x*dx_tilde  ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2.*dx2*dy2*metric.c12 ;
				
// 				if(j==i+1)
// 					cout<<std::scientific << std::setprecision(7)<<"distance x"<<dx2<<" distance"<<sqrt(r2)<<endl;

	
				
// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;
// // 
// // 
// 				double r2 =  dx2*dx2 + dy2*dy2 ;

// 				double dx2 = length0_x*dx_tilde  ;
// 				double dy2 = length0_y*dy_tilde ;
// 
// 
// 				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;
// 				cout<<"square distance between 2 atoms i and j "<<i<<" "<<j<<" r= "<<r2<<endl;


				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
				e0 +=  force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = force_energy::calculation::getInstance().c.fptr6(r)/r;
				f0 +=  tempf;
				
				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	  (tempf*dx2*dy2);
				

// 				cout<<"energy"<<e0<<endl;

			}
			


			
		}
// 		write_to_a_file(c,k++);
// 		e0/= 2.*nx*ny;
// // 		e0/= 2.;
// 		
// 		f0/=  2.*nx*ny;
// // 		f0/= 2.;	
// 		
// 		sc11/=  2.*nx*ny;
// // 		sc11/= 2.;			
// 		
// 		sc22/=  2.*nx*ny;
// // 		sc22/= 2.;
// 		
// 		sc12/=  2.*nx*ny;
// 		sc12/= 2.;			
					
		double vol = metric.c11*metric.c22-metric.c12*metric.c12;
		detc=vol;
// 		cout<<"vol= "<<vol<<endl;
		energy_stream<<std::scientific << std::setprecision(7)<<vol <<" "<<e0<<endl;
		stress_stream<<std::scientific << std::setprecision(7)<<vol <<" "<<f0<<endl;

		struct cella_stru  dtemp;
		dtemp.c11 = sc11;
		dtemp.c22 = sc22;
		dtemp.c12 = sc12;
		
// 		struct cella_stru temp2 = riduci_matrix_reductions_stress_inverse(dtemp, m2);
		struct cella_stru temp2 = dtemp;

		c11_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c11<<endl;
		c22_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c22<<endl;
		c12_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c12<<endl;


	    temp2 = riduci_matrix_reductions_stress(dtemp, m2);
	    base_stru v1 = real_vec;
		//phi,11
		double P11 = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
		//phi,21
		double P21= temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
		//phi,12
		double P12 = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
		//phi,22
		double P22 = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


		piola_stream << std::scientific << std::setprecision(7) << " " << alpha << " " 
		<< P12 << " " 
		<< P21 << " " 
		<< P11 << " " 
		<< P22 << endl;



		alpha+=0.001;
// 		cout<<"e0 "<<e0<<endl;
		e0=0;
		f0=0;
		rho=0;
		drho=0;
		sc11=0;
		sc22=0;
		sc12=0;
	}
	
	energy_stream.close();
	stress_stream.close();
	c11_stream.close();
	c22_stream.close();
	c12_stream.close();

	

}



void coarse_grain_eam(coordinates& tilde_c,coordinates& c, double angle)
{

	matrix2D grad_def;
	matrix<double> eigenvalues(2,1);
	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;

	double start =4.1;
	double alpha=-start;
	double theta_d = angle*dlib::pi/180.;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	struct matrix_stru F;
	double rho =  0;
	double drho =  0;
	coordinates c3;
// 	tilde_coordinates  tilde_c2, c_dx_tilde;
// 	int k=3;

//      
	string  filename="alpha_energy_angle_" + IntToStr(angle) +".dat";
	string  filename2="alpha_energy_derivative_angle_" + IntToStr(angle) +".dat";

	string  filename3="alpha_stress_c11_" + IntToStr(angle) +".dat";
	string  filename4="alpha_stress_c22_" + IntToStr(angle) +".dat";
	string  filename5="alpha_stress_c12_" + IntToStr(angle) +".dat";

	string  filename6="alpha_piola" + IntToStr(angle) +".dat";


// 	
// 	
	double length0_x = c.length0_x;
	double length0_y = c.length0_y; 

    fstream energy_stream;
	energy_stream.open (filename, fstream::in | fstream::out | fstream::app);
    fstream stress_stream;
	stress_stream.open (filename2, fstream::in | fstream::out | fstream::app);


    fstream c11_stream;
	c11_stream.open (filename3, fstream::in | fstream::out | fstream::app);
    fstream c22_stream;
	c22_stream.open (filename4, fstream::in | fstream::out | fstream::app);
    fstream c12_stream;
	c12_stream.open (filename5, fstream::in | fstream::out | fstream::app);

    fstream piola_stream;
	piola_stream.open (filename6, fstream::in | fstream::out | fstream::app);


// 	struct shearing_vectors base1= create_vectors(c,angle);

	
// 	for(int k=0;k<400;++k){
	cout<<"entering"<<endl;
	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;

	while(alpha<start){
	

		base_stru real_vec  = shearing_vectors(alpha,angle,0);
	
		base_stru vec       = shearing_vectors(alpha,angle,1);

		struct matrix_stru m2 = find_integer_matrix(real_vec, vec);
// 		cout<<"lagrange _reduction m"<<endl;
// 		m2.print();

		F.m11=(vec.e1[0]);
		F.m21=(vec.e1[1]);
		F.m12=(vec.e2[0]);
		F.m22=(vec.e2[1]);	
// 	    cout<<"printing reducing matrix"<<endl;
// 		m2.print();
		struct cella_stru metric = faicella(vec);
		
// 		cout<<alpha<<endl;

		
// 		transform_to_original_coordinates(c,   tilde_c,  F );		
		
		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=nx/2-1;i<nx/2;i++){
// 		for(int i=0;i<1;i++){
				
		//find \barrho		
		rho = 0;
		for(int j=0;j<tilde_c.x.size();j++){
			if(i==j)
				continue;
			double dx_tilde = (tilde_c.x[i] - tilde_c.x[j]);
			double dy_tilde = (tilde_c.y[i] - tilde_c.y[j]);



		if(abs(dx_tilde) > 0.5) 
			dx_tilde -=   copysign(1., dx_tilde) ;
		if(abs(dy_tilde) > 0.5) 
			dy_tilde -=   copysign(1., dy_tilde) ;




		
		double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
		double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;
		double r2 =  dx2*dx2 + dy2*dy2 ;



		if( r2 >= r_cutoff_sq )
			continue;
		double r = sqrt(r2)*BLK1::scale;
		rho  +=   EAM_electron(r);
		
		}
			
			

			for(int j=0;j<tilde_c.x.size();j++){
				
				if(i==j)
					continue;
				


				
				double dx_tilde = (tilde_c.x[i] - tilde_c.x[j]);
				double dy_tilde = (tilde_c.y[i] - tilde_c.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	


	
				
				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;
// 
// 
				double r2 =  dx2*dx2 + dy2*dy2 ;

// 				double dx2 = length0_x*dx_tilde  ;
// 				double dy2 = length0_y*dy_tilde ;
// 
// 
// 				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;
// 				cout<<"square distance between 2 atoms i and j "<<i<<" "<<j<<" r= "<<r2<<endl;


				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
				e0 +=  force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = force_energy::calculation::getInstance().c.fptr6(r)/r;
				f0 +=  tempf;
				f0 +=  EAM_function_der(rho)*EAM_electron_der(r)/r;
				
				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	  (tempf*dx2*dy2);
				

// 				cout<<"energy"<<e0<<endl;

			}
			
				e0 += EAM_function(rho);


			
		}
// 		write_to_a_file(c,k++);
// 		e0/= 2.*nx*ny;
// // 		e0/= 2.;
// 		
// 		f0/=  2.*nx*ny;
// // 		f0/= 2.;	
// 		
// 		sc11/=  2.*nx*ny;
// // 		sc11/= 2.;			
// 		
// 		sc22/=  2.*nx*ny;
// // 		sc22/= 2.;
// 		
// 		sc12/=  2.*nx*ny;
// 		sc12/= 2.;			
					
		energy_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<e0<<endl;
		stress_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<f0<<endl;

		struct cella_stru  dtemp;
		dtemp.c11 = sc11;
		dtemp.c22 = sc22;
		dtemp.c12 = sc12;
		
// 		struct cella_stru temp2 = riduci_matrix_reductions_stress_inverse(dtemp, m2);
		struct cella_stru temp2 = dtemp;

		c11_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c11<<endl;
		c22_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c22<<endl;
		c12_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<temp2.c12<<endl;


	    temp2 = riduci_matrix_reductions_stress(dtemp, m2);
	    base_stru v1 = real_vec;
		//phi,11
		double P11 = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
		//phi,21
		double P21= temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
		//phi,12
		double P12 = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
		//phi,22
		double P22 = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


		piola_stream << std::scientific << std::setprecision(7) << " " << alpha << " " 
		<< P12 << " " 
		<< P21 << " " 
		<< P11 << " " 
		<< P22 << endl;



		alpha+=0.005;
		e0=0;
		f0=0;
		rho=0;
		drho=0;
		sc11=0;
		sc22=0;
		sc12=0;
	}
	
	energy_stream.close();
	stress_stream.close();
	c11_stream.close();
	c22_stream.close();
	c12_stream.close();

	

}




struct energy_stress coarse_grain_fast(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v)
{

	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	int counter=0;

	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	double length0_x = force_energy::calculation::getInstance().c.coo.length0_x;
	double length0_y = force_energy::calculation::getInstance().c.coo.length0_y; 
	double n = force_energy::calculation::getInstance().c.tilde_coo.x.size();

	
	struct energy_stress temp;
	struct matrix_stru F;
	F.m11 =v.e1[0];
	F.m22 =v.e2[1];
	F.m12 =v.e2[0];
	F.m21 =v.e1[1];
	double c11= metric.c11;
	double c22= metric.c22;
	double c12= metric.c12;
	double sqrtdetc=sqrt(c11*c22-c12*c12);
	

		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=ny*nx/2+nx/2 -1;i<ny*nx/2+nx/2 ;i++){
// 		for(int i=0;i<1;i++){

			for(int j=0;j<n;j++){
				
				if(i==j)
					continue;
			
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	
				double dx2 = length0_x*dx_tilde ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2.*dx2*dy2*metric.c12 ;
				
				if( r2 >= r_cutoff_sq )
					continue;
			
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
				e0 +=  0.5*force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = 0.5*force_energy::calculation::getInstance().c.fptr6(r)/r;
// 				f0 +=  tempf;
				
				//dphi/dC
				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	  (tempf*dx2*dy2);
// 				counter++;

// 				cout<<"energy"<<e0<<endl;

			}
			
		}
		

		temp.energy  =  e0-force_energy::calculation::getInstance().c.zeroing;
		temp.r_stress.c11 = sc11;
		temp.r_stress.c22 = sc22;
		temp.r_stress.c12 = sc12;

		
		return temp;
					
	

}

struct cella_stru coarse_grain_fast_stress(const struct cella_stru& metric, double burgers, double alloy)
{

	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	int counter=0;

	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	double length0_x = force_energy::calculation::getInstance().c.coo.length0_x;
	double length0_y = force_energy::calculation::getInstance().c.coo.length0_y; 
	double n = force_energy::calculation::getInstance().c.tilde_coo.x.size();

	
	struct cella_stru temp;
	struct matrix_stru F;
// 	F.m11 =v.e1[0];
// 	F.m22 =v.e2[1];
// 	F.m12 =v.e2[0];
// 	F.m21 =v.e1[1];
	double c11= metric.c11;
	double c22= metric.c22;
	double c12= metric.c12;
	double sqrtdetc=sqrt(c11*c22-c12*c12);
	

		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=nx/2-1;i<nx/2;i++){
// 		for(int i=0;i<1;i++){

			for(int j=0;j<n;j++){
				
				if(i==j)
					continue;
			
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	
				double dx2 = length0_x*dx_tilde ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2.*dx2*dy2*metric.c12 ;
// 				double r2 =  dx2*dx2 + dy2*dy2 ;
// 				if(abs(r2-r3) >0.00001 )
// 				cout<< r2 << " " << r3<<endl;
				
				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
// 				e0 +=  0.5*force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = 0.5*force_energy::calculation::getInstance().c.fptr6(r)/r;
// 				f0 +=  tempf;
				
				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	  (tempf*dx2*dy2);
// 				counter++;

// 				cout<<"energy"<<e0<<endl;

			}
			
		}
		
		double K=0.;

// 		temp.c11 = sc11/(1) + K*(c22/burgers-c22/(c11*c22-c12*c12));
// 		temp.c22 = sc22/(1) + K*(c11/burgers-c11/(c11*c22-c12*c12));
// 		temp.c12 = sc12/(1) - K*((c12*2.0)/burgers-(c12*2.0)/(c11*c22-c12*c12));

		double div = 1.; //(2.*nx*ny
// 		temp.energy  =  e0/(div);
		temp.c11 = sc11/(div);
		temp.c22 = sc22/(div);
		temp.c12 = sc12/(div);
		
		return temp;
					
	

}

struct energy_stress coarse_grain_fast_EAM(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v)
{

	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	int counter=0;

	double e0=0;
	double f0=0;
	double rho=0;
	double drho=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	double length0_x = force_energy::calculation::getInstance().c.coo.length0_x;
	double length0_y = force_energy::calculation::getInstance().c.coo.length0_y; 
	double n = force_energy::calculation::getInstance().c.tilde_coo.x.size();

	
	struct energy_stress temp;
	struct matrix_stru F;
	F.m11 =v.e1[0];
	F.m22 =v.e2[1];
	F.m12 =v.e2[0];
	F.m21 =v.e1[1];
	

		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=nx/2-1;i<nx/2;i++){
// 		for(int i=0;i<1;i++){


		rho = 0;
		for(int j=0;j<n;j++){
			if(i==j)
				continue;
			double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
							  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
			double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
							  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



		if(abs(dx_tilde) > 0.5) 
			dx_tilde -=   copysign(1., dx_tilde) ;
		if(abs(dy_tilde) > 0.5) 
			dy_tilde -=   copysign(1., dy_tilde) ;




		
		double dx2 = length0_x*dx_tilde  ;
		double dy2 = length0_y*dy_tilde ;
		double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;



		if( r2 >= r_cutoff_sq )
			continue;
		double r = sqrt(r2)*BLK1::scale;
		rho  +=   EAM_electron(r);
		
		}

			for(int j=0;j<n;j++){
				
				if(i==j)
					continue;
			
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	
				double dx2 = length0_x*dx_tilde  ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;
// 				double r2 =  dx2*dx2 + dy2*dy2 ;
// 				if(abs(r2-r3) >0.00001 )
// 				cout<< r2 << " " << r3<<endl;
				
				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
				e0 +=  force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = force_energy::calculation::getInstance().c.fptr6(r)/r;
// 				f0 +=  tempf;
				tempf +=  EAM_function_der(rho)*EAM_electron_der(r)/r;

				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	  (tempf*dx2*dy2);
// 				counter++;

// 				cout<<"energy"<<e0<<endl;

			}
				e0 += EAM_function(rho);
			
		}
		
		temp.energy  =  e0/(1);
		temp.r_stress.c11 = sc11/(1);
		temp.r_stress.c22 = sc22/(1);
		temp.r_stress.c12 = sc12/(1);


// 		temp.energy  =  e0/(2.*nx*ny);
// 		temp.r_stress.c11 = sc11/(2.*nx*ny);
// 		temp.r_stress.c22 = sc22/(2.*nx*ny);
// 		temp.r_stress.c12 = sc12/(2.*nx*ny);
		
		return temp;
					
	

}

struct double_cella_stru coarse_grain_fast_AT(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v)
{

	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	int counter=0;

	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	double length0_x = force_energy::calculation::getInstance().c.coo.length0_x;
	double length0_y = force_energy::calculation::getInstance().c.coo.length0_y; 
	double n = force_energy::calculation::getInstance().c.tilde_coo.x.size();
	
	struct double_cella_stru temp_AT;
	struct energy_stress temp;
	
	
	temp_AT.c11.c11 =0;
	temp_AT.c22.c22 =0;
	temp_AT.c12.c12 =0;
	temp_AT.c22.c12 =0;
	temp_AT.c11.c12 =0;
	temp_AT.c11.c22 =0;

	temp_AT.c22.c11 = temp_AT.c11.c22;
	temp_AT.c12.c11 = temp_AT.c11.c12;
	temp_AT.c12.c22 = temp_AT.c22.c12;

	struct matrix_stru F;

	F.m11 =v.e1[0];
	F.m22 =v.e2[1];
	F.m12 =v.e2[0];
	F.m21 =v.e1[1];
	

		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=ny*nx/2+nx/2 -1;i<ny*nx/2+nx/2 ;i++){
// 		for(int i=0;i<1;i++){

			for(int j=0;j<n;j++){
				
				if(i==j)
					continue;
			
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	
				double dx2 = length0_x*dx_tilde  ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;
// 				double r2 =  dx2*dx2 + dy2*dy2; ;

				
				if( r2 >= r_cutoff_sq )
					continue;
				
				double r = sqrt(r2)*BLK1::scale;
				
				e0 +=  .5*force_energy::calculation::getInstance().c.fptr5(r);
				//first derivative of energy with respect to r
				double tempf = .5*force_energy::calculation::getInstance().c.fptr6(r)/r;
				//second derivative of energy with respect to r
				double tempf2 = .5*force_energy::calculation::getInstance().c.fptr7(r)/(r);

				
				sc11 +=   0.5*tempf*dx2*dx2;
				sc22 +=   0.5*tempf*dy2*dy2;
				sc12 +=	      tempf*dx2*dy2;
				double A = dx2*dx2*dx2*dx2;
				double B = dy2*dy2*dy2*dy2;
				double C = dx2*dx2*dy2*dy2;
				double D = dx2*dx2*dx2*dy2;
				double E = dy2*dy2*dy2*dx2;
				
				temp_AT.c11.c11 += 0.25*tempf2*A/r - 0.25*tempf*A/(r*r);//since it already divided by r;normally there is r^2 and r^3 see notes
				temp_AT.c22.c22 += 0.25*tempf2*B/r - 0.25*tempf*B/(r*r);
				temp_AT.c12.c12 +=      tempf2*C/r - 1.00*tempf*C/(r*r);
				
				temp_AT.c11.c12 += 0.5 *tempf2*D/r  - 0.5 *tempf*D/(r*r);
				temp_AT.c22.c12 += 0.5 *tempf2*E/r  - 0.5 *tempf*E/(r*r);
				temp_AT.c11.c22 += 0.25*tempf2*C/r  - 0.25*tempf*C/(r*r);



			}
			
		}

		temp.energy  =  e0;
		temp.r_stress.c11 = sc11;
		temp.r_stress.c22 = sc22;
		temp.r_stress.c12 = sc12;

// 		temp_AT.c11.c11 /=(div);
// 		temp_AT.c22.c22 /=(div);
// 		temp_AT.c12.c12 /=(div);
// 		temp_AT.c22.c12 /=(div);
// 		temp_AT.c11.c12 /=(div);
// 		temp_AT.c11.c22 /=(div);

		temp_AT.c22.c11 = temp_AT.c11.c22;
		temp_AT.c12.c11 = temp_AT.c11.c12;
		temp_AT.c12.c22 = temp_AT.c22.c12;
		
		
		return temp_AT;
					

}

struct double_cella_stru coarse_grain_fast_AT_EAM(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v)
{

	int nx=dimensions::sub_nx;
	int ny=dimensions::sub_ny;
	int counter=0;

	double e0=0;
	double f0=0;
	double sc11=0;
	double sc22=0;
	double sc12=0;
	double rho=0;
	double r_cutoff_sq = atomistic::cut_off*atomistic::cut_off;
	double length0_x = force_energy::calculation::getInstance().c.coo.length0_x;
	double length0_y = force_energy::calculation::getInstance().c.coo.length0_y; 
	double n = force_energy::calculation::getInstance().c.tilde_coo.x.size();
	
	struct double_cella_stru temp_AT;
	struct energy_stress temp;
	
	
	temp_AT.c11.c11 =0;
	temp_AT.c22.c22 =0;
	temp_AT.c12.c12 =0;
	temp_AT.c22.c12 =0;
	temp_AT.c11.c12 =0;
	temp_AT.c11.c22 =0;

	temp_AT.c22.c11 = temp_AT.c11.c22;
	temp_AT.c12.c11 = temp_AT.c11.c12;
	temp_AT.c12.c22 = temp_AT.c22.c12;

	struct matrix_stru F;

	F.m11 =v.e1[0];
	F.m22 =v.e2[1];
	F.m12 =v.e2[0];
	F.m21 =v.e1[1];
	

		
// 		for(int i=0;i<tilde_c.x.size();i++){
		for(int i=nx/2-1;i<nx/2;i++){
// 		for(int i=0;i<1;i++){
		rho = 0;
		for(int j=0;j<n;j++){
			if(i==j)
				continue;
			double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
							  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
			double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
							  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



		if(abs(dx_tilde) > 0.5) 
			dx_tilde -=   copysign(1., dx_tilde) ;
		if(abs(dy_tilde) > 0.5) 
			dy_tilde -=   copysign(1., dy_tilde) ;




		
		double dx2 = length0_x*dx_tilde  ;
		double dy2 = length0_y*dy_tilde ;
		double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;



		if( r2 >= r_cutoff_sq )
			continue;
		double r = sqrt(r2)*BLK1::scale;
		rho  +=   EAM_electron(r);
		
		}

			for(int j=0;j<n;j++){
				
				if(i==j)
					continue;
			
				double dx_tilde = (force_energy::calculation::getInstance().c.tilde_coo.x[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.x[j]);
				double dy_tilde = (force_energy::calculation::getInstance().c.tilde_coo.y[i] 
								  -force_energy::calculation::getInstance().c.tilde_coo.y[j]);



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
	
				double dx2 = length0_x*dx_tilde  ;
				double dy2 = length0_y*dy_tilde ;

// 				double dx2 = F.m11*length0_x*dx_tilde + F.m12*length0_y*dy_tilde ;
// 				double dy2 = F.m21*length0_x*dx_tilde + F.m22*length0_y*dy_tilde ;

				double r2 =  dx2*dx2*metric.c11 + dy2*dy2*metric.c22 + 2*dx2*dy2*metric.c12 ;
// 				double r2 =  dx2*dx2 + dy2*dy2; ;

				
				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2)*BLK1::scale;
				
// 				e0 +=  lennard_jones_energy(r);
				e0 +=  force_energy::calculation::getInstance().c.fptr5(r);
				double tempf = force_energy::calculation::getInstance().c.fptr6(r)/r;
				//second derivative of energy with respect to r
				double tempf2 = force_energy::calculation::getInstance().c.fptr7(r)/r;
				
				double eam1   = EAM_function_der(rho)*EAM_electron_der(r)/r;
				double eam2   = EAM_function_der(rho)*EAM_electron_sder(r)/r;
				
				double eam3p = EAM_function_sder(rho);
				
				double eam3  = (EAM_electron_der(r))/(r);

// 				f0 +=  tempf;
				
				sc11 +=   0.5*(tempf+eam1)*dx2*dx2 ;
				sc22 +=   0.5*(tempf+eam1)*dy2*dy2 ;
				sc12 +=	  ((tempf+eam1)*dx2*dy2)    ;
				
				double A = dx2*dx2*dx2*dx2;
				double B = dy2*dy2*dy2*dy2;
				double C = dx2*dx2*dy2*dy2;
				double D = dx2*dx2*dx2*dy2;
				double E = dy2*dy2*dy2*dx2;
				
				temp_AT.c11.c11 += 0.25*(tempf2 + eam2)*A/r - 0.25*(tempf + eam1)*A/(r*r);//since it already divided by r;normally there is r^2 and r^3 see notes
				temp_AT.c22.c22 += 0.25*(tempf2 + eam2)*B/r - 0.25*(tempf + eam1)*B/(r*r);
				temp_AT.c12.c12 +=      (tempf2 + 2*eam2)*C/r - 1.00*(tempf + eam1)*C/(r*r);
				
				temp_AT.c11.c12 += 0.5 *(tempf2+ eam2)*D/r  - 0.5 *(tempf + eam1)*D/(r*r);
				temp_AT.c22.c12 += 0.5 *(tempf2+ eam2)*E/r  - 0.5 *(tempf + eam1)*E/(r*r);
				temp_AT.c11.c22 += 0.25*(tempf2+ eam2)*C/r  - 0.25*(tempf + eam1)*C/(r*r);



				temp_AT.c11.c11 += eam3p*0.25*eam3*eam3*A;//since it already divided by r;normally there is r^2 and r^3 see notes
				temp_AT.c22.c22 += eam3p*0.25*eam3*eam3*B;
				temp_AT.c12.c12 += eam3p*eam3*eam3*C;
				
				temp_AT.c11.c12 += eam3p*0.5*eam3*eam3*D;
				temp_AT.c22.c12 += eam3p*0.5*eam3*eam3*E;
				temp_AT.c11.c22 += eam3p*0.25*eam3*eam3*C;


// 				counter++;

// 				cout<<"energy"<<e0<<endl;

			}
			
		}


		temp.energy  =  e0/(1);
		temp.r_stress.c11 = sc11/(1);
		temp.r_stress.c22 = sc22/(1);
		temp.r_stress.c12 = sc12/(1);

		temp_AT.c11.c11 /=(1);
		temp_AT.c22.c22 /=(1);
		temp_AT.c12.c12 /=(1);
		temp_AT.c22.c12 /=(1);
		temp_AT.c11.c12 /=(1);
		temp_AT.c11.c22 /=(1);

		temp_AT.c22.c11 = temp_AT.c11.c22;
		temp_AT.c12.c11 = temp_AT.c11.c12;
		temp_AT.c12.c22 = temp_AT.c22.c12;
		

		
// 		temp.energy  =  e0/(2.*nx*ny);
// 		temp.r_stress.c11 = sc11/(2.*nx*ny);
// 		temp.r_stress.c22 = sc22/(2.*nx*ny);
// 		temp.r_stress.c12 = sc12/(2.*nx*ny);
// 
// 		temp_AT.c11.c11 /=(2.*nx*ny);
// 		temp_AT.c22.c22 /=(2.*nx*ny);
// 		temp_AT.c12.c12 /=(2.*nx*ny);
// 		temp_AT.c22.c12 /=(2.*nx*ny);
// 		temp_AT.c11.c12 /=(2.*nx*ny);
// 		temp_AT.c11.c22 /=(2.*nx*ny);
// 
// 		temp_AT.c22.c11 = temp_AT.c11.c22;
// 		temp_AT.c12.c11 = temp_AT.c11.c12;
// 		temp_AT.c12.c22 = temp_AT.c22.c12;
		
		return temp_AT;
					

}


double lennard_jones_energy(double r){
	int it=0;
	int jt=0;


	  double  nn =12;
	  double  mm =6;
	  double eps = 0.5;
	  double sigma= 2*sin(dlib::pi/10.);	
	  double n_sur_m = nn / mm;
	  double m=6;
	  double n=12;

	


// 	double energy = a(it,jt) *(pow((r0(it,jt)/r),nn)- n_sur_m*pow((r0(it,jt)/r),mm));
// 	double energy = pow(1./r,nn)- n_sur_m*pow((1./r),mm);
//	double energy = 4*eps*(-(pow(sigma,6)/pow(r,6)) + pow(sigma,12)/pow(r,12));

	//used one
//  	double energy = 4.*eps*(-pow(sigma/r,m) + pow(sigma/r,n));
	
	double energy;
	double r_in = -1. + sqrt(5.);
	double r_cut = 1.5450849718747373;
	
	if(r<r_in)
		energy =0.014495849609375 + 2.*(pow(-1 + sqrt(5.),12)/(4096.*pow(r,12)) - pow(-1 + sqrt(5),6)/(64.*pow(r,6)));
	
	else if(r>=r_in && r<r_cut )
		energy  =  -0.016265869140625 + 0.1469503524938869*(1 - sqrt(5.) + r) - 0.4045936315621786*pow(1 - sqrt(5.) + r,2) + 
    0.20683925671385717*pow(1 - sqrt(5.) + r,3) + 0.37148697180357115*pow(1 - sqrt(5.) + r,4);
    
    else if(r>=r_cut)
    	energy=0;

	return energy;

}

double lennard_jones_energy_der(double r)
{
// 	namespace lennard_jones = lennard_jones;

	  double  nn =12;
	  double  mm =6;
	  double eps = 0.5;
	  double sigma= 2*sin(dlib::pi/10.);	
	  double n_sur_m = nn / mm;
	  double m=6;
	  double n=12;
	
  	//double f0 = mm*n_sur_m*pow(1./r,1 + mm) - nn*pow(1./r,1 + nn);
  	// 	double f0 = 4*eps*((6*pow(sigma,6))/pow(r,7) - (12*pow(sigma,12))/pow(r,13));

	//used one
//  	double f0 =4.*eps*((m*sigma*pow(sigma/r,-1. + m))/pow(r,2.) - (n*sigma*pow(sigma/r,-1. + n))/pow(r,2.));

	double r_in = -1. + sqrt(5.);
	double r_cut = 1.5450849718747373;

	double f0;
	
	if(r<r_in)
		f0 =2.*((-3*pow(-1 + sqrt(5.),12))/(1024.*pow(r,13)) + (3*pow(-1 + sqrt(5.),6))/(32.*pow(r,7)));
	else if(r>=r_in && r<r_cut )
		f0  = 0.1469503524938869 - 0.8091872631243572*(1 - sqrt(5.) + r) + 0.6205177701415715*pow(1 - sqrt(5.) + r,2) + 1.4859478872142846*pow(1 - sqrt(5.) + r,3);
    else if(r>=r_cut)
    	f0=0;



	return f0;

}

double lennard_jones_energy_sder(double r)
{
// 	namespace lennard_jones = lennard_jones;


	  double  nn =12;
	  double  mm =6;
	  double m=6;
	  double n=12;

	
	  double n_sur_m = nn / mm;

	  double eps = 0.5;
	  double sigma= 2*sin(dlib::pi/10.);	


//   	double f0 = -(mm*(1 + mm)*n_sur_m*pow(1/r,2 + mm)) + nn*(1 + nn)*pow(1/r,2 + nn);
//double f0 = 4*eps*((-42*pow(sigma,6))/pow(r,8) + (156*pow(sigma,12))/pow(r,14));

//used one 
// double f0 = 4*eps*(-(((-1. + m)*m*pow(sigma,2.)*pow(sigma/r,-2. + m))/pow(r,4.)) - 
//      (2.*m*sigma*pow(sigma/r,-1. + m))/pow(r,3.) + 
//      ((-1. + n)*n*pow(sigma,2.)*pow(sigma/r,-2. + n))/pow(r,4.) + 
//      (2.*n*sigma*pow(sigma/r,-1. + n))/pow(r,3.));

double f0;
	double r_in = -1. + sqrt(5.);
	double r_cut = 1.5450849718747373;
	if(r<r_in)
		f0 = 2.*((39*pow(-1 + sqrt(5.),12))/(1024.*pow(r,14)) - (21*pow(-1 + sqrt(5.),6))/(32.*pow(r,8)));
	else if(r>=r_in && r<r_cut )
		f0 =-0.8091872631243572 + 1.241035540283143*(1 - sqrt(5.) + r) + 4.457843661642854*pow(1 - sqrt(5.) + r,2);
    else if(r>=r_cut)
    	f0=0;



	return f0;

}





double lennard_jones_energy_v2(double r){
	 
	
	double energy;
	double rc =2.5;

	
	if(r<=rc)
		energy =-4*(pow(rc,-12) - pow(rc,-6)) + 4*(pow(r,-12) - pow(r,-6)) - 
      4*(-12/pow(rc,13) + 6/pow(rc,7))*(-rc + r);

	
	else if(r>rc)
		energy  =  0;
    

	return energy;

}


double lennard_jones_energy_der_v2(double r)
{
	double f0;
	double rc =2.5;

	if(r<=rc)
		f0  = -4*(-12/pow(rc,13) + 6/pow(rc,7)) + 4*(-12/pow(r,13) + 6/pow(r,7));

	
	else if(r>rc)
		f0  =  0;


	return f0;

}

double lennard_jones_energy_sder_v2(double r){

	double f0;
	double rc =2.5;

	if(r<=rc)
		f0  = 4*(156/pow(r,14) - 42/pow(r,8));

	
	else if(r>rc)
		f0  =  0;


	return f0;



}





double square_energy(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;

//double energy= 0.672/pow(E,42.242*pow(-1.8248 + r,2)) + pow(r,-12) - 2/pow(r,6);
// double energy= 0.828/pow(E,26.5*pow(-1.79 + r,2)) + pow(r,-12) - 2./pow(r,6);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
	double energy=-2./pow(E,8*pow(-1.425 + r,2)) - 2./pow(E,8*pow(-1 + r,2)) + pow(r,-12);

	return energy;

}

//dphi/dr
double square_energy_der(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;

// 	double der   =(-56.773248*(-1.8248 + r))/pow(E,42.242*pow(-1.8248 + r,2)) - 12/pow(r,13) + 
//    12/pow(r,7);

// double der = (-43.884*(-1.79 + r))/pow(E,26.5*pow(-1.79 + r,2)) - 12/pow(r,13) + 12/pow(r,7);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
   double der = (32*(-1.425 + r))/pow(E,8*pow(-1.425 + r,2)) + 
   (32*(-1 + r))/pow(E,8*pow(-1 + r,2)) - 12/pow(r,13);



	return der;

}

double square_energy_sder(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;


// double sder =-56.773248/pow(E,42.242*pow(-1.8248 + r,2)) + 
//    (4796.4310840319995*pow(-1.8248 + r,2))/pow(E,42.242*pow(-1.8248 + r,2)) + 
//    156/pow(r,14) - 84/pow(r,8);
   
//    double sder = -43.884/pow(E,26.5*pow(-1.79 + r,2)) + 
//    (2325.852*pow(-1.79 + r,2))/pow(E,26.5*pow(-1.79 + r,2)) + 156/pow(r,14) - 
//    84/pow(r,8);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
double sder =  32./pow(E,8*pow(-1.425 + r,2)) + 32/pow(E,8*pow(-1. + r,2)) - 
   (512*pow(-1.425 + r,2))/pow(E,8*pow(-1.425 + r,2)) - 
   (512*pow(-1. + r,2))/pow(E,8*pow(-1. + r,2)) + 156/pow(r,14);

	return sder;

}

double square_energy_v2(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;

// double energy= 0.672/pow(E,42.242*pow(-1.8248 + r,2)) + pow(r,-12) - 2/pow(r,6);
double energy= 0.828/pow(E,26.5*pow(-1.79 + r,2)) + pow(r,-12) - 2./pow(r,6);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
// 	double energy=-2/pow(E,8*pow(-1.425 + r,2)) - 2/pow(E,8*pow(-1 + r,2)) + pow(r,-12);

	return energy;

}

//dphi/dr
double square_energy_der_v2(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;

// 	double der   =(-56.773248*(-1.8248 + r))/pow(E,42.242*pow(-1.8248 + r,2)) - 12/pow(r,13) + 
//    12/pow(r,7);

double der = (-43.884*(-1.79 + r))/pow(E,26.5*pow(-1.79 + r,2)) - 12/pow(r,13) + 12/pow(r,7);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
//    double der = (32*(-1.425 + r))/pow(E,8*pow(-1.425 + r,2)) + 
//    (32*(-1 + r))/pow(E,8*pow(-1 + r,2)) - 12/pow(r,13);



	return der;

}

double square_energy_sder_v2(double r){

	double si = 1./1.12246;
	double E=2.71828182846;
	double m=6;
	double n=12;
	double r0=1.4;
	double sigmasq=0.02;
	double eps=1.1;


// double sder =-56.773248/pow(E,42.242*pow(-1.8248 + r,2)) + 
//    (4796.4310840319995*pow(-1.8248 + r,2))/pow(E,42.242*pow(-1.8248 + r,2)) + 
//    156/pow(r,14) - 84/pow(r,8);
   
   double sder = -43.884/pow(E,26.5*pow(-1.79 + r,2)) + 
   (2325.852*pow(-1.79 + r,2))/pow(E,26.5*pow(-1.79 + r,2)) + 156/pow(r,14) - 
   84/pow(r,8);

//Molecular-dynamics study of the melting of hexagonal and square lattices in two dimensions
// double sder =  32./pow(E,8*pow(-1.425 + r,2)) + 32/pow(E,8*pow(-1. + r,2)) - 
//    (512*pow(-1.425 + r,2))/pow(E,8*pow(-1.425 + r,2)) - 
//    (512*pow(-1. + r,2))/pow(E,8*pow(-1. + r,2)) + 156/pow(r,14);

	return sder;

}




double EAM_electron(double r){

	double re = 4.05;
	double fe= 1.0;
	double xi = 2.5;

	double temp = fe*exp(-xi*(r-re));

	return temp;

}

double EAM_electron_der(double r){

	double re = 4.05;
	double fe= 1.0;
	double xi = 2.5;
	double E=2.71828;

	double temp = -((fe*xi)/pow(E,(r - re)*xi));

	return temp;

}


double EAM_electron_sder(double r){

	double re = 4.05;
	double fe= 1.0;
	double xi = 2.5;
	double E=2.71828;

	double temp = (fe*pow(xi,2))/pow(E,(r - re)*xi);

	return temp;

}



double EAM_function(double rho){

	double rhoe =1.5;
	double fe= 1.0;
	double xi = 2.5;
	double n=0.5;
	double F1 = -0.1392;
	double F0 = 3.32 - 0.73;

	double temp = (F1*rho)/rhoe - F0*pow(rho/rhoe,n)*(1 - log(pow(rho/rhoe,n)));

	return temp;

}


double EAM_function_der(double rho){

	double rhoe = 1.5;
	double fe= 1.0;
	double xi = 2.5;
	double n=0.5;
	double F1 = -0.1392;
	double F0 = 3.32 - 0.73;

	double temp = F1/rhoe + (F0*n*pow(rho/rhoe,n))/rho - (F0*n*pow(rho/rhoe,-1 + n)*(1 - log(pow(rho/rhoe,n))))/rhoe;

	return temp;

}


double EAM_function_sder(double rho){

	double rhoe = 1.5;
	double fe= 1.0;
	double xi = 2.5;
	double n=0.5;
	double F1 = -0.1392;
	double F0 = 3.32 - 0.73;

	double temp = (2*F0*pow(n,2)*pow(rho/rhoe,-1 + n))/(rhoe*rho) - (F0*n*pow(rho/rhoe,n))/pow(rho,2) - 
   (F0*(-1 + n)*n*pow(rho/rhoe,-2 + n)*(1 - log(pow(rho/rhoe,n))))/pow(rhoe,2);

	return temp;

}


double EAM_energy(double r){

	double si = 1./1.12246;
	double E=2.71828;
	double alpha = 0.0834;
	double beta = 7.5995;
	double ra = 3.0169;

	double energy = -((alpha*(1 + (-1 + r/ra)*beta))/pow(E,(-1 + r/ra)*beta));

	return energy;

}

//dphi/dr
double EAM_energy_der(double r){

	double si = 1./1.12246;
	double E=2.71828;
	double alpha = 0.0834;
	double beta = 7.5995;
	double ra = 3.0169;

	double der = -((alpha*beta)/(pow(E,(-1 + r/ra)*beta)*ra)) + 
   (alpha*beta*(1 + (-1 + r/ra)*beta))/(pow(E,(-1 + r/ra)*beta)*ra);


	return der;

}

double EAM_energy_sder(double r){

	double si = 1./1.12246;
	double E=2.71828;
	double alpha = 0.0834;
	double beta = 7.5995;
	double ra = 3.0169;

	double sder = (2*alpha*pow(beta,2))/(pow(E,(-1 + r/ra)*beta)*pow(ra,2)) - 
   (alpha*pow(beta,2)*(1 + (-1 + r/ra)*beta))/(pow(E,(-1 + r/ra)*beta)*pow(ra,2));

	return sder;

}




//it take s angs
struct base_stru shearing_vectors(double alpha, double theta, int ans){



	double dx = 0.05;
	int limit = 2 * abs(alpha) / dx;
	theta = theta*dlib::pi/180;
	// 	int limit = .6/dx;
	matrix_stru rot;
	rot.m11 = cos(theta);
	rot.m22 = cos(theta);
	rot.m12 = -sin(theta);
	rot.m21 = sin(theta);

	base_stru n, nr;
	base_stru a, ar;
	base_stru co,co_dual;
	dlib::matrix<double> co1(2,1),co2(2,1);
	//SQUARE LATTICE
// 	if (BLK1::crystal_symmetry.compare("square") == 0){
		co.e1[0] = 1.;
		co.e1[1] = 0.;
		co.e2[0] = 0.;
		co.e2[1] = 1.;
		co1(0,0)=1;
		co1(1,0)=0;
		co2(0,0)=0;
		co2(1,0)=1;
	//}

	//HEXAGONAL LATTICE
	if (BLK1::crystal_symmetry.compare("hex") == 0){
		co.e1[0] = pow(4./3.,1./4.);
		co.e1[1] = 0.;
		co.e2[0] = pow(4./3.,1./4.)/2.;
		co.e2[1] = sqrt(3.)*pow(4./3.,1./4.)/2.;




		co1(0,0)=co.e1[0];
		co1(1,0)=co.e1[1];
		co2(0,0)=co.e2[0];
		co2(1,0)=co.e2[1];
	}

	dlib::matrix<double> co_mform(2,2), inv_co_mform(2,2);
	
	co_mform(0,0) = co.e1[0];
	co_mform(1,0) = co.e1[1];
	co_mform(0,1) = co.e2[0];
	co_mform(1,1) = co.e2[1];
	inv_co_mform = inv(co_mform);
	dlib::matrix<double> dual1(2,1),dual2(2,1);


	dual1(0,0) = inv_co_mform(0,0);
	dual1(1,0) = inv_co_mform(0,1);
	dual2(0,0) = inv_co_mform(1,0);
	dual2(1,0) = inv_co_mform(1,1);
// 	cout<<"check1= "<<dot(co1,dual1)<<endl;
// 	cout<<"check2= "<<dot(co2,dual2)<<endl;
// 

	// GLIDING PLANE
	a.e1[0] = co.e1[0] ;
	a.e1[1] = co.e1[1] ;
	//uncomment for no dual lattice
	a.e1[0] = 1. ;
	a.e1[1] = 0. ;	


	//NORMAL TO  GLIDING PLANE 
	n.e1[0] = dual2(0,0);
	n.e1[1] = dual2(1,0);
// 	cout<<"dual n.e1[0]: "<<n.e1[0]<<endl;
// 	cout<<"dual n.e1[1]: "<<n.e1[1]<<endl;
	
	//uncomment for no dual lattice
	n.e1[0] =  0.;
	n.e1[1] =  1.;


// 	cout<<"n.e1[0]: "<<n.e1[0]<<endl;
// 	cout<<"n.e1[1]: "<<n.e1[1]<<endl;


	nr.e1[0] = rot.m11*n.e1[0] + rot.m12*n.e1[1];
	nr.e1[1] = rot.m21*n.e1[0] + rot.m22*n.e1[1];
	ar.e1[0] = rot.m11*a.e1[0] + rot.m12*a.e1[1];
	ar.e1[1] = rot.m21*a.e1[0] + rot.m22*a.e1[1];

		
// 	double sfe1 = nr.e1[0] * co.e1[0] + nr.e1[1] * co.e1[1];
// 	double sfe2 = nr.e1[0] * co.e2[0] + nr.e1[1] * co.e2[1];
	
	
	dlib::matrix<double> ad(2,1), nd(2,1),h1(2,1),h2(2,1);
	dlib::matrix<double> shear(2,2), sheartemp(2,2),identity(2,2);
	
	ad(0,0) = ar.e1[0];
	ad(1,0) = ar.e1[1];
	nd(0,0) = nr.e1[0];
	nd(1,0) = nr.e1[1];
	
	h1(0,0) = co.e1[0];
	h1(1,0) = co.e1[1];
	h2(0,0) = co.e2[0];
	h2(1,0) = co.e2[1];
	
	identity(0,0) = 1./BLK1::scale;
	identity(1,1) = 1./BLK1::scale;
	identity(0,1) = 0;
	identity(1,0) = 0;
	

// 	cout << "limit= " << limit << endl;

		base_stru c;

// 		shear = identity + alpha*trans(dlib::tensor_product(ad,nd));
	shear(0,0)   = 1. +  alpha*ad(0,0)*nd(0,0);
	shear(1,1)   = 1. +  alpha*ad(1,0)*nd(1,0);
	shear(0,1)   =  alpha*ad(0,0)*nd(1,0);
	shear(1,0)   =  alpha*ad(1,0)*nd(0,0);
// 	cout<<"shear(0,0): "<<shear(0,0)<<endl;
// 	cout<<"shear(1,1): "<<shear(1,1)<<endl;
// 	cout<<"shear(0,1): "<<shear(0,1)<<endl;
// 	cout<<"shear(1,0): "<<shear(1,0)<<endl;
// 
// 	cout<<"shear(0,0): "<<ad(0,0)*nd(0,0)<<endl;
// 	cout<<"shear(1,1): "<<ad(1,0)*nd(1,0)<<endl;
// 	cout<<"shear(0,1): "<<ad(0,0)*nd(1,0)<<endl;
// 	cout<<"shear(1,0): "<<ad(1,0)*nd(0,0)<<endl;

		
		if(BLK1::macro_def.compare("shear_rhombic") ==0){
			shear(0,0) = cosh(alpha);
			shear(1,1) = cosh(alpha);
			shear(0,1) = sinh(alpha);
			shear(1,0) = sinh(alpha);
		}
		
		if(BLK1::macro_def.compare("shear_rect") ==0){
			shear(0,1) = 0.*sinh(alpha);
			shear(1,0) = 0.*sinh(alpha);
	        shear(0,0) = cosh(alpha/2.) - sinh(alpha/2.);
	        shear(1,1) = cosh(alpha/2.) + sinh(alpha/2.);
			
		}		
		
		if(BLK1::macro_def.compare("tension") ==0){
			shear(0,1) = 0.;
			shear(1,0) = 0.;
	        shear(0,0) = sqrt(alpha);
	        shear(1,1) = sqrt(alpha);
		}


		
		
// 		if(BLK1::macro_def.compare("shear_rect_shear") ==0){
// 			double x = 0.2 ;
// 			sheartemp(0,1) = 0.*sinh(x);
// 			sheartemp(1,0) = 0.*sinh(x);
// 	        sheartemp(0,0) = cosh(x/2.) - sinh(x/2.);
// 	        sheartemp(1,1) = cosh(x/2.) + sinh(x/2.);	
// 	        shear=shear*sheartemp;	
// 		}	

		
		
		//F*e
		dlib::matrix<double> h1n=shear*h1;
		dlib::matrix<double> h2n=shear*h2;

// 		cout<< std::scientific
// 		<< std::setprecision(16) 
// 		<<"shear(0,0): "<<shear(0,0)<<endl;
// 		cout<< std::scientific
// 		<< std::setprecision(16) 
// 		<<"shear(1,1): "<<shear(1,1)<<endl;
// 		cout<< std::scientific
// 		<< std::setprecision(16) 
// 		<<"shear(0,1): "<<shear(0,1)<<endl;
// 		cout<< std::scientific
// 		<< std::setprecision(16) 
// 		<<"shear(1,0): "<<shear(1,0)<<endl;



		//Real vectors
// 		c.e1[0] = co.e1[0]  + alpha*sfe1*ar.e1[0];
// 		c.e1[1] = 0. + alpha*sfe1*ar.e1[1];
// 		c.e2[0] = co.e2[0] + alpha*sfe2*ar.e1[0];
// 		c.e2[1] = co.e2[1] + alpha*sfe2*ar.e1[1];


		//sheared vectors
		c.e1[0] = h1n(0,0);
		c.e1[1] = h1n(1,0);
		c.e2[0] = h2n(0,0);
		c.e2[1] = h2n(1,0);
		
//this remaisn to be understood, DO NOT USE but it was useful
// 	if (BLK1::crystal_symmetry.compare("hex") == 0){
// 		
// 		double sp=pow(4./3.,1./4.);
// 		c.e1[0] = alpha*(sp);
// 		c.e1[1] = 0;
// 		c.e2[0] = sp/2;
// 		c.e2[1] = alpha*sp*sqrt(3.)/2.;
// 	}
// 	
// 	if (BLK1::crystal_symmetry.compare("hex") != 0){
// 		
// 		double sp=1.;
// 		c.e1[0] = alpha*(sp);
// 		c.e1[1] = 0;
// 		c.e2[0] = 0.;
// 		c.e2[1] = alpha*sp;
// 	}	

		if(ans !=1)
			return c;
		else{
// 			cout<<"returning reduced vectors"<<endl;
			base_stru r = riduci_vectors(c);
			return r;
		}

				

}



//         struct de_stru d1;
// 		d1= cauchy(metric_rr,c,m2,burgers,BLK1::beta_Var);
//  		filestr2 << std::scientific << std::setprecision(7) << metric_a.c12 << " " <<  d1.reduced_stress.f12<< "\n";

double  contraction_with_shear(struct de_stru& d1, double theta){



	double dx = 0.05;
// 	int limit = 2 * abs(alpha) / dx;
	//swith to radian
	theta = theta*dlib::pi/180;
	// 	int limit = .6/dx;
	matrix_stru rot;
	rot.m11 = cos(theta);
	rot.m22 = cos(theta);
	rot.m12 = -sin(theta);
	rot.m21 = sin(theta);

	base_stru n, nr;
	base_stru a, ar;
	base_stru co,co_dual;
	dlib::matrix<double> co1(2,1),co2(2,1);
	//SQUARE LATTICE
// 	if (BLK1::crystal_symmetry.compare("square") == 0){
		co.e1[0] = 1.;
		co.e1[1] = 0.;
		co.e2[0] = 0.;
		co.e2[1] = 1.;
		co1(0,0)=1;
		co1(1,0)=0;
		co2(0,0)=0;
		co2(1,0)=1;
	//}

	//HEXAGONAL LATTICE
// 	if (BLK1::crystal_symmetry.compare("hex") == 0){
// 		co.e1[0] = pow(4./3.,1./4.);
// 		co.e1[1] = 0.;
// 		co.e2[0] = pow(4./3.,1./4.)/2.;
// 		co.e2[1] = sqrt(3.)*pow(4./3.,1./4.)/2.;
// 		co1(0,0)=co.e1[0];
// 		co1(1,0)=co.e1[1];
// 		co2(0,0)=co.e2[0];
// 		co2(1,0)=co.e2[1];
// 	}

	dlib::matrix<double> co_mform(2,2), inv_co_mform(2,2);
	
	co_mform(0,0) = co.e1[0];
	co_mform(1,0) = co.e1[1];
	co_mform(0,1) = co.e2[0];
	co_mform(1,1) = co.e2[1];
	inv_co_mform = inv(co_mform);
	dlib::matrix<double> dual1(2,1),dual2(2,1);


	dual1(0,0) = inv_co_mform(0,0);
	dual1(1,0) = inv_co_mform(0,1);
	dual2(0,0) = inv_co_mform(1,0);
	dual2(1,0) = inv_co_mform(1,1);
// 	cout<<"check1= "<<dot(co1,dual1)<<endl;
// 	cout<<"check2= "<<dot(co2,dual2)<<endl;
// 

	// GLIDING PLANE
	a.e1[0] = co.e1[0] ;
	a.e1[1] = co.e1[1] ;
	//uncomment for no dual lattice
// 	a.e1[0] = 1. ;
// 	a.e1[1] = 0. ;	


	//NORMAL TO  GLIDING PLANE 
	n.e1[0] = dual2(0,0);
	n.e1[1] = dual2(1,0);
// 	cout<<"dual n.e1[0]: "<<n.e1[0]<<endl;
// 	cout<<"dual n.e1[1]: "<<n.e1[1]<<endl;
	
	//uncomment for no dual lattice
// 	n.e1[0] =  0.;
// 	n.e1[1] =  1.;
// 	cout<<"n.e1[0]: "<<n.e1[0]<<endl;
// 	cout<<"n.e1[1]: "<<n.e1[1]<<endl;


	nr.e1[0] = rot.m11*n.e1[0] + rot.m12*n.e1[1];
	nr.e1[1] = rot.m21*n.e1[0] + rot.m22*n.e1[1];
	ar.e1[0] = rot.m11*a.e1[0] + rot.m12*a.e1[1];
	ar.e1[1] = rot.m21*a.e1[0] + rot.m22*a.e1[1];


		
	double sfe1 = nr.e1[0] * co.e1[0] + nr.e1[1] * co.e1[1];
	double sfe2 = nr.e1[0] * co.e2[0] + nr.e1[1] * co.e2[1];
	
	
	dlib::matrix<double> ad(2,1), nd(2,1),h1(2,1),h2(2,1);
	dlib::matrix<double>shear(2,2),identity(2,2),tildeshear(2,2);
	
	ad(0,0) = ar.e1[0];
	ad(1,0) = ar.e1[1];
	nd(0,0) = nr.e1[0];
	nd(1,0) = nr.e1[1];
	
	h1(0,0) = co.e1[0];
	h1(1,0) = co.e1[1];
	h2(0,0) = co.e2[0];
	h2(1,0) = co.e2[1];
	
	identity(0,0) = 0./BLK1::scale;
	identity(1,1) = 0./BLK1::scale;
	identity(0,1) = 0;
	identity(1,0) = 0;
	
	//dF/d\alpha
// 	shear = 0.*identity + trans(dlib::tensor_product(ad,nd));
	double gamma = pow(4./3.,1./4.); 
	tildeshear(0,0) = gamma;
	tildeshear(1,0) = 0.*gamma;
	tildeshear(0,1) = (1./2.)*gamma;
	tildeshear(1,1) = (sqrt(3.)/2.)*gamma;
	

	shear(0,0)   = ad(0,0)*nd(0,0);
	shear(1,1)   = ad(1,0)*nd(1,0);
	shear(0,1)   = ad(0,0)*nd(1,0);
	shear(1,0)   = ad(1,0)*nd(0,0);

	
	matrix<double> finalshear(2,2);
	
	double alpha = force_energy::calculation::getInstance().c.load;
	
	//derivative of loads
	if(BLK1::macro_def.compare("shear_rhombic") ==0){
		shear(0,0) = sinh(alpha);//cosh(alpha);
		shear(1,1) = sinh(alpha);//cosh(alpha);
		shear(0,1) = cosh(alpha);//sinh(alpha);
		shear(1,0) = cosh(alpha);//sinh(alpha);
		
// 		shear(0,0) =  ( 0.5*sinh(alpha))/sqrt(cosh(alpha)) ;
// 		shear(1,1) =  (-0.5*sinh(alpha))/pow(cosh(alpha),1.5);
// 		shear(0,1) =  sqrt(cosh(alpha)) - (0.5*pow(sinh(alpha),2))/pow(cosh(alpha),1.5);
// 		shear(1,0) =  0.;
	}
	
	if(BLK1::macro_def.compare("shear_rect") ==0){
		shear(0,0) = sinh(alpha)-cosh(alpha);
		shear(1,1) = sinh(alpha)+cosh(alpha);
		shear(0,1) = 0;
		shear(1,0) = 0;
	}	
	

	if(BLK1::macro_def.compare("tension") ==0){
			shear(0,1) = 0.;
			shear(1,0) = 0.;
	        shear(0,0) = 1./(2.*sqrt(alpha));
	        shear(1,1) = 1./(2.*sqrt(alpha));
	}	
	
	
	if (BLK1::crystal_symmetry.compare("hex") == 0){

		finalshear = shear*tildeshear;
	}

	else if (BLK1::crystal_symmetry.compare("square") == 0){

		finalshear = shear;
	}

		
	double sum=0;

	sum = d1.reduced_stress.f11*finalshear(0,0) + 
		  d1.reduced_stress.f22*finalshear(1,1) +
		  d1.reduced_stress.f12*finalshear(0,1) +
		  d1.reduced_stress.f21*finalshear(1,0);

	return sum;	

}


