#pragma once
#include "common.h"
#include "structures.h"
#include "vector_functions.h"
#include "calculations.h"
#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"

void write_to_a_file(struct conf_stru& c, int t);

void read_from_a_file(struct conf_stru& c);

//for INSIDE CG steps, very expensive don't use
void write_to_a_file_with_location(struct conf_stru& c, int t, const string f1);

void write_to_a_ovito_file(struct conf_stru& c, int t);
void write_to_a_ovito_file_eigenvectors(struct conf_stru& c, int t, int k);


void write_to_a_ovito_file_special(struct conf_stru& c, int t);

void write_to_a_ovito_file_remove_Fmacro(struct conf_stru& c, int t, double load);

void read_input_file();

void ps_file(struct conf_stru& c, int t, string answer);



