#pragma once

#include "common.h"
#include "structures.h"
#include "dlib.h"
#include "vector_functions.h"
#include "plot.h"
using namespace dlib;
typedef matrix<double,0,1> column_vector;
typedef matrix<int,0,1> column_vector_int;
typedef matrix<double,2,2> matrix2D;



void atomistic_grid(coordinates& c);
void atomistic_grid_square(coordinates& c);
void transform_to_tilde_coordinates( coordinates&  c,coordinates&  tilde_c, struct def_grad& F );
void transform_to_original_coordinates( coordinates&  c,coordinates&  tilde_c, struct def_grad& F );
void coarse_grain(coordinates& tilde_c,coordinates& c, double angle);
void coarse_grain_eam(coordinates& tilde_c,coordinates& c, double angle);




void coarse_grain_volume(coordinates& tilde_c,coordinates& c);
struct energy_stress coarse_grain_fast(const struct cella_stru& metric, double burgers, double alloy,const struct base_stru& v);
struct energy_stress coarse_grain_fast_EAM(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v);
struct double_cella_stru coarse_grain_fast_AT(const struct cella_stru& metric, double burgers, double alloy,const struct base_stru& v);
struct double_cella_stru coarse_grain_fast_AT_EAM(const struct cella_stru& metric, double burgers, double alloy,const struct base_stru& v);

double lennard_jones_energy(double r);
double lennard_jones_energy_der(double r);
double lennard_jones_energy_sder(double r);


double lennard_jones_energy_v2(double r);
double lennard_jones_energy_der_v2(double r);
double lennard_jones_energy_sder_v2(double r);


double square_energy(double r);
double square_energy_der(double r);
double square_energy_sder(double r);


double square_energy_v2(double r);
double square_energy_der_v2(double r);
double square_energy_sder_v2(double r);



struct base_stru shearing_vectors(double alpha, double theta, int ans);
double  contraction_with_shear(struct de_stru& d1, double theta_deg);

struct energy_stress coarse_grain_fast(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v);
struct energy_stress coarse_grain_fast_EAM(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v);

struct double_cella_stru coarse_grain_fast_AT(const struct cella_stru& metric, double burgers, double alloy, const struct base_stru& v);
struct cella_stru coarse_grain_fast_stress(const struct cella_stru& metric, double burgers, double alloy);


double EAM_electron(double r);
double EAM_electron_der(double r);
double EAM_electron_sder(double r);
double EAM_function(double rho);
double EAM_function_der(double rho);
double EAM_function_sder(double rho);


double EAM_energy(double r);

//dphi/dr
double EAM_energy_der(double r);
double EAM_energy_sder(double r);
