#include "slip_calculation.h"
#include "assign_solution.h"

double slip_calculations(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();

	int n=c.p.size();
	double value=0.;
	


for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 	
for (int i=0; i<c.p.size(); i++) {

	int iy = i / nx;
	int ix = i % nx;


	struct de_stru d,dold;
	struct base_stru v,vold;
	struct matrix_stru def_now,def_before;
	struct punto_stru p = c.p[i];

	//no triangle

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	d.p[0] = c.p[i];
	if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
		goto label1;
	d.p[1] = c.p[p.nn[k]];
	d.p[2] = c.p[p.nn[k+1]];

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	dold.p[0] = c.pold[i];
	dold.p[1] = c.pold[p.nn[k]];
	dold.p[2] = c.pold[p.nn[k+1]];


//---------------------------------BOUNDARY CONDITIONS------------------------------------


	c.fptr(i,ix,iy,k,d,c);
	c.fptr(i,ix,iy,k,dold,c);

	v=vectorize(d);
	vold=vectorize(dold);
// cout<<"--------new-------"<<endl;
// cout 
//  <<" v1_x "<<v.e1[0] 
//  <<" v1_y "<<v.e1[1]  
//  <<" v2_x "<<v.e2[0]  
//  <<" v2_y "<<v.e2[1] <<endl;
// cout<<"--------old-------"<<endl;
// 
// cout 
//  <<" v1_x "<<vold.e1[0] 
//  <<" v1_y "<<vold.e1[1]  
//  <<" v2_x "<<vold.e2[0]  
//  <<" v2_y "<<vold.e2[1]<<endl; 


	def_now    = v.matrix_form();
	def_before = vold.matrix_form();
	value += abs(def_now.m12-def_before.m12);
	value += abs(def_now.m21-def_before.m21);

	label1: ;

}			
	
	

	return value;

}


int slip_calculations_integer(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();

	int n=c.p.size();
	int value=0.;
	


for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 	
for (int i=0; i<c.p.size(); i++) {

	int iy = i / nx;
	int ix = i % nx;


	struct de_stru d,dold;
	struct base_stru v,vold;
	struct matrix_stru def_now,def_before;
	struct punto_stru p = c.p[i];

	//no triangle

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	d.p[0] = c.p[i];
	if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
		goto label1;
	if(!check(c.bc_cond[i][k],2))	
		goto label1;

	d.p[1] = c.p[p.nn[k]];
	d.p[2] = c.p[p.nn[k+1]];

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	dold.p[0] = c.pold[i];
	dold.p[1] = c.pold[p.nn[k]];
	dold.p[2] = c.pold[p.nn[k+1]];


//---------------------------------BOUNDARY CONDITIONS------------------------------------


	c.fptr(i,ix,iy,k,d,c);
	c.fptr(i,ix,iy,k,dold,c);

	v=vectorize(d);
	vold=vectorize(dold);
// cout<<"--------new-------"<<endl;
// cout 
//  <<" v1_x "<<v.e1[0] 
//  <<" v1_y "<<v.e1[1]  
//  <<" v2_x "<<v.e2[0]  
//  <<" v2_y "<<v.e2[1] <<endl;
// cout<<"--------old-------"<<endl;
// 
// cout 
//  <<" v1_x "<<vold.e1[0] 
//  <<" v1_y "<<vold.e1[1]  
//  <<" v2_x "<<vold.e2[0]  
//  <<" v2_y "<<vold.e2[1]<<endl; 


	def_now    = v.matrix_form();
	def_before = vold.matrix_form();
	value += abs(round(def_now.m12)-round(def_before.m12));
	value += abs(round(def_now.m21)-round(def_before.m21));

	label1: ;

}			
	
	

	return value;

}

double m_matrix_calculations(struct conf_stru& c, double load, double theta, std::vector <std::vector<matrix_stru> >&  m_field

,std::vector <std::vector<int> >& well_locator, int n_direction  )
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();

	int n=c.p.size();
	double value=0.;
	
	std::vector <double>  x,y,counter_array;
	x.resize(n);
	y.resize(n);
	counter_array.resize(n);
	for (int i=0; i<c.p.size(); i++)
		counter_array[i]=0.;
	
	//save     c.pold to recover later
for (int i=0; i<c.p.size(); i++) 
{   x[i] = c.pold[i].x;
    y[i] = c.pold[i].y;
    c.pold[i].x = c.p[i].x;			
	c.pold[i].y = c.p[i].y;
    
}
	struct boundary_conditions set1,setactual,setnew;
	
	fstream peng;
	string penga =  "peng_jumps.dat";

	
	cout<<"loop starts"<<endl;
	double alpha=0;
	int counter = 0;
	double count = 0;
	double gamma = pow(4. / 3., 1. / 4.);

// 	std::vector <std::vector<int> > well_locator;
// 	well_locator.resize(n);
// 	for (int i = 0; i < n; ++i)
// 		well_locator[i].resize(4);

for(int b=0;b<1000;b++){
	setnew.setParameters(theta,load+alpha);
   	choose_bc(setnew);
   	apply_BC_generale(c,setnew);

   	counter=0;
   	count=0;
   	for (int i=0; i<c.p.size(); i++)
		count += counter_array[i];

	if(count >= 1)
		goto label5;



	set1.setParameters(theta,alpha);
   	choose_bc(set1);

for (int i=0; i<c.p.size(); i++) {   	
	c.pold[i].x = c.p[i].x * set1.f.f11 + c.p[i].y * set1.f.f12;
			
	c.pold[i].y = c.p[i].x * set1.f.f21 + c.p[i].y * set1.f.f22;
}

	

			
for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) {

#pragma omp parallel
#pragma omp for

for (int i=0; i<c.p.size(); i++) {

	int iy = i / nx;
	int ix = i % nx;


	struct de_stru d,dold;
	struct base_stru v,vold;
	struct matrix_stru def_now,def_before;
	struct punto_stru p = c.p[i];
	struct punto_stru p_old ;
	struct base_stru vecr;
	struct base_stru vecr2;
	struct matrix_stru m2,m2_old;
	int well,wellold;

	//no triangle

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	d.p[0] = c.p[i];
	if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
		goto label1;
// 	d.p[1] = c.p[p.nn[k]];
// 	d.p[2] = c.p[p.nn[k+1]];

//---------------------------------CONSTRUCT THE DEFORMED TRIANGLE------------------------------------

	dold.p[0] = c.pold[i];
	dold.p[1] = c.pold[p.nn[k]];
	dold.p[2] = c.pold[p.nn[k+1]];
	

//---------------------------------BOUNDARY CONDITIONS------------------------------------


	
	c.fptr(i,ix,iy,k,dold,c);
	vold=v=vectorize(dold);
	
	
	if(BLK1::crystal_symmetry.compare("hex") == 0){
		vold.e1[0] = v.e1[0]/gamma - v.e1[1]/(sqrt(3.)*gamma);
		vold.e1[1] =  0. + 2.*v.e1[1]/(sqrt(3.)*gamma);

		vold.e2[0] = v.e2[0]/gamma - v.e2[1]/(sqrt(3.)*gamma);
		vold.e2[1] =  0. + 2.*v.e2[1]/(sqrt(3.)*gamma);
	}

	
// 	vecr2=riduci_vectors(vold);
// 	m2_old = find_integer_matrix(vold,vecr2);
// 	
// 
// 	m2=m_field[i][k];
// 	if(b==0)
// 		well_locator[i][k]= riduci_vectors_wellnumber(vold);
// 	if(b!=0){
		well = riduci_vectors_wellnumber(vold);
		if(well!= well_locator[i][k]){
			counter++;
			counter_array[i] += 1.;
// 		}

	}
	
	
	
// 	cout<<"well= "<<well<<endl;


// cout<<"--------new-------"<<endl;
// cout 
//  <<" v1_x "<<v.e1[0] 
//  <<" v1_y "<<v.e1[1]  
//  <<" v2_x "<<v.e2[0]  
//  <<" v2_y "<<v.e2[1] <<endl;
// cout<<"--------old-------"<<endl;
// 
// cout 
//  <<" v1_x "<<vold.e1[0] 
//  <<" v1_y "<<vold.e1[1]  
//  <<" v2_x "<<vold.e2[0]  
//  <<" v2_y "<<vold.e2[1]<<endl; 


// 	if(round(m2.m11) != round(m2_old.m11)) {counter++;cout<<"warning at ix at m11 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m11<<" "<<m2_old.m11<<endl;goto label5;}
// 	if(round(m2.m22) != round(m2_old.m22)) {counter++;cout<<"warning at ix at m22 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m22<<" "<<m2_old.m22<<endl;goto label5;}
// 	if(round(m2.m12) != round(m2_old.m12)) {counter++;cout<<"warning at ix at m12 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m12<<" "<<m2_old.m12<<endl;goto label5;}
// 	if(round(m2.m21) != round(m2_old.m21)) {counter++;cout<<"warning at ix at m21 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m21<<" "<<m2_old.m21<<endl;goto label5;}
// 	if(round(m2.m11) != round(m2_old.m11)) {counter++;}//cout<<"warning at ix at m11 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m11<<" "<<m2_old.m11<<endl;}
// 	if(round(m2.m22) != round(m2_old.m22)) {counter++;}//cout<<"warning at ix at m22 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m22<<" "<<m2_old.m22<<endl;}
// 	if(round(m2.m12) != round(m2_old.m12)) {counter++;}//cout<<"warning at ix at m12 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m12<<" "<<m2_old.m12<<endl;}
// 	if(round(m2.m21) != round(m2_old.m21)) {counter++;}//cout<<"warning at ix at m21 "<<ix<<" "<<iy<<" at " << alpha<<" "<<m2.m21<<" "<<m2_old.m21<<endl;}



	
// 	if(counter >= 250*(nx*ny)/(120*120))
// 	if(counter >= 1)
// 
// 		goto label5;
	label1: ;

}
}
	
	
	
	alpha+=0.000002*pow(-1.,n_direction);
	

}			
	
	
	label5: ;
	cout<<"counter= "<<count<<" at "<< alpha<<endl;

	


for (int i=0; i<c.p.size(); i++) {   	
	c.p[i].x = c.pold[i].x;
	c.p[i].y = c.pold[i].y;
	c.pold[i].x = x[i];
    c.pold[i].y = y[i];
    	
}
	
	
	peng.open (penga, fstream::in | fstream::out | fstream::app);
	peng << std::scientific << std::setprecision(7)  << " " << alpha<<endl ;
	peng.close();

	double zero = 0.00000000000001;
	if (counter == 0)
		alpha /=8.;
	alpha= alpha + zero*pow(-1.,n_direction);
	return alpha;

}



void actual_m_matrix_calculations(struct conf_stru& c, std::vector <std::vector<matrix_stru> >& m_field,
std::vector <std::vector<int> >& well_locator )
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();

	int n=c.p.size();
	double value=0.;
	double gamma = pow(4. / 3., 1. / 4.);

	

			
for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) {

for (int i=0; i<c.p.size(); i++) {

	int iy = i / nx;
	int ix = i % nx;


	struct de_stru d,dold;
	struct base_stru v,vold;
	struct matrix_stru def_now,def_before;
	struct punto_stru p = c.p[i];
	struct punto_stru p_old ;
	struct base_stru vecr;
	struct base_stru vecr2;
	struct matrix_stru m2,m2_old;

	//no triangle

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
	d.p[0] = c.p[i];
	if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
		goto label1;
	d.p[1] = c.p[p.nn[k]];
	d.p[2] = c.p[p.nn[k+1]];



//---------------------------------BOUNDARY CONDITIONS------------------------------------

	c.fptr(i,ix,iy,k,d,c);
	v=vectorize(d);
	
	
	if(BLK1::crystal_symmetry.compare("hex") == 0){
		vold.e1[0] = v.e1[0]/gamma - v.e1[1]/(sqrt(3.)*gamma);
		vold.e1[1] =  0. + 2.*v.e1[1]/(sqrt(3.)*gamma);

		vold.e2[0] = v.e2[0]/gamma - v.e2[1]/(sqrt(3.)*gamma);
		vold.e2[1] =  0. + 2.*v.e2[1]/(sqrt(3.)*gamma);
		v=vold;
	}

	
	well_locator[i][k] = riduci_vectors_wellnumber(v);
	vecr=riduci_vectors(v);
	m_field[i][k]=find_integer_matrix(v,vecr);
	

	
	label1: ;

}
}
	
			


}

