#pragma once

#include <cstddef>
#include <iostream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
#include <boost/random.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "polar_decomposition.h"
#include <boost/random.hpp>
#include <boost/numeric/ublas/io.hpp>

// #include "polar_decomposition.h"
#include "common.h"
#include "dlib.h"

#include "rotation.h"
#include "structures.h"
#include "struct_functions.h"
#include "namespaces.h"
#include "string_to_integer.h"
 using namespace std;
// double example(struct matrix_stru& F);
 #define ARMA_ALLOW_FAKE_GCC 
 #define ARMA_DONT_USE_WRAPPER
// 
 #define ARMA_DONT_USE_HDF5

#include "ap.h"
#include <armadillo>


void energy_landscape_gorbushin();
double rot_calcul_roberta(struct cella_stru& c, struct matrix_stru& f);
