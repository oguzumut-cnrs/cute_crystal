#pragma once
#include "structures.h"
#include "namespaces.h"
#include "common.h"
#include "dlib.h"
struct de_stru  stress_zanzotto(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, const struct matrix_stru& P);


struct de_stru  piola_one(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy);
	
struct de_stru  cauchy(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy);
	
struct de_stru  second_piola(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy);
	
	struct de_stru  stress_UL(const struct cella_stru& dtemp,
	const struct base_stru& v1, const struct matrix_stru& m,const struct matrix_stru& ftoti);