#include "calculations.h"
#include "calculations_homogenous.h"
#include "rotation.h"
#include "input_output.h"
#include "energy_functions.h"
#include "result_memorize.h"
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using namespace std;


//void calcstresses_CG_en_homogenous(struct conf_stru& c,double alpha,int k,ofstream& filestr)
void calcstresses_CG_en_homogenous(struct conf_stru& c,double alpha)

{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	
// 	c.energy = 0.;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);
	static int loop=2;
	
// 	void (*fptr)( int,  int,   int,  struct de_stru& , struct conf_stru& ); //Declare a function pointer to voids with no params

// 	c.fptr = &per_BC; //fptr -> one

	
	for (int i=0; i<npunti; ++i)
	{
		c.gr[i].x=c.gr[i].y=c.energy_local[i] =0.;
		
		for(int l=0;l<12;++l)
			c.grp1[i][l].x = c.grp1[i][l].y=0.;
	}

	int tt=6;
	
    // map<string,double(*)(struct cella_stru& c,double &burgers,double alloy)> double_map;



	

	//k -> loop on the triangles
 	for (int k=0; k<4; ++k) 
 	{

		//this is the loop on triangle vertices			
 		for (int i=0; i<npunti; i++){
//		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)

// 			cout<<"k "<<k<<"; i "<<i<<endl;
		
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;
// 			cout<<"i: "<<i<<endl; 

			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr;
			//burgers = 1 if there is no precipitate
			double burgers  = c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr;
			double mesh=0;
			struct energy_stress es;
			



// 				if(allequal(c.bc_cond[i][k],0,1,5))	
			if(!check(c.bc_cond[i][k],2))	
				goto label3;


//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

 		    c.fptr(i,ix,iy,k,d,c);



//---------------------------------VECTORS OF CELLS------------------------------------		
			v=vectorize(d);
//---------------------------------REAL METRICS------------------------------------		

		    metrics  = faicella(v);


// 			if(k>0){
// 			cout<<"i: "<<i<<" k: "<<k 
// 						  <<" c11: "<<metrics.c11 
// 						  <<" c22: "<<metrics.c22 
// 						  <<" c12: "<<metrics.c12
// 						  <<" detC: "<<metrics.c11*metrics.c22-metrics.c12*metrics.c12
// 						  <<" ix "<<ix 
// 						  <<" iy "<<iy 
// 						  <<" v1_x "<<p.nn[k] % nx
// 						  <<" v1_y "<<p.nn[k] / nx
// 						  <<" v2_x "<<p.nn[k+1] % nx
// 						  <<" v2_y "<<p.nn[k+1] / nx
// 						  << endl;
// 
// 			}

			 //REDUCED VECTOR VECR; NON REDUCED V
// 			 cout<<"reducing: "<<endl;
			 
			 
			vecr=riduci_vectors(v);

			//FIND INTEGER MATRIX			
			m=find_integer_matrix(v,vecr);
		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);


			es = c.fptr2(metric_rr,c.disorder[i][k],c.alloying[i][k],vecr);			
			energy_thread = es.energy;
				

			c.energy_local[i] += energy_thread;


// 			d1= stress_zanzotto(es.r_stress,v,m,c.disorder[i],c.alloying[i]);
	

//----------------------------------------------------------------------------------------
// 
// 	//phi,11
// 	d.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
// 	//phi,21
// 	d.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
// 	//phi,12
// 	d.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
// 	//phi,22
// 	d.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];

			
			c.gr[i].x         += d1.gr[0].x;
			c.gr[i].y         += d1.gr[0].y;
// 
//  	  //no need to sum the cross terms
/* 
			c.grp1[i][l].x   = d1.gr[1].x ; 
			c.grp1[i][l].y   = d1.gr[1].y ; 
			c.grp1[i][l+1].x = d1.gr[2].x ; 
			c.grp1[i][l+1].y = d1.gr[2].y ; 
 */
			
			
// 			if(ix==nx/2 && iy==ny/2){
// 				filestr << std::scientific << std::setprecision(7) << " " << alpha << " " 
// 				<< d1.gr[2].x  << " " <<  " " 
// 				<< d1.gr[1].y << " " 
// 				<< d1.gr[1].x << " " 
// 				<< d1.gr[2].y << endl;	
// // 				piolaw.close();
// // 				cauchyw.close();
// 	
// 			}	
			
		
// 			c.gr[i].x         += d1.gr[0].x;
// 			c.gr[i].y         += d1.gr[0].y;
// 			
			c.gr[p.nn[k]].x   += d1.gr[1].x;
			c.gr[p.nn[k]].y   += d1.gr[1].y;
			c.gr[p.nn[k+1]].x += d1.gr[2].x;
			c.gr[p.nn[k+1]].y += d1.gr[2].y; 
			label3: ;

			
			
			
			
//		});
 		}

	}



	string filename3 ="./dir_force/forces"      + DoubleToStr(alpha);


	ofstream fx;
	fx.open(filename3.c_str());
	fx << c.p.size() << endl;
	fx << " " << endl;

	for (int i = 0; i<c.p.size(); i++)
	{
		fx << std::scientific << std::setprecision(7)
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< c.gr[i].x << " "
			<< c.gr[i].y << " "
			<< endl;
	}


 	fx.close();

    //THIS ONE CANNOT BE PARALLALIZED
    

}

