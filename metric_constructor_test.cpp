/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <vector>
#include <cmath>

#include "metric_constructor_test.h"
#include "struct_functions.h"

void metric_print_test(struct conf_stru& c)
{


//this is the loop on triangle vertices			
    
    for (int k=0; k<4; ++k)
    for (int i=0; i<c.p.size(); ++i){
        //REAL BASIS VECTORS
        struct base_stru v;
        //REAL METRICS
        struct cella_stru metrics;
	struct punto_stru p = c.p[i];
	struct de_stru d;
        
        if(p.is_boundary == 1 && ((p.nn[k] == -1) || (p.nn[k+1] == -1))   )
            continue;
        
        //---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
        d.p[0] = p;
        d.p[1] = c.p[p.nn[k]];
        d.p[2] = c.p[p.nn[k + 1]];

        //---------------------------------BOUNDARY CONDITIONS------------------------------------

        //---------------------------------VECTORS OF CELLS------------------------------------		
        v.e1[0] = d.p[1].x - d.p[0].x;
        v.e1[1] = d.p[1].y - d.p[0].y;
        v.e2[0] = d.p[2].x - d.p[0].x;
        v.e2[1] = d.p[2].y - d.p[0].y;
        //---------------------------------REAL METRICS------------------------------------		
        metrics = faicella(v);
        std::cout << "i: " << i << " c11: " << metrics.c11
                << " c22: " << metrics.c22
                << " c12: " << round(metrics.c12)
                << " nb1: " << p.nn[k]
                << " nb2: " << p.nn[k + 1]
                << std::endl;


	}

}