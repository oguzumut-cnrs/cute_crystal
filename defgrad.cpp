
class boundary_conditions {
   public:	  
   	  double theta_d,load_in; 
	  struct  def_grad f;      // Member functions declaration

      struct def_grad macro_shear(  );
      struct def_grad macro_compression(  );
      void setParameters( double, double  );
      void def_frame(struct conf_stru&, boundary_conditions&);
};




struct def_grad boundary_conditions:: macro_shear()
{

	string  BC  = BLK1::BC;
	double perturb = 0*dlib::pi / 180;
// 			std::vector<double> rx,ry;
// 			rx.resize(nx*ny);
// 			ry.resize(nx*ny);
// 			double av= 0;
// 			double std=0.01;
// 			rx = wiener(av, std);
// 			ry = wiener(av, std);
	
	
	f.f11 = (1. -  load_in*cos(theta_d+perturb)*sin(theta_d+perturb)) ;
	f.f22 = (1. +  load_in*cos(theta_d-perturb)*sin(theta_d-perturb));
	f.f12 =   load_in* pow(cos(theta_d + perturb),2.);
	f.f21 =  -load_in* pow(sin(theta_d - perturb),2.);
	
	return f;


}


struct def_grad boundary_conditions:: macro_compression()
{

	string  BC  = BLK1::BC;
	
	
// 	f.f11 = 1 +0*load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
// 	f.f22 = 1 +load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
// 	f.f12 = 0.;
// 	f.f21 = 0.;

    double load=1+load_in;
	
	f.f11 =  1.;
	f.f22 =  load;
	f.f12 = 0.;
	f.f21 = 0.;

	double load_in2 = 0.05;
// 	f.f11 = (1. -  load_in2*cos(theta_d)*sin(theta_d))			;
// 	f.f22 = (1. +  load_in2*cos(theta_d)*sin(theta_d))  +load_in ;
// 	f.f12 =   load_in2* pow(cos(theta_d),2.);
// 	f.f21 =  -load_in2* pow(sin(theta_d),2.);
	
	return f;

}

void  boundary_conditions:: setParameters(double value1, double value2)
{
	theta_d =value1*dlib::pi / 180.0;
	load_in =value2;

// 	return f;

}


void boundary_conditions:: def_frame(struct conf_stru& c, struct boundary_conditions& f)

{
	double dny,diy,dnx;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
 	double theta_d = f.theta_d;
 	double load_in = f.load_in;




//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
// 
//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;
   	
//    	struct def_grad f = combiner(load_in,theta_d,macro_shear);

			double k,m;
//----------------------periodic boundaries------------------------------------

			
			
//----------------------deform the reference state------------------------------------
			


			for(int i =0;i<nx*ny;++i)
			{
// 				int iy = i / nx;
// 				int ix = i % nx;
// 				c.pfix[i].x = ((double) ix-c.dnx) * f.f.f11 + ((double) iy-c.dny) * f.f.f12;
// 				
// 				c.pfix[i].y = ((double) ix-c.dnx) * f.f.f21 + ((double) iy-c.dny) * f.f.f22;

				int iy = i / nx;
				int ix = i % nx;
				c.pfix[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
				
				c.pfix[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;

	
			}

}


// double Combiner(double a, double b, std::function<double (double,double)> func){
//   return func(a,b);
// }
// 
// double Add(double a, double b){
//   return a+b;
// }
// 
// double Mult(double a, double b){
//   return a*b;
// }
// 
// int main(){
//   Combiner(12,13,Add);
//   Combiner(12,13,Mult);
// }