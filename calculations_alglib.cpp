#include "rotation.h"
#include "input_output.h"
#include "energy_functions.h"
#include "result_memorize.h"
#include "atomistic_grid.h"
#include "delaunay.h"
#include "assign_solution.h"
#include "calculations_alglib.h"
#define ARMA_ALLOW_FAKE_GCC 
#define ARMA_DONT_USE_WRAPPER

#define ARMA_DONT_USE_HDF5

#include "ap.h"

#include <cmath>
#include <vector>

using std::cout;
using std::endl;


void rosen_alglib(const alglib::real_1d_array &m, double &func, alglib::real_1d_array &grad, void *ptr) {
	double x, y,sum;
	int i,ix,iy,nx,ny;
	double dnx,dny,size,sum2;
	nx=dimensions::nx ;
	ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

// 	double load=loading::testNum ;
	double load=BLK1::load;
	double theta = BLK1::theta;
	double dt=BLK1::dt*10;
	static int step = 1;
	static int step2 = 0;

	static int t = 0;
	
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number;
	
// 	struct conf_stru * c_local;
// 	c_local = &force_energy::calculation::getInstance().c;

	dnx=nx;
	dny=ny;
	size=dnx*dny;




	for(int i=0;i<npunti;++i)
	{ 
        force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + m[i];
        force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + m[i+npunti];
	} 

	if(fexists("roule") == false)
	{ 
		cout<<"CODE HAS BEEN STOPPED IN THE CG"<<endl;
		exit (EXIT_FAILURE); 
	}
	

// 	if(step2<BLK1::num_threads)
// 		if(step%BLK1::cg_freq == 0 && BLK1::cg_save == 1){
// 			step =1;
// 			string CG = "CG";
// 			result_memorize(force_energy::calculation::getInstance().c);
// 			string answer = "l";
// 			int tprime  = t + 9999;
// 			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);
// 		}


    
	if(BLK1::constraint_min.compare("no") ==0)
		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	
// 	if(BLK1::constraint_min.compare("no") ==0)
// 		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	
	if(BLK1::BC.compare("eulerian") != 0)
		calcstresses_CG_en(force_energy::calculation::getInstance().c);
	else if(BLK1::BC.compare("eulerian") == 0) 
		calcstresses_CG_en_UL(force_energy::calculation::getInstance().c,m);
		
	if(BLK1::constraint_min.compare("no") ==0)
		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	
	sum = 0.;
	double h=dimensions::dh;
	

	for(int i=0;i<n;++i){ 
		sum+= force_energy::calculation::getInstance().c.energy_local[i];
		
		double x = force_energy::calculation::getInstance().c.gr[i].x;
		double y = force_energy::calculation::getInstance().c.gr[i].y; 

		grad[i]     = h*x;
		grad[i+n]   = h*y;
   }



		
	func= h*h*sum;
	if(step2<BLK1::num_threads)
		if(step%BLK1::cg_freq ==0 && BLK1::cg_save == 1){
			
			double grad_sum=0.;
			for(int i=0;i<n;++i)
			grad_sum += pow(grad[i],2) + pow(grad[i+n],2);

			cout<< std::scientific << std::setprecision(16)<<"function value inside LFBGS: "<<func
			<<" sqrt of grad_sum value inside LFBGS: "<<sqrt(grad_sum)<<" at step: "<<step2<<endl;
			
			step =1;
			string CG = "CG";
			result_memorize(force_energy::calculation::getInstance().c);
			string answer = "l";
			int tprime  = t + 9999;
			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);

		
		}
		
		
	step2++;
    ++step;
    

}


//remove the fixed atoms; smaller matrix

void rosen_alglib_2(const alglib::real_1d_array &m, double &func, alglib::real_1d_array &grad, void *ptr) {
	double x, y,sum;
	int i,ix,iy,nx,ny;
	double dnx,dny,size,sum2;
	nx=dimensions::nx ;
	ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

// 	double load=loading::testNum ;
	double load=BLK1::load;
	double theta = BLK1::theta;
	double dt=BLK1::dt*10;
	static int step = 1;
	static int step2 = 0;

	static int t = 0;
	
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number;
	
// 	struct conf_stru * c_local;
// 	c_local = &force_energy::calculation::getInstance().c;

	dnx=nx;
	dny=ny;
	size=dnx*dny;
	int mm;

	if(BLK1::bc_x.compare("fix") ==0)
		mm=2*(nx-2)*(ny-2);
	 else if(BLK1::bc_x.compare("pbc") ==0)
		mm=2*n;


	int n_temp=0;
	for(int i=0;i<npunti;++i)
	{ 
   		 int iy = i / nx;
		 int ix = i % nx;
		 if(BLK1::bc_x.compare("fix") ==0)
		 //This line should be done better but it works so far
		 //may be assign a flag to fix nodes ??? 
		 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1){
			
// 			force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale;
//             force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale;
			continue;
			
		}

        force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + m[n_temp];
        force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + m[n_temp+mm/2];
        n_temp++;
	} 

	if(fexists("roule") == false)
	{ 
		cout<<"CODE HAS BEEN STOPPED IN THE CG"<<endl;
		exit (EXIT_FAILURE); 
	}
	


	
	calcstresses_CG_en(force_energy::calculation::getInstance().c);
	
	if(BLK1::constraint_min.compare("no") ==0)
		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	
	sum = 0.;
	double h=dimensions::dh;
	
	n_temp=0;
	for(int i=0;i<n;++i){ 
		sum+= force_energy::calculation::getInstance().c.energy_local[i];
		
		double x = force_energy::calculation::getInstance().c.gr[i].x;
		double y = force_energy::calculation::getInstance().c.gr[i].y; 
   		int iy = i / nx;
		int ix = i % nx;
		if(BLK1::bc_x.compare("fix") ==0)
		 //This line should be done better but it works so far
		 //may be assign a flag to fix nodes ??? 
		if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1){
			force_energy::calculation::getInstance().c.gr[i].x = 0;
			force_energy::calculation::getInstance().c.gr[i].y = 0;
			continue;		
		}

        grad[n_temp]      = h*x;
        grad[n_temp+mm/2] = h*y;
        n_temp++;
	} 





		
	func= h*h*sum;
	if(step2<BLK1::num_threads)
		if(step%BLK1::cg_freq ==0 && BLK1::cg_save == 1){
			
			double grad_sum=0.;
			for(int i=0;i<n;++i)
			grad_sum += pow(force_energy::calculation::getInstance().c.gr[i].x,2) 
			          + pow(force_energy::calculation::getInstance().c.gr[i].y,2);

			cout<< std::scientific << std::setprecision(16)<<"function value inside LFBGS: "<<func
			<<" sqrt of grad_sum value inside LFBGS: "<<sqrt(grad_sum)<<" at step: "<<step2<<endl;
			
			step =1;
			string CG = "CG";
			result_memorize(force_energy::calculation::getInstance().c);
			string answer = "l";
			int tprime  = t + 9999;
			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);

		
		}
		
		
	step2++;
    ++step;
    

}

void apply_BC_box_alglib(struct conf_stru& c,alglib::real_1d_array& sp_min, alglib::real_1d_array& sp_max)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();
	int inc = force_energy::calculation::getInstance().c.inc ;

	dny = ny;
	string_to_integer bset;
	double minu,maxu,minv,maxv,minvp,maxvp;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
	


	
	for (int k=0; k<BLK1::triangle; k=k+inc) 
		for (int i=0; i<n; ++i){
			
			//fix boundary condition
			if(check(c.bc_cond[i][k],0)){
				 //u_min and v_min
				 sp_min[i]    = c.full[i].x -c.pfix[i].x ;
				 sp_min[i+n]  = c.full[i].y -c.pfix[i].y ;
				 //u_max and v_max
				 sp_max[i]    = c.full[i].x -c.pfix[i].x ;
				 sp_max[i+n]  = c.full[i].y -c.pfix[i].y ;
				 
			}


		 }
					
	
}


