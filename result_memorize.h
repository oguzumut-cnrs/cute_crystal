#pragma once

#include "structures.h"
#include "struct_functions.h"
#include "namespaces.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "calculations.h"
// #include "spline.h"
#include "assign_solution.h"

struct base_stru elastic_vectors(base_stru c);
struct matrix_stru f_plastic(base_stru c);
double example(struct matrix_stru F);
void result_memorize(struct conf_stru& c);
void remove_macro_def(struct conf_stru& c);
