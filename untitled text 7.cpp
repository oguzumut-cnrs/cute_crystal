#include "calculations.h"
#include "rotation.h"
#include "energy_functions.h"
#include <cmath>
#include <vector>
using namespace std;
std::pair<double, double> get_XY_for_rotated_square(int i, int j, double angle) {
	double beta = atan2(1.0*j , 1.0*i);
	double gamma = angle + beta;
	double r = sqrt(1.0*i*i + j*j);
	double x = r*cos(gamma);
	double y = r*sin(gamma);

	return std::pair<double, double>(x, y);
}
// 

void new_init_rotated_square(struct conf_stru& c, double angle) {
	int width  = dimensions::nx;
	int height = dimensions::ny;
	conf_stru temp_structure;
	punto_stru origin_point;
	origin_point.x = 0;
	origin_point.y = 0;
	origin_point.i = 0;
	origin_point.j = 0;
// 	temp_structure.p.push_back(origin_point);

	bool i_loop = true;
	bool jmax_loop = true;
	bool jmin_loop = true;
	int i = 0;
	int j = 0;

	angle = toRadians(angle);
        cout<<"while loop starts for rotation angle: "<<angle<<endl;
    bool added = true;
	while (added) {
			added = false;

			j = 0;
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0.0 && x - width <= 0.1) && (y - height <= 0.1)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}

		j = 1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0 && x - width <= 0.1) && (y - height <= 0.1 )) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i;
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (y >= height) 
				break;
			j++;
		}

		j = -1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x - width <= 0.1) && (y >= -0.01)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
				j--;		
			}
			else {
				break;
			}
		}

		i++;
	}


	/* neighbors*/
	for (int k = 0; k < temp_structure.p.size(); ++k) {
		for (int s = 0; s < 5; s++) {
			temp_structure.p[k].nn[s] = 0;
			temp_structure.p[k].is_boundary = 0; //it is not a boundary
		}
		int ii = temp_structure.p[k].i;
		int jj = temp_structure.p[k].j;
		temp_structure.p[k].nn[0] = temp_structure.get_punto(ii + 1, jj);
		temp_structure.p[k].nn[1] = temp_structure.get_punto(ii, jj + 1);
		temp_structure.p[k].nn[2] = temp_structure.get_punto(ii - 1, jj);
		temp_structure.p[k].nn[3] = temp_structure.get_punto(ii, jj - 1);
		temp_structure.p[k].nn[4] = temp_structure.p[k].nn[0];
        temp_structure.p[k].which_boundary = 0;

// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_structure.p[k].nn[0]<<" "<<temp_structure.p[k].nn[1]<<" "<<temp_structure.p[k].nn[2]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_structure.p[k].nn[3]<<" "<<temp_structure.p[k].nn[4]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_structure.p[k].x<<" "<<temp_structure.p[k].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_structure.p[temp_structure.p[k].nn[0]].x<<" "<<temp_structure.p[temp_structure.p[k].nn[0]].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_structure.p[temp_structure.p[k].nn[1]].x<<" "<<temp_structure.p[temp_structure.p[k].nn[1]].y<<endl;
        
		
		int counter2=0;

		for (int s = 0; s < 4; s++)
			if (temp_structure.p[k].nn[s] == -1) {
				temp_structure.p[k].is_boundary = 1;
				counter2++;
				for (int l=s+1;l<4;++l){
					if (temp_structure.p[k].nn[l] == -1)
						counter2++;		
				}
			if(counter2 >0)
				break;
					
		}
// 		temp_structure.p[k].which_boundary = 4-counter2;
				double c =1.;
				if(abs(width-temp_structure.p[k].x) < c)        
						temp_structure.p[k].which_boundary = 1; //right
				if(abs(temp_structure.p[k].x) < c)  			
						temp_structure.p[k].which_boundary = 2; //left
				if(abs(height-temp_structure.p[k].y) < c)		
						temp_structure.p[k].which_boundary = 3; //top
				if(abs(temp_structure.p[k].y) < c) 				
						temp_structure.p[k].which_boundary = 4; //bottom

                
                
	}

	int n = temp_structure.p.size();// > c.p.size() ? c.p.size() : temp_structure.p.size();
    force_energy::calculation::getInstance().setSize(n, width, height);
	cout << c.p.size() << endl;
	cout << n << endl;
        
	for (int k = 0; k < n; ++k) {
		c.p[k]    = temp_structure.p[k];
		c.pcons[k] = temp_structure.p[k];
		c.pfix[k]  = temp_structure.p[k];
		c.alloying[k] = BLK1::beta_Var;

	}
        //periodicity efforts
//         int ax;
//        for (int k = 0; k < n; ++k) {
//            if(c.p[k].which_boundary == 2){ // find the left b.atoms
//                	int jj = temp_structure.p[k].j;
// 				int ii = temp_structure.p[k].i;
// 				int limit = sqrt(width*width+height*height*1.0);
// 				for(int l=ii + 1;l<=limit;++l){
// 					int g = temp_structure.get_punto(l, jj);
// 					int g2 = temp_structure.get_punto(l+1, jj); //check if right ind exists
// 				    if(temp_structure.p[g].is_boundary==1  && g2==-1  ){
// 						ax= g;
// 						c.p[ax].which_boundary = 10;
// 						auto real_coordinates  = get_XY_for_rotated_square(ii-1, jj, angle);
// 						auto real_coordinates2 = get_XY_for_rotated_square(l, jj, angle);
// 
// 						double x1 = real_coordinates.first;
// 						double y1 = real_coordinates.second;
// 						double x2 = real_coordinates2.first;
// 						double y2 = real_coordinates2.second;
// 						//translation vector
// 						next_point.x = (x2-x1);
// 						next_point.y = (y2-y1);
// 						//indices of periodic friend
// 						next_point.i = l;
// 						next_point.j = jj;
// 						c.pbc[k].push_back(next_point);
// // 						c.p[ax].x -= (x2-x1);
// // 						c.p[ax].y -= (y2-y1);
// 						
// 						break;
// 					}
// 				}
// 
//             }                
// 		}
   
//        for (int k = 0; k < n; ++k) {
//            if(c.p[k].which_boundary == 1){ // find the right b.atoms
//                	int jj = temp_structure.p[k].j;
// 				int ii = temp_structure.p[k].i;
// 				for(int l=ii - 1;l>=0;--l){
// 					int g = temp_structure.get_punto(l, jj);
// 					int g2 = temp_structure.get_punto(l-1, jj);//check if left ind exists
// 				    if(temp_structure.p[g].is_boundary==1  && g2==-1  ){
// 						ax= g;
// 						c.p[ax].which_boundary = 10;
// 						auto real_coordinates = get_XY_for_rotated_square(ii+1, jj, angle);
// 						auto real_coordinates2 = get_XY_for_rotated_square(l, jj, angle);
// 
// 						double x1 = real_coordinates.first;
// 						double y1 = real_coordinates.second;
// 						double x2 = real_coordinates2.first;
// 						double y2 = real_coordinates2.second;
// 
// 						c.p[ax].x -= (x2-x1);
// 						c.p[ax].y -= (y2-y1);
// 						break;
// 					}
// 				}
// 
//             }                
// 		}
//    
   
        
//         c.pcons = c.p;
//         c.pfix  = c.p;
        

}

void init(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	//   int dnx = dimensions::nx/2. -1.;
	//   int dny = dimensions::ny/2. -1.;

	double dnx = c.dnx;
	double dny = c.dny;

	double dc = BLK1::symmetry_constantx;
	double dcx = BLK1::symmetry_constantx;
	double dcy = BLK1::symmetry_constanty;

	int ix, iy, ntot = c.p.size();

	for (int i = 0; i<c.p.size(); ++i)
	{
		int iy = i / nx;
		int ix = i % nx;

		double dix = double(ix - dnx);
		double diy = double(iy - dny);


		if (BLK1::crystal_symmetry.compare("hex") == 0)
		{
			if (iy % 2 == 0)
			{
				c.p[i].x = dc*dix;
				c.p[i].y = dc*diy*sqrt(3) / 2;
				c.pfix[i].x = dc*dix;
				c.pfix[i].y = dc*diy*sqrt(3) / 2;

			}


			if (iy % 2 != 0)
			{

				c.p[i].x = dc*(dix + 0.5);
				c.pfix[i].x = dc*(dix + 0.5);

				c.pfix[i].y = dc*diy*sqrt(3) / 2;
				c.p[i].y = dc*diy*sqrt(3) / 2;

			}
		}


		else if (BLK1::crystal_symmetry.compare("square") == 0)
		{

			c.p[i].x = (dix - dnx);
			c.p[i].y = (diy - dny);
			c.pfix[i].x = (dix - dnx);
			c.pfix[i].y = (diy - dny);

		}


		else if (BLK1::crystal_symmetry.compare("tilted") == 0)
		{

			if (iy % 2 == 0)
			{
				c.p[i].x = dcx*dix;
				c.p[i].y = dcy*diy;

				c.pfix[i].x = dcx*dix;
				c.pfix[i].y = dcy*diy;

			}


			if (iy % 2 != 0)
			{

				c.p[i].x = dcx*dix + dcx / 2.;
				c.pfix[i].x = dcx*dix + dcx / 2.;

				c.pfix[i].y = dcy*diy;
				c.p[i].y = dcy*diy;

			}
		}





		c.disp[i].x = 0;
		c.disp[i].y = 0;

		//  NEIGHBORDS OF POINTS TRIANGULATION

		c.p[i].nn[0] = (iy)*nx + ix + 1; //EAST
		c.p[i].nn[1] = (iy + 1)*nx + ix; //NORTH    
		c.p[i].nn[2] = (iy)*nx + ix - 1; //WEST
		c.p[i].nn[3] = (iy - 1)*nx + ix; //SOUTH     


										 //IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS

		if (ix == nx - 1)
			c.p[i].nn[0] = (iy)*nx; 	         //EAST	    
		if (iy == ny - 1)
			c.p[i].nn[1] = ix;              //NORTH			
		if (ix == 0)
			c.p[i].nn[2] = (iy)*nx + nx - 1; 	 //WEST	    
		if (iy == 0)
			c.p[i].nn[3] = (iy + (ny - 1))*nx + ix; //SOUTH			

		c.p[i].nn[4] = c.p[i].nn[0];
		c.p[i].nn[4] = c.p[i].nn[0];

		if (BLK1::crystal_symmetry.compare("tilted") == 0)
		{
			int no, ne, nw, se, sw;

			if (iy % 2 == 0)
			{
				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix;
				nw = (iy + 1)*nx + ix - 1;
				se = (iy - 1)*nx + ix;
				sw = (iy - 1)*nx + ix - 1;

				if (ix == 0)
				{
					nw = (iy + 1)*nx + (nx - 1);
					sw = (iy - 1)*nx + (nx - 1);


				}

				if (iy == 0)
				{
					se = (ny - 1)*nx + ix;
					sw = (ny - 1)*nx + ix - 1;

				}

				if (iy == 0 && ix == 0)
				{
					ne = (iy + 1)*nx + ix;
					nw = (iy + 1)*nx + nx - 1;
					se = (ny - 1)*nx + 0;
					sw = (ny - 1)*nx + nx - 1;



				}

			}


			if (iy % 2 != 0)
			{

				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix + 1;
				nw = (iy + 1)*nx + ix;
				se = (iy - 1)*nx + ix + 1;
				sw = (iy - 1)*nx + ix;

				if (iy == ny - 1)
				{
					ne = (0)*nx + ix + 1;
					nw = (0)*nx + ix;

				}

				if (ix == nx - 1)
				{
					ne = (iy + 1)*nx + 0;
					se = (iy - 1)*nx + 0;

				}

				if (iy == ny - 1 && ix == nx - 1)
				{
					ne = 0;
					nw = nx - 1;

				}

			}

			//  NEIGHBORDS OF POINTS TRIANGULATION FOR TILTED

			c.p[i].nn[0] = ne; //NORTHEAST
			c.p[i].nn[1] = nw; //NORTHWEST    
			c.p[i].nn[2] = sw; //SOUTHEAST
			c.p[i].nn[3] = se; //SOUTHWEST     


							   //IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS


			c.p[i].nn[4] = c.p[i].nn[0];
			c.p[i].nn[4] = c.p[i].nn[0];


		}

		//alloying
		int cooxsq = (ix - nx / 2)*(ix - nx / 2);
		int cooysq = (iy - ny / 2)*(iy - ny / 2);
		c.alloying[i] = BLK1::beta_Var;
		// 	if(cooxsq + cooysq <=10  ) 
		// 		c.alloying[i]  = BLK1::beta_Var+1;



	}

	c.pcons = c.pfix;




}

void assign_bc_MJ(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;


	int ix, iy, ntot = c.p.size();
	int k;
	string_to_integer bset;

	string  BC = BLK1::BC;
	string  lattice = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);

	bset.convert_new(BLK1::bc_x, BLK1::bc_y);
	std::vector<int> miss_ind;
	miss_ind.resize(0);
	cout<<"boundary conditions:"<<endl;
	cout<<"right: "<<bset.x <<endl;
	cout<<"left: "<< bset.x<<endl;
	cout<<"top: "<< bset.y<<endl;
	cout<<"bottom: "<<bset.y <<endl;

		for (int k = 0; k<c.p.size(); ++k)
			for (int s = 0; s < 4; s++){
				c.bc_cond[k][s] = 2; // full interaction
		}	
	
		for (int k = 0; k<c.p.size(); ++k)
		{
// 			int counter2=0;
// 			miss_ind.resize(0);

		int ii = c.p[k].i;
		int jj = c.p[k].j;
		
		if(c.p[k].nn[0]==-1)
		{
			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
				c.bc_cond[k][0] = bset.x;
				c.bc_cond[k][3] = bset.x;
			}
			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
				c.bc_cond[k][0] = bset.y;
				c.bc_cond[k][3] = bset.y;
			}							
		}
		if(c.p[k].nn[1]==-1)
		{
			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
				c.bc_cond[k][0] = bset.x;
				c.bc_cond[k][1] = bset.x;
			}
			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
				c.bc_cond[k][0] = bset.y;
				c.bc_cond[k][1] = bset.y;
			}							
		}
		if(c.p[k].nn[2]==-1)
		{
			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
				c.bc_cond[k][1] = bset.x;
				c.bc_cond[k][2] = bset.x;
			}
			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
				c.bc_cond[k][1] = bset.y;
				c.bc_cond[k][2] = bset.y;
			}							
		}
		
		if(c.p[k].nn[3]==-1)
		{
			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
				c.bc_cond[k][2] = bset.x;
				c.bc_cond[k][3] = bset.x;
			}
			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
				c.bc_cond[k][2] = bset.y;
				c.bc_cond[k][3] = bset.y;
			}							
		}		
		
// 		c.p[k].nn[0] = c.get_punto(ii + 1, jj);
// 		c.p[k].nn[1] = c.get_punto(ii, jj + 1);
// 		c.p[k].nn[2] = c.get_punto(ii - 1, jj);
// 		c.p[k].nn[3] = c.get_punto(ii, jj - 1);
// 		c.p[k].nn[4] = c.p[k].nn[0];
// 		cout<<"space_i: "<<c.p[k].x<<" space_j: "<<c.p[k].y<<endl;
// 		cout<<"east: "<<c.p[k].nn[0]<<" north: "<<c.p[k].nn[1]<<endl;
// 		cout<<"west: "<<c.p[k].nn[2]<<" south: "<<c.p[k].nn[3]<<endl;
// 		cout<<"east: "<<c.p[k].nn[4]<<" east: "<<c.p[k].nn[4]<<endl;
// 		cout<<"--------"<<endl;

// 			for (int s = 0; s < 5; s++)
// 				if (c.p[k].nn[s] == -1){ //check if a bond is  is missing
// 					c.p[k].is_boundary = 1; //assign as boundary
// 					counter2++;
// 					miss_ind.push_back(s);
// 					for (int l=s+1;l<5;++l){
// 						if (c.p[k].nn[l] == -1)//check if another bond is missing
// 							counter2++;	
// 							miss_ind.push_back(l);	
// 					}
// 					if(counter2 >0)
// 						break;		
// 			}
			
// 						temp_structure.p[k].which_boundary = 1; //right
// 				if(abs(temp_structure.p[k].x) < c)  			
// 						temp_structure.p[k].which_boundary = 2; //left
// 				if(abs(height-temp_structure.p[k].y) < c)		
// 						temp_structure.p[k].which_boundary = 3; //top
// 				if(abs(temp_structure.p[k].y) < c) 				
// 						temp_structure.p[k].which_boundary = 4; //bottom
			

// 				if(counter2 >0) // it means missing bond has been detected
// 				{
// 					//right
// 					if(c.p[k].which_boundary ==1 ){
// 					
// 					c.bc_cond[k][0] = bset.x;
// 					c.bc_cond[k][3] = bset.x;
// 					
// 					
// 					}
// 					//left
// 					if(c.p[k].which_boundary ==2 ){
// 					
// 					c.bc_cond[k][1] = bset.x;
// 					c.bc_cond[k][2] = bset.x;
// 					
// 					
// 					}				
// 
// 					//top
// 					if(c.p[k].which_boundary ==3 ){
// 					
// 					c.bc_cond[k][0] = bset.y;
// 					c.bc_cond[k][1] = bset.y;
// 					
// 					
// 					}
// 
// 					//bottom
// 					if(c.p[k].which_boundary ==4 ){
// 					
// 					c.bc_cond[k][2] = bset.y;
// 					c.bc_cond[k][3] = bset.y;
// 					
// 					
// 					}					
// 
// 				if(counter2 >0){
// 					for (int i = 0; i<miss_ind.size(); ++i){
// 					cout<<"space_i: "<<c.p[k].x<<" space_j: "<<c.p[k].y<<" miss_ind[i]: "<< miss_ind[i]<<endl;
// 						
// 						if(miss_ind[i]==0) {
// // 							if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 							c.bc_cond[k][0] = bset.x;
// 							c.bc_cond[k][3] = bset.x;
// // 							}
// // 							if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// // 							c.bc_cond[k][0] = bset.y;
// // 							c.bc_cond[k][3] = bset.y;
// // 							}							
// 							
// 						}
// 						
// 
// 						if(miss_ind[i]==1) {
// // 							if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 							c.bc_cond[k][0] = bset.x;
// 							c.bc_cond[k][1] = bset.x;
// // 							}
// // 							if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// // 							c.bc_cond[k][0] = bset.y;
// // 							c.bc_cond[k][1] = bset.y;
// // 							}							
// // 							
// 						}	
// 						if(miss_ind[i]==2) {
// // 							if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 							c.bc_cond[k][1] = bset.x;
// 							c.bc_cond[k][2] = bset.x;
// // 							}
// // 							if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// // 							c.bc_cond[k][1] = bset.y;
// // 							c.bc_cond[k][2] = bset.y;
// // 							}							
// // 							
// 						}						
// 						
// 						if(miss_ind[i]==3) {
// // 							if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 							c.bc_cond[k][2] = bset.x;
// 							c.bc_cond[k][3] = bset.x;
// // 							}
// // 							if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// // 							c.bc_cond[k][2] = bset.y;
// // 							c.bc_cond[k][3] = bset.y;
// // 							}							
// 							
// 						}																		
// 						
// 					}
// 				}
					
	
				}			
			
			

			//guarantees full interaction in each cell
		
	
}


void assign_bc(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;


	int ix, iy,ntot=c.p.size();
	int k;
	string_to_integer bset;

	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);

	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
  

  
	if(BLK1::crystal_symmetry.compare("tilted") == 0)
	{
		for(int i =0;i<ntot;++i)
		{
			int iy = i / nx;
			int ix = i % nx;

			//guarantees full interaction in each cell
			c.bc_cond[i][0] = 2;
			c.bc_cond[i][1] = 2;
			c.bc_cond[i][2] = 2;
			c.bc_cond[i][3] = 2;

			//change the boundary condition on y
			if( (iy%2 ==0 && ix == 0)   )
			{
				
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][1] = bset.x;
				c.bc_cond[i][2] = bset.x;
			}
			
			if( (iy%2 !=0 && ix == nx-1) )
			{
				
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][2] = bset.x;
				c.bc_cond[i][3] = bset.x;
			}		
			
				//change the boundary condition on y 
			if( iy == ny-1 )
			{
				c.bc_cond[i][0] = bset.y;	
				c.bc_cond[i][1] = bset.y;	
				c.bc_cond[i][3] = bset.y;		
				
			}	
			
			if( iy == 0 )
			{
				c.bc_cond[i][1] = bset.y;	
				c.bc_cond[i][2] = bset.y;	
				c.bc_cond[i][3] = bset.y;		
				
			}
			//special points
				
				
		}
	}


		else if(BLK1::crystal_symmetry.compare("tilted") != 0)
		{
			//guarantees full interaction in each cell
			
			for(int k =0;k<4;++k)
			for(int i =0;i<ntot;++i)
			{
				int iy = i / nx;
				int ix = i % nx;
				c.bc_cond[i][k] = 2;
			
				//change the boundary condition on x
				if((ix == nx-1 && k == 0) || (ix == 0 && k == 2) || (ix == 0 && k == 1)  || (ix == nx-1 && k == 3) )
					c.bc_cond[i][k] = bset.x;
				//change the boundary condition on y, overrides on corners
				if((iy == ny-1 && k == 0) || (iy == 0 && k == 3) || (iy == ny-1 && k == 1) || (iy == 0 && k == 2))
					c.bc_cond[i][k] = bset.y;

	// 			if( (iy == ny-2 && ix == nx-1 && k ==0) || (iy == ny-2 && ix == 0 && k==2) )
	// 				c.bc_cond[i][k] = bset.y;
	// 			if( (iy == 1 && ix == nx-1 && k ==0) || (iy == 1 && ix == 0 && k==2) )
	// 				c.bc_cond[i][k] = bset.y;

			}
			
		}

			
 	
   
    
       
}
std::pair<double, double> get_XY_for_rotated_hex(int ix, int iy, double angle) {
	double i = (iy % 2 == 0) ? ix : ix + 0.5;
	double j = iy * sqrt(3) / 2.0;

	double beta = atan2(1.0*j , 1.0*i);
	double gamma = angle + beta;
// 	double r = sqrt(1.0*i*i + j*j);
// 	double x = r*cos(gamma);
// 	double y = r*sin(gamma);
	double r = sqrt(1.0*i*i + j*j);
	double x = r*cos(gamma);
	double y = r*sin(gamma);


	return std::pair<double, double>(x, y);
}
// 

void new_init_rotated_hex(struct conf_stru& c, double angle) {
	int width  = dimensions::nx;
	int height = dimensions::ny;
	conf_stru temp_structure;
	punto_stru origin_point;
	origin_point.x = 0;
	origin_point.y = 0;
	origin_point.i = 0;
	origin_point.j = 0;
// 	temp_structure.p.push_back(origin_point);

	bool i_loop = true;
	bool jmax_loop = true;
	bool jmin_loop = true;
	int i = 0;
	int j = 0;

	angle = toRadians(angle);
        cout<<"while loop starts for rotation angle: "<<angle<<endl;
    bool added = true;
	while (added) {
			added = false;

			j = 0;
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0.0 && x - width <= 0.1) && (y - height <= 0.1)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}

		j = 1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0 && x - width <= 0.1) && (y - height <= 0.1 )) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i;
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (y >= height) 
				break;
			j++;
		}

		j = -1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x - width <= 0.1) && (y >= -0.01)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
				j--;		
			}
			else {
				break;
			}
		}

		i++;
	}
	
		int n = temp_structure.p.size();
		force_energy::calculation::getInstance().setSize(n, width, height);

		for (int k = 0; k < n; ++k) {
			c.p[k]    = temp_structure.p[k];
			c.pcons[k] = temp_structure.p[k];
			c.pfix[k]  = temp_structure.p[k];
			c.alloying[k] = BLK1::beta_Var;

		}


}

int toIdx1D(int x, int y, int rowWidth) {
	return y*rowWidth + x;
}


//classical hexagonal deal with periodic boundaries
void initHex6(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int ix, iy;

	for (int i = 0; i < nx*ny; ++i)
	{
		iy = i / nx;
		ix = i % nx;


		// dnx, dny - TO BE CHECKED; MAYBE IMPORTANT !!!!
		double hix, hiy;
		hix = (iy % 2 == 0) ? ix : ix + 0.5;
		hiy = iy * sqrt(3) / 2.0;

		c.p[i].x = hix;
		c.p[i].y = hiy;

		//double dix = double(ix - dnx);
		//double diy = double(iy - dny);

		//c.p[i].x = dix;
		//c.p[i].y = diy;

		//  NEIGHBORDS OF POINTS TRIANGULATION

		if (iy % 2 != 0)
		{
			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[1] = toIdx1D(ix, iy + 1, nx); //NORTH EAST
			c.p[i].nn[2] = toIdx1D(ix - 1, iy + 1, nx); //NORTH WEST
			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[4] = toIdx1D(ix - 1, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[5] = toIdx1D(ix, iy - 1, nx); //SOUTH EAST
		}
		else
		{
			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[1] = toIdx1D(ix + 1, iy + 1, nx); //NORTH EAST
			c.p[i].nn[2] = toIdx1D(ix, iy + 1, nx); //NORTH WEST
			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[4] = toIdx1D(ix, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[5] = toIdx1D(ix + 1, iy - 1, nx); //SOUTH EAST

		}

		c.p[i].nn[6] = c.p[i].nn[0];
	}


	// PERIODIC CONDITIONS FOR X LINES (BOTTOM Y=0; TOP Y = NY-1) WITHOUT CORNERS
	for (int px = 1; px < nx - 1 - 1; ++px) {
		c.p[(ny - 1)*nx + px].nn[1] = toIdx1D(px + 1, 0, nx); //NORTH EAST
		c.p[(ny - 1)*nx + px].nn[2] = toIdx1D(px, 0, nx); //NORTH WEST

		c.p[px].nn[4] = toIdx1D(px - 1, ny - 1, nx); //SOUTH WEST
		c.p[px].nn[5] = toIdx1D(px, ny - 1, nx); //SOUTH EAST
	}

	// PERIODIC CONDITIONS FOR Y LINES (LEFT X=0; RIGHT X = NX-1) WITHOUT CORNERS
	for (int py = 2; py < ny - 1; py += 2) {
		c.p[toIdx1D(0, py, nx)].nn[2] = toIdx1D(nx - 1, py + 1, nx); //NORTH WEST
		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST
		c.p[toIdx1D(0, py, nx)].nn[4] = toIdx1D(nx - 1, py - 1, nx); //SOUTH WEST

		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
	}

	for (int py = 1; py < ny - 1; py += 2) {
		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST

		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
		c.p[toIdx1D(nx - 1, py, nx)].nn[1] = toIdx1D(0, py + 1, nx); //NORTH EAST
		c.p[toIdx1D(nx - 1, py, nx)].nn[5] = toIdx1D(0, py - 1, nx); //SOUTH EAST	
	}

	// PERIODIC CONDITIONS -- CORNERS

	// BOTTOM LEFT
	c.p[0].nn[2] = toIdx1D(nx - 1, 1, nx); //NORTH WEST
	c.p[0].nn[3] = toIdx1D(nx - 1, 0, nx); //WEST
	c.p[0].nn[4] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH WEST
	c.p[0].nn[5] = toIdx1D(0, ny - 1, nx); //SOUTH EAST


	// BOTTOM RIGTH	
	c.p[nx - 1].nn[0] = toIdx1D(0, 0, nx); //EAST
	c.p[nx - 1].nn[4] = toIdx1D(nx - 2, ny - 1, nx); //SOUTH WEST
	c.p[nx - 1].nn[5] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH EAST

	// TOP RIGHT
	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[0] = toIdx1D(0, ny - 1, nx); //EAST
	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[1] = toIdx1D(0, 0, nx); //NORTH EAST
	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[2] = toIdx1D(nx - 1, 0, nx); //NORTH WEST
	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[5] = toIdx1D(0, ny - 2, nx); //SOUTH EAST

	// TOP LEFT
	c.p[toIdx1D(0, ny - 1, nx)].nn[1] = toIdx1D(1, 0, nx); //NORTH EAST
	c.p[toIdx1D(0, ny - 1, nx)].nn[2] = toIdx1D(0, 0, nx); //NORTH WEST
	c.p[toIdx1D(0, ny - 1, nx)].nn[3] = toIdx1D(nx - 1, ny - 1, nx); //WEST
}







