#pragma once
#include <cmath>
#include "common.h"
#include "namespaces.h"
#include "structures.h"
struct energy_stress energy_zanzotto(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v);
struct energy_stress energy_log(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v);
struct energy_stress energy_lj(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v);
struct energy_stress energy_morse(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v);
struct energy_stress energy_nikolai(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v);

double energy_isotrope(const struct cella_stru& c, double burgers, double alloy);