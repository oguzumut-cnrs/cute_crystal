#include "non_iterative_static.h"
#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
using namespace dlib;

inline bool exist(const std::string& name)
{
	std::ifstream infile(name);
	return infile.good();
}

void non_iterative_statique(struct conf_stru& c)
{
   
   
   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();
	c.load = BLK1::load;
	double initial_strain  =BLK1::load;

	double load=0.;
	double temperature = BLK1::beta_Var;
// 	initial_strain = initial_strain/(double)ny;

	int t=0;
	static int n_direction = 2;
 	if(BLK1::strain_rate <0)
 		n_direction = 3;
		
	
	const char* filename = "roule";

	ofstream myfile;
	myfile.open ("roule");
	
	//SHEARING ANGLE
 	double theta=BLK1::theta;

	int temp_counter=0;
	//for(int k=0;k<2;k=k+c.inc)
	for(int k=0;k<dimensions::triangle_limit;k=k+c.inc)
	for(int i=0;i<n;i++)
	{
		if(c.bc_cond[i][k] == 2)
			temp_counter++;
	}  
	
	c.total_triangular_number = temp_counter;
	cout<<"c.total_triangular_number= "<<c.total_triangular_number<<endl;
	write_to_a_ovito_file(c, 999);


   //IF FRAME DEFORMATION ACTIVATED, C_PFIX STARTS AS A DEFORMED CONFIGURATION
		boundary_conditions set0,setold;
		set0.setParameters(theta,load);

	if(BLK1::bc_mode.compare("frame") ==0)
		set0.def_frame(force_energy::calculation::getInstance().c,set0);
	else
		set0.macro_shear();
	setold=set0;
   //These are displacements needed in CG

	for(int i=0;i<n;i++)
	{
		c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		c.disp[i].y  = c.p[i].y -c.pfix[i].y ;   

	}  

	////dipole v


//    std::vector<double > v3 = edge_dislocation(ny/2-1    ,  40);    
//    std::vector<double > v4 = edge_dislocation(ny/2 ,nx/2+nx/4	);


    std::vector<double > v3 = edge_dislocation(ny/2-1    ,nx/2-20);    
    std::vector<double > v4 = edge_dislocation(ny/2      ,nx-8);


	//dipole v
//  
     std::vector<double > v1 = edge_dislocation2( 70 ,nx-16  );    
     std::vector<double > v2 = edge_dislocation2(ny-80,nx-17 );
 



    column_vector starting_point,sp_max,sp_min,starting_point2;
	starting_point.set_size(2*n);
	starting_point2.set_size(2*n);

	sp_max.set_size(2*n);
	sp_min.set_size(2*n);

	dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
		starting_point(i)     = c.disp[i].x  ;
		starting_point(i+n)   = c.disp[i].y  ;   	
	});

//to make the model scalar

	if(BLK1::scalar.compare("yes") ==0){
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0. ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = 0.  ;   	


		});  
	} 


	else if(BLK1::scalar.compare("semi") ==0){
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0.3 ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = -0.3  ;   	


		});  
	} 

	else if(BLK1::scalar.compare("no") ==0){
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     =  1000.  ;
			sp_max(i+n)   =  1000. ;   	
			sp_min(i)     = -1000.  ;
			sp_min(i+n)   = -1000.  ;   	

				double h=1;	
				starting_point(i)     =  0*dimensions::dh*1.1*(1.1*v3[i] - 1.1*v4[i])	;
				starting_point(i+n)   = +0*1.1*(1.1*v1[i] - 1.1*v2[i]) ;   	

				 int iy = i / nx;
				 int ix = i % nx;
//  				 if(iy==ny/2-1 && (ix>nx/8 && ix< nx- nx/8)  )
//  				 	starting_point(i) = .5001;
//  				 if(iy==ny/2 && (ix>nx/8 && ix< nx- nx/8)  )
//  				 	starting_point(i) = -.5001;


// 				 if(iy==ny/2 && ix>nx/4 )
// 				 	starting_point(i) = .35;


				c.disp[i].x +=starting_point(i) ;
				c.disp[i].y +=starting_point(i+n)   ;   	


		});  	
		
	} 
	
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
	if(BLK1::IC.compare("none") !=0 ){

		std::vector<double > u_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );    
		std::vector<double > v_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );  
		for(int i=0;i<n;i++){
			  c.disp[i].x +=u_dis[i];
			  c.disp[i].y +=v_dis[i];
		}
	}


	if(BLK1::IC.compare("volume") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(1., BLK1::d2,c.p.size());
	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;


		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][3] =dv4[i];


	}
	
	
	if(BLK1::IC.compare("alloying") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());

	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
				c.alloying[i][k]  = BLK1::beta_Var;


		for(int i=0;i<n;i++)
			  c.alloying[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][3] =dv4[i];


	}	
	
	
	if(BLK1::IC.compare("precip") ==0 || BLK1::IC.compare("precipx") ==0  ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 amp=BLK1::d1;
	 std::vector<double > dv1,dv2,dv3;
	  
	  dv1 =   perturba_precipate(amp,c.p.size());
	  dv2 =   perturba_precipate(amp,c.p.size());
	  dv3 =   perturba_precipate(amp,c.p.size());

		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;
		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv3[i];
		
	}	
	
	

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());

	for(int i=0;i<n;i++)
	{
		int iy = i / nx;
		int ix = i % nx;
		
		

		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ; 
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
		starting_point2(i)   = starting_point(i);
		starting_point2(i+n) = starting_point(i+n);

	}  


	write_to_a_ovito_file(c, 99);

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

	
	string dissipation,av_sizes,av_sizes2,av_sizes3,size_vs_energy,strain_vs_energy,slip;
	string  av_sizespitteri = "int_total_slip_vs_stress_drop.dat";
	string half_stress =  "half_stress_drop.dat";
	string full_stress =  "full_stress_drop.dat";
	string piola_stress =  "well_drop.dat";
	string piola_stress_full =  "well_drop_vs_energy_drop.dat";

	dissipation="dissipation.dat" ;
	slip="alpha_vs_drop.dat" ;

	av_sizes="avalanches.dat" ;
	av_sizes2="stress_drop.dat" ;
	av_sizes3="strain_jump.dat" ;
	size_vs_energy="well_drop_vs_stress_drop.dat.dat" ;
	strain_vs_energy="alpha_vs_dissipation.dat" ;

	string slip_grad  = "total_slip.dat";

	
	fstream filestr,filestrav,filestrav2,filestrav3,filestrse;
	
	fstream peng1,peng2,peng3,peng4;
	string penge =  "peng_energy.dat";
	string pengs =  "peng_stress.dat";
	string pengde =  "peng_dstress.dat";
	string pengds =  "peng_denergy.dat";


	string fulldata =  "full_data.dat";
	fstream fsfullstream;

	
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	std::vector <std::vector<matrix_stru> > m_field;
	m_field.resize(n);
	for (int i = 0; i < n; ++i)
		m_field[i].resize(6);
                    
                
	std::vector <std::vector<int> > well_locator,well_locator_old;
	well_locator.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator[i][k]=0;


	well_locator_old.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator_old[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator_old[i][k]=0;

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////


	
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i)
		{
			starting_point(i)     = c.disp[i].x  ;
			starting_point(i+n)   = c.disp[i].y  ;   	
		});  
		
		double drop =0;
		double drop3 =0;

		double stress_initial=0;
		double stress_final =0;
		double strain_initial =initial_strain;
		double strain_final   =initial_strain;
		double energy_initial =0;
		double energy_final   =0;
		double slip_initial =0;
		double slip_final =0;
		double stress_final_pitteri=0;
		double stress_initial_pitteri=0;
		double total_slip =0;
		double stress_initial_full=0;
		double stress_final_full=0;
		double stress_initial_piola=0;
		double stress_final_piola=0;
		double stress_initial_piola_full=0;
		double stress_final_piola_full=0;


		
		c.soft = 0.0;
		boundary_conditions set1;

		

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	int memorynumber =BLK1::memory; //0 memory //-1normal
    for(int iter=0;iter<BLK1::total_iteration;++iter){	
		

// 	    if(iter>475)
// 	    	BLK1::setCg_save(1,5);
	
		if(iter> memorynumber){
	    result_memorize(c);
	    if(iter%1 == 0){
	    	string answer = "l";
	    	//ps_file(c,t, answer);
	    }

		if(BLK1::constraint_min.compare("yes") ==0)
			apply_BC_box(c,sp_min,sp_max); 
		
// 		double energy_initial = energy_for_avalanches(c,set0,setold);
		//recall here that is is the energy and stress of loaded but not relaxed system
		energy_initial = rosen(starting_point)*(1.0);
		stress_initial = avalanche_sizes(c);
		stress_initial_pitteri = avalanche_sizes_half(c);
		stress_initial_full = avalanche_sizes_full(c);
		stress_initial_piola = avalanche_sizes_piola(c);
		stress_initial_piola_full = avalanche_sizes_piola_full(c);

			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(7)  << " " << c.load<<" "<<energy_initial/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(7)  << " " << c.load<<" "<<stress_initial/(1.*c.total_triangular_number)<<endl ;
				peng2.close();
 			}



		cout<<"conjugate gradient starts"<<endl;
		
		double wall0 = get_wall_time();
		double cpu0  = get_cpu_time();



		if(BLK1::constraint_min.compare("no") ==0){
		find_min(lbfgs_search_strategy(14),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, -n);
		}

// 		if(t>=117)
// 				BLK1::setCg_save(1,10);


		if(BLK1::constraint_min.compare("yes") ==0){
// 			find_min_box_constrained(lbfgs_search_strategy(7),  // The 10 here is basically a measure of how much memory L-BFGS will use.
// 			gradient_norm_stop_strategy(BLK1::precision).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 			rosen, rosen_derivative, starting_point, sp_min,sp_max);

			find_min_box_constrained(lbfgs_search_strategy(12),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, sp_min,sp_max);

    		// Set up parameters
//     		LBFGSParam<double> param;
//     		param.epsilon = 5e-3;
//     		param.max_iterations = 10000;
// 
//     		// Create solver and function object
//     		LBFGSSolver<double> solver(param);
//     		Rosenbrock fun(2*n);
// // 
//     		// Initial guess
//     		VectorXd x = VectorXd::Zero(2*n);
//     		// x will be overwritten to be the best point found
//     		double fx;
//     		for (int i = 0; i < n; ++i){
//     			x[i] = starting_point(i);
// 	    		x[i+n] = starting_point(i+n);
//     		}
//     		int niter = solver.minimize(fun, x, fx);
// // 
//     		std::cout << niter << " iterations" << std::endl;
// //     		std::cout << "x = \n" << x.transpose() << std::endl;
// //     		std::cout << "f(x) = " << fx << std::endl;
//             for (int i = 0; i < n; ++i){
//     			starting_point(i)  = x[i];
//     			starting_point(i+n)= x[i+n];
//     		}


		}
		

		cout<<"conjugate gradient ends"<<endl;



		double wall1 = get_wall_time();
		double cpu1  = get_cpu_time();

		cout << "Wall Time = " << wall1 - wall0 << endl;
		cout << "CPU Time  = " << cpu1  - cpu0  << endl;
		
//  		cout<<rosen(starting_point);
		double sizes=0;
		energy_final = rosen(starting_point)*(1.0);
		



		 //it updates the  solution for measurements and keeps the old solution
		 for(int i=0;i<n;i++)
		 { 
			 double xx =  starting_point(i);
			 double yy =  starting_point(i+n); 



			 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
			 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;

		 }

		 

		 result_memorize(c);
		 measurements(c);
		 double total_slips = slip_calculations(c);
		 int total_slips_int = slip_calculations_integer(c);

		// cout <<"total_slips= "<<total_slips<<endl;
		// cout <<"total_slips_integer= "<<total_slips_int<<endl;




		 stress_final = avalanche_sizes(c);
		 stress_final_pitteri = avalanche_sizes_half(c);
		 stress_final_full = avalanche_sizes_full(c);
		 stress_final_piola = avalanche_sizes_piola(c);
		 stress_final_piola_full = avalanche_sizes_piola_full(c);
		 
			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(7)  << " " << c.load<<" "<<energy_final/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				
				
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(7)  << " " << c.load<<" "<<stress_final/(1.*c.total_triangular_number)<<endl ;
				peng2.close();

// 				double drop_energy=energy_initial-energy_final;
// 
// 				peng3.open (pengde, fstream::in | fstream::out | fstream::app);
// 				peng3 << std::scientific << std::setprecision(7)   << " "<< load<<" "<<drop_energy<<endl ;
// 				peng3.close();
// 				
// 				double drop_stress=stress_initial-stress_final;
// 				peng4.open (pengds, fstream::in | fstream::out | fstream::app);
// 				peng4 << std::scientific << std::setprecision(7)  << " " << load<<" "<<drop_stress<<endl ;
// 				peng4.close();
// 				
// 				cout<<energy_initial<<" "<<energy_final<<" "<<energy_initial-energy_final<<" "<<drop_energy<<endl;


			}


		 //EULERIAN OR LAGRANGIAN
		 assign_solution(c,starting_point);
		//keep the solution against any change
// 		for (int i=0; i<n; ++i){	
// 	  	  starting_point2(i)     = starting_point(i);
// 	 	  starting_point2(i+n)   = starting_point(i+n);			 
// 		}

		well_locator_old = well_locator;
		actual_m_matrix_calculations(c,m_field,well_locator); 
		int well_jumps=0;;
		for(int k=0;k<dimensions::triangle_limit;k=k+c.inc){
			for(int i=0;i<n;i++){
				well_jumps +=	abs(well_locator[i][k] - well_locator_old[i][k]);
	
		}
			}
			
		cout<<"well_jumps= "<<well_jumps<<endl;


		 //drop of stress
		 drop = (stress_initial-stress_final)*pow(-1.,n_direction);
		 //drop of  half stress
		 double drop4 = (stress_initial_pitteri-stress_final_pitteri)*pow(-1.,n_direction);
		 double dropfull = (stress_initial_full-stress_final_full)*pow(-1.,n_direction);
		 double droppiola = (stress_initial_piola-stress_final_piola)*pow(-1.,n_direction);
		 double droppiolafull = (stress_initial_piola_full-stress_final_piola_full)*pow(-1.,n_direction);

		 cout<<"total_slips_int= "<<total_slips_int<<endl;
		 cout<<"drop= "<<drop<<endl;

		if(iter>1 ){
			//cout<< energy_initial-energy_final << endl;
			
			//energy dissipation
			filestr.open (dissipation, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(7)   << " "<<energy_initial-energy_final<<endl ;
			filestr.close();

			//total slip
			filestrav.open (av_sizes, fstream::in | fstream::out | fstream::app);
			filestrav << std::scientific << std::setprecision(7)   << " "<<total_slips<<endl ;
			filestrav.close();

			//stress drop
			filestrav2.open (av_sizes2, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<drop<<endl ;
			filestrav2.close();

			//half stress drop
			filestrav2.open (half_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<<c.load<<" "<<drop4<<endl ;
			filestrav2.close();

			//full stress drop
			filestrav2.open (full_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<dropfull<<endl ;
			filestrav2.close();
			//piola stress drop
			filestrav2.open (piola_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<endl ;
			filestrav2.close();

			//piola stress drop
			filestrav2.open (piola_stress_full, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<" "<<energy_initial-energy_final<<endl ;
			filestrav2.close();



			//distance to jumps
			double drop2  = abs(c.load - strain_initial);
			strain_initial = c.load;

			filestrav3.open (av_sizes3, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(drop)<< " "<<energy_initial-energy_final <<endl ;
			filestrav3.close();

			//total_slips_int versus strain
			filestrav3.open (av_sizespitteri, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips_int)<< " "<<abs(drop) <<endl ;
			filestrav3.close();

			
			//total_slip_integer versus energy dissipation
			filestrse.open (size_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<< " "<<drop <<endl ;
			filestrse.close();
			
			//total_slip versus energy dissipation
			filestrse.open (strain_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<c.load<< " "<<energy_initial-energy_final <<endl ;
			filestrse.close();			
			
			//total_slip integer
			filestrse.open (slip, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<c.load<< " "<<drop<<endl ;
			filestrse.close();	
			
			fsfullstream.open (fulldata, fstream::in | fstream::out | fstream::app);
			fsfullstream << std::scientific << std::setprecision(7)   
			<< " "<<c.load<< " "<<well_jumps<<" " <<drop<<" "<< energy_initial-energy_final << 
			" "<< drop4<<" "<< stress_initial/c.total_triangular_number<< 
			" "<< stress_initial_pitteri/c.total_triangular_number<<
			" "<<dropfull<<
			" "<< stress_final/c.total_triangular_number<<
			" "<< stress_final_pitteri/c.total_triangular_number<<
			" "<< stress_initial_full/c.total_triangular_number<<
			" "<< stress_final_full/c.total_triangular_number<<
			" "<< energy_initial/c.total_triangular_number<<
			" "<< energy_final/c.total_triangular_number<<endl ;
			fsfullstream.close();			
					


		}

		
		 
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
		set1.setParameters(theta,load);
		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
// 		if(BLK1::macro_def.compare("macro_shear_rhombic") ==0)
// 			set1.uni_macro_compression();

		
		
		//keep the previous solution
		for(int i =0;i<c.p.size();++i){
			temp_coo[i].x = c.p[i].x   ;				
			temp_coo[i].y = c.p[i].y   ;
			c.pold[i].x = c.p[i].x ;
			c.pold[i].y = c.p[i].y ;

		}

// 		def_grad temp;	
// 		temp= set1.f.multiply(setold.f.inverse(setold.f));
	// 
	// 	//guess the next solution incrementally		

		int inc = force_energy::calculation::getInstance().c.inc ;	
	

			//output operations
// 			ps_file(c,t); 
		if(iter%100 == 0 || well_jumps > 50*nx/200){
			//slicer(c,t);
			write_to_a_ovito_file(c,t);
			string filename2 = "all_load.dat";
			all_data_to_a_file(t,c.load,filename2);

			write_to_a_file(c,t);
			write_to_a_ovito_file_remove_Fmacro(c,t++,c.load);
			string filename = "last_load.dat";
			single_data_to_a_file(load,filename);
			if(BLK1::IC.compare("precipx") ==0 && iter==0){
				for(int k=0;k<3;k++)
					for(int i=0;i<n;i++)
			 		 c.disorder[i][k] =1;

			}
		//	do_delaunay(force_energy::calculation::getInstance().c,t,theta,load);
 		}
  		}
 		
 		if(iter == memorynumber){
 			string filename2  =  BLK1::config ;
			if(exist(filename2) == false){ 
		 		cout<<"no file sorry  "<<filename2<<"\n"; 
			}

			ifstream disp;

		//    cout<< filename << "  \n";
			disp.open(filename2.c_str());
			double temp;
			int nol;
			disp >> nol;
			cout<<"number of lines in the file"<<nol<<endl;
			for (int i = 0; i<n; ++i){
			disp >> std::scientific >> std::setprecision(16)>> c.p[i].x >> c.p[i].y >> temp >> temp >> temp ;
// 			if(i<2 || i>nol-2)
// 				cout << std::scientific << std::setprecision(14)<< c.p[i].x << " "<<c.p[i].y << " "<<temp << " "<< temp << " "<<temp <<endl;
			c.pfix[i].x = c.p[i].x;
			c.pfix[i].y = c.p[i].y; 
			c.pold[i].x  = c.p[i].x  ; 
			c.pold[i].y  = c.p[i].y  ; 		

			}

			disp.close();
			
			//string filename3  =  "../all_load.dat" ;
			string filename3  =  BLK1::config_u ;
			disp.open(filename3.c_str());


			for (int i = 0; i<=BLK1::memory_number; ++i)
				disp >> std::scientific >> std::setprecision(16)>> initial_strain>> temp  ;
			
			disp.close();
			c.load = initial_strain;

		    load = 0 ;
		   	BLK1::setNum(load);
		   	cout<<"initial strain memory= "<<initial_strain<<endl;
 		
   			set1.setParameters(theta,load);
   			choose_bc(set1);

        
        	apply_BC_generale(c,set1);
 			apply_BC(c,load);

		   	actual_m_matrix_calculations(c,m_field,well_locator); 
		   	result_memorize(c);
		   	measurements(c);


 		
 		}
		




	


		 if(fexists(filename) == false){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
				 
 	


	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////C is changing here due to the loading///////////////////////////
//         apply_BC_generale(c,set1);
//  	apply_BC(c,load);
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

//		double calculated_rate = BLK1::strain_rate;
//		double calculated_rate = m_matrix_calculations(c, load,theta,m_field,well_locator,n_direction);
 		double calculated_rate = BLK1::strain_rate;
		 
	


		if(n_direction % 2 == 0 && c.load>   BLK1::loading_limit ){
			n_direction++;
// 			goto label44 ;
		}
// 		if(n_direction % 2 != 0 && load< - BLK1::loading_limit ){
		if(n_direction % 2 != 0 && c.load< - 0 ){
		    printf ("Error opening file");
    		exit (EXIT_FAILURE);
			n_direction++;
		}

		
//		initial_strain+=calculated_rate;
// 		calculated_rate = BLK1::strain_rate;
 // 		initial_strain+=calculated_rate*pow(-1.,n_direction);
 		initial_strain+=calculated_rate;

// 		temperature+=0.01;
		load  = 0.;
		c.load = initial_strain;
		//c.soft = 0.;
		BLK1::setNum(load);
// 		BLK1::setBeta(temperature);
 		
   		set1.setParameters(theta,load);
   		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
			
		setold=set0;
		set0=set1;
        
        apply_BC_generale(c,set1);
 		apply_BC(c,load);
 		
        struct conf_stru guess;
		struct boundary_conditions setnew;
		setnew.setParameters(theta,calculated_rate);
   		choose_bc(setnew);





	for (int k=0; k<1; k=k+c.inc) 
		for (int i=0; i<n; ++i){
			
			//fix boundary condition
// 			if(check(c.bc_cond[i][k],0)){
				 //u_min and v_min
	  	      	starting_point(i)     =  c.pfix[i].x * setnew.f.f11 + c.pfix[i].y * setnew.f.f12 - c.pfix[i].x;
	 	 		starting_point(i+n)   =  c.pfix[i].x * setnew.f.f21 + c.pfix[i].y * setnew.f.f22 - c.pfix[i].y;
	 	 		c.p[i].x = starting_point(i)   + c.pfix[i].x;
	 	 		c.p[i].y = starting_point(i+n) + c.pfix[i].y;
				 
// 			}

// 				starting_point2(i)     = c.p[i].x - c.pfix[i].x;
// 	 	 		starting_point2(i+n)   = c.p[i].y - c.pfix[i].y;
// 				if(BLK1::BC.compare("eulerian") == 0){
// 				  starting_point2(i)    = -c.p[i].x + c.p[i].x * temp.f11 + c.p[i].y * temp.f12  ;				
// 				  starting_point2(i+n)  = -c.p[i].y + c.p[i].x * temp.f21 + c.p[i].y * temp.f22  ;
// 				}
// 
// 
// 				if(BLK1::BC.compare("eulerian") != 0){
// 				  starting_point2(i)    = -c.pfix[i].x + c.p[i].x * temp.f11 + c.p[i].y * temp.f12  ;				
// 				  starting_point2(i+n)  = -c.pfix[i].y + c.p[i].x * temp.f21 + c.p[i].y * temp.f22  ;
// 				}


		 }


		 if(fexists(filename) == false || load >= BLK1::loading_limit){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
		 	

				 
 	}

	label44: ; 
 	
}