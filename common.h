#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_

// ---------
// MJ include files
#include <vector>
#include <map>
#include <string>
#include "structures.h"
#include "struct_functions.h"
#include "energy_functions.h"


using std::string;
// ---------

namespace force_energy
{

	class calculation
	{
		public:
			static calculation& getInstance()
			{
				static calculation    instance; // Guaranteed to be destroyed.
									            // Instantiated on first use.
				return instance;
			}
			
		private:
			calculation() 
			{				 
			};                   // Constructor? (the {} brackets) are needed here.
			

			// C++ 11
			// =======
			// We can use the better technique of deleting the methods
			// we don't want.
		public:
			calculation(calculation const&)     = delete;
			void operator=(calculation const&)  = delete;

			// Note: Scott Meyers mentions in his Effective Modern
			//       C++ book, that deleted functions should generally
			//       be public as it results in better error messages
			//       due to the compilers behavior to check accessibility
			//       before deleted status

			
			//WHAT IS BELOW CANNOT BE ACCESED WITHOUT GET INSTANCE
			conf_stru c,c_fix;

// 			double energy(struct cella_stru& );
// 			struct cella_stru force(struct cella_stru&, struct base_stru&);
	
			void setSize(int n,int nx,int ny)
			{
				c.p.resize(n);
				c.pfix.resize(n);
				c.pcons.resize(n);
				c.pold.resize(n);
				c.pref.resize(n);

				c.disp.resize(n);
				c.energy_local.resize(n);
				c.gr.resize(n);
				c.m_field_full.resize(n);
				c.bc_cond.resize(n);
				c.grad_umut.resize(n);

				c.reduced.resize(n);
				c.non_reduced.resize(n);
				c.pitteri.resize(n);
				c.reduced_stress.resize(n);
				c.m_field.resize(n);
				c.fo.resize(n);
				c.metric_full.resize(n);
				c.metricr_full.resize(n);
				c.alloying.resize(n);
				c.beta_inhomo.resize(n);
				c.curl.resize(n);
				c.piola_one.resize(n);
				c.cauchy.resize(n);


				c.volume.resize(n);

				c.npunti = n;
				c.energy=0;
				c.grp1.resize(0);
				c.grp1.resize(n);

				c.rfx.resize(n);
				c.rfy.resize(n);
				c.disorder.resize(n);
                                
				//Boundary layers
				c.bottom.resize(nx);
				c.top.resize(nx);
				c.left.resize(ny);
				c.right.resize(ny);
				c.left_per.resize(ny);
				c.right_per.resize(ny);
				c.top_per.resize(nx);
				c.bottom_per.resize(nx);

				c.full.resize(n);
				

				//if not initiated properly, it can cause a bug for c.disorder = 0

				for (int i = 0; i < n; ++i)
				{
                    c.grp1[i].resize(12);
                    c.m_field_full[i].resize(4);
                    c.metric_full[i].resize(4);
                    c.metricr_full[i].resize(4);
                    c.fo[i].resize(4);

                    c.bc_cond[i].resize(6);
                    c.disorder[i].resize(4);
                    c.alloying[i].resize(4);
                    
                }
                
   				for (int i = 0; i < n; ++i){
					c.disorder[i][0]=1.;
					c.disorder[i][1]=1.;
					c.disorder[i][2]=1.;
					c.disorder[i][3]=1.;
				}


             
//                 
//                 //to be understood better later
//                 string s1="A";
//     			c.energy_map[s1] = &energy_zanzotto;
//     			string s2="B";
//     			c.energy_map[s2] = &energy_zanzotto_detc;
// 
//     			c.stress_map[s1] = &stress_easy;
//     			c.stress_map[s2] = &stress_easy_detC;
//     			
//     			//removes volumetric part from calculations
//     			string s3="C";
//     			c.stress_map[s3] = &stress_easy_pure_shear_log;
//     			string s4="D";
//     			c.stress_map[s4] = &stress_easy_pure_shear_detC;
// 

			}



			void forces_with_CG( )
			{
			
					
			
			}			
			
			
	};


}




#endif //


