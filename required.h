#pragma once
#include "common.h"
// #define ARMA_ALLOW_FAKE_CLANG
// #define ARMA_ALLOW_FAKE_GCC
// #define ARMA_DONT_USE_WRAPPER
// #define ARMA_DONT_USE_HDF5
// #define ARMA_DONT_USE_BLAS

#include "structures.h"
#include "atomistic_grid.h"
#include "acoustic_tensor.h"

#include <iostream>
#include <map>


void required();	
