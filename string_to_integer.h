#pragma once
#include <string>
using std::string;
class string_to_integer {
public:
	int vbc;
	int vlattice;
	int x, y;
	void convert(string);
	void convert_lattice(string);
	void convert_new(string, string);
};

void inline string_to_integer::convert(string value1)
{
	if (value1.compare("fix") == 0)
		vbc = 0;
	if (value1.compare("free") == 0)
		vbc = 1;
	if (value1.compare("pbc") == 0)
		vbc = 2;
	if (value1.compare("pbcy") == 0)
		vbc = 3;
	if (value1.compare("pbcyfx") == 0)
		vbc = 4;
	if (value1.compare("pbcyfxsp") == 0)
		vbc = 5;

	// 	return vbc;

}

void inline string_to_integer::convert_lattice(string value1)
{
	if (value1.compare("square") == 0)
		vlattice = 0;
	if (value1.compare("hex") == 0)
		vlattice = 1;
	if (value1.compare("tilted") == 0)
		vlattice = 2;

	// 	return vbc;

}

void inline string_to_integer::convert_new(string value1, string value2)
{
	if (value1.compare("fix") == 0)
		x = 0;
	if (value1.compare("free") == 0)
		x = 1;
	if (value1.compare("pbc") == 0)
		x = 2;

	if (value2.compare("fix") == 0)
		y = 0;
	if (value2.compare("free") == 0)
		y = 1;
	if (value2.compare("pbc") == 0)
		y = 2;

	// 	return vbc;

}