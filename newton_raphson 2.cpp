#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"

#include "calculations_alglib.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include "newton_raphson.h"

#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
using namespace dlib;



void newton_raphson(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point){


	   	 int nx = dimensions::nx;
		 int ny = dimensions::ny;
		 int n=c.p.size();
		 
		 double grad_sum,energy_final,func;
		 void *ptr;
    	 alglib::real_1d_array grad;


    	 std::vector<double> temp_vector;
   		 temp_vector.resize(2*n);
     	 grad.setcontent(temp_vector.size(), &(temp_vector[0]));

			 int mm;
			 if(BLK1::bc_x.compare("fix") ==0)
				 mm=2*(nx-2)*(ny-2);
			 if(BLK1::bc_x.compare("pbc") ==0)
			     mm=2*n;
		
		rosen_alglib(starting_point,func,grad,ptr);
			cout<<"-------theoretical     size: "<<mm<<endl;
// 			cout<<"-------after counting size: "<<2*n_temp<<endl;
	 
		 for(int iter_solver=0; iter_solver<100; iter_solver++ ){
			 // sp_mat squezed(2*n,2*n);   
			 sp_mat squezed_sliced(2*n,2*n); 
    		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;
  
		 	 //construct Hessian
			 squezed_sliced = Hessian_2D(force_energy::calculation::getInstance().c);
			 //apply fix boundaries
			 if(BLK1::bc_x.compare("fix") ==0)
			 	 slicing_filling_global_stifness(squezed_sliced);
    		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;
			 
// 			 double cond_number = arma::cond(squezed);
// 			 cout<<"condition number of the Hessian: "<<cond_number<<endl;
			 
			 arma::vec x_arma(mm);
			 arma::vec b(mm);
			 int n_temp=0;
			 
			 for(int i=0;i<n;i++){
			     int iy = i / nx;
				 int ix = i % nx;
				 if(BLK1::bc_x.compare("fix") ==0)
				 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
				 	continue;

				 b[n_temp]             = -grad[i]  ;
				 b[n_temp+mm/2]        = -grad[i+n];
				 x_arma[n_temp]        = 0.;
				 x_arma[n_temp+mm/2]   = 0.;
				 n_temp++;	 
			 }
			cout<<"-------checking sizes-------"<<endl;
			cout<<"-------theoretical     size: "<<mm<<endl;
			cout<<"-------after counting size: "<<2*n_temp<<endl;


			cout<<"-------solving Hessian type solution-------"<<endl;

			 bool status = spsolve(x_arma,squezed_sliced,b,"superlu");
			 if(status == false) {cout<< "no solution"<<endl;break;}
			 if(status == true) {cout<< " solution exists"<<endl;}
			 
			 n_temp = 0;
			 for(int i=0;i<n;i++)
			 { 
				 
				 int iy = i / nx;
				 int ix = i % nx;
				 if(BLK1::bc_x.compare("fix") ==0)
				 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
				 	continue;

				 
				 double xx =  starting_point[i]    + 0.6*x_arma[n_temp];
				 double yy =  starting_point[i+n]  + 0.6*x_arma[n_temp+mm/2]; 
// 				 cout<< starting_point[i] <<" "<<x_arma[n_temp]<<endl;

				 n_temp++;

				 starting_point[i]   = xx;
				 starting_point[i+n] = yy; 


				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;
			 }
			 
			 
			cout<<"-------checking sizes second time-------"<<endl;
			cout<<"-------theoretical     size: "<<mm<<endl;
			cout<<"-------after counting size: "<<2*n_temp<<endl;

			 
			 //calculate RHS
			 rosen_alglib(starting_point,func,grad,ptr);
			 energy_final = func;
			 grad_sum=0.;
			 for(int i=0;i<n;++i)
				grad_sum += pow(grad[i],2) + pow(grad[i+n],2);
			
			 cout<<"iteration solver: "<<iter_solver<<endl;
			 cout<<"after second order solver epsg ALGLIB: "<<sqrt(grad_sum)<<endl;
			 
			 if(sqrt(grad_sum) < pow(10,-5)){
			 	cout<<"convergence reached at iteration: "<<iter_solver<<endl;
			 	break;	 	
			 }
		}
		
}

void newton_eigen(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point,int t){


	   	 int nx = dimensions::nx;
		 int ny = dimensions::ny;
		 int n=c.p.size();
		 
		 double grad_sum,energy_final,func;
		 void *ptr;
    	 alglib::real_1d_array grad;


    	 std::vector<double> temp_vector;
   		 temp_vector.resize(2*n);
     	 grad.setcontent(temp_vector.size(), &(temp_vector[0]));

		 int mm;
		 if(BLK1::bc_x.compare("fix") ==0)
			 mm=2*(nx-2)*(ny-2);
		 if(BLK1::bc_x.compare("pbc") ==0)
			 mm=2*n;
		
		rosen_alglib(starting_point,func,grad,ptr);
		
		cout<<"-------theoretical     size: "<<mm<<endl;
		sp_mat squezed_sliced(2*n,2*n); 
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;

		 //construct Hessian
		 squezed_sliced = Hessian_2D(force_energy::calculation::getInstance().c);
		 //apply fix boundaries
		 if(BLK1::bc_x.compare("fix") ==0)
			 slicing_filling_global_stifness(squezed_sliced);
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;
		 
		//eigenvalues analysis
		arma::vec eigval;
		arma::mat eigvec;
		cout << "eigenvalues analysis starts "  << endl;


		int req_e=50;
		arma::eigs_sym(eigval, eigvec, squezed_sliced, req_e,"sm");  // find 5 eigenvalues/eigenvectors
// 		eigval.print("eigval:");
		//    eigvec.print("eigvec:");
// 		cout<<"eigvec size"<<eigvec.size()<<endl		;
		string ev="eigenvalues_" + IntToStr(t)+  ".dat";
		string ev2="smallest_eigenvalues.dat";

		for(int i=0;i<eigval.size();++i){
			    fstream filestr;	
				filestr.open (ev, fstream::in | fstream::out | fstream::app);
				filestr << std::scientific << std::setprecision(16)   << " "<<eigval[i] <<endl ;
				filestr.close();
		}	

		fstream filestr;
		filestr.open (ev2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16) << 	c.load  << " "<<eigval.min() <<endl ;
		filestr.close();
		

		
		
}