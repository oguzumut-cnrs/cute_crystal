#pragma once
#include "dynamic_static.h"

#include "structures.h"
#include "calculations_homogenous.h"
#include "delaunay.h"
#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
using namespace dlib;
void initial_perturbation(conf_stru& c);
void dynamic(struct conf_stru& c);
void measurements(struct conf_stru& c);
void statique(struct conf_stru& c);
void homogenous_frame_load(struct conf_stru& c);
void copy_from_small_to_large(const column_vector& starting_point,struct conf_stru& c );
void copy_from_large_to_small(column_vector& starting_point,struct conf_stru& c );
double avalanche_sizes(struct conf_stru& c );

double slip_sizes(struct conf_stru& c );
double avalanche_sizes_half(struct conf_stru& c );
double avalanche_sizes_full(struct conf_stru& c );
double avalanche_sizes_piola(struct conf_stru& c );
double avalanche_sizes_piola_full(struct conf_stru& c );

double def_frad_calcul(struct conf_stru& c );
double update_m(struct conf_stru& c,std::vector <std::vector<matrix_stru> >& m_old );

void measurements_GB(struct conf_stru& c, double theta );

std::vector<double>  edge_dislocation(double dx, double dy);
std::vector<double>  edge_dislocation2(double dx, double dy);


void non_iterative_statique(struct conf_stru& c);