#pragma once
#include "structures.h"
#include "calculations_homogenous.h"
#include "ap.h"
#include "newton_raphson.h"

void initial_perturbation(conf_stru& c);
void dynamic(struct conf_stru& c);
void measurements(struct conf_stru& c);
void statique(struct conf_stru& c);
void statique_alglib(struct conf_stru& c);

void homogenous_frame_load(struct conf_stru& c);
void copy_from_small_to_large(const column_vector& starting_point,struct conf_stru& c );
void copy_from_large_to_small(column_vector& starting_point,struct conf_stru& c );
double avalanche_sizes(struct conf_stru& c );

double slip_sizes(struct conf_stru& c );
double avalanche_sizes_half(struct conf_stru& c );
double avalanche_sizes_full(struct conf_stru& c );
double avalanche_sizes_piola(struct conf_stru& c );
double avalanche_sizes_piola_full(struct conf_stru& c );

double def_frad_calcul(struct conf_stru& c );
double update_m(struct conf_stru& c,std::vector <std::vector<matrix_stru> >& m_old );

void measurements_GB(struct conf_stru& c, double theta );

void data_transfer_fixb_back(std::vector<punto_stru>& output, const alglib::real_1d_array& sp);
void data_transfer_fixb_forw(std::vector<punto_stru>& input,alglib::real_1d_array& output);
double dphidalpha(struct conf_stru& c, bool on_off  );
bool exist(const std::string& name);

#include "delaunay.h"
#include "second.h"
