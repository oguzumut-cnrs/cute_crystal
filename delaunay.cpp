#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <ctime>
#include "delaunay.h"
//#include "./delaunator-cpp/include/delaunator.hpp"
//#include <cstdio>
#include <Fade_2D.h>
#include <stdio.h>

using namespace GEOM_FADE2D;

bool getVoronoiCell(Point2* pVtx,std::vector<Point2>& vVoronoiVerticesOut)
{
	TriangleAroundVertexIterator start_it(pVtx);
	TriangleAroundVertexIterator end_it=start_it;
	do
	{
		if(*start_it==NULL) // Infinite cell
		{
			vVoronoiVerticesOut.clear();
			return false;
		}
		vVoronoiVerticesOut.push_back((*start_it)->getDual().first);
	} while(++start_it!=end_it);
	return true;
}


bool isEven(int i) 
{ 
    if (i  != 0) 
        return true; 
    else
        return false; 
} 
  


void after_delaunay(struct conf_stru& c,int t, int* triangle_node, 
				    int triangle_num, int triangle_order);

void print_output_ovito_del(struct conf_stru& c,int t,
	  std::vector<double>&  defects);
void read_from_a_ovito_file(struct conf_stru& c,int t);


void do_delaunay(struct conf_stru& c,int t,double theta, double load)
{


      	int nx = dimensions::nx;
      	int ny = dimensions::ny;


// 	read_from_a_ovito_file(c,t);

    remove_macro_def(c);
    


	// Some input points
	std::vector<Point2> vInputPoints;
	

	// Create and insert 4 points
	for (int i=0;i<c.p.size();++i)
		 vInputPoints.push_back(Point2(c.rfx[i],c.rfy[i]));
	    
	for(size_t i=0;i<vInputPoints.size();++i)
    {
        vInputPoints[i].setCustomIndex(i);
    }


	Fade_2D dt; // The Delaunay triangulation
	std::vector<Point2*> vVertexPointersBack; // Used to store the pointers

	// All-at-once method (returned pointers have the same order as in vInputPoints)
	dt.insert(vInputPoints,vVertexPointersBack);


	// *** Goal: Access and draw elements of the triangulation
// 	dt.show("./dir_triangles/triangulation" + IntToStr(t)  +   ".ps");
	string filename  =  "./dir_triangles/triangulation" + IntToStr(t)  +   ".ps" ;
	char* name = new char[filename.length() + 1];
	strcpy(name, filename.c_str());

	Visualizer2 vis(name);
	dt.show(&vis,false);
	std::vector<Point2*> vAllPoints;
	dt.getVertexPointers(vAllPoints);
	std::cout<<"vAllPoints.size()="<<vAllPoints.size()<<std::endl;
 	std::vector<double>  defects;
 	defects.resize(c.p.size());


	for(std::vector<Point2*>::iterator it( vAllPoints.begin());it!=vAllPoints.end();++it)
	{
		Point2* pVtx(*it);
		std::vector<Point2> vVoronoiVertices;
		int a = pVtx->getCustomIndex();
		bool bFiniteCell(getVoronoiCell(pVtx,vVoronoiVertices));
		defects[a] = 0 ;
		if(vVoronoiVertices.size() ==5 || vVoronoiVertices.size() ==7){
		//std::cout<<"vVoronoiVertices.size()="<<vVoronoiVertices.size()<<std::endl;
		//std::cout<<"vVoronoiVertices.it="<< a<<std::endl;
		 int iy = a / nx;
         int ix = a % nx;

			if(ix > 1 && ix<nx-2 && iy > 1 && iy<ny-2){
				if(vVoronoiVertices.size() ==5 )defects[a] = 1.;
				if(vVoronoiVertices.size() ==7 )defects[a] = -1.;
			}
		
		}
	}


       	

	//   		 int num_items3 = std::count_if(defects.begin(), defects.end(), [](int i){return defects[i]  == 0;});
    int noEven = count_if(defects.begin(), defects.end(), 
                                     isEven); 

   		 std::cout << "number defects: " << noEven << '\n';
     	
		string dislo_number;
		dislo_number="dislo_number.dat" ;

		fstream filestr_dislo;
		filestr_dislo.open (dislo_number, fstream::in | fstream::out | fstream::app);
		filestr_dislo << std::scientific << std::setprecision(7)   <<" "<<1.0*noEven/2.<<endl ;
		filestr_dislo.close();


	   	
       	
		print_output_ovito_del(c,t,defects);  	
       	


}









void print_output_ovito_del(struct conf_stru& c,int t,
	  std::vector<double>&  defects)
{


	string filename;

	filename="./dir_del/for_ovito_" + IntToStr(t)+".xyz" ;

	ofstream filestr;
	filestr.open(filename.c_str());
	
	filestr << c.p.size()<<endl;	
	filestr << " "<<endl;	

	for(int i=0;i<c.p.size();i++)
	{

		filestr << std::scientific << std::setprecision(7)
				<<c.p[i].x<<" "<<c.p[i].y<<" "<<defects[i]<< endl ;
	}
	filestr.close();
	
	string filename2;

	filename2="./dir_del/for_ovito_short_" + IntToStr(t)+".xyz" ;

	ofstream filestr2;
	filestr2.open(filename2.c_str());
	int counter=0;
	for(int i=0;i<c.p.size();i++){
		if(defects[i] !=0)
			counter++;
	}
	
	filestr2 << counter<<endl;	
	filestr2 << " "<<endl;	

	for(int i=0;i<c.p.size();i++)
	{
		if(defects[i] !=0)
		filestr2 << std::scientific << std::setprecision(7)
				<<c.p[i].x<<" "<<c.p[i].y<<" "<<defects[i]<< endl ;
	}
	
	filestr2.close();	
	
	
	string dislo_number;
	dislo_number="dislo_number.dat" ;

	fstream filestr_dislo;
	filestr_dislo.open (dislo_number, fstream::in | fstream::out | fstream::app);
	filestr_dislo << std::scientific << std::setprecision(7)   <<" "<<1.0*counter/2.<<endl ;
	filestr_dislo.close();
	
	

}

void read_from_a_ovito_file(struct conf_stru& c,int& t)
{
	int nx = nx;
	int ny = ny;
	int n = nx*ny;

   ifstream disp;
   string filename2;
   filename2="./dir_affine/for_ovito_" + IntToStr(t)+".xyz" ;

   disp.open(filename2.c_str());
   int temp;
   double temp2,temp3,temp4,temp5;

   disp >>temp;
   cout << "line 1 = "<<temp<<endl;
//    disp >>temp2;
//    cout << "line 1 = "<<temp2<<endl;

   
   for(int i=0;i<n;++i)
   {
   		disp >>temp >> c.rfx[i] >> c.rfy[i] >> temp2 >> temp3 >> temp4 >> temp5;
//    		cout <<temp << c.rfx[i] << c.rfy[i] << temp2 << temp3 << temp4 << temp5 << endl;
   }
   disp.close();

}

