
#include "rotation_roberta.h"
#include "result_memorize.h"

void result_memorize(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	bset.convert(BC);
	
	c.energy = 0.;
	int n=c.p.size();
	int counter;

	double theta=BLK1::theta;

	boundary_conditions set1;
	set1.setParameters(theta,c.load);
	choose_bc(set1);
	cout<<"set1.f.f11 "<<set1.f.f11<<endl;
	cout<<"set1.f.f22 "<<set1.f.f22<<endl;
	cout<<"set1.f.f12 "<<set1.f.f12<<endl;
	cout<<"set1.f.f21 "<<set1.f.f21<<endl;


	
		for (int i=0; i<c.p.size(); i++){
			c.energy_local[i] =0. ;
			c.reduced[i].c11 = 0. ;
			c.reduced[i].c12 = 0. ;		
			c.reduced[i].c22 = 0. ;		
			c.non_reduced[i].c11 = 0. ;
			c.non_reduced[i].c12 = 0. ;		
			c.non_reduced[i].c22 = 0. ;		
			c.pitteri[i].c11 = 0. ;
			c.pitteri[i].c12 = 0. ;		
			c.pitteri[i].c22 = 0. ;		
			c.cauchy[i].f11 = 0. ;
			c.cauchy[i].f12 = 0. ;		
			c.cauchy[i].f22 = 0. ;		

			c.piola_one[i].f11 = 0. ;
			c.piola_one[i].f12 = 0. ;		
			c.piola_one[i].f22 = 0. ;		
			c.piola_one[i].f21 = 0. ;		

			c.volume[i] = pow(BLK1::scale,-4.) ;		
		}


	def_grad temp;
	temp= set1.f.inverse(set1.f);
// 	temp= set1.f;
	struct matrix_stru temp_load;
	temp_load.m11 = temp.f11;
	temp_load.m22 = temp.f22;
	temp_load.m12 = temp.f12;
	temp_load.m21 = temp.f21;
	cout<<"inverse set1.f.f11 "<<temp.f11<<endl;
	cout<<"inverse set1.f.f22 "<<temp.f22<<endl;
	cout<<"inverse set1.f.f12 "<<temp.f12<<endl;
	cout<<"inverse set1.f.f21 "<<temp.f21<<endl;

	remove_macro_def(c);

	//k -> loop on the triangles
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 	{
	
			c.t1 = force_energy::calculation::getInstance().c.j1[k];
			c.t2 = force_energy::calculation::getInstance().c.j2[k];




	#pragma omp parallel
	#pragma omp for
		for (int i=0; i<c.p.size(); i++) {
			int iy = i / nx;
			int ix = i % nx;

			
			int ii,jj,ne;
            struct punto_stru p = c.p[i];
            struct punto_stru p2,p3;
			struct de_stru d,d2,d3,d1,dtemp;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;
			double rot_angle = 0.;

			double burgers,energy_thread;
			struct cella_stru metrics;
			struct matrix_stru m,def_grad;
			struct cella_stru metric_rr ;
			double sqdetc,detf;
			struct cella_stru metric_prr,stresses;
			struct base_stru vecr;
			struct matrix_stru m2,m3,fp2,fp3,curl,dumb,fp;
			struct base_stru vecr2,vecr3,vecrtemp,vtemp;
			struct base_stru v,v2,v3,ref;
			struct base_stru elastic_fundemental;
			struct matrix_stru f_elastic;
			struct energy_stress es;
			double h=dimensions::dh;
			struct matrix_stru aa;
// 			struct def_grad temp;

			flag1=0;
			flag2=0;
// 		    if(k == 0 || (iy==ny-1 && k==4) || (ix == nx-1 && k==1)){		

			//no triangle
			if(!check(c.bc_cond[i][k],2))
				goto label1;				

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
			d.p[0] = p;
// 			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) && BLK1::grid_creator.compare("cutting") ==0)	
			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
				goto label1;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];
//---------------------------------BOUNDARY CONDITIONS------------------------------------


 		    c.fptr(i,ix,iy,k,d,c);


			v.e1[0]= c.t1*(d.p[1].x-d.p[0].x)/h ;
			v.e1[1]= c.t1*(d.p[1].y-d.p[0].y)/h ;
			v.e2[0]= c.t2*(d.p[2].x-d.p[0].x)/h ;
			v.e2[1]= c.t2*(d.p[2].y-d.p[0].y)/h ;	
		
		
			//REAL METRICS
			metrics = faicella(v);
			c.metric_full[i][k] = metrics;

            //REDUCED VECTOR VECR; NON REDUCED V
            vecr=riduci_vectors(v);

 
//  		rot_angle= example(vecrtemp.matrix_form());
			//FIND INTEGER MATRIX
			m=find_integer_matrix(v,vecr);

			def_grad.m11 = v.e1[0] ;//d.p[0].x - c.rfx[i];//0.*set1.f.f11 ;
			def_grad.m22 = v.e2[1] ;//d.p[0].y - c.rfy[i];// 0.*set1.f.f22;
			def_grad.m12 = v.e2[0] ;
			def_grad.m21 = v.e1[1] ;

			c.m_field_full[i][k] = def_grad;


		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);
		    c.metricr_full[i][k] = metric_rr;

            //PITTERI REDUCED METRIC
// 			metric_prr = riduci_pitteri(metrics,flag1,flag2);	    
		    sqdetc = metric_rr.c11*metric_rr.c22- metric_rr.c12*metric_rr.c12;
		    sqdetc =sqrt(sqdetc);

			es =  c.fptr2(metric_rr,c.disorder[i][k],c.alloying[i][k],vecr);
			energy_thread = es.energy;
// 			stresses      =  c.fptr3(metric_rr,c.disorder[i],c.alloying[i]);

			stresses = es.r_stress;
				

			c.energy_local[i] += energy_thread;

		    //calculate only in the existing triangles


			detf =    (v.e1[0]*v.e2[1] - v.e2[0]*v.e1[1]) ;


					
				label2: ;
			    if(k==0){
					c.reduced[i].c11 = metric_rr.c11/sqdetc;
					c.reduced[i].c22 = metric_rr.c22/sqdetc;
					c.reduced[i].c12 = metric_rr.c12/sqdetc;
				
					c.non_reduced[i].c11 = metrics.c11/sqdetc;
					c.non_reduced[i].c22 = metrics.c22/sqdetc;
					c.non_reduced[i].c12 = metrics.c12/sqdetc;		
				
				
					matrix_stru vm = v.matrix_form();
					c.volume[i] += 	sqdetc;
				}		
					d1= cauchy(metric_rr,v,m,c.disorder[i][k],c.alloying[i][k]);

					c.cauchy[i].f12  += (d1.reduced_stress.f12);
					c.cauchy[i].f11  += (d1.reduced_stress.f11);
					c.cauchy[i].f22  += (d1.reduced_stress.f22);
					d1= piola_one(metric_rr,v,m,c.disorder[i][k],c.alloying[i][k]);
					c.piola_one[i].f12  += d1.reduced_stress.f12*c.t1;
					c.piola_one[i].f11  += d1.reduced_stress.f11*c.t1;
					c.piola_one[i].f22  += d1.reduced_stress.f22*c.t1;
					c.piola_one[i].f21  += d1.reduced_stress.f21*c.t1;



				if(k==0){
				
					d1= cauchy(metric_rr,v,m,c.disorder[i][k],c.alloying[i][k]);
// 					c.cauchy[i].f12  = (d1.reduced_stress.f12);
// 					c.cauchy[i].f11  = (d1.reduced_stress.f11);
// 					c.cauchy[i].f22  = (d1.reduced_stress.f22);

					c.pitteri[i].c11 = d1.reduced_stress.f11;
					c.pitteri[i].c22 = d1.reduced_stress.f22;
					c.pitteri[i].c12 = d1.reduced_stress.f12;	
					
				}
			    
				label1: ;
			
			
			
			
		}
		
		
	
		
			

	//}

//}

}

}
void remove_macro_def(struct conf_stru& c){

	
	double theta=BLK1::theta;

	boundary_conditions set1;
	set1.setParameters(theta,c.load);
	choose_bc(set1);
	
	def_grad temp;
	temp= set1.f.inverse(set1.f);
// 	temp= set1.f;
	

// 
	//this is the displacement with respect to the current deformation
// 	temp= set1.f;
	for (int i=0;i<c.p.size();++i){
// 		 c.rfx[i] = c.p[i].x - (c.pcons[i].x * temp.f11 + c.pcons[i].y * temp.f12);				
// 		 c.rfy[i] = c.p[i].y - (c.pcons[i].x * temp.f21 + c.pcons[i].y * temp.f22);
		 c.rfx[i] = (c.p[i].x * temp.f11 + c.p[i].y * temp.f12);				
		 c.rfy[i] = (c.p[i].x * temp.f21 + c.p[i].y * temp.f22);

// 	
	}
// 
// 	for (int i=0;i<c.p.size();++i){
// 		 c.p[i].x -= c.rfx[i];				
// 		 c.p[i].y -= c.rfy[i];
// 	
// 	}
	
}

// void result_memorize(struct conf_stru& c)
// {// 
// 	int nx = dimensions::nx;
// 	int ny = dimensions::ny;
// 
// 	int npunti = c.p.size();
// 	struct cella_stru lengths;
// 	double dny = ny;
// 	double dnx = nx;
// 	double dix,diy,en,sum;
// 	int a,b;
// 	double load=BLK1::load;
// 	int flag1 = 0;
// 	int flag2 = 0;
// 	string_to_integer bset;
// 	string  BC  = BLK1::BC;
// 	bset.convert(BC);
// 	
// 	c.energy = 0.;
// 	
// 	for (int i=0; i<npunti; ++i)
// 	{
// 		c.energy_local[i] =0.;
// 		
// 	}
// 
// 	int tt=6;
// 	
// 
// 	//k -> loop on the triangles
// 	for (int k=0; k<4;k=k+2) 
// 	{
// 		
// 		//this is the loop on triangle vertices			
// 		for (int i=0; i<npunti; ++i) 
// 		{
// 			int l=2*k;
// 			int iy = i / nx;
// 			int ix = i % nx;
//             struct punto_stru p = c.p[i];
// 			struct de_stru d,d2,d3;
// 			double diy=iy;
// 			double dix=ix;
// 			double dny = ny;
// 			double dnx = nx;
// 			double burgers,energy_thread;
// 			struct cella_stru metrics;
// 			struct matrix_stru m,m2,m3,fp2,fp3;
// 			struct cella_stru metric_rr ;
// 			double sqdetc;
// 			struct cella_stru metric_prr;
// 			struct base_stru vecr,vecr2,vecr3;
// //---------------------------------CONSTRUCT THE TRIANGLE------------------------------------
// 			d.p[0] = p;
// 			if(p.nn[k] == -1 || p.nn[k+1] == -1 ) 	
// 				goto label1;
// 
// 			d.p[1] = c.p[p.nn[k]];
// 			d.p[2] = c.p[p.nn[k+1]];
// 
// 
// 
// 
// //---------------------------------BOUNDARY CONDITIONS------------------------------------
// 			if(!check(c.bc_cond[i][k],2))	
// 				goto label1;
// 
// // 			per_BC(ix,iy,k,d,c);
// 
// //---------------------------------BOUNDARY CONDITIONS------------------------------------
// 
// // 
// // 			//REAL BASIS VECTORS
// 			struct base_stru v;
// // 			//F11
// 			v.e1[0]=d.p[1].x-d.p[0].x;
// 			//F21
// 			v.e1[1]=d.p[1].y-d.p[0].y;
// 			//F12
// 			v.e2[0]=d.p[2].x-d.p[0].x;
// 			//F22
// 			v.e2[1]=d.p[2].y-d.p[0].y;
// // 
// // 			//REAL METRICS
// 			 metrics = faicella(v);
// // //             struct cella_stru metrics = faicella2(p,d.p[1],d.p[2]);
// // 
// //             //REDUCED VECTOR VECR; NON REDUCED V
// // 
//             vecr=riduci_vectors(v);
// // //             struct base_stru vecr= v ;
// // 
// // 			//FIND INTEGER MATRIX
// 			m=find_integer_matrix(v,vecr);
// 			c.m_field_full[i][k].m11 = m.m11;
// 			c.m_field_full[i][k].m22 = m.m22;
// 			c.m_field_full[i][k].m12 = m.m12;
// 			c.m_field_full[i][k].m21 = m.m21;
// // 
// // 		    //REDUCED METRIC
// 		    metric_rr  = faicella(vecr);
// //             //PITTERI REDUCED METRIC
// //             metric_prr = riduci_pitteri(metrics,flag1,flag2);	    
// 		    sqdetc = metric_rr.c11*metric_rr.c22
// 		    			  - metric_rr.c12*metric_rr.c12;
// 		    sqdetc =sqrt(sqdetc);
// // 
// // 
// // 			//burgers = 1 if there is no precipitate
// 			burgers  = 1;
// 			c.alloying[i]=-0.25;
// // 			energy_thread =c.energy_map[BLK1::first](metric_rr,burgers,-0.25);
// 				energy_thread = energy_zanzotto_detc(metric_rr,burgers,c.alloying[i]);		
// 
// 				c.energy_local[i] += energy_thread/4.;
// 			 if(energy_thread > 1)
// 				 std::cout<<i<<	" " <<energy_thread<<std::endl;
// 
// 		    //calculate only in the bulk
// // 		    if(k == 0 && c.p[i].is_boundary != 1)
// 		    if(k == 0)
// 
// 		    {
// 
// // 				double detf =    (v.e1[0]*v.e2[1] - v.e2[0]*v.e1[1]) ;
// // 				
// // 				
// // 				//south friend
// // 				struct punto_stru p2 = c.p[i];
// // 				//west friend
// // 				struct punto_stru p3 = c.p[i];
// // 
// // 				// north friend
// // 				d2.p[0] = p2;
// // 				if(p2.nn[k] == -1 || p2.nn[k+1] == -1 ) 	
// // 				goto label2;
// // 
// // 				d2.p[1] = c.p[p2.nn[2]];
// // 				d2.p[2] = c.p[p2.nn[3]];
// // 
// // 				// east friend
// // 				d3.p[0] = p3;
// // 			    if(p3.nn[k] == -1 || p3.nn[k+1] == -1 ) 	
// // 				goto label2;
// // 
// // 				d3.p[1] = c.p[p3.nn[1]];
// // 				d3.p[2] = c.p[p3.nn[2]];
// // 
// // // 			//F11
// // 	// 			v.e1[0]=d.p[1].x-d.p[0].x;
// // 	// 			//F21
// // 	// 			v.e1[1]=d.p[1].y-d.p[0].y;
// // 	// 			//F12
// // 	// 			v.e2[0]=d.p[2].x-d.p[0].x;
// // 	// 			//F22
// // 	// 			v.e2[1]=d.p[2].y-d.p[0].y;
// // 
// // 				
// // 				//F11 NORTH
// // 				struct base_stru v2;
// // 				v2.e1[0]=d2.p[1].x-d2.p[0].x;
// // 				//F21 NORTH
// // 				v2.e1[1]=d2.p[1].y-d2.p[0].y;
// // 				v2.e2[0]=d2.p[2].x-d2.p[0].x;
// // 				v2.e2[1]=d2.p[2].y-d2.p[0].y;
// //                 vecr2=riduci_vectors(v2);
// //      			m2=find_integer_matrix(v2,vecr2);
// //      			fp2=m2.inverse(m2).transpose();
// //      			
// // 
// // 
// // 				struct base_stru v3;
// // 				//F12 EAST
// // 				v3.e2[0]=d3.p[2].x-d3.p[0].x;
// // 				//F22 EAST
// // 				v3.e2[1]=d3.p[2].y-d3.p[0].y;
// // 
// // 				v3.e1[0]=d3.p[1].x-d3.p[0].x;
// // 				//F21
// // 				v3.e1[1]=d3.p[1].y-d3.p[0].y;
// //                 vecr3=riduci_vectors(v3);
// //      			m3=find_integer_matrix(v3,vecr3);
// //      			fp3=m3.inverse(m3).transpose();
// //      			std::cout<<"self"<<std::endl;
// //      			m3.print();
// //      			std::cout<<"inverse"<<std::endl;
// //      			m3.inverse(m3).print();
// //        			std::cout<<"inverse transpose"<<std::endl;
// //      			m3.inverse(m3).transpose().print();
// // 				std::cout<<"*************************************************************"<<std::endl;
// 
// 
// 				//F11,y and F12,x
// // 				c.curl[i].f21 = -(-m.inverse(m).transpose().m11  + fp2.m11 ) + (-m.inverse(m).transpose().m12  + fp3.m12 );				
// // 				//F21,y and F22,x
// // 
// // 				c.curl[i].f22 = -(-m.inverse(m).transpose().m21  + fp2.m21 ) + (-m.inverse(m).transpose().m22  + fp3.m22 );				
// // 				
// // 				std::cout<<c.curl[i].f22<<" "<<detf<<  " " <<v2.e1[0]<<" "<<v.e1[0]<<std::endl;
// // 				std::cout<<c.curl[i].f22<<" "<<detf<<  " " <<v3.e2[0]<<" "<<v.e2[0]<<std::endl;
// // 				
// // 
// // 				std::cout<<c.curl[i].f22<<" "<<detf<<  " " <<v2.e1[0]-v.e1[0]<<std::endl;
// // 				std::cout<<c.curl[i].f22<<" "<<detf<<  " " <<v3.e2[0]-v.e2[0]<<std::endl;
// // 				std::cout<<"*************************************************************"<<std::endl;
// // 
// // 				std::cout<<c.curl[i].f21<<" "<<detf<<  " " <<v2.e1[1]<<" "<<v.e1[1]<<std::endl;
// // 				std::cout<<c.curl[i].f21<<" "<<detf<<  " " <<v3.e2[1]<<" "<<v.e2[1]<<std::endl;
// // 				
// // 
// // 				std::cout<<c.curl[i].f21<<" "<<detf<<  " " <<v2.e1[1]-v.e1[1]<<std::endl;
// // 				std::cout<<c.curl[i].f21<<" "<<detf<<  " " <<v3.e2[1]-v.e2[1]<<std::endl;
// // 
// // // 				
// // 				std::cout<<"-----------------------------------------------------------------"<<std::endl;
// 
// 
// 
// 
// 
// // 				label2: ;
// // 				c.reduced[i].c11 = c.curl[i].f21 ;
// // 				c.reduced[i].c12 = c.curl[i].f22 ;
// 
// 				
// // 				c.reduced[i].c11 = metric_rr.c11/sqdetc;
// // 				c.reduced[i].c22 = metric_rr.c22/sqdetc;
// // 				c.reduced[i].c12 = metric_rr.c12/sqdetc;
// // 				c.non_reduced[i].c11 = metrics.c11/sqdetc;
// // 				c.non_reduced[i].c22 = metrics.c22/sqdetc;
// // 				c.non_reduced[i].c12 = metrics.c12/sqdetc;		
// // 				c.pitteri[i].c11 = metric_prr.c11/sqdetc;
// // 				c.pitteri[i].c22 = metric_prr.c22/sqdetc;
// // 				c.pitteri[i].c12 = metric_prr.c12/sqdetc;		
// // 				c.volume[i] = 	metrics.c11*metrics.c22-metrics.c12*metrics.c12	;		
// // 				struct de_stru d1= stress_zanzotto_visual(metric_rr,v,m,burgers,c.alloying[i]);
// // 				c.reduced_stress[i].f12  = (d1.reduced_stress.f11*v.e1[1]+d1.reduced_stress.f12*v.e2[1])/detf;
// // 				c.reduced_stress[i].f11  = (d1.reduced_stress.f11*v.e1[0]+d1.reduced_stress.f12*v.e2[0])/detf;
// // 				c.reduced_stress[i].f22  = (d1.reduced_stress.f21*v.e1[1]+d1.reduced_stress.f22*v.e2[1])/detf;
// 	
// 			}
// 
// 			
// 			
// 			label1: ;
// 			
// 			
// 		}
// // 		);
// // 		}
// 	}
// 
// 
// 
// }
