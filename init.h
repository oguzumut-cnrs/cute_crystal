#pragma once
#include "dlib.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "structures.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "custom_inline_functions.h"
#include "periodicity.h"
#include <iostream>
#include <map>


std::pair<double, double> get_XY_for_rotated_hex(int ix, int iy, double angle);
// 

void new_init_rotated_hex(struct conf_stru& c, double angle) ;

void init_rotated_square(struct conf_stru& c, double angle);
void new_init_rotated_square(struct conf_stru& c, double angle);
void new_init_rotated_hex(struct conf_stru& c, double angle);
// std::pair<double, double> get_XY_for_rotated_hex(int i, int j);
void init(struct conf_stru& c);
void initHex6(struct conf_stru& c);
void assign_bc(struct conf_stru& c);
void assign_bc_MJ(struct conf_stru& c);
void new_init_tilted_square(struct conf_stru& c);
int toIdx1D(int x, int y, int rowWidth) ;
void assign_bc_hex_MJ(struct conf_stru& c);
void test_knn2(struct conf_stru& c ,int nn);