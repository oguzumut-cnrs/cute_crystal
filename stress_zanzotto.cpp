#include "stress_zanzotto.h"

struct de_stru  stress_zanzotto(const struct cella_stru& dtemp,
	const struct base_stru& v1, const struct matrix_stru& m,const struct matrix_stru& Q)
	//struct cella_stru& c MUST be the reduced one
{

//does not use burgers or alloys; they are already calculated in the evolution of stress

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);


	struct de_stru  d;
	double a = force_energy::calculation::getInstance().c.t1;
	double b = force_energy::calculation::getInstance().c.t2;
	double dnx = dimensions::nx;
	double h=1.;
	double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;
	double soft = force_energy::calculation::getInstance().c.soft;
	double sum_F12 = force_energy::calculation::getInstance().c.sum_F12;
	double load = force_energy::calculation::getInstance().c.load;
		
	struct matrix_stru P;
	//phi,11
	P.m11 = d.gr[1].x =  (temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0])*a;
	//phi,21
	P.m21 = d.gr[1].y =  (temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1])*a;
	
	//phi,12
	P.m12 = d.gr[2].x =  (temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0])*b;
	//phi,22
	P.m22 = d.gr[2].y =  (temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1])*b;

	

// 	double angle = burgers*dlib::pi/180;
// 	d.gr[1].x = P11*cos(angle) - P21*sin(angle);
// 	d.gr[2].x = P12*cos(angle) - P22*sin(angle);
// 	d.gr[1].y = P21*cos(angle) + P11*sin(angle);
// 	d.gr[2].y = P22*cos(angle) + P12*sin(angle);

// 
// 	P = rotation_nonsym(P,Q);
// 	d.gr[1].x = P.m11;
// 	d.gr[1].y = P.m21;
// 	d.gr[2].x = P.m12;
// 	d.gr[2].y = P.m22;
// 		
		

	//forces      
    d.gr[0].x = -d.gr[1].x - d.gr[2].x;
 	d.gr[0].y = -d.gr[1].y - d.gr[2].y;

//  	cout<<"f_i: "<<d.gr[0].x<<endl;
//  	cout<<"f_j: "<<d.gr[0].y<<endl;
// 	cout<<"------"<<endl;


	return d;



}

struct de_stru  stress_UL(const struct cella_stru& dtemp,
	const struct base_stru& v1, const struct matrix_stru& m,const struct matrix_stru& ftoti)
	//struct cella_stru& c MUST be the reduced one
{

//does not use burgers or alloys; they are already calculated in the evolution of stress

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);


	struct de_stru  d,d1,kirchhoff;
	double a = force_energy::calculation::getInstance().c.t1;
	double b = force_energy::calculation::getInstance().c.t2;
	double dnx = dimensions::nx;
	double h=1.;
		
	struct matrix_stru P;
	//phi,11
	P.m11 = d1.gr[1].x =  (temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0]);
	//phi,21
	P.m21 = d1.gr[1].y =  (temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1]);
	
	//phi,12
	P.m12 = d1.gr[2].x =  (temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0]);
	//phi,22
	P.m22 = d1.gr[2].y =  (temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1]);

	
    //det F_2
    double detfc =    (v1.e1[0]*v1.e2[1] - v1.e2[0]*v1.e1[1]) ;
	d1.reduced_stress.f11 = d1.gr[1].x;
	d1.reduced_stress.f12 = d1.gr[2].x;
	d1.reduced_stress.f21 = d1.gr[1].y;
	d1.reduced_stress.f22 = d1.gr[2].y;
	
	//Kirchhoff current
	kirchhoff.reduced_stress.f12  = (d1.reduced_stress.f11*v1.e1[1]+d1.reduced_stress.f12*v1.e2[1]);
	kirchhoff.reduced_stress.f11  = (d1.reduced_stress.f11*v1.e1[0]+d1.reduced_stress.f12*v1.e2[0]);
	kirchhoff.reduced_stress.f22  = (d1.reduced_stress.f21*v1.e1[1]+d1.reduced_stress.f22*v1.e2[1]);

	//tau*grad N
	d.gr[1].x  =  (kirchhoff.reduced_stress.f11*ftoti.m11 + kirchhoff.reduced_stress.f12*ftoti.m12)*a ;
	d.gr[1].y  =  (kirchhoff.reduced_stress.f12*ftoti.m11 + kirchhoff.reduced_stress.f22*ftoti.m12)*a ;
	

	d.gr[2].x  =  (kirchhoff.reduced_stress.f11*ftoti.m21 + kirchhoff.reduced_stress.f12*ftoti.m22)*a ;	
	d.gr[2].y  =  (kirchhoff.reduced_stress.f12*ftoti.m21 + kirchhoff.reduced_stress.f22*ftoti.m22)*a ;


	//forces   on node centrale    
    d.gr[0].x = -d.gr[1].x - d.gr[2].x;
 	d.gr[0].y = -d.gr[1].y - d.gr[2].y;
 	
//  	cout<<"f_i: "<<d.gr[0].x<<endl;
//  	cout<<"f_j: "<<d.gr[0].y<<endl;
// 	cout<<"------"<<endl;


	return d;



}


struct de_stru  piola_one(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

// 	dtemp= stress_easy (c,burgers,alloy);

	struct energy_stress temps = force_energy::calculation::getInstance().c.fptr2(c,burgers,alloy,v1);	
    dtemp=temps.r_stress;

// 	struct cella_stru temp2 = dtemp;
	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);


	struct de_stru  d,dt;
	double h=dimensions::dh;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
	//phi,22
	d.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;


	double P11 = d.gr[1].x;
	double P12 = d.gr[2].x;
	double P21 = d.gr[1].y;
	double P22 = d.gr[2].y;
	
	
	d.reduced_stress.f11 = P11;
	d.reduced_stress.f12 = P12;
	d.reduced_stress.f21 = P21;
	d.reduced_stress.f22 = P22;

	return d;
}



struct de_stru  cauchy(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

// 	dtemp= stress_easy (c,burgers,alloy);

	struct energy_stress temps = force_energy::calculation::getInstance().c.fptr2(c,burgers,alloy,v1);	
    dtemp=temps.r_stress;

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);

	struct de_stru  d1,cauchy;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d1.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d1.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d1.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0]  ;
	//phi,22
	d1.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;

    double detf =    (v1.e1[0]*v1.e2[1] - v1.e2[0]*v1.e1[1]) ;
	d1.reduced_stress.f11 = d1.gr[1].x;
	d1.reduced_stress.f12 = d1.gr[2].x;
	d1.reduced_stress.f21 = d1.gr[1].y;
	d1.reduced_stress.f22 = d1.gr[2].y;
	
	cauchy.reduced_stress.f12  = (d1.reduced_stress.f11*v1.e1[1]+d1.reduced_stress.f12*v1.e2[1])/detf;
	cauchy.reduced_stress.f11  = (d1.reduced_stress.f11*v1.e1[0]+d1.reduced_stress.f12*v1.e2[0])/detf;
	cauchy.reduced_stress.f22  = (d1.reduced_stress.f21*v1.e1[1]+d1.reduced_stress.f22*v1.e2[1])/detf;


	return cauchy;
}



struct de_stru  second_piola(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

// 	dtemp= stress_easy (c,burgers,alloy);
	struct energy_stress temps = force_energy::calculation::getInstance().c.fptr2(c,burgers,alloy,v1);	
    dtemp=temps.r_stress;

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);

	struct de_stru  d1,pk2;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d1.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d1.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d1.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
	//phi,22
	d1.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;

    double detf =    (v1.e1[0]*v1.e2[1] - v1.e2[0]*v1.e1[1]) ;
	d1.reduced_stress.f11 = d1.gr[1].x;
	d1.reduced_stress.f12 = d1.gr[2].x;
	d1.reduced_stress.f21 = d1.gr[1].y;
	d1.reduced_stress.f22 = d1.gr[2].y;
	double f11,f22,f21,f12;
	f11 = v1.e1[0];
	f22 = v1.e2[1];
	f12 = v1.e2[0];
	f21  =v1.e1[1];
	

	pk2.reduced_stress.f11  =  (d1.reduced_stress.f11*f22  -  d1.reduced_stress.f21*f12)/detf;
	pk2.reduced_stress.f22  = -(d1.reduced_stress.f12*f21  -  d1.reduced_stress.f22*f11)/detf;
	
	pk2.reduced_stress.f12  =  (d1.reduced_stress.f12*f22  -  d1.reduced_stress.f22*f12)/detf;
	pk2.reduced_stress.f21  = -(d1.reduced_stress.f11*f21  -  d1.reduced_stress.f21*f11)/detf;


	return pk2;
}