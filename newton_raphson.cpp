#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"

#include "calculations_alglib.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include "newton_raphson.h"

#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
using namespace dlib;




void newton_raphson(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point){


	   	 int nx = dimensions::nx;
		 int ny = dimensions::ny;
		 int n=c.p.size();
		 
		 double grad_sum,energy_final,func;
		 void *ptr;
    	 alglib::real_1d_array grad;


    	 std::vector<double> temp_vector;
   		 temp_vector.resize(2*n);
     	 grad.setcontent(temp_vector.size(), &(temp_vector[0]));



// 			 for(int i=0;i<n;i++){ 
// 				 
// 
// 				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
// 				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;
// 			}



			 int mm;
			 
			 if(BLK1::bc_x.compare("fix") ==0)
				 mm=2*(nx-2)*(ny-2);
			 if(BLK1::bc_x.compare("pbc") ==0 || BLK1::bc_x.compare("free") ==0)
			     mm=2*n;
		
			rosen_alglib(starting_point,func,grad,ptr);
			cout<<"-------theoretical     size: "<<mm<<endl;
// 			cout<<"-------after counting size: "<<2*n_temp<<endl;
	 
		 for(int iter_solver=0; iter_solver<9; iter_solver++ ){
			 // sp_mat squezed(2*n,2*n);   
			 sp_mat squezed_sliced(2*n,2*n); 
//     		cout << "size of squezed: " << size(squezed_sliced) << endl;
  
		 	 //construct Hessian
			 squezed_sliced = Hessian_2D(force_energy::calculation::getInstance().c);
			 //apply fix boundaries
			 if(BLK1::bc_x.compare("fix") ==0)
			 	 slicing_filling_global_stifness(squezed_sliced);
//     		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;
			 
// 			 double cond_number = arma::cond(squezed);
// 			 cout<<"condition number of the Hessian: "<<cond_number<<endl;
			 
			 arma::vec x_arma(mm);
			 arma::vec b(mm);
			 int n_temp=0;
			 
			 for(int i=0;i<n;i++){
			     int iy = i / nx;
				 int ix = i % nx;
				 if(BLK1::bc_x.compare("fix") ==0)
				 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
				 	continue;

				 b[n_temp]             = -grad[i]  ;
				 b[n_temp+mm/2]        = -grad[i+n];
				 x_arma[n_temp]        = 0.;
				 x_arma[n_temp+mm/2]   = 0.;
				 n_temp++;	 
			 }
// 			cout<<"-------checking sizes-------"<<endl;
// 			cout<<"-------theoretical     size: "<<mm<<endl;
// 			cout<<"-------after counting size: "<<2*n_temp<<endl;


			cout<<"-------solving Hessian type solution-------"<<endl;


			 bool status = spsolve(x_arma,squezed_sliced,b,"superlu");
// 			 bool status = true;
			 if(status == false) {cout<< "no solution"<<endl;break;}
			 if(status == true) {cout<< " solution exists"<<endl;}
			 
			 n_temp = 0;
			 for(int i=0;i<n;i++)
			 { 
				 
				 int iy = i / nx;
				 int ix = i % nx;
				 if(BLK1::bc_x.compare("fix") ==0)
				 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
				 	continue;

				 
				 double xx =  starting_point[i]    + 0.85*x_arma[n_temp];
				 double yy =  starting_point[i+n]  + 0.85*x_arma[n_temp+mm/2]; 
// 				 cout<< starting_point[i] <<" "<<x_arma[n_temp]<<endl;

				 n_temp++;

				 starting_point[i]   = xx;
				 starting_point[i+n] = yy; 


				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;
			 }
			 
			 
// 			cout<<"-------checking sizes second time-------"<<endl;
// 			cout<<"-------theoretical     size: "<<mm<<endl;
// 			cout<<"-------after counting size: "<<2*n_temp<<endl;

			 
			 //calculate RHS
			 rosen_alglib(starting_point,func,grad,ptr);
			 energy_final = func;
			 grad_sum=0.;
			 for(int i=0;i<n;++i)
				grad_sum += pow(grad[i],2) + pow(grad[i+n],2);
			
			 cout<<"iteration solver: "<<iter_solver<<endl;
			 cout<<"NEWTON solver grad value: "<<sqrt(grad_sum)<<endl;
			 
		 	if(sqrt(grad_sum) <= pow(10,-8.)){
			 	cout<<"convergence reached at iteration: "<<iter_solver<<endl;
			 	break;	 	
			 }
		}
		
}


//eigenvalues and vector with Armadilo

void newton_eigen(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point,int t){


	   	 int nx = dimensions::nx;
		 int ny = dimensions::ny;
		 int n=c.p.size();
		 
		 double grad_sum,energy_final,func;
		 void *ptr;
    	 alglib::real_1d_array grad;


    	 std::vector<double> temp_vector;
   		 temp_vector.resize(2*n);
     	 grad.setcontent(temp_vector.size(), &(temp_vector[0]));

		 int mm;
		 if(BLK1::bc_x.compare("fix") ==0)
			 mm=2*(nx-2)*(ny-2);
		 if(BLK1::bc_x.compare("pbc") ==0 || (BLK1::bc_x.compare("free") ==0 && BLK1::bc_y.compare("free")))
			 mm=2*n;
		
		rosen_alglib(starting_point,func,grad,ptr);
		
		cout<<"-------theoretical     size: "<<mm<<endl;
		sp_mat squezed_sliced(2*n,2*n); 
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;

		 //construct Hessian
		 cout << "construct Hessian: " << size(squezed_sliced) << endl;
		 squezed_sliced = Hessian_2D(force_energy::calculation::getInstance().c);
		 //apply fix boundaries
		 if(BLK1::bc_x.compare("fix") ==0)
			 slicing_filling_global_stifness(squezed_sliced);
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;
		 
		//eigenvalues analysis
		arma::vec eigval;
		arma::mat eigvec;
		cout << "eigenvalues analysis starts "  << endl;


		int req_e=BLK1::req;

		cout<<"warning! # of requested eigenvalues  is: "<<req_e<<endl;

		arma::eigs_sym(eigval, eigvec, squezed_sliced, req_e,"sm");  // find 5 eigenvalues/eigenvectors
		cout<<"warning! # of eigenvalues found is: "<<eigval.size()<<endl;
// 		eigval.print("eigval:");
		//    eigvec.print("eigvec:");
// 		cout<<"eigvec size"<<eigvec.size()<<endl		;
		string ev="eigenvalues_" + IntToStr(t)+  ".dat";
		string ev2="smallest_eigenvalues.dat";

		for(int i=0;i<eigval.size();++i){
			    fstream filestr;	
				filestr.open (ev, fstream::in | fstream::out | fstream::app);
				filestr << std::scientific << std::setprecision(16)   << " "<<eigval[i] <<endl ;
				filestr.close();
		}	

		if(eigval.size()>0){
			fstream filestr;
			filestr.open (ev2, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(16) << 	c.load  << " "<<eigval.min() <<endl ;
			filestr.close();
		}
		

		
		
}


//eigenvalues and vector with Eigen


void newton_eigen_version(struct conf_stru& c,alglib::real_1d_array& starting_point,int t){


	   	 int nx = dimensions::nx;
		 int ny = dimensions::ny;
		 int n=c.p.size();
		 
		 double grad_sum,energy_final,func;
		 void *ptr;
    	 alglib::real_1d_array grad;


    	 std::vector<double> temp_vector;
   		 temp_vector.resize(2*n);
     	 grad.setcontent(temp_vector.size(), &(temp_vector[0]));

		 int mm;
		 
		 if(BLK1::bc_x.compare("fix") ==0)
			 mm=2*(nx-2)*(ny-2);
		 if(BLK1::bc_x.compare("pbc") ==0 || (BLK1::bc_x.compare("free") ==0 && BLK1::bc_y.compare("free")))
			 mm=2*n;
		
		rosen_alglib(starting_point,func,grad,ptr);


		cout<<"-------theoretical     size: "<<mm<<endl;
		sp_mat squezed_sliced(2*n,2*n); 
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;

		 //construct Hessian
		 cout << "construct Hessian: " << size(squezed_sliced) << endl;
		 squezed_sliced = Hessian_2D(force_energy::calculation::getInstance().c);
		 //apply fix boundaries
		 if(BLK1::bc_x.compare("fix") ==0)
			 slicing_filling_global_stifness(squezed_sliced);
		cout << "size of squezed_sliced: " << size(squezed_sliced) << endl;




		
		SpMatEigen squezed_sliced_eigen(mm,mm); 
		cout<<"-------transfer to eigen: "<<mm<<endl;

		slicing_filling_global_stifness_eigen(squezed_sliced,squezed_sliced_eigen);
		cout<<"------eigen values-------"<<endl;










		SparseGenMatProd<double> op(squezed_sliced_eigen);
		 // Construct eigen solver object, requesting the largest three eigenvalues
		 //ncv = 2⋅nev+1 suggested
		     int nev = BLK1::req;
		cout<<"warning! # of requested eigenvalues  is: "<<nev<<endl;

		 int ncv = 2*nev+1;
         SymEigsSolver< double, SMALLEST_MAGN, SparseGenMatProd<double> > eigs(&op, nev, ncv);
         
		// Initialize and compute
		eigs.init();
		int nconv = eigs.compute();

		// Retrieve results
		Eigen::VectorXcd evalues;
		evalues = eigs.eigenvalues();


   	  	if(evalues.size()>0)
   	  		std::cout << "Eigenvalues found:\n" << evalues[evalues.size()-1] << std::endl;
// 		int req_e=70;
// 		arma::eigs_sym(eigval, eigvec, squezed_sliced, req_e,"sm");  // find 5 eigenvalues/eigenvectors
// // 		eigval.print("eigval:");
// 		//    eigvec.print("eigvec:");
// // 		cout<<"eigvec size"<<eigvec.size()<<endl		;
		string ev="eigenvalues_" + IntToStr(t)+  ".dat";
		string ev2="smallest_eigenvalues.dat";
// 
// 		for(int i=0;i<eigval.size();++i){
// 			    fstream filestr;	
// 				filestr.open (ev, fstream::in | fstream::out | fstream::app);
// 				filestr << std::scientific << std::setprecision(16)   << " "<<eigval[i] <<endl ;
// 				filestr.close();
// 		}	
// 
		if(evalues.size()>0){
			fstream filestr;
			filestr.open (ev2, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(16) << 	c.load  << " "<<evalues(evalues.size()-1).real() <<endl ;
			filestr.close();




			
		 for(int i=0;i<n;i++){
			c.p[i].x = force_energy::calculation::getInstance().c.pref[i].x/BLK1::scale ;
			c.p[i].y = force_energy::calculation::getInstance().c.pref[i].y/BLK1::scale ;
		 }
// 		 write_to_a_ovito_file_eigenvectors(c,t,-1);
		 cout<<"check point; eigenvalue #: "<<evalues.size()<<endl;
	
		 for(int ii=0;ii<evalues.size();ii++){	
		  Eigen::VectorXcd eigvec;	
		  eigvec.resize(0);
		  int n_temp = 0;

		 cout<<"treated eigenvalue #: "<<ii<<endl;
		 cout<<"treated eigenvalue value: "<<evalues(ii).real()<<endl;

		  eigvec =   eigs.eigenvectors().col(ii).real()	;

			 for(int i=0;i<n;i++){ 
			 
				 int iy = i / nx;
				 int ix = i % nx;
				 if(BLK1::bc_x.compare("fix") ==0)
				 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
					continue;

			 
				 double xx =   eigvec(n_temp).real()    ;
				 double yy =   eigvec(n_temp+mm/2).real()  ; 

				 n_temp++;


				 c.p[i].x = force_energy::calculation::getInstance().c.pref[i].x/BLK1::scale + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pref[i].y/BLK1::scale + yy;
	
				
			 }
			 result_memorize(c);

// 			 write_to_a_ovito_file_eigenvectors(c,t,ii);
		 }


	
// 		    cout <<"\nHere is the matrix whose columns are eigenvectors of the Laplacian Matrix \n"
//          	<<"corresponding to these eigenvalues: \n"
//          	<< eigs.eigenvectors() <<endl;
	
	}
	
		if(evalues(evalues.size()-1).real() < 0)
			exit(EXIT_FAILURE);

					
		
}