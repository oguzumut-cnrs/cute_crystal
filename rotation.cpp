#include "rotation.h"
#include <cmath> 
#include "dlib.h"
using namespace dlib;


double toRadians(double angle) {
	double k=180.0;
	return angle*dlib::pi / k;
}

double toDegrees(double angle) {
	double k=180.0;
	return angle*k/dlib::pi;
}

void rotateLattice(struct conf_stru& c, float angle) {
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int ix, iy, ntot = nx*ny;

	float center_x = (nx - 1) / 2.0;
	float center_y = (ny - 1) / 2.0;

	for (int i = 0; i < ntot; ++i)
	{
		int iy = i / nx;
		int ix = i % nx;

		int old_x = c.p[i].x;
		int old_y = c.p[i].y;
		// rotate counterclockwise
		c.p[i].x = cos(toRadians(angle))*(old_x - center_x) - sin(toRadians(angle))*(old_y - center_y) + center_x;
		c.p[i].y = sin(toRadians(angle))*(old_x - center_x) + cos(toRadians(angle))*(old_y - center_y) + center_y;
	}
	
}


