#pragma once
#define ARMA_ALLOW_FAKE_GCC 
#define ARMA_DONT_USE_WRAPPER

#define ARMA_DONT_USE_HDF5

#include "dlib.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "structures.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "custom_inline_functions.h"
#include <iostream>
#include <map>
#include "ap.h"

// class MJTriList {
// public:
// 	int i, j, id;
// private:
// 	std::map<std::pair<int, int>, int> ID_list;
// public:
// 	void insert(int i, int j, int id) {
// 		ID_list.insert(std::pair<std::pair<int, int>, int>(std::pair<int, int>(i, j), id));
// 	}
// 	int fget_id(int i, int j) {
// 		return ID_list.at(std::pair<int, int>(i, j));
// 	}
// 
// 	int safe_get_id(int i, int j) {
// 		int id;
// 		try {
// 			id = ID_list.at(std::pair<int, int>(i, j));      // vector::at throws an out-of-range
// 		}
// 		catch (const std::out_of_range& oor) {
// 			id = 0;
// 			//std::cerr << "Out of Range error: " << oor.what() << '\n';
// 		}
// 		return id;
// 	}
// 
// 	MJTriList() {}
// 	~MJTriList() {}
// };
//void rotateLattice(struct conf_stru& c, float angle);

// void init_rotated_square(struct conf_stru& c, double angle);
// void new_init_rotated_square(struct conf_stru& c, double angle);
// void new_init_rotated_hex(struct conf_stru& c, double angle);
// // std::pair<double, double> get_XY_for_rotated_hex(int i, int j);
// void init(struct conf_stru& c);
// void initHex6(struct conf_stru& c);
// void assign_bc(struct conf_stru& c);
// void assign_bc_MJ(struct conf_stru& c);

void apply_BC(struct conf_stru& c, double& load);
void apply_BC_CG(struct conf_stru& c, double& load);

void apply_BC_full_generale(struct conf_stru& c, class boundary_conditions& f);
void apply_BC_generale(struct conf_stru& c, class boundary_conditions& f);
//void apply_BC_full(struct conf_stru& c, double& load);
//----------------------------------------------------

// struct de_stru stress_zanzotto(struct cella_stru& c, struct base_stru& v1,struct matrix_stru& m);
void fix_y_and_periodic_x_BC(int&ix, int&iy, int& k, struct de_stru& d,double& load);
void per_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c);
void per_BC_MJ(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c);
void per_BC_hex(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c);

void fix_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c);
void apply_BC_box(struct conf_stru& c,column_vector& sp_min, column_vector& sp_max);
void def_grad_fix(struct conf_stru& c);
void calcstresses_CG_en(struct conf_stru& c);
void calcstresses_CG_en_UL(struct conf_stru& c, const alglib::real_1d_array &m);

/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/
double rosen(const column_vector& m);

/*!
ensures
- returns the gradient vector for the rosen function
!*/
const column_vector rosen_derivative(const column_vector& m);


// This function computes the Hessian matrix for the rosen() fuction.  This is
// the matrix of second derivatives.
dlib::matrix<double> rosen_hessian(const column_vector& m);

class test_function
{
    /*
        This object is an example of what is known as a "function object" in C++.
        It is simply an object with an overloaded operator().  This means it can 
        be used in a way that is similar to a normal C function.  The interesting
        thing about this sort of function is that it can have state.  
        
        In this example, our test_function object contains a column_vector 
        as its state and it computes the mean squared error between this 
        stored column_vector and the arguments to its operator() function.

        This is a very simple function, however, in general you could compute
        any function you wanted here.  An example of a typical use would be 
        to find the parameters of some regression function that minimized 
        the mean squared error on a set of data.  In this case the arguments
        to the operator() function would be the parameters of your regression
        function.  You would loop over all your data samples and compute the output 
        of the regression function for each data sample given the parameters and 
        return a measure of the total error.   The dlib optimization functions 
        could then be used to find the parameters that minimized the error.
    */
public:

    test_function (
        const column_vector& input
    )
    {
        target = input;
    }

    double operator() ( const column_vector& arg) const
    {
        // return the mean squared error between the target vector and the input vector
        return mean(squared(target-arg));
    }

private:
    column_vector target;
};

class rosen_model 
{
    /*!
        This object is a "function model" which can be used with the
        find_min_trust_region() routine.  
    !*/

public:
    typedef ::column_vector column_vector;
    typedef dlib::matrix<double> general_matrix;

    double operator() (
        const column_vector& x
    ) const { return rosen(x); }

    void get_derivative_and_hessian (
        const column_vector& x,
        column_vector& der,
        general_matrix& hess
    ) const
    {
        der = rosen_derivative(x);
        hess = rosen_hessian(x);
    }
};

// ----------------------------------------------------------------------------------------

struct conf_stru  timestep_euler(struct conf_stru& c, double& load);

void  timestep_euler2(struct conf_stru& c, double& load);
