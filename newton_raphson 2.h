#pragma once
#include "structures.h"
#include "second.h"

#include "calculations_homogenous.h"
#include "ap.h"
#include <armadillo>

using namespace arma; 

void newton_raphson(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point);
void newton_eigen(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point,int t);
