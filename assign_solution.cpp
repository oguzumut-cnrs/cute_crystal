#include "assign_solution.h"

void assign_solution(conf_stru& c, column_vector& starting_point){

	int n= c.p.size();
	
		if(BLK1::BC.compare("eulerian") != 0){
			 for(int i=0;i<n;i++){
				 double xx =  starting_point(i);
				 double yy =  starting_point(i+n); 
				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y + yy;
				 c.disp[i].x     = xx ;
				 c.disp[i].y     = yy ;

			 }
		 }
		 
		 
		if(BLK1::BC.compare("eulerian") == 0){
			for(int i=0;i<n;i++){
				double xx =  starting_point(i);
				double yy =  starting_point(i+n); 


				c.pfix[i].x = force_energy::calculation::getInstance().c.pfix[i].x + xx;
				c.pfix[i].y = force_energy::calculation::getInstance().c.pfix[i].y + yy;

				c.p[i].x = c.pfix[i].x ;
				c.p[i].y = c.pfix[i].y ;

				c.disp[i].x     = 0 ;
				c.disp[i].y     = 0 ;
				starting_point(i) =0;
				starting_point(i+n) =0;

			}
		 }
		 
// 		if(BLK1::IC.compare("wienerc") ==0){
// 			//0.005 for square
// 			std::vector<double > u_dis = wiener( 0 ,0.01,c.p.size()  );    
// 			std::vector<double > v_dis = wiener( 0 ,0.01,c.p.size()  );  
// 			for(int i=0;i<n;i++){
// 				  starting_point(i)   +=u_dis[i];
// 				  starting_point(i+n) +=v_dis[i];
// 			}
// 		}	
		
}


void choose_bc(boundary_conditions& set1){

		if(BLK1::macro_def.compare("shear") ==0)
			set1.macro_shear();
		if(BLK1::macro_def.compare("shear_comb") ==0)
			set1.macro_multipled_loading();
		if(BLK1::macro_def.compare("tension") ==0)
			set1.macro_compression();
		if(BLK1::macro_def.compare("uni_tension") ==0)
			set1.uni_macro_compression();
		if(BLK1::macro_def.compare("shear_rhombic") ==0)
			set1.macro_shear_rhombic();
		if(BLK1::macro_def.compare("shear_rect") ==0)
			set1.macro_shear_rectangular();
		if(BLK1::macro_def.compare("rhombic_tension") ==0)
			set1.macro_rhombic_compression();
		if(BLK1::bc_mode.compare("frame") ==0)
			set1.def_frame(force_energy::calculation::getInstance().c,set1);

		
}

void assign_solution_alglib(conf_stru& c, alglib::real_1d_array& starting_point){

	int n= c.p.size();
	
		if(BLK1::BC.compare("eulerian") != 0){
			 for(int i=0;i<n;i++){
				 double xx =  starting_point(i);
				 double yy =  starting_point(i+n); 
				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y + yy;
				 c.disp[i].x     = xx ;
				 c.disp[i].y     = yy ;

			 }
		 }
		 
		 
		if(BLK1::BC.compare("eulerian") == 0){
			for(int i=0;i<n;i++){
				double xx =  starting_point(i);
				double yy =  starting_point(i+n); 


				c.pfix[i].x = force_energy::calculation::getInstance().c.pfix[i].x + xx;
				c.pfix[i].y = force_energy::calculation::getInstance().c.pfix[i].y + yy;

				c.p[i].x = c.pfix[i].x ;
				c.p[i].y = c.pfix[i].y ;

				c.disp[i].x     = 0 ;
				c.disp[i].y     = 0 ;
				starting_point[i] =0;
				starting_point[i+n] =0;

			}
			def_grad_fix(c);
		 }
		 
// 		if(BLK1::IC.compare("wienerc") ==0){
// 			//0.005 for square
// 			std::vector<double > u_dis = wiener( 0 ,0.01,c.p.size()  );    
// 			std::vector<double > v_dis = wiener( 0 ,0.01,c.p.size()  );  
// 			for(int i=0;i<n;i++){
// 				  starting_point[i]   +=u_dis[i];
// 				  starting_point[i+n] +=v_dis[i];
// 			}
// 		}	
		
}