
#pragma once
#include <cmath>
#include <vector>
#include "structures.h"
using namespace std;
#include <algorithm>
#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
#include <iomanip>      // std::setprecision

double maxval(std::vector<double>& m);

double minval(std::vector<double>& m);

int maxloc(std::vector<double>& m);
int minloc(std::vector<double>& m);
void single_data_to_a_file(double t1, string t2);
void all_data_to_a_file(double t1, double t3, string t2);

void fourth_order_tensor_allocate(std::vector < std::vector<std::vector< std::vector<double> > >>& Am);
