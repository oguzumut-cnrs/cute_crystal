#include "energy_map.h"






void xyzE()
{


	string filename,filename_c12,filename_trace;
	filename = "xyz_energy.dat";
	ofstream energy;

	energy.open(filename.c_str());


	filename_c12 = "z_energy.dat";
	ofstream energy_c12;
	energy_c12.open(filename_c12.c_str());


	for(double c12=-2.;c12<=2.;c12+=0.01){
		for(double c11=0.8;c11<=1.3;c11+=0.01){	
	
			double c22 = (1. + c12*c12)/c11;
	
			struct cella_stru metric_a;
			metric_a.c11 = c11;
			metric_a.c22 = c22;
			metric_a.c12 = c12;
// 			cout<<"detC= "<<c11*c22-c12*c12<<endl;
	
			struct cella_stru metric_rr = riduci(metric_a);
			double burgers = 1.;
			double energy_thread = energy_zanzotto(metric_rr,burgers, BLK1::beta_Var);
	
			energy << std::scientific << std::setprecision(7) << " " 
			<< metric_a.c11  << " " 
			<< metric_a.c22  << " " 
			<< metric_a.c12  << " "
			<< energy_thread << endl;


			energy_c12 << std::scientific << std::setprecision(7) << " " 
			<< metric_a.c12  << " "
			<< energy_thread << endl;


		}
			
	}
		
	


	energy.close();
	energy_c12.close();

}