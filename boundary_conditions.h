#pragma once
#include "structures.h"


class boundary_conditions {
public:
	double theta_d, load_in;
	struct  def_grad f;      // Member functions declaration

	struct def_grad macro_shear();
	struct def_grad macro_shear_rhombic();
	struct def_grad macro_shear_rectangular();
	struct def_grad macro_compression();
	struct def_grad macro_rhombic_compression();
	struct def_grad uni_macro_compression();
	struct def_grad macro_multipled_loading();

	void setParameters(double, double);
	void def_frame(struct conf_stru&, boundary_conditions&);
	void triangle_number(int);

};


// double Combiner(double a, double b, std::function<double (double,double)> func){
//   return func(a,b);
// }
// 
// double Add(double a, double b){
//   return a+b;
// }
// 
// double Mult(double a, double b){
//   return a*b;
// }
// 
// int main(){
//   Combiner(12,13,Add);
//   Combiner(12,13,Mult);
// }