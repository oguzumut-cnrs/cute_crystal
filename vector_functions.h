#pragma once
#include <random>
#include <algorithm>
#include <vector>
#include <iostream>
#include <ctime>
#include "namespaces.h"
using std::cout;
using std::endl;

double maxval(std::vector<double>& m);
double minval(std::vector<double>& m);
std::vector<double>  wiener(double av, double std,int n);
std::vector<double>  uniform(double a, double b, double amp,int n);
std::vector<double>  uniform_volumetric(double a, double b, double amp,int n);
std::vector<double> perturba(double& amp,int n);
std::vector<double> perturba_precipate(double& amp,int n);