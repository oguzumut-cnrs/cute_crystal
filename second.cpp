#include "second.h"
#include "calculations.h"
#include "result_memorize.h"
inline bool exist(const std::string& name)
{
	std::ifstream infile(name);
	return infile.good();
}


void stifness_tensor(std::vector < std::vector<std::vector< std::vector<double> > >>& Am, 
const struct base_stru& v, const struct cella_stru& c, struct matrix_stru& z);

void zeroing_local_stifness( std::vector < std::vector<std::vector< std::vector<double> > >>& Km ){
  	 
  	 
  	 for (int a = 0; a < 3; ++a){
   		for (int b = 0; b < 3; ++b)
   			for (int i= 0; i < 2; ++i)
   			for (int k= 0; k < 2; ++k)
   				Km[a][i][b][k]=0.;
  } 

}

void zeroing_Am( std::vector < std::vector<std::vector< std::vector<double> > >>& Am ){
  	 
  	 
   for (int i = 0; i < 2; ++i)
   for (int j = 0; j < 2; ++j)
   for (int k = 0; k < 2; ++k)
   for (int l = 0; l < 2; ++l)
   		Am[i][j][k][l]=0.;
  

}



void filling_local_stifness( std::vector < std::vector<std::vector< std::vector<double> > >>& Km,
std::vector < std::vector<std::vector< std::vector<double> > >>& Am, std::vector<std::vector<double>>&nv){
  	 
  	 
   for (int i = 0; i < 2; ++i){
   for (int k = 0; k < 2; ++k)
   for (int a = 0; a < 3; ++a)
   for (int b = 0; b < 3; ++b)
  
   	for (int j= 0; j < 2; ++j){
   		for (int l= 0; l < 2; ++l){
   			 Km[a][i][b][k] += Am[i][j][k][l]*nv[a][j] *nv[b][l];

   		}
   	}
   	}	


}

void filling_local_stifness_eulerian( std::vector < std::vector<std::vector< std::vector<double> > >>& Km,
std::vector < std::vector<std::vector< std::vector<double> > >>& Am, std::vector<std::vector<double>>&nv,
 std::vector<std::vector<double>>& F_inv){
  	 
  	 
   for (int a = 0; a < 3; ++a){
   for (int i = 0; i < 2; ++i)
   for (int b = 0; b < 3; ++b)
   for (int k = 0; k < 2; ++k)
  
		for (int j= 0; j < 2; ++j){
			for (int m= 0; m < 2; ++m){			
				for (int l= 0; l < 2; ++l){
				 Km[a][i][b][k] += Am[i][j][k][l]*nv[b][l] *nv[a][m]*F_inv[m][j];
				}
			}
		}	


	}
	
//    for (int a = 0; a < 3; ++a)
//    for (int i = 0; i < 2; ++i)
//    for (int b = 0; b < 3; ++b)
//    for (int k = 0; k < 2; ++k)
//    	cout<< a << " " << i << " "  << b << " "  << k <<  " " <<Km[a][i][b][k]<<endl; 
	
}

void filling_global_stifness(sp_mat& squezed,std::vector < std::vector<std::vector< std::vector<double> > >>& Km,
int ii,int kk, struct punto_stru& p){
  	 
  	int d1,d2,trans,ind	; 
  	int nx= dimensions::nx;
  	int ny= dimensions::ny;
  	int n=nx*ny;
    
//     struct punto_stru p = c.p[ii];
	struct de_stru d;
  	

  	
   for (int a = 0; a < 3; ++a){
   		for (int b = 0; b < 3; ++b){
   			for (int i= 0; i < 2; ++i){
   				for (int k= 0; k < 2; ++k){

   					if(a==0 && i==0) d1=ii;
   					if(a==1	&& i==0) d1=p.nn[kk];
   					if(a==2 && i==0) d1=p.nn[kk+1];
   					
   					if(a==0 && i==1) d1=ii		   + n;
   					if(a==1	&& i==1) d1=p.nn[kk]   + n;
   					if(a==2 && i==1) d1=p.nn[kk+1] + n;

   					if(b==0 && k==0) d2=ii;
   					if(b==1	&& k==0) d2=p.nn[kk];
   					if(b==2 && k==0) d2=p.nn[kk+1] ;
   					
   					if(b==0 && k==1) d2=ii         +n;
   					if(b==1	&& k==1) d2=p.nn[kk]   +n;
   					if(b==2 && k==1) d2=p.nn[kk+1] +n;

 
   
   					squezed.at(d1,d2) += Km[a][i][b][k];
   		
   				}
   			}
   		}
	}
	
	


}

void filling_global_stifness_eigen(SpMatEigen& squezed,std::vector < std::vector<std::vector< std::vector<double> > >>& Km,
int ii,int kk, struct punto_stru& p){
  	 
  	int d1,d2,trans,ind	; 
  	int nx= dimensions::nx;
  	int ny= dimensions::ny;
  	int n=nx*ny;
  	int ix,iy;
    
//     struct punto_stru p = c.p[ii];
	struct de_stru d;
  	
	int k1,k2;
	k1=0;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;


	 iy = ii / nx;
	 ix = ii % nx;
	 if(ii>n){
		 iy = (ii-n) / nx;
		 ix = (ii-n) % nx;			 
	 }
	

  	
   for (int a = 0; a < 3; ++a){
   		for (int b = 0; b < 3; ++b){
   			for (int i= 0; i < 2; ++i){
   				for (int k= 0; k < 2; ++k){
					
				//does not fill for fix boundaries
				//but keeps indices of full matrix (it is different than Armadillo)
				if(check(force_energy::calculation::getInstance().c.bc_cond[ii][k1],2) 
				&& check(force_energy::calculation::getInstance().c.bc_cond[ii][k2],2)){

   					if(a==0 && i==0) d1=ii;
   					if(a==1	&& i==0) d1=p.nn[kk];
   					if(a==2 && i==0) d1=p.nn[kk+1];
   					
   					if(a==0 && i==1) d1=ii		   + n;
   					if(a==1	&& i==1) d1=p.nn[kk]   + n;
   					if(a==2 && i==1) d1=p.nn[kk+1] + n;

   					if(b==0 && k==0) d2=ii;
   					if(b==1	&& k==0) d2=p.nn[kk];
   					if(b==2 && k==0) d2=p.nn[kk+1] ;
   					
   					if(b==0 && k==1) d2=ii         +n;
   					if(b==1	&& k==1) d2=p.nn[kk]   +n;
   					if(b==2 && k==1) d2=p.nn[kk+1] +n;

//  					cout<<"d1: "<<d1<<" d2: "<<d2<<endl;
   					squezed.coeffRef(d1,d2) += Km[a][i][b][k];
   				}
   		
   				}
   			}
   		}
	}
	
	


}



void slicing_filling_global_stifness(sp_mat& squezed){
  	 
  	int d1,d2,trans,ind	; 
  	int nx= dimensions::nx;
  	int ny= dimensions::ny;
  	int n=nx*ny;
    
//     struct punto_stru p = c.p[ii];
	struct de_stru d;
  	
	int k=0;
	int l=0;
  	int iy ;
	int ix 	;
// std::vector<arma::uword> bad_ids; // The rows that we want to shed 
// bad_ids.resize(0);

   //loop over u_i and v_i
   for (int i = 0; i < 2*n; ++i){

   				 iy = i / nx;
				 ix = i % nx;
				 if(i>n){
	   			 iy = (i-n) / nx;
				 ix = (i-n) % nx;			 
				 }
	
   			//all the boundary nodes
   			//change the below condition to implement other types of fix boundaries
   			if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1 ){
//     				bad_ids.push_back(i-k);
//     				cout<<"i= "<<i+n<<endl;
//     				cout << "size of A: " << size(squezed) << endl;
//     				cout << "size of A: " << squezed.n_rows<< endl;
					int l = i-k;
    				squezed.shed_row(l);
//     				squezed.shed_row(i+n-k);
// 					cout<<i+n-k++<<endl;
//     				cout << "size of A: " << size(squezed) << endl;
//     				cout << "size of A: " << squezed.n_rows<< endl;
// 
//     				squezed.shed_row(l+(squezed.n_rows+1)/2-1);
    				squezed.shed_col(l);

     				k++;
//     				squezed.shed_col(i+n-l++);

//     				cout << "size of A: " << size(squezed) << endl;

//     			bad_ids.push_back(i+n);
   			}
   				
	}
	
// 	 k=0;
// 	 l=0;
//    for (int i = 0; i < n; ++i){
// 
//    				 iy = i / nx;
// 				 ix = i % nx;
// 				 
// 	
//    			if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1 ){
// //     				bad_ids.push_back(i-k);s
// //     				cout<<"i= "<<i+n<<endl;
// //     				cout << "size of A: " << size(squezed) << endl;
// //     				cout << "size of A: " << squezed.n_cols<< endl;
// 					int l = i-k;
//     				squezed.shed_col(l);
// //     				squezed.shed_row(i+n-k);
// // 					cout<<i+n-k++<<endl;
// //     				cout << "size of A: " << size(squezed) << endl;
// //     				cout << "size of A: " << squezed.n_rows<< endl;
// // 
//     				squezed.shed_col(l+(squezed.n_cols+1)/2-1);
//      				k++;
// //     				squezed.shed_col(i+n-l++);
// 
// //     				cout << "size of A: " << size(squezed) << endl;
// 
// //     			bad_ids.push_back(i+n);
//    			}
//    				
// 	}	
// 	
// 	   for (int i = 0; i < n; ++i){
// 
//    				 iy = i / nx;
// 				 ix = i % nx;
// 				 
// 	
//    			if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1 ){
// //     				bad_ids.push_back(i-k);
// //     				cout<<"i= "<<i+n<<endl;
//     				cout << "size of A: " << size(squezed) << endl;
//     				cout << "size of A: " << squezed.n_rows<< endl;
// 					int l = i-k;
//     				squezed.shed_row(l + squezed.n_rows);
// //     				squezed.shed_row(i+n-k);
// // 					cout<<i+n-k++<<endl;
// //     				cout << "size of A: " << size(squezed) << endl;
// //     				cout << "size of A: " << squezed.n_rows<< endl;
// // 
// //     				squezed.shed_row(l+squezed.n_rows);
// //     				k++;
// //     				squezed.shed_col(i+n-l++);
// 
// //     				cout << "size of A: " << size(squezed) << endl;
// 
// //     			bad_ids.push_back(i+n);
//    			}
//    				
// 	}
// 	
	
// cout << "size of A: " << size(squezed) << endl;
// // uvec indices = {4, 6, 8};
// for (int i = 0; i < bad_ids.size(); ++i)
// 	cout<<bad_ids[i]<<endl;
// 
// 	cout<<bad_ids.size()<<endl;
// // for (const auto &bad_id : bad_ids) { 
//   squezed.shed_rows(bad_ids);
//   squezed.shed_cols(bad_ids);

// }
//     squezed.shed_cols(4);

// cout << "size of A: " << size(squezed) << endl;
// 
// arma::mat A = randu<arma::mat>(5,10);
// arma::mat B = randu<arma::mat>(5,10);
// 
// A.shed_row(2);
// A.shed_cols(2,4);
// 
// uvec indices = {4, 6, 8};
// B.shed_cols(indices);
// squezed.shed_cols(indices);

}

//transfers sliced armadillo matrix to eigen
void  slicing_filling_global_stifness_eigen(sp_mat& squezed, SpMatEigen& squezed_eigen){
  	 
  	int d1,d2,trans,ind	; 
  	int nx= dimensions::nx;
  	int ny= dimensions::ny;
  	int n=nx*ny;
    
	sp_mat::const_iterator start = squezed.begin();
	sp_mat::const_iterator end   = squezed.end();

	for(sp_mat::const_iterator it = start; it != end; ++it){
//   		cout << "location: " << it.row() << "," << it.col() << "  ";
//   		cout << "value: " << (*it) << endl;
  		squezed_eigen.insert(it.row(),it.col()) = (*it);
  	}


}


int K_matrix(struct conf_stru& c)
{
  
  int n= c.p.size();
  
  std::vector < std::vector<std::vector< std::vector<double> > >> Am;
  std::vector < std::vector<std::vector< std::vector<double> > >> Km;
  
  fourth_order_tensor_allocate(Am);

			//BLK1::memory_number has been used here as an iteration increment
			for(int t=0;t<10; ++t){
				
 			double theta=BLK1::theta;
 			double load;

//

  			string filename2  =  BLK1::config ;
// 			string filename2  =  BLK1::config ;

			if(exist(filename2) == false){ 
		 		cout<<"FILE NOT FOUND inside second.cpp  "<<filename2<<"\n";
		 		cout<<"EXITING THE CODE"<<endl;
				exit (EXIT_FAILURE); 
			}

			ifstream disp;

		//    cout<< filename << "  \n";
			disp.open(filename2.c_str());
			double temp;
			int nol;
			disp >> nol;
			cout<<"number of lines in the file: "<<nol<<endl;
			if(nol != n){
				cout<<"the number of the system is WRONG in the input file"<<endl;
				cout<<"EXITING THE CODE"<<endl;
				exit (EXIT_FAILURE); 
			}
			for (int i = 0; i<n; ++i){
			disp >> std::scientific >> std::setprecision(16)>> c.p[i].x >> c.p[i].y >> temp >> temp >> temp ;
// 			if(i<2 || i>nol-2)
// 				cout << std::scientific << std::setprecision(14)<< c.p[i].x << " "<<c.p[i].y << " "<<temp << " "<< temp << " "<<temp <<endl;
			c.pfix[i].x = c.p[i].x;
			c.pfix[i].y = c.p[i].y; 
			c.pold[i].x  = c.p[i].x  ; 
			c.pold[i].y  = c.p[i].y  ; 		

			}

			disp.close();
			
			//string filename3  =  "../all_load.dat" ;
			string filename3  =  BLK1::config_u ;
			if(exist(filename3) == false){ 
		 		cout<<"FILE NOT FOUND inside second.cpp  "<<filename3<<"\n";
		 		cout<<"EXITING THE CODE"<<endl;
				exit (EXIT_FAILURE); 
			}
			disp.open(filename3.c_str());

			double initial_strain;
			for (int i = 0; i<=BLK1::memory_number; ++i)
				disp >> std::scientific >> std::setprecision(16)>> initial_strain>> temp  ;
			
			disp.close();

		    c.load = load = initial_strain ;
		    c.load=0.;
		   	BLK1::setNum(load);
		   	cout<<"initial strain memory= "<<initial_strain<<endl;
 		
   			boundary_conditions set1;

   			set1.setParameters(theta,load);
   			choose_bc(set1);

        
        	apply_BC_generale(c,set1);
 			apply_BC(c,load);
 			
 			



 		
 		



//element stifness matrix
    Km.resize(3);
	for (int i = 0; i < 3; ++i)
		Km[i].resize(2);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
		Km[i][j].resize(3);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
			for (int k= 0; k < 3; ++k)
				Km[i][j][k].resize(2);


 //shape functions

   std::vector<std::vector<double>> nv,nv2;

   nv.resize(3);
   for (int i = 0; i < 3; ++i)
   	nv[i].resize(2);

   nv2.resize(3);

   for (int i = 0; i < 3; ++i)
   	nv2[i].resize(2);




   nv[0][0]=nv[0][1]=-1.;
   
   nv[1][0]=1.;
   nv[1][1]=0.;
   
   nv[2][0]=0.;
   nv[2][1]=1.;

   nv2[0][0]=nv2[0][1]=1.;
   
   nv2[1][0]=-1.;
   nv2[1][1]=0.;
   
   nv2[2][0]=0.;
   nv2[2][1]=-1.;
 
   //Inverse Deformation matrix in tensorial form
   std::vector<std::vector<double>>  F_inv;
   F_inv.resize(2);
   for (int i = 0; i < 2; ++i)
   	F_inv[i].resize(2); 
   

  sp_mat squezed(2*n,2*n);   // don't need to explicitly reserve the number of non-zeros
		

   	
   int d1,d2;
   



// 	A = sprandu<sp_mat>(n, n, 0.1);
// A=sprandu<sp_mat>(n, n, 0.1);

// 		Am[0][0][0][0] = 9.66037;
// 		Am[0][0][0][1] = -0.215906;
// 		Am[0][0][1][0] = -3.18286;
// 		Am[0][0][1][1] = 6.38581;
// // 
// 		Am[0][1][0][0] =-0.215906;
// 		Am[0][1][0][1] = 26.4953;
// 		Am[0][1][1][0] =27.5383;
// 		Am[0][1][1][1] =-5.52872;
// // 
// 		Am[1][0][0][0] =-3.18286;
// 		Am[1][0][0][1] =27.5383;
// 		Am[1][0][1][0] = 28.2203;
// 		Am[1][0][1][1] =-4.98544;
// 
// 		Am[1][1][0][0] = 6.38581;
// 		Am[1][1][0][1] =-5.52872;
// 		Am[1][1][1][0] =-4.98544;
// 		Am[1][1][1][1] = 10.6572;


	int nx = dimensions::nx;
	int ny = dimensions::ny;
	cout<<"-------constructing the global matrix-------"<<endl;
  
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) {
// 	for (int k=0; k<1; k=k+c.inc) {


		c.t1 = force_energy::calculation::getInstance().c.j1[k];
		c.t2 = force_energy::calculation::getInstance().c.j2[k];
		zeroing_local_stifness(Km);

	

   	
		for (int i=0; i<nx*ny; i++) {
// 		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)
// 		{
			
			
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  =c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			struct cella_stru metric_pr;	
			struct energy_stress es;
			double h=dimensions::dh;
			double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;
			struct matrix_stru def_grad,inv_def_grad;
			if(!check(c.bc_cond[i][k],2))	
				goto label3;
			zeroing_local_stifness(Km);
// 			zeroing_Am(Am);

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

 		    c.fptr(i,ix,iy,k,d,c);



//---------------------------------VECTORS OF CELLS------------------------------------		
			
		v.e1[0]= (d.p[1].x-d.p[0].x)*c.t1 ;//+  c.average_f.f11*c.t1;
		v.e1[1]= (d.p[1].y-d.p[0].y)*c.t1  ;//+  c.average_f.f21*c.t1;
		v.e2[0]= (d.p[2].x-d.p[0].x)*c.t2  ;//+  (0.1 +c.average_f.f12*c.t2);
		v.e2[1]= (d.p[2].y-d.p[0].y)*c.t2  ;//+  c.average_f.f22*c.t2;			


// 		v.e1[0]= 1.;//+  c.average_f.f11*c.t1;
// 		v.e1[1]= 0. ;//+  c.average_f.f21*c.t1;
// 		v.e2[0]= 0.1 ;//+  (0.1 +c.average_f.f12*c.t2);
// 		v.e2[1]= 1.  ;//+  c.average_f.f22*c.t2;			


		metrics  = faicella(v);
		vecr=riduci_vectors(v);
		//FIND INTEGER MATRIX			
		m2=find_integer_matrix(v,vecr);
		//REDUCED METRIC
		metric_rr  = faicella(vecr);
// 		def_grad.m11 = v.e1[0];
// 		def_grad.m22 = v.e2[1];
// 		def_grad.m12 = v.e2[0];
// 		def_grad.m21 = v.e1[1];
// 		inv_def_grad = def_grad.inverse(def_grad);	
// 		F_inv[0][0] = 	inv_def_grad.m11;	
// 		F_inv[1][1] = 	inv_def_grad.m22;	
// 		F_inv[0][1] = 	inv_def_grad.m12;	
// 		F_inv[1][0] = 	inv_def_grad.m21;	


// 		cout<<"FOR I= "<<i << " AND k= "<<k<<endl;
// 		cout<<"FOR I= "<<i << " AND k= "<<k<<endl;
// 		cout<<"FOR I= "<<i << " AND k= "<<k<<endl;
		stifness_tensor(Am,v,metric_rr, m2);

		
		if(k==0)
// 			filling_local_stifness_eulerian(Km,Am,nv,F_inv);
			filling_local_stifness(Km,Am,nv);

		else if(k==2)
// 			filling_local_stifness_eulerian(Km,Am,nv2,F_inv);
			filling_local_stifness(Km,Am,nv2);



		filling_global_stifness(squezed,Km,i,k,p);	


			label3: ;

			
			
			
			
// 	});
	 		}
	}

//    squezed.print("squezed:");

	cout<<"--------------eigenvalue calculations started--------------"<<endl;
//     squezed.print("squezed:");

	arma::vec eigval;
	arma::mat eigvec;
//   eigs_gen(eigval, eigvec, squezed, 1, "sr");  // find smallest eigenvalue ---> INCORRECT RESULTS
 //   eigs_sym(eigval, eigvec, squezed, 4	, "sa");
 
//   arma::eigs_gen(eigval, eigvec, squezed, 25,"sm");  // find 5 eigenvalues/eigenvectors

   int req_e=n-10;
   arma::eigs_sym(eigval, eigvec, squezed, req_e,"sm");  // find 5 eigenvalues/eigenvectors
   eigval.print("eigval:");
//    eigvec.print("eigvec:");
   cout<<"eigvec size"<<eigvec.size()<<endl		;
	string ev="eigenvalues_" + IntToStr(t)+  ".dat";
	for(int i=0;i<eigval.size();++i){
		fstream filestr;	
			filestr.open (ev, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(16)   << " "<<eigval[i] <<endl ;
			filestr.close();
	}	
	
// 	string evr="eigenvecs_" + IntToStr(t)+  ".dat"; 
// 	
// 	
// 	for(int i=0;i<eigvec.size();++i){
// 		fstream filestr;	
// 			filestr.open (evr, fstream::in | fstream::out | fstream::app);
// 			filestr << std::scientific << std::setprecision(16)   << " "<<eigvec[i] <<endl ;
// 			filestr.close();
// 	}
	
	   cout<<"participation ratio calculation:"<<endl;



 	string pr="lambda_ratio_" + IntToStr(t)+  ".dat"; 
 	string prw="w_ratio_" + IntToStr(t)+  ".dat"; 

	double ae=0;
	for(int h=0;h<eigval.size();++h){
		ae=0;
		for(int i=h*2*n;i<n*2*(h+1);++i){
			ae += (eigvec[i]*eigvec[i])*(eigvec[i]*eigvec[i])	;
		}
		fstream filestr;	
		filestr.open (pr, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16)   << " "<<eigval[h]<<" "<< 1./(2.*n*ae)<<endl ;
		filestr.close();
		filestr.open (prw, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16)   << " "<<sqrt(eigval[h])<<" "<< 1./(2.*n*ae)<<endl ;
		filestr.close();


	}
	
	
	}	
	
	

//    eigval.print("eigval:");
// 	string ev="eigenvalues.dat";
// 	for(int i=0;i<eigval.size();++i){
// 		fstream filestr;	
// 			filestr.open (ev, fstream::in | fstream::out | fstream::app);
// 			filestr << std::scientific << std::setprecision(7)   << " "<<eigval[i]<<endl ;
// 			filestr.close();
// 	}		

  return 0;
}

sp_mat Hessian_2D(struct conf_stru& c)
{
  
  int n= c.p.size();
  
  std::vector < std::vector<std::vector< std::vector<double> > >> Am;
  std::vector < std::vector<std::vector< std::vector<double> > >> Km;
  
  fourth_order_tensor_allocate(Am);




//element stifness matrix
    Km.resize(3);
	for (int i = 0; i < 3; ++i)
		Km[i].resize(2);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
		Km[i][j].resize(3);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
			for (int k= 0; k < 3; ++k)
				Km[i][j][k].resize(2);


 //shape functions respecting the notations for triangle k=0 (1,0),(0,1)
 //shape functions respecting the notations for triangle k=2 (-1,0),(0,-1)
   std::vector<std::vector<double>> nv,nv2;

   nv.resize(3);
   for (int i = 0; i < 3; ++i)
   	nv[i].resize(2);

   nv2.resize(3);

   for (int i = 0; i < 3; ++i)
   	nv2[i].resize(2);




   nv[0][0]=nv[0][1]=-1.;
   
   nv[1][0]=1.;
   nv[1][1]=0.;
   
   nv[2][0]=0.;
   nv[2][1]=1.;

   nv2[0][0]=nv2[0][1]=1.;
   
   nv2[1][0]=-1.;
   nv2[1][1]=0.;
   
   nv2[2][0]=0.;
   nv2[2][1]=-1.;
 
   //Inverse Deformation matrix in tensorial form
   std::vector<std::vector<double>>  F_inv;
   F_inv.resize(2);
   for (int i = 0; i < 2; ++i)
   	F_inv[i].resize(2); 
   

  sp_mat squezed(2*n,2*n);   // don't need to explicitly reserve the number of non-zeros

   	
   int d1,d2;
   



	int nx = dimensions::nx;
	int ny = dimensions::ny;
	cout<<"-------constructing the global matrix-------"<<endl;
  
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) {


		c.t1 = force_energy::calculation::getInstance().c.j1[k];
		c.t2 = force_energy::calculation::getInstance().c.j2[k];
		zeroing_local_stifness(Km);

	

   	
		for (int i=0; i<nx*ny; i++) {
// 		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)
// 		{
			
			
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  =c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			struct cella_stru metric_pr;	
			struct energy_stress es;
			double h=dimensions::dh;
			double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;
			struct matrix_stru def_grad,inv_def_grad;
			if(!check(c.bc_cond[i][k],2))	
				goto label3;
			zeroing_local_stifness(Km);
// 			zeroing_Am(Am);

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

 		    c.fptr(i,ix,iy,k,d,c);



//---------------------------------VECTORS OF CELLS------------------------------------		
			
		v.e1[0]= (d.p[1].x-d.p[0].x)*c.t1 ;//+  c.average_f.f11*c.t1;
		v.e1[1]= (d.p[1].y-d.p[0].y)*c.t1  ;//+  c.average_f.f21*c.t1;
		v.e2[0]= (d.p[2].x-d.p[0].x)*c.t2  ;//+  (0.1 +c.average_f.f12*c.t2);
		v.e2[1]= (d.p[2].y-d.p[0].y)*c.t2  ;//+  c.average_f.f22*c.t2;			


		metrics  = faicella(v);
		vecr=riduci_vectors(v);
		//FIND INTEGER MATRIX			
		m2=find_integer_matrix(v,vecr);
		//REDUCED METRIC
		metric_rr  = faicella(vecr);
		stifness_tensor(Am,v,metric_rr, m2);

		
		if(k==0)
// 			filling_local_stifness_eulerian(Km,Am,nv,F_inv);
			filling_local_stifness(Km,Am,nv);

		else if(k==2 && BLK1::crystal_symmetry.compare("square") == 0)

// 			filling_local_stifness_eulerian(Km,Am,nv2,F_inv);
			filling_local_stifness(Km,Am,nv2);

		else if(k==3 && BLK1::crystal_symmetry.compare("hex") == 0)
			filling_local_stifness(Km,Am,nv2);


		filling_global_stifness(squezed,Km,i,k,p);	


			label3: ;


	 		}
	}


  return squezed;
}



SpMatEigen Hessian_2D_eigen(struct conf_stru& c)
{
  
  int n= c.p.size();
  
  std::vector < std::vector<std::vector< std::vector<double> > >> Am;
  std::vector < std::vector<std::vector< std::vector<double> > >> Km;
  
  fourth_order_tensor_allocate(Am);




//element stifness matrix
    Km.resize(3);
	for (int i = 0; i < 3; ++i)
		Km[i].resize(2);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
		Km[i][j].resize(3);
	for (int i = 0; i < 3; ++i)
		for (int j= 0; j < 2; ++j)
			for (int k= 0; k < 3; ++k)
				Km[i][j][k].resize(2);


 //shape functions respecting the notations for triangle k=0 (1,0),(0,1)
 //shape functions respecting the notations for triangle k=2 (-1,0),(0,-1)
   std::vector<std::vector<double>> nv,nv2;

   nv.resize(3);
   for (int i = 0; i < 3; ++i)
   	nv[i].resize(2);

   nv2.resize(3);

   for (int i = 0; i < 3; ++i)
   	nv2[i].resize(2);




   nv[0][0]=nv[0][1]=-1.;
   
   nv[1][0]=1.;
   nv[1][1]=0.;
   
   nv[2][0]=0.;
   nv[2][1]=1.;

   nv2[0][0]=nv2[0][1]=1.;
   
   nv2[1][0]=-1.;
   nv2[1][1]=0.;
   
   nv2[2][0]=0.;
   nv2[2][1]=-1.;
 
   //Inverse Deformation matrix in tensorial form
   std::vector<std::vector<double>>  F_inv;
   F_inv.resize(2);
   for (int i = 0; i < 2; ++i)
   	F_inv[i].resize(2); 
   

  SpMatEigen squezed_eigen(2*n,2*n);
  //guess the number of non zero on each row
  squezed_eigen.reserve(Eigen::VectorXi::Constant(2*n,8));	

// Definition of a 4x6 single precision row-major matrix
    int d1,d2;
   



	int nx = dimensions::nx;
	int ny = dimensions::ny;
	cout<<"-------constructing the global matrix eigen-------"<<endl;
  
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) {


		c.t1 = force_energy::calculation::getInstance().c.j1[k];
		c.t2 = force_energy::calculation::getInstance().c.j2[k];
		zeroing_local_stifness(Km);

	

   	
		for (int i=0; i<nx*ny; i++) {
// 		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)
// 		{
			
			
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  =c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			struct cella_stru metric_pr;	
			struct energy_stress es;
			double h=dimensions::dh;
			double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;
			struct matrix_stru def_grad,inv_def_grad;
			if(!check(c.bc_cond[i][k],2))	
				goto label3;
			zeroing_local_stifness(Km);
// 			zeroing_Am(Am);

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

 		    c.fptr(i,ix,iy,k,d,c);



//---------------------------------VECTORS OF CELLS------------------------------------		
			
		v.e1[0]= (d.p[1].x-d.p[0].x)*c.t1 ;//+  c.average_f.f11*c.t1;
		v.e1[1]= (d.p[1].y-d.p[0].y)*c.t1  ;//+  c.average_f.f21*c.t1;
		v.e2[0]= (d.p[2].x-d.p[0].x)*c.t2  ;//+  (0.1 +c.average_f.f12*c.t2);
		v.e2[1]= (d.p[2].y-d.p[0].y)*c.t2  ;//+  c.average_f.f22*c.t2;			


		metrics  = faicella(v);
		vecr=riduci_vectors(v);
		//FIND INTEGER MATRIX			
		m2=find_integer_matrix(v,vecr);
		//REDUCED METRIC
		metric_rr  = faicella(vecr);
		stifness_tensor(Am,v,metric_rr, m2);

		
		if(k==0)
// 			filling_local_stifness_eulerian(Km,Am,nv,F_inv);
			filling_local_stifness(Km,Am,nv);

		else if(k==2 && BLK1::crystal_symmetry.compare("square") == 0)

// 			filling_local_stifness_eulerian(Km,Am,nv2,F_inv);
			filling_local_stifness(Km,Am,nv2);

		else if(k==3 && BLK1::crystal_symmetry.compare("hex") == 0)
			filling_local_stifness(Km,Am,nv2);


		filling_global_stifness_eigen(squezed_eigen,Km,i,k,p);	


			label3: ;


	 		}
	}

  squezed_eigen.makeCompressed();                        // optional
  return squezed_eigen;
}

