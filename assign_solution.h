#pragma once
#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "utilities.h"
#include <iostream>
#include <fstream>
void assign_solution(conf_stru& c, column_vector& starting_point);
void choose_bc(boundary_conditions& set1);
void assign_solution_alglib(conf_stru& c, alglib::real_1d_array& starting_point);
