#pragma once
#include "struct_functions.h"
#include <functional>
#include <fstream>
#include <time.h>       /* time */

#ifdef _WIN32 // MSC_VER
#define WIN32_LEAN_AND_MEAN
// *sigh* no gettimeofday on Win32/Win64
 inline int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
	 //
	 // TO BE CONTINUED ......
	 //
	 //
	return 0;
}
#else
#include <sys/time.h>
#endif // _WIN32

inline struct def_grad combiner(double a, double b, std::function<def_grad(double, double)> func)
{
	return func(a, b);
}

// functions from main
inline bool fexists(const char *fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

template <typename T, typename U, typename V, typename W>
bool allequal(const T &t, const U &u, const V &v, const W &w)
{
	return (t == u) || (t == v) || (t == w);
}

template <typename T, typename U>
bool check(const T &t, const U &u)
{
	return (t == u);
}

inline int FuncA(int x)
{
	return 1;
}

inline float FuncB()
{
	return 2;
}

inline double get_wall_time()
{
	struct timeval time;
	if (gettimeofday(&time, NULL))
	{
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

inline double get_cpu_time()
{
	return (double)clock() / CLOCKS_PER_SEC;
}