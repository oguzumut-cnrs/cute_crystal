#include "calculations.h"
#include "rotation.h"
#include "input_output.h"
#include "energy_functions.h"
#include "result_memorize.h"
#include <cmath>
#include <vector>

using std::cout;
using std::endl;



void calcstresses_CG_en(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	
	c.energy = 0.;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);
	static int loop=2;
	
// 	void (*fptr)( int,  int,   int,  struct de_stru& , struct conf_stru& ); //Declare a function pointer to voids with no params

// 	c.fptr = &per_BC; //fptr -> one

	
	for (int i=0; i<npunti; ++i)
	{
		c.gr[i].x=c.gr[i].y=c.energy_local[i] =0.;
		
		for(int l=0;l<12;++l)
			c.grp1[i][l].x = c.grp1[i][l].y=0.;
	}

	int tt=6;
	
	
// 	cout<<"here"<<endl;
	std::vector<double> c1,c2;
// 	cout<<"here"<<endl;
	c1.resize(0);
// 	cout<<"here"<<endl;
	c2.resize(0);
// 	cout<<"here"<<endl;
// 	
	c1.push_back(1);
	c1.push_back(1);
	c1.push_back(-1);
	c1.push_back(-1);
// 	cout<<"herec2"<<endl;
// 
	c2.push_back(1);
	c2.push_back(-1);
	c2.push_back(-1);
	c2.push_back(1); 
// 	cout<<"aherec2"<<endl;
// 	c.t1=1;
// 	c.t2=1;

// 	c.c1.resize(1);
// 	c.c2.resize(1);
    // map<string,double(*)(struct cella_stru& c,double &burgers,double alloy)> double_map;


	
try
{
	//k -> loop on the triangles
// 	for (int k=0; k<BLK1::triangle; ++k) 
	for (int k=0; k<3; k=k+2) 

	{
			c.t1=c1[k];
			c.t2=c2[k];		
		//this is the loop on triangle vertices			
// 		for (int i=0; i<nx*ny; i++) 
		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)
		{
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;
// 			cout<<"i: "<<i<<endl; 

			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  = c.disorder[i];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			double mesh=0;
			struct cella_stru metric_pr;	
			struct matrix_stru minv,fl,fu,f;
			struct spec_for_reduction vandc;
			
			



// 				if(allequal(c.bc_cond[i][k],0,1,5))	
			if(!check(c.bc_cond[i][k],2))	
				goto label3;


//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

//      		per_BC_hex(ix,iy,k,d,c);
 
//  		    per_BC(ix,iy,k,d,c);
//  		    fptr(ix,iy,k,d,c); //=> one()
 		    c.fptr(i,ix,iy,k,d,c);

//             per_BC_MJ(d,i,c,k);


//---------------------------------VECTORS OF CELLS------------------------------------		
			v=vectorize(d);
			
		v.e1[0]= c.t1*(d.p[1].x-d.p[0].x) ;
		v.e1[1]= c.t1*(d.p[1].y-d.p[0].y) ;
		v.e2[0]= c.t2*(d.p[2].x-d.p[0].x) ;
		v.e2[1]= c.t2*(d.p[2].y-d.p[0].y) ;			
//---------------------------------REAL METRICS------------------------------------		

		    metrics  = faicella(v);
// 		    tempm = metrics;
// 			metric_pr = riduci_umut(tempm);
// 			c.grad_umut[i].identity();


// 
// 			if(k>=0){
// 			cout<<"i: "<<i<<" k: "<<k 
// 						  <<" c11: "<<metrics.c11 
// 						  <<" c22: "<<metrics.c22 
// 						  <<" c12: "<<metrics.c12
// 						  <<" detC: "<<metrics.c11*metrics.c22-metrics.c12*metrics.c12
// 						  <<" ix "<<ix 
// 						  <<" iy "<<iy 
// 						  <<" v1_x "<<p.nn[k] % nx
// 						  <<" v1_y "<<p.nn[k] / nx
// 						  <<" v2_x "<<p.nn[k+1] % nx
// 						  <<" v2_y "<<p.nn[k+1] / nx
// 						  << endl;
// 
// 			}

			 //REDUCED VECTOR VECR; NON REDUCED V
// 			 cout<<"reducing: "<<endl;
			 
			 
// 			vecr=riduci_vectors(v);
			vandc  =  riduci_full(v,metrics);

			//FIND INTEGER MATRIX			
			m2=find_integer_matrix(v,vandc.v);
// 			fl.m11 = vecr.e1[0];
// 			fl.m22 = vecr.e2[1];
// 			fl.m12 = vecr.e2[0];
// 			fl.m21 = vecr.e1[1];
		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);
// 			m.m11= 1.;
// 			m.m22= 1.;
// 			m.m12= 0;
// 			m.m21= 0;
// 
// 
// 		if(metric_pr.c12 >= 0 && metric_pr.c11 <= metric_pr.c22){
// 			m.m11= 1.;
// 			m.m22= 1.;
// 			m.m12= 0;
// 			m.m21= 0;
// 		}
// 
// 		if(metric_pr.c12 >= 0 && metric_pr.c22 < metric_pr.c11 ){
// 			m.m11= 0;
// 			m.m22= 0;
// 			m.m12= 1.;
// 			m.m21= 1.;
// 		}
// 
// 
// 
// 		if(metric_pr.c12 <= 0 && metric_pr.c11 <= metric_pr.c22){
// 			m.m11=  1.;
// 			m.m22= -1.;
// 			m.m12=  0;
// 			m.m21=  0;
// 		}
// 
// 
// 		if(metric_pr.c12 <= 0 && metric_pr.c22 <= metric_pr.c11 ){
// 			m.m11=  0;
// 			m.m22=  0;
// 			m.m12= -1.;
// 			m.m21=  1.;
// 		}


// 		c.m_field_full[i][k] = m;
// 		minv.m11 = (m.m22/(-m.m12*m.m21 + m.m11*m.m22));
// 		minv.m22 =  m.m11/(-m.m12*m.m21 + m.m11*m.m22);
// 		minv.m12 = -(m.m12/(-m.m12*m.m21 + m.m11 *m.m22));
// 		minv.m21 = -(m.m21/(-m.m12* m.m21 + m.m11* m.m22));
// 		cout<<"print m"<<endl;
// 		m.print();

// 		if(round(minv.m11) == 0.0 &&  round(minv.m22) == 0.0 && round(minv.m12) == 0.0 && round(minv.m21) == 0.0)
		
// 		cout<<"print inv m"<<endl;
// 		minv.print();

// 		vecru.e1[0]  = minv.m11*vecr.e1[0] +  minv.m21*vecr.e2[0];
// 		vecru.e1[1]  = minv.m11*vecr.e1[1] +  minv.m21*vecr.e2[1]; 
// 		
// 		vecru.e2[0]  = minv.m12*vecr.e1[0] +  minv.m22*vecr.e2[0];
// 		vecru.e2[1]  = minv.m12*vecr.e1[1] +  minv.m22*vecr.e2[1]; 
// 		if(isnan(-minv.m12* minv.m21 + minv.m11* minv.m22)){
// 			cout<<"reduced by matrix umut "<<metric_pr.c11<<" "<<metric_pr.c22<<" "<<metric_pr.c12<<endl;
// 			exit(0);
// 		
// 		}
// 		if(k==0){
// 			c.grad_umut[i] = fl;
// // 	 		cout<<"grad_umut"<<endl;
// // 			c.grad_umut[i].print();
// 		}
// 		cout<<"e1x= "<<  vecru.e1[0]<< " "<< vecru.e1[1]<<endl;
// 		cout<<"e2x= "<<  vecru.e2[0]<< " "<< vecru.e2[1]<<endl;

//    			energy_thread =c.energy_map[BLK1::first](metric_rr,burgers,c.alloying[i]);


// 			 energy_thread = energy_zanzotto(metric_rr,burgers,c.alloying[i]);	
// 			cout<<c.disorder[i]<<endl;	

			//in mario's case use not reduced metric
			 energy_thread = c.fptr2(vandc.c,c.disorder[i],c.alloying[i]);		

			 c.energy_local[i] += energy_thread;
			 d1= stress_zanzotto(vandc.c,v,m2,c.disorder[i],c.alloying[i]);
	

//----------------------------------------------------------------------------------------

			
			c.gr[i].x         += d1.gr[0].x;
			c.gr[i].y         += d1.gr[0].y;
// 
//  	  //no need to sum the cross terms
			c.grp1[i][l].x   = d1.gr[1].x ; 
			c.grp1[i][l].y   = d1.gr[1].y ; 
			c.grp1[i][l+1].x = d1.gr[2].x ; 
			c.grp1[i][l+1].y = d1.gr[2].y ; 
		
// 			c.gr[i].x         += d1.gr[0].x;
// 			c.gr[i].y         += d1.gr[0].y;
// 			
// 			c.gr[p.nn[k]].x   += d1.gr[1].x;
// 			c.gr[p.nn[k]].y   += d1.gr[1].y;
// 			c.gr[p.nn[k+1]].x += d1.gr[2].x;
// 			c.gr[p.nn[k+1]].y += d1.gr[2].y; 
			label3: ;

			
			
			
			
		});
// 		}
	}
}

catch (std::exception& e)
{
	cout << e.what() << endl;
}

    //THIS ONE CANNOT BE PARALLALIZED
    
//     for (int k=0; k<BLK1::triangle; ++k) 
 	for (int k=0; k<3; k=k+2) 
    {
		 
		for (int i=0; i<npunti; ++i) 
		{ 
			int l=2*k;
			struct punto_stru p=c.p[i];
			if(!check(c.bc_cond[i][k],2))	
				goto label4;

			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
				cout<<"warning3"<<endl;

			c.gr[p.nn[k]].x   += c.grp1[i][l].x;
			c.gr[p.nn[k]].y   += c.grp1[i][l].y;
			c.gr[p.nn[k+1]].x += c.grp1[i][l+1].x;
			c.gr[p.nn[k+1]].y += c.grp1[i][l+1].y;  
			label4: ;  
    	} 
    }

}

void fix_y_and_periodic_x_BC(int&ix, int&iy, int& k, struct de_stru& d, double& load)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;

	if (ix == nx - 1 && k == 0)    d.p[1].x += dnx;
	if (ix == nx - 1 && k == 3)    d.p[2].x += dnx;
	if (ix == 0 && k == 1)     d.p[2].x -= dnx;
	if (ix == 0 && k == 2)     d.p[1].x -= dnx;

	// 			if(iy == ny-1 && k ==0)    d.p[2].y += dny;
	// 			if(iy == ny-1 && k ==1)    d.p[1].y += dny;
	// 			if(iy == 0 && k   ==2)     d.p[2].y -= dny;
	// 			if(iy == 0 && k   ==3)     d.p[1].y -= dny;  


	//THIS PART IS NOT IMPORTANT SINCE IT CONCERNS ONLY FIXED BOUNDARY UNIT
	//THEY DO NOT MOVE
	//WE ONLY CALCULATE THEM NOT TO BREAK THE LOOP

	//BECAUSE OF LOADING, WE NEED ALSO TO FIX THE X COORDINATES; THE BOX CHANGES ITS SHAPE

	// 		if(iy == ny-1 && k ==0)    d.p[2].y = dny;
	// 		if(iy == ny-1 && k ==1)    d.p[1].y = dny;
	// 		if(iy == 0 && k   ==2)     d.p[2].y = -1.;
	// 		if(iy == 0 && k   ==3)     d.p[1].y = -1.;  
	// 
	// 
	// 		if(iy == ny-1 && k ==0)    d.p[2].x =  dix + load*(diy+1-(dny-1)/2)/(dny-1.);
	// 		if(iy == ny-1 && k ==1)    d.p[1].x =  dix + load*(diy+1-(dny-1)/2)/(dny-1.);
	// 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + load*(diy-1-(dny-1)/2)/(dny-1.);
	// 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + load*(diy-1-(dny-1)/2)/(dny-1.);        

	//THIS PART IS NOT IMPORTANT SINCE IT CONCERNS ONLY FIXED BOUNDARY UNIT
	//THEY DO NOT MOVE
	//WE ONLY CALCULATE THEM NOT TO BREAK THE LOOP
	// 		if(iy == ny-1 && k ==0)    d.p[2].x =  dix + load*(diy+1)/(dny-1.);
	// 		if(iy == ny-1 && k ==1)    d.p[1].x =  dix + load*(diy+1)/(dny-1.);
	// 		
	// // 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + load*(diy-1)/(dny-1.);
	// // 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + load*(diy-1)/(dny-1.);     
	// 
	// 		//IT IS NOT NECESSARY TO MULTIPLY WITH LOAD HERE SINCE u(iy=0) = 0
	// 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + (diy-1)/(dny-1.);
	// 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + (diy-1)/(dny-1.);     

}
//  				per_BC(ix,iy,k,d,c);


void write_to_a_file_with_location(struct conf_stru& c, int t, const string f1);
double rosen(const column_vector& m)
/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/
{
	double x, y,sum;
	int i,ix,iy,nx,ny;
	double dnx,dny,size,sum2;
	nx=dimensions::nx ;
	ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

// 	double load=loading::testNum ;
	double load=BLK1::load;
	double dt=BLK1::dt*10;
	static int step = 1;
	static int step2 = 0;

	static int t = 0;
	
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number/2.;
	
    
	dnx=nx;
	dny=ny;
	size=dnx*dny;




	for(int i=0;i<npunti;++i)
	{ 
        force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x + m(i);
        force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y + m(i+npunti);
	} 

	if(fexists("roule") == false)
	{ 
		cout<<"CODE HAS BEEN STOPPED IN THE CG"<<endl;
		exit (EXIT_FAILURE); 
	}
	


    	if((step%BLK1::cg_freq ==0 && BLK1::cg_save == 1))
    	{
    		step =1;
//     		exit(1);

//     		ps_file(force_energy::calculation::getInstance().c,t);
//     		slicer(force_energy::calculation::getInstance().c,t);
    		string CG = "CG";
    		result_memorize(force_energy::calculation::getInstance().c);
    		write_to_a_file(force_energy::calculation::getInstance().c, t);
			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);
//     		cout<<"iteration:  "<<step<<" energy: "<<sum<<endl;

    	}

	step2++;
	
  
    ++step;
	apply_BC(force_energy::calculation::getInstance().c,load);
	calcstresses_CG_en(force_energy::calculation::getInstance().c);
	apply_BC(force_energy::calculation::getInstance().c,load);
	
	sum = 0.;
	
	if(BLK1::method.compare("dynamic") ==0)
	{
		for(int i=0;i<n;++i)
		{ 

			sum+=0.5*pow(m(i)   - force_energy::calculation::getInstance().c.disp[i].x + (dt)*force_energy::calculation::getInstance().c.rfx[i] ,2.) 
								+ dt*force_energy::calculation::getInstance().c.energy_local[i];
			sum+=0.5*pow(m(i+n) - force_energy::calculation::getInstance().c.disp[i].y + (dt)*force_energy::calculation::getInstance().c.rfy[i] ,2.) 
								+ dt*force_energy::calculation::getInstance().c.energy_local[i];
		} 	
	}

	if(BLK1::method.compare("statique") ==0)
	{

		for(int i=0;i<n;++i)
		{ 
			sum+= force_energy::calculation::getInstance().c.energy_local[i];
// 			cout<<"energy at node i= "<<i<< " " <<force_energy::calculation::getInstance().c.energy_local[i]<<endl;
    	}
    

    }



// 	cout<<"sum="<<sum/(BLK1::triangle*1.0*n)<<endl;

	return sum/(n_special);
// 	return sum/(2.0*n);

}

const column_vector rosen_derivative(const column_vector& m)
/*!
ensures
- returns the gradient vector for the rosen function
!*/
{

	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

	double dt=BLK1::dt*10;
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number/2.;

	
	column_vector res;
	res.set_size(2*n);

	if(BLK1::method.compare("statique") ==0)
	{
		for(int i=0;i<n;i++)
		{ 
			double x = force_energy::calculation::getInstance().c.gr[i].x;
			double y = force_energy::calculation::getInstance().c.gr[i].y; 
			res(i)     = x/(n_special);
			res(i+n)   = y/(n_special);
// 			cout<<"forces x at node i= "<<i<< " " <<res(i) <<endl;
// 			cout<<"forces y at node i= "<<i<< " " <<res(i+n) <<endl;

// 			res(i)     = x/(2.0*n);
// 			res(i+n)   = y/(2.0*n);

		}
	}

	
	if(BLK1::method.compare("dynamic") ==0)
	{
		for(int i=0;i<n;i++)
		{
			res(i)  =(m(i)   - force_energy::calculation::getInstance().c.disp[i].x 
							+ dt*force_energy::calculation::getInstance().c.gr[i].x
							+ (dt)*force_energy::calculation::getInstance().c.rfx[i]);
		
			res(i+n)=(m(i+n) - force_energy::calculation::getInstance().c.disp[i].y 
							+ dt*force_energy::calculation::getInstance().c.gr[i].y
							+ (dt)*force_energy::calculation::getInstance().c.rfy[i]);


		}
	}
    return res;
}


// This function computes the Hessian matrix for the rosen() fuction.  This is
// the matrix of second derivatives.
dlib::matrix<double> rosen_hessian(const column_vector& m)
{
	const double x = m(0);
	const double y = m(1);

	dlib::matrix<double> res(2, 2);

	// now compute the second derivatives 
	res(0, 0) = 1200 * x*x - 400 * y + 2; // second derivative with respect to x
	res(1, 0) = res(0, 1) = -400 * x;   // derivative with respect to x and y
	res(1, 1) = 200;                 // second derivative with respect to y
	return res;
}

void per_BC_hex(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;

	boundary_conditions f;
	f.setParameters(BLK1::theta,BLK1::load);
	if(BLK1::macro_def.compare("shear") ==0)
		f.macro_shear();
	if(BLK1::macro_def.compare("tension") ==0)
		f.macro_compression();
		//deform translation vectors

	double m=(double) nx *   BLK1::symmetry_constantx;
	double dx  =  ( + m) * f.f.f11;
	double dy  =  ( + m) * f.f.f21;


//*********************************************

		if (iy%2 != 0 && ix == 0 && k == 2)
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;
		}
		if (iy%2 != 0 && ix == 0 && k == 3)
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
		}


//*********************************************

		if (iy%2 == 0 && ix == nx-1  && k == 0)
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}
		if (iy%2 == 0 && ix == nx-1 && k == 5)
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
		}


//*********************************************

		if (iy%2 != 0 && ix ==  nx-1 && k == 0 )
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
			d.p[2].x += dx;
			d.p[2].y += dy;

		}
		if (iy%2 != 0 && ix ==  nx-1 && k == 1 )
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}

		if (iy%2 != 0 && ix ==  nx-1 && k == 4 )
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
		}
		if (iy%2 != 0 && ix ==  nx-1 && k == 5 )
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
			d.p[1].x += dx;
			d.p[1].y += dy;

		}



//*********************************************


		if (iy%2 == 0 && ix == 0 && k == 1 )
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;
		}
		if (iy%2 == 0 && ix ==  0 && k == 2 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
			d.p[2].x -= dx;
			d.p[2].y -= dy;

		}

		if (iy%2 == 0 && ix ==  0 && k == 3 && iy >0 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
			d.p[2].x -= dx;
			d.p[2].y -= dy;


		}
		if (iy%2 == 0 && ix ==  0 && k == 4 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
		}


//*********************************************

	double m2=(double) ny *   BLK1::symmetry_constanty;
	double dx2  =  ( + m2) * f.f.f12;
	double dy2  =  ( + m2) * f.f.f22;
	
	
		if (iy%2 != 0 && iy ==  ny-1 && k == 0 )
		{
			d.p[2].x += dx2;
			d.p[2].y += dy2;
		}	

		if (iy%2 != 0 && iy ==  ny-1 && k == 1 )
		{
			d.p[2].x += dx2;
			d.p[2].y += dy2;

			d.p[1].x += dx2;
			d.p[1].y += dy2;

		}	


		if (iy%2 != 0 && iy ==  ny-1 && k == 2 )
		{

			d.p[1].x += dx2;
			d.p[1].y += dy2;

		}	



		if (iy ==  0 && ix ==0 && k == 3)
		{

			d.p[1].x -= dx;
			d.p[1].y -= dy;

			d.p[2].x -= dx2+dx;
			d.p[2].y -= dy2+dy;


		}	

		if (iy ==  0 && ix >0 && k == 3 )
		{

			d.p[2].x -= dx2;
			d.p[2].y -= dy2;

		}	


		if (iy ==  0 && ix >=0 && k == 4 )
		{

			d.p[1].x -= dx2;
			d.p[1].y -= dy2;

			d.p[2].x -= dx2;
			d.p[2].y -= dy2;

		}	


		if (iy ==  0 && ix >=0 && k == 5 )
		{

			d.p[1].x -= dx2;
			d.p[1].y -= dy2;


		}	


}
//void per_BC_MJ(struct de_stru& d, int i, struct conf_stru& c,int k)
void per_BC_MJ(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{
		double load =BLK1::load;
		double theta=BLK1::theta;
		double dx,dy;


		boundary_conditions f;
		f.setParameters(theta,load);
		if(BLK1::macro_def.compare("shear") ==0)
			f.macro_shear();
		if(BLK1::macro_def.compare("tension") ==0)
			f.macro_compression();
		//deform translation vectors
		


		if( c.p[i].pu[k] !=-1){
		    dx =  c.p[c.p[i].pu[k]].tvx * f.f.f11  + c.p[c.p[i].pu[k]].tvy * f.f.f12;
		    dy =  c.p[c.p[i].pu[k]].tvx * f.f.f21  + c.p[c.p[i].pu[k]].tvy * f.f.f22;
		}



		//left


		if (c.p[i].which_boundary == 2 && k ==1)
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;

		}



		if (c.p[i].which_boundary == 2 && k==2 )
		{
	
			d.p[1].x -= dx;
			d.p[1].y -= dy;

		}

		//right

		if (c.p[i].which_boundary == 1 && k==0 )
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}



		if (c.p[i].which_boundary == 1 && k ==3)
		{
	
			d.p[2].x += dx;
			d.p[2].y += dy;
		}


		//corners
		int c2= c.p[i].corner;
// 		std::cout<<"c2: "<<c2<<std::endl;

		//right top
		if(c2 ==1 && k==3  )
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
		}
	

		if(c2 ==2  && k==0)
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}
		
		//left top
		if(c2 ==3  && k==2)
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
		}
		
		if(c2 ==4  && k==1)
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;
		}

}


void per_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;
	//this is periodic because d.p[].x and d.p[].y s keeps displacement of periodic friend
	if (BLK1::crystal_symmetry.compare("tilted") != 0)
	{


		// 		if(BC.compare("pbc")  ==0 || BC.compare("pbcy")  ==0 )
		// 	    {
		if (ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
		}
		if (ix == nx - 1 && k == 3)
		{
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}



		if (ix == 0 && k == 2)
		{
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;
		}

		if (ix == 0 && k == 1)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
		}

		// 		}




		//PERIODIC IN Y
		// 		if(BC.compare("pbcy") ==0 || BC.compare("pbcyfx") ==0 )
		// 	    {
		if (iy == ny - 1 && k == 0)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;
		}


		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
		}

		if (iy == 0 && k == 2)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;
		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
		}

		// 		}

	}


	else if (BLK1::crystal_symmetry.compare("tilted") == 0)
	{

		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 0)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
		}

		if (iy % 2 == 0 && ix == 0 && k == 2)
		{
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;
		}

		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 2)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 2)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}


		//ok---------------------------------

		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
		}

		if (iy % 2 != 0 && ix == nx - 1 && k == 2)
		{
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}

		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 0)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}




		//ok---------------------------------


		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 1)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;

		}



		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 1)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 1)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;

		}


		//ok---------------------------------
		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 3)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}



		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 3)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 3)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;

		}



	}
}

void fix_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;
	//this is periodic because d.p[].x and d.p[].y s keeps displacement of periodic friend
	if (BLK1::crystal_symmetry.compare("tilted") != 0)
	{


		// 		if(BC.compare("pbc")  ==0 || BC.compare("pbcy")  ==0 )
		// 	    {
		if (ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.right[iy].x;
			d.p[1].y = c.right[iy].y;
		}
		if (ix == nx - 1 && k == 3)
		{
			d.p[2].x = c.right[iy].x;
			d.p[2].y = c.right[iy].y;
		}



		if (ix == 0 && k == 2)
		{
			d.p[1].x = c.left[iy].x;
			d.p[1].y = c.left[iy].y;
		}

		if (ix == 0 && k == 1)
		{
			d.p[2].x = c.left[iy].x;
			d.p[2].y = c.left[iy].y;
		}

		// 		}




		//PERIODIC IN Y
		// 		if(BC.compare("pbcy") ==0 || BC.compare("pbcyfx") ==0 )
		// 	    {
		if (iy == ny - 1 && k == 0)
		{
			d.p[2].x = c.top[ix].x;
			d.p[2].y = c.top[ix].y;
		}


		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x = c.top[ix].x;
			d.p[1].y = c.top[ix].y;
		}

		if (iy == 0 && k == 2)
		{
			d.p[2].x = c.bottom[ix].x;
			d.p[2].y = c.bottom[ix].y;
		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x = c.bottom[ix].x;
			d.p[1].y = c.bottom[ix].y;
		}

		// 		}

	}


	else if (BLK1::crystal_symmetry.compare("tilted") == 0)
	{

		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 0)
		{
			d.p[2].x = c.left_per[iy].x;
			d.p[2].y = c.left_per[iy].y;
		}

		if (iy % 2 == 0 && ix == 0 && k == 2)
		{
			d.p[1].x = c.left_per[iy].x;
			d.p[1].y = c.left_per[iy].y;
		}

		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 2)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 2)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}


		//ok---------------------------------

		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.right_per[iy].x;
			d.p[1].y = c.right_per[iy].y;
		}

		if (iy % 2 != 0 && ix == nx - 1 && k == 2)
		{
			d.p[2].x = c.right_per[iy].x;
			d.p[2].y = c.right_per[iy].y;
		}

		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 0)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}




		//ok---------------------------------


		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 1)
		{
			d.p[2].x = c.left_per[iy].x;
			d.p[2].y = c.left_per[iy].y;
			d.p[1].x = c.left_per[iy].x;
			d.p[1].y = c.left_per[iy].y;

		}



		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 1)
		{
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 1)
		{
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;

		}


		//ok---------------------------------
		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 3)
		{
			d.p[1].x = c.right_per[iy].x;
			d.p[1].y = c.right_per[iy].y;
			d.p[2].x = c.right_per[iy].x;
			d.p[2].y = c.right_per[iy].y;
		}



		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 3)
		{
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 3)
		{
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;

		}



	}
}

void apply_BC(struct conf_stru& c, double& load)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();

	dny = ny;
	string_to_integer bset;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
    if(bset.x ==0 ||  bset.x ==0){	
		for (int k=0; k<BLK1::triangle; ++k) 
			for (int i=0; i<n; ++i)
			{
				if(check(c.bc_cond[i][k],0))
				{
					 c.p[i].x     = c.full[i].x     ; //c.gr[i].x = 0;
					 c.p[i].y     = c.full[i].y     ; //c.gr[i].y = 0;
				}

			 }
	}
					
	
}


void apply_BC_box(struct conf_stru& c,column_vector& sp_min, column_vector& sp_max)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();

	dny = ny;
	string_to_integer bset;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
	
	
	for (int k=0; k<BLK1::triangle; ++k) 
		for (int i=0; i<n; ++i)
		{
			if(check(c.bc_cond[i][k],0))
			{
				 sp_min(i)    = c.p[i].x -c.pfix[i].x ;
				 sp_min(i+n)  = c.p[i].y -c.pfix[i].y ;
				 sp_max(i)    = c.p[i].x -c.pfix[i].x ;
				 sp_max(i+n)  = c.p[i].y -c.pfix[i].y ;
				 
			}


		 }
					
	
}


void apply_BC_full_generale(struct conf_stru& c, struct boundary_conditions& f)
{
	
	//it should be called once to change initial material coo
	double dnx,dny,diy;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
//  	dny = ny/2-1;
//  	dnx = nx/2-1;
 	dny = c.dny;
 	dnx = c.dnx;

				
	int n= c.p.size();
				
			for(int i =0;i<n;++i)
			{
				int iy = i / nx;
				int ix = i % nx;
// 				c.p[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.p[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;

// 				c.pfix[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
// 				
// 				c.pfix[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y  * f.f.f22;

				if(BLK1::BC.compare("special") == 0)
				{
					c.p[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;				
					c.p[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;
				}

		
			}



      			
   
}

void apply_BC_generale(struct conf_stru& c, struct boundary_conditions& f)
{
	double dny,diy,dnx;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();

//  	dny = ny/2-1;
//  	dnx = nx/2-1;

 	dny = c.dny;
 	dnx = c.dnx;

 	double theta_d = f.theta_d;
 	double load_in = f.load_in;


//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
// 
//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;
   	
//    	struct def_grad f = combiner(load_in,theta_d,macro_shear);

			double k,m;
//----------------------periodic boundaries------------------------------------
				
			for(int i =0;i<ny;++i)
			{				
				k=0;
				m=(double) nx *  BLK1::symmetry_constantx;
				c.left_per[i].x  = (-k + m) * f.f.f11;
				c.left_per[i].y  = (-k + m) * f.f.f21;
				
			}

			
			for(int i =0;i<ny;++i)
			{				
				k=0;
				m=(double) nx *   BLK1::symmetry_constantx;
				c.right_per[i].x  =  (-k + m) * f.f.f11;
				c.right_per[i].y  =  (-k + m) * f.f.f21;
			}
			
			for(int i =0;i<nx;++i)
			{				
				k=0;
				m=(double) ny *   BLK1::symmetry_constanty;
				c.top_per[i].x  = (-k + m)* f.f.f12;
				c.top_per[i].y  = (-k + m)* f.f.f22;
				
			}

			
			for(int i =0;i<nx;++i)
			{				
				k=0;
				m=(double) ny*   BLK1::symmetry_constanty;
				c.bottom_per[i].x  = (-k + m) * f.f.f12;
				c.bottom_per[i].y  = (-k + m) * f.f.f22;
			}			
			
			
//----------------------fix boundaries------------------------------------
			

			for(int i =0;i<ny;++i)
			{
				
				int iy;
				iy = i;
				int ix;
				ix=nx;

				int k = iy*nx + ix;


// 				c.right[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12;
// 				
// 				c.right[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22;
				
				c.right[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
				
				c.right[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;

				
				ix=-1;
				
				k = iy*nx + ix;
				
// 				c.left[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12;
// 				
// 				c.left[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22;

				c.left[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
				
				c.left[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;
	
			}



			for(int i =0;i<nx;++i)
			{
				int ix;
				ix = i;
				int iy;
				iy = -1;
				
				int k = iy*nx + ix;
				
				
				c.bottom[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
				
				c.bottom[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;


// 				c.bottom[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12  ;
// 				
// 				c.bottom[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22  ;

			    iy = ny;
			    k = iy*nx + ix;
				
// 				c.top[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12 ;
// 				
// 				c.top[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22 ;
				
				c.top[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
				
				c.top[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;

			}
			
			
// 			for(int i =0;i<nx;++i)
// 			{
// 				int ix;
// 				ix = i;
// 				int iy=ny-1;
// 
// // 				c.top[i].x = ((double) ix-dnx) * (1. - load_in* cos(theta_d)*sin(theta_d)) +
// // 						     ((double) iy-dny) * load_in* pow(cos(theta_d),2.);
// // 				c.top[i].y =-((double) ix-dnx) * load_in* pow(sin(theta_d),2.) +
// // 						     ((double) iy-dny) * (1. +  load_in*cos(theta_d)*sin(theta_d));
// 
// 				c.top[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.top[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;
// 
// 
// 
// 			}						
// 			

			for(int i =0;i<n;++i)
			{	
					int iy = i / nx;
					int ix = i % nx;

// 					c.full[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
// 			
// 					c.full[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y  * f.f.f22;

					
					
					c.full[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
			
					c.full[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y  * f.f.f22;
					

// 					c.full[i].x = ((double) ix-dnx) * (1. - load_in* cos(theta_d)*sin(theta_d)) +
// 								  ((double) iy-dny) * load_in* pow(cos(theta_d),2.)             - 
// 								  ((double) ix-dnx)  ;
// 					c.full[i].y =-((double) ix-dnx) * load_in* pow(sin(theta_d),2.) +
// 								  ((double) iy-dny) * (1. +  load_in*cos(theta_d)*sin(theta_d)) -
// 								  ((double) ix-dnx) ;
								  
// 					double m = ((double) ix-dnx);
// 					double n = ((double) iy-dny);
// // 					c.full[i].x = -(load_in*n*pow(cos(theta_d),2))       + load_in*m*cos(theta_d)*sin(theta_d);
// // 					c.full[i].y = -(load_in*n*cos(theta_d)*sin(theta_d)) + load_in*m*pow(sin(theta_d),2);
// 					c.full[i].x = -n*f.f.f12       +m*(1.-f.f.f11 );
// 					c.full[i].y =  n*(1.-f.f.f22 ) -m*f.f.f21;

      			}

}

// void apply_BC_generale(struct conf_stru& c, struct boundary_conditions& f)
// 
// {
// 	double dny,diy,dnx;
// 	int nx=dimensions::nx ;
// 	int ny=dimensions::ny ;
//  	dny = ny/2-1;
//  	dnx = nx/2-1;
//  	double theta_d = f.theta_d;
//  	double load_in = f.load_in;
// 
// 
//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
// 
//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;
//    	
// //    	struct def_grad f = combiner(load_in,theta_d,macro_shear);
// 
// 			double k,m;
// //----------------------periodic boundaries------------------------------------
// 
// 			
// 			
// //----------------------fix boundaries------------------------------------
// 			
// 
// 			for(int i =0;i<nx*ny;++i)
// 			{
// 
// 				c.pfix[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.pfix[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;
// 
// 				
// 	
// 			}
// 
// }

struct conf_stru  timestep_euler(struct conf_stru& c, double& load)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int npunti = c.p.size();
	double dt = BLK1::dt;



	apply_BC(c, load);
	calcstresses_CG_en(c);
	apply_BC(c, load);

	struct conf_stru guess;
	guess.p.resize(c.p.size());
	guess.disp.resize(c.p.size());


	for (int i = 0; i<npunti; i++)
	{

		guess.disp[i].x = c.disp[i].x - dt*(c.gr[i].x) + dt*c.rfx[i];
		guess.disp[i].y = c.disp[i].y - dt*(c.gr[i].y) + dt*c.rfy[i];

		//KEEP DATA AT OLDER TIME BEFORE UPDATING
		//UPDATE 	  	
	}

	// 	double sum=0; 
	// 	for (int i=0; i<nx*ny; i++) 	
	// 		sum+=c.energy_local[i];


	// 	cout<<"energy= "<<sum<<endl;
	return guess;

}


void  timestep_euler2(struct conf_stru& c, double& load)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int npunti = c.p.size();
	double dt = BLK1::dt;



	apply_BC(c, load);
	calcstresses_CG_en(c);
	apply_BC(c, load);

	//     struct conf_stru guess;
	//     guess.p.resize(nx*ny);


	for (int i = 0; i<npunti; i++)
	{

		c.p[i].x -= dt*(c.gr[i].x);
		c.p[i].y -= dt*(c.gr[i].y);

		//KEEP DATA AT OLDER TIME BEFORE UPDATING
		//UPDATE 	  	
	}


}



