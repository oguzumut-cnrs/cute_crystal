#include "reductions.h"
struct cella_stru riduci(struct cella_stru c)
{

	if (c.c12<0)
	{
		c.c12 = -c.c12;

	}
	if (c.c22<c.c11)
	{
		double a;
		a = c.c11;
		c.c11 = c.c22;
		c.c22 = a;
	}

	if (2 * c.c12>c.c11)
	{
		struct cella_stru d;
		d.c11 = c.c11;
		d.c12 = c.c12 - c.c11;
		d.c22 = c.c22 + c.c11 - 2 * c.c12;
		return riduci(d);
	}



	return c;
}