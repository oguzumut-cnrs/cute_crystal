#pragma once

#include "common.h"
#include "structures.h"
#include "dlib.h"
#include "vector_functions.h"
#include "plot.h"
#include <iostream>
#include <cmath>
#include "namespaces.h"

#include "dlib.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "structures.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "custom_inline_functions.h"
#include <iostream>
#include <map>
#include <dlib/matrix.h>
#include <iostream>
#include "dlib.h"

using namespace dlib;
#include <dlib/matrix.h>

#include <boost/random.hpp>
#include <boost/numeric/ublas/io.hpp>

//#include "polar_decomposition.h"
#include "rotation.h"
#include "dlib.h"

     
void energy_hessian_plotter(struct conf_stru& c);	
void energy_hessian_plotter_v2(struct conf_stru& c);	

	
