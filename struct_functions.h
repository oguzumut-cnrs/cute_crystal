#pragma once
#include <vector>
#include <array>
#include "structures.h"
#include "namespaces.h"
struct base_stru elastic_vectors(base_stru c);
struct matrix_stru f_plastic(base_stru c);
using namespace std;
struct cella_stru riduci_matrix_operation(struct cella_stru& c, struct matrix_stru& m);
struct cella_stru riduci_matrix_reductions_stress(const struct cella_stru& c, const struct matrix_stru& m);
struct cella_stru riduci_matrix_reductions_stress_inverse(const struct cella_stru& c,  struct matrix_stru& m2);


//*****stress functions*****////
struct cella_stru stress_easy(const struct cella_stru& c, double burgers, double alloy);
struct cella_stru stress_log(const struct cella_stru& c, double burgers, double alloy);
struct cella_stru stress_lj(const struct cella_stru& c, double burgers, double alloy);
struct cella_stru stress_morse(const struct cella_stru& c, double burgers, double alloy);

//*************************////

struct cella_stru riduci(struct cella_stru c);
struct cella_stru riduci_pitteri(struct cella_stru c, int flag1, int flag2);

struct cella_stru riduci_umut(struct cella_stru c);
struct cella_stru riduci_matthieu(struct cella_stru c);

struct cella_stru faicella(const struct base_stru& v1);
struct base_stru pre_riduci_vectors(struct matrix_stru& m, const struct base_stru& vec_nr);
struct base_stru riduci_vectors(struct base_stru c);
struct matrix_stru  find_integer_matrix(const struct base_stru& vec_nr, const struct base_stru& vec);
struct cella_stru faicella2(struct punto_stru p1, struct punto_stru p2, struct punto_stru p3);
struct matrix_stru  curl1(const  de_stru& d2, const  de_stru& d,int ne);

struct base_stru riduci_vectors_pitteri(struct base_stru c);
struct base_stru  vectorize(const struct de_stru& rhs);
struct cella_stru stress_isotrope(const struct cella_stru& c, double burgers, double alloy);
struct base_stru riduci_vectors_umut(struct base_stru c);
struct spec_for_reduction riduci_full(const struct base_stru v,const struct cella_stru c);
 int riduci_vectors_wellnumber(struct base_stru c);

struct cella_stru stress_niko(const struct cella_stru& c, double burgers, double alloy);


//ROTATION OPERATIONS FOR PC

struct cella_stru rotation_sym(const  struct cella_stru& c,const  struct matrix_stru& Q);

struct matrix_stru rotation_nonsym(const  struct matrix_stru& P,const  struct matrix_stru& Q);
