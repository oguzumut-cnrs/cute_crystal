// #include "itensor/all.h"
// #define ARMA_ALLOW_FAKE_GCC 
// #define ARMA_DONT_USE_WRAPPER
// 
// #define ARMA_DONT_USE_HDF5
#include <dlib/matrix.h>

#include "dlib/threads.h"
#include <dlib/optimization.h>
#include "dlib/graph_utils_threaded.h"
#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>
#include "dlib.h"
#include <omp.h>
#include "calculations.h"

#include "calculations_alglib.h"
#include "newton_raphson.h"
#include "allocate.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "calculations.h"

#include "init.h"
#include "common.h"
#include "input_output.h"

#include "dynamic_static.h"
//#include "non_iterative_static.h"
#include "plot.h"
#include "metric_constructor_test.h"
#include "acoustic_tensor.h"
#include "energy_map.h"
#include "atomistic_grid.h"
// #include "spline.h"
#include "structures.h"
#include "required.h"
#include "utilities.h"
#include "plot_energy_hessian.h"
#include "assign_solution.h"
#include "rotation_roberta.h"

// #define ARMA_ALLOW_FAKE_CLANG
// #define ARMA_ALLOW_FAKE_GCC
// #define ARMA_DONT_USE_WRAPPER
// #define ARMA_DONT_USE_HDF5
// #define ARMA_DONT_USE_BLAS
#include "second.h"
#include "ap.h"
#include "stdafx.h"

#include <fstream>
#include <iostream>



using namespace dlib;

using namespace std;
using namespace dlib;
void angle_shear(double& theta);
// void angle_shear_spline(double, BSpline&);
void atensor_analysis(double theta);
struct base_stru elastic_vectors(base_stru c);
struct matrix_stru f_plastic(base_stru c);

void energy_landscape_gorbushin();
#include <armadillo>

using namespace arma; 


int main()
{

   
    	read_input_file();
    	required();
    


	
		int nx = dimensions::nx;
		int ny = dimensions::ny;
		int n = nx*ny;

		ofstream sizes, sizes2;
		sizes.open("information_tailles");
		sizes << " " << nx << "   " << ny << "   " << 1 << "   " << "mode sauvegarde: complete" << "\n" << " 3" << "	" << "0" << "\n";
		sizes.close();

		sizes2.open("sizes.dat");
		sizes2 << nx << endl;
		sizes2 << ny << endl;
		sizes2.close();



		//initiate SINGLETON c (its name is c)
		initiate_coo(n, nx, ny);
		force_energy::calculation::getInstance().c.dnx = 0.;
		force_energy::calculation::getInstance().c.dny = 0.;
		
		if(BLK1::crystal_symmetry.compare("square") == 0 || BLK1::crystal_symmetry.compare("tilted") == 0 ){
			force_energy::calculation::getInstance().c.inc = 2;
			double temp= dimensions::dh;
			if(round(temp) !=1 )
				temp = 1./(1.*nx);
			dimensions::setNumparam(3, temp);
			
		}
// 		if(BLK1::crystal_symmetry.compare("square") == 0 || BLK1::crystal_symmetry.compare("tilted") == 0 )
// 			force_energy::calculation::getInstance().c.inc = 1;

		if(BLK1::crystal_symmetry.compare("hex") == 0){
			force_energy::calculation::getInstance().c.inc = 3;
			double temp= dimensions::dh;
			if(round(temp) !=1 )
				temp = 1./(1.*nx);	
			dimensions::setNumparam(4, temp);
			string lattice = "six_triangle";
			BLK1::setGrid_creator(lattice);
		}

		
		boundary_conditions set1;
		//loading parameter
		double initial_strain = BLK1::initial_strain;
		initial_strain = initial_strain;
		BLK1::setNum(initial_strain);

		double load = BLK1::load;
		double theta = BLK1::theta;

		//INITIAL GRID AND EVERYTHING
		if(BLK1::grid_creator.compare("cutting") ==0){
			//keep below the RHS of if condition until hex is implemented properly
			if(BLK1::crystal_symmetry.compare("square") == 0 || BLK1::crystal_symmetry.compare("square") == 0 ){
				set1.triangle_number(4);
				if(BLK1::bc_x.compare("pbc") ==0 || BLK1::bc_y.compare("pbc") ==0 ){
					cout<<"warning, no PBC for cutting crystal"<<endl;
					cout<<"warning, changing it to free fir cutting crystal"<<endl;
					cout<<"exiting from the program"<<endl;
    				exit (EXIT_FAILURE);
				}
				new_init_rotated_square(force_energy::calculation::getInstance().c,BLK1::cutting_angle);
				assign_bc_MJ(force_energy::calculation::getInstance().c);
				write_to_a_ovito_file(force_energy::calculation::getInstance().c,999);
// 				exit (EXIT_FAILURE);
			}

			if(BLK1::crystal_symmetry.compare("hex") == 0){
				if(BLK1::bc_x.compare("pbc") ==0 || BLK1::bc_y.compare("pbc") ==0 ){
					cout<<"warning, no PBC for cutting crystal"<<endl;
					cout<<"warning, changing it to free fir cutting crystal"<<endl;
					cout<<"exiting from the program"<<endl;
    				exit (EXIT_FAILURE);
				}	
				set1.triangle_number(6);			
				new_init_rotated_hex(force_energy::calculation::getInstance().c,BLK1::cutting_angle);
				assign_bc_hex_MJ(force_energy::calculation::getInstance().c);
				write_to_a_ovito_file(force_energy::calculation::getInstance().c,999);
// 				exit (EXIT_FAILURE);
			}

		}

// 		cout<<"inithex6"<<endl;
		if(BLK1::grid_creator.compare("six_triangle") ==0){
			set1.triangle_number(6);
// 			new_init_tilted_square(force_energy::calculation::getInstance().c);
			initHex6(force_energy::calculation::getInstance().c);
			assign_bc(force_energy::calculation::getInstance().c);
		}

		if(BLK1::grid_creator.compare("classic") ==0){
 			cout<<"classic method with 4 triangles"<<endl;
 			set1.triangle_number(4);
 			init(force_energy::calculation::getInstance().c);
			assign_bc(force_energy::calculation::getInstance().c);		
		}
//  		atensor_analysis(BLK1::theta*dlib::pi/180);

	   if(BLK1::grid_creator.compare("classic") ==0)
		  force_energy::calculation::getInstance().c.fptr = &per_BC; //fptr -> one
	   if(BLK1::grid_creator.compare("six_triangle") ==0)
		  force_energy::calculation::getInstance().c.fptr = &per_BC_hex; //fptr -> one
	   if(BLK1::grid_creator.compare("cutting") ==0)
		  force_energy::calculation::getInstance().c.fptr = &per_BC_MJ; //fptr -> one	           	  

	   //reference state  never moves
	   force_energy::calculation::getInstance().c.pref  = force_energy::calculation::getInstance().c.pfix;



		//WRITE INITIAL CONDITONS
		cout<<"writing initial conditions"<<endl;
// 		write_to_a_ovito_file(force_energy::calculation::getInstance().c, 0);
// 		write_to_a_file(force_energy::calculation::getInstance().c, 0);
                
  		cout<<"boundary configurations"<<endl;
		set1.setParameters(theta,load);
		choose_bc(set1);

        if(BLK1::method.compare("ni")==0){
        	set1.setParameters(theta,0);
			choose_bc(set1);
		}

		
		//initial deformation that updates c.p; does NOT work for full periodic BC
		//because we deform frame y =/bar F + u
		cout<<"apply_BC_full_generale"<<endl;
		apply_BC_full_generale(force_energy::calculation::getInstance().c, set1);
			
		//this calculates all the fixed and periodic boundary condition
		//C.FULL WHICH IS THE COO OF DEFORMED LATTICE
		//therefore it must be called every time

		cout<<"apply_BC_generale"<<endl;
        apply_BC_generale(force_energy::calculation::getInstance().c,set1);
// 		//WRITE INITIAL CONDITONS
// 		write_to_a_ovito_file(force_energy::calculation::getInstance().c, 1);
// 		write_to_a_file(force_energy::calculation::getInstance().c, 1);
                

		if(BLK1::model.compare("CG") ==0){
		
			if(BLK1::method.compare("statique")==0         || BLK1::method.compare("statique_eigen")==0 || 
			   BLK1::method.compare("statique_newton")==0  || BLK1::method.compare("no_minim")==0       || 
			   BLK1::method.compare("statique_eigen_ve")==0){
				cout<<"method: "<<BLK1::method<<endl;
				statique_alglib(force_energy::calculation::getInstance().c);
			}

	// 		if(BLK1::method.compare("statique_alglib")==0)
	// 			statique_alglib(force_energy::calculation::getInstance().c);

			
	// 		if(BLK1::method.compare("ni")==0)
	// 			non_iterative_statique(force_energy::calculation::getInstance().c);
			
			
			if(BLK1::method.compare("hl")==0){
				cout<<"entering to  homogenous_frame_load"<<endl;
				homogenous_frame_load(force_energy::calculation::getInstance().c);		
			}
		
			else if(BLK1::method.compare("second") ==0){
				cout<<"second variation starts"<<endl;
				int k= K_matrix(force_energy::calculation::getInstance().c);
		    }

			else if(BLK1::method.compare("manipulation") ==0){

				

				
				string neigbor_list  =  "neigbor_list.txt";

				ofstream file_neigbor_list;
				file_neigbor_list.open(neigbor_list.c_str());
				for(int i=0;i<force_energy::calculation::getInstance().c.p.size();++i){
					file_neigbor_list << std::scientific << std::setprecision(16)<<i<<" "<<
					force_energy::calculation::getInstance().c.p[i].nn[0]<<" "<<
					force_energy::calculation::getInstance().c.p[i].nn[1]<<" "<<
					force_energy::calculation::getInstance().c.p[i].nn[2]<<" "<<
					force_energy::calculation::getInstance().c.p[i].nn[3]<<endl;
					
				}
				
				file_neigbor_list.close();
			
				
				string filename_loading  =  BLK1::config_u;
				cout<<"input name:  "<<filename_loading<<"\n"; 
				cout<<"searching for the file:  "<<filename_loading<<"\n"; 
				if(exist(filename_loading) == false){ 
					cout<<"no file sorry  "<<filename_loading<<"\n"; 
					exit (EXIT_FAILURE); 
				}

				ifstream file;
				file.open(filename_loading.c_str());

				string line;
				int count=0;
				while (getline(file, line))
				count++;
				cout << "Numbers of lines in the loading file : " << count << endl;
				file.close();
				std::vector<double> load_list;
				load_list.resize(count);

				file.open(filename_loading.c_str());
				double temp0;

				for(int i=0;i<count;++i){
					file >> std::scientific >> std::setprecision(16)>>load_list[i]>>temp0;
				}



				string filename2  =  BLK1::config ;
				cout<<"input name:  "<<filename2<<"\n"; 

				string filename_final ;
				int size_c = force_energy::calculation::getInstance().c.p.size();
	
				cout<<"BLK1::memory_number: "<<BLK1::memory_number<<endl;
				
				for(int i=BLK1::memory_number;i<BLK1::total_iteration;i=i+BLK1::dt){
		
					//equilibrium
// 					filename_final = filename2+IntToStr(i)+ ".xyz";
					//non equilibrium
					filename_final = filename2+IntToStr(i);

					cout<<"searching for the file:  "<<filename_final<<"\n"; 
					if(exist(filename_final) == false){ 
						cout<<"no file sorry  "<<filename_final<<"\n"; 
						exit (EXIT_FAILURE); 
					}

					ifstream disp;

					disp.open(filename_final.c_str());
					double temp;
					int nol;
					disp >> nol;
					cout<<"number of lines in the file "<<filename_final<<" :"<<nol<<endl;

					for (int ii = 0; ii<size_c; ++ii){
						disp >> std::scientific >> std::setprecision(16)>> 
						force_energy::calculation::getInstance().c.p[ii].x >> 
						force_energy::calculation::getInstance().c.p[ii].y >> 
						temp >> temp >> temp >>
						temp >> temp >> temp>> temp >> temp >> temp>> temp >> temp >> temp>> temp >> temp  ;
				
					}
			
					disp.close();

// 					force_energy::calculation::getInstance().c.load = load_list[i];
					force_energy::calculation::getInstance().c.load = 1.7475000000000002e-01;// load_list[i];

					string answer = "l";
					
					result_memorize(force_energy::calculation::getInstance().c);
					ps_file(force_energy::calculation::getInstance().c,i, answer);
					write_to_a_ovito_file(force_energy::calculation::getInstance().c,i);
// 					do_delaunay(force_energy::calculation::getInstance().c,i,theta,load);

				}
			}

					
	  }
	
	else if(BLK1::model.compare("shearing") ==0){

	// 	pure_shear();
	// 	diagonal_shear();
// 		double xx=45;
// 		//conversion from degree to radian
// 		double theta = xx*dlib::pi/180.;

		cout<<"shearing"<<endl;
		std::vector<double>  angles;
		
		angles.push_back(0. *dlib::pi/180);
// 		angles.push_back(30. *dlib::pi/180);
//  		angles.push_back(45. *dlib::pi/180);
//  		angles.push_back(135. *dlib::pi/180);
// 		angles.push_back(atan(1./ 2.));
// 		angles.push_back(atan(2.    ));
// 		angles.push_back(60. *dlib::pi/180);
// 		angles.push_back(90. *dlib::pi/180);
// 		angles.push_back(120. *dlib::pi/180);
// 		angles.push_back(150. *dlib::pi/180);
// 		angles.push_back(180. *dlib::pi/180);
// 		angles.push_back(15. *dlib::pi/180);
// 		angles.push_back(80. *dlib::pi/180);
  
		for(int i=0;i<angles.size();++i)
			angle_shear(angles[i]);
		cout<<"energy_landscape_gorbushin"<<endl;
		energy_landscape_gorbushin();

	}	
		 //creates an anergy map 
	else if(BLK1::model.compare("at") ==0){

	// 	pure_shear();
	// 	diagonal_shear();
// 		double xx=45;
// 		//conversion from degree to radian
// 		double theta = xx*dlib::pi/180.;
		std::vector<double>  angles;
		
		angles.push_back(0. *dlib::pi/180);
// 		angles.push_back(30. *dlib::pi/180);
//  		angles.push_back(45. *dlib::pi/180);
// //  		angles.push_back(135. *dlib::pi/180);
// 		angles.push_back(atan(1./ 2.));
// // 		angles.push_back(atan(2.    ));
// 		angles.push_back(60. *dlib::pi/180);
// 		angles.push_back(90. *dlib::pi/180);
// // 		angles.push_back(120. *dlib::pi/180);
// // 		angles.push_back(150. *dlib::pi/180);
// // 		angles.push_back(180. *dlib::pi/180);
// 		angles.push_back(15. *dlib::pi/180);
// 		angles.push_back(80. *dlib::pi/180);
  
		
		for(int i=0;i<angles.size();++i)
		 	atensor_analysis(angles[i]);
		}
		
	else if(BLK1::model.compare("hessian") ==0){
		cout<<"energy_hessian_plotter starts"<<endl;
		energy_hessian_plotter_v2(force_energy::calculation::getInstance().c);
	}






}



//------------------------------------------------------
//------------------------------------------------------
//----------------------AFTER MAIN----------------------
//------------------------------------------------------
//------------------------------------------------------

void atensor_analysis(double theta)
{
	double alpha;
	double dx=BLK1::strain_rate;


	

// 	cout << "limit= " << limit << endl;


	string filenameal = "smallest_alpha" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
	std::fstream fs3;
	fs3.open (filenameal, std::fstream::in | std::fstream::out | std::fstream::app);
	
	std::vector<double>  smallest_alpha;
	smallest_alpha.resize(0);

	std::vector<double>  smallest_xsi;
	smallest_xsi.resize(0);

	std::vector<cella_stru>  metrics_keep;
	metrics_keep.resize(0);

	int check=0;
	int counter=0;
	
	for ( alpha = BLK1::initial_strain; alpha <=BLK1::loading_limit; alpha+=dx){
	
		base_stru c;
		cout<<"-------------------SHEARING--------------------"<<endl;
		
		c = shearing_vectors(alpha,theta*180./dlib::pi,0);


		cout<<"-------------------AT--------------------"<<endl;

		cout<<"alpha="<<alpha<<endl;

		base_stru r = riduci_vectors(c);
		struct matrix_stru m = find_integer_matrix(c, r);


		struct cella_stru metric_a = faicella(c);

		//Lagrange reduced metric in the fundementale domain
		struct cella_stru metric_r = faicella(r);

		//Lagrange reduced metric in the fundementale domain (it should give the same as above)
// 		struct cella_stru metric_rr = riduci(metric_a);


		cout<<"alpha="<<"entering to acoustic_tensor"<<endl;
		at_analysis detAc =  acoustic_tensor(c,metric_r,m,alpha,theta);
		cout<<"alpha="<<"exited from acoustic_tensor"<<endl;
		cout<<"detAc.detAc: "<<detAc.detAc<<endl;
		
		string ev2="smallest_eigenvalues_acoustictensor_" + DoubleToStr(theta*180. / dlib::pi) + ".dat" ;
		fstream filestr;
		filestr.open (ev2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16) << 	alpha  << " "<<detAc.detAc <<endl ;
		filestr.close();

		if(detAc.detAc <= 0){
			smallest_alpha.push_back(abs(alpha));
			smallest_xsi.push_back(detAc.xsi);
			metrics_keep.push_back(metric_a);
			check=1;		
		}
		
	}

	if(check==1){
	
		int min_index = minloc(smallest_alpha);
		fs3 << std::scientific << std::setprecision(7) 
			<< minval(smallest_alpha) << " " 
			<< smallest_xsi[min_index]<< " "
			<< metrics_keep[min_index].c11 << " " 
			<< metrics_keep[min_index].c22 << " " 
			<< metrics_keep[min_index].c12 << " " 	
			<< (metrics_keep[min_index].c11 - metrics_keep[min_index].c22)/sqrt(2) << " " 
			<< endl;
	}

}

void angle_shear(double& theta)
{
	double alpha = - 2.5;

	string file1, file2, file3, file4;
// 	theta_degree=theta*180. / dlib::pi;



	string filename, filename2, filename3, filename4, filename5;
	string filenamepiola,filenamecauchy;




	file1 = "alpha_energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
	file2 = "alpha_cauchy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
	file3 = "alpha_resolvedstress" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
	file4 = "path" + DoubleToStr(theta*180. / dlib::pi) + ".dat";

	ofstream filestr, filestr2, filestr3, filestr4;


	filestr.open(file1.c_str());
	filestr2.open(file2.c_str());
	filestr3.open(file3.c_str());
	filestr4.open(file4.c_str());


	double dx = 0.01;
	double min=100000000;
	double detc=1.;

	int limit = 2 * abs(alpha) / dx;

	cout<<"---------------------------------------"<<endl;
	cout<<"plotting homogenous energies for angle: "<<theta*180./dlib::pi<<endl;


// 	cout << "limit= " << limit << endl;
// 	for (int i = 0; i <= limit; i++){
	dx=BLK1::strain_rate;

	for ( alpha = BLK1::initial_strain; alpha <=BLK1::loading_limit; alpha+=dx){


		base_stru c,r,rp;


// 		cout<<"alpha="<<alpha<<endl;
        c = shearing_vectors(alpha,theta*180./dlib::pi,0);

		r = riduci_vectors(c);

// 		rp =  riduci_vectors_umut(c);


		struct cella_stru metric_a  = faicella(c);
		struct cella_stru metric_r  = faicella(r);
		struct cella_stru metric_rr = riduci(metric_a);
// 		struct cella_stru metric_pr = riduci_umut(metric_a);

// 		struct cella_stru metric_pr = faicella(rp);
		
		struct matrix_stru m2 = find_integer_matrix(c, r);
// 		cout<<"lagrange _reduction m"<<endl;
// 		m2.print();

		



		// 		cout<<"-------------"<<endl;
		double burgers = 1.;//pow(BLK1::scale,-4);
		// 		cout<<"det = "<<metric_a.c11*metric_a.c22-metric_a.c12*metric_a.c12<<endl;
		struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_rr, burgers, BLK1::beta_Var,r);

// 		filestr << std::scientific << std::setprecision(7) << alpha << " " << energy_zanzotto(metric_rr, burgers, BLK1::beta_Var) << "\n";
		filestr << std::scientific
		<< std::setprecision(16) 
		<< alpha << " "
		<< es.energy << "\n";
		
		if(es.energy < min){
// 			cout<<min<<" "<<es.energy<<endl;
			min=es.energy;
			detc= metric_a.c11*metric_a.c22-metric_a.c12*metric_a.c12;
		}

        struct de_stru d1;
		d1= cauchy(metric_rr,c,m2,burgers,BLK1::beta_Var);
 		filestr2 << std::scientific << std::setprecision(16) << alpha << " " <<  d1.reduced_stress.f12<< "\n";

		d1= piola_one(metric_rr,c,m2,burgers,BLK1::beta_Var);
		double dpda  =  contraction_with_shear(d1,theta*180./dlib::pi);
 		filestr3 << std::scientific << std::setprecision(16) << alpha << " " <<  dpda<< "\n";

 		filestr4 << std::scientific << std::setprecision(16) << 	
 		(metric_a.c11 - metric_a.c22)/sqrt(2.)<< " " <<  
 		metric_a.c12 << " "  <<  
 		es.energy<<endl;


		alpha += dx;
	}


	cout<<std::setprecision(16)<<"energy is minimized for detc: "<<detc<<endl;
	cout<<std::setprecision(16)<<"square root of  detc: "<<pow(detc,1./4.)<<endl;
	
	filestr.close();
	filestr2.close();
	filestr3.close();
	filestr4.close();

}



// 
// void angle_shear_spline(double theta, & spline)
// {
// 	double alpha = - 1.6;
// 
// 	string file1, file2, file3, file4;
// // 	theta_degree=theta*180. / dlib::pi;
// 
// 
// 	// 	filestr.open ("alpha_energy.dat", fstream::in | fstream::out | fstream::trunc);
// 	// 	filestr2.open ("C12_energy.dat", fstream::in | fstream::out | fstream::trunc);
// 	// 	filestr3.open ("C12p_energy.dat", fstream::in | fstream::out | fstream::app);
// 
// 	string filename, filename2, filename3, filename4, filename5;
// 	string filenamepiola,filenamecauchy;
// 
// 	filename = "rm_angle_" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	filename2 = "energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	filename3 = "nrm_angle_" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	filename4 = "m_field" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	filename5 = "prm_angle_" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	string filename6 = "reduced_stress" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	string filename7 = "prm_stress_angle_" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 
// 
// 	file1 = "alpha_energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	file2 = "C12_energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	file3 = "C12r_energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	file4 = "C12p_energy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 
// 	filenamepiola = "alpha_piola" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 	filenamecauchy = "alpha_cauchy" + DoubleToStr(theta*180. / dlib::pi) + ".dat";
// 
// 	ofstream angle, energy_s, angle2, m_field, pitteri, stress, stress_pitteri;
// 	ofstream filestr, filestr2, filestr3, filestr4;
// 	ofstream piolaw,cauchyw;
// 
// 	angle.open(filename.c_str());
// 	energy_s.open(filename2.c_str());
// 	angle2.open(filename3.c_str());
// 	m_field.open(filename4.c_str());
// 	pitteri.open(filename5.c_str());
// 	stress.open(filename6.c_str());
// 	stress_pitteri.open(filename7.c_str());
// 
// 
// 	filestr.open(file1.c_str());
// 	filestr2.open(file2.c_str());
// 	filestr3.open(file3.c_str());
// 	filestr4.open(file4.c_str());
// 
// 
// 	piolaw.open (filenamepiola.c_str());
// 	cauchyw.open(filenamecauchy.c_str());
// 
// 
// 	double dx = 0.01;
// 	int limit = 2 * abs(alpha) / dx;
// 
// 	
// 
// 	cout << "limit= " << limit << endl;
// 	for (int i = 0; i <= limit; i++){
// 
// 
// 		base_stru c,r,rp;
// 
// 		cout<<"---------------------------------------"<<endl;
// 
// 		cout<<"alpha="<<alpha<<endl;
//         c = shearing_vectors(alpha,theta,0);
// 
// 		r = riduci_vectors(c);
// 
// // 		rp =  riduci_vectors_umut(c);
// 
// 
// 		struct cella_stru metric_a  = faicella(c);
// 		struct cella_stru metric_r  = faicella(r);
// 		struct cella_stru metric_rr = riduci(metric_a);
// // 		struct cella_stru metric_pr = riduci_umut(metric_a);
// 
// // 		struct cella_stru metric_pr = faicella(rp);
// 		
// 		struct matrix_stru m2 = find_integer_matrix(c, r);
// 		cout<<"lagrange _reduction m"<<endl;
// 		m2.print();
// 
// 		
// 
// 
// 
// 		// 		cout<<"-------------"<<endl;
// 		double burgers = pow(BLK1::scale,-4);
// 		// 		cout<<"det = "<<metric_a.c11*metric_a.c22-metric_a.c12*metric_a.c12<<endl;
// 		DenseVector x(2);
// 
// 		x(0) =metric_r.c12;
// 		x(1) =metric_r.c11;
// 
// 
// // 		filestr << std::scientific << std::setprecision(7) << alpha << " " << energy_zanzotto(metric_rr, burgers, BLK1::beta_Var) << "\n";
// 		filestr << std::scientific << std::setprecision(7) << alpha << " " << 
// // 		force_energy::calculation::getInstance().c.fptr2(metric_rr, burgers, BLK1::beta_Var) << "\n";
// 		spline.eval(x) << "\n";
// 
// 
// 
// // 		filestr2 << std::scientific << std::setprecision(7) << metric_a.c12 << " " << energy_zanzotto(metric_rr, burgers, BLK1::beta_Var) << "\n";
// // 		filestr3 << std::scientific << std::setprecision(7) << metric_r.c12 << " " << energy_zanzotto(metric_rr, burgers, BLK1::beta_Var) << "\n";
// 		// 		filestr4<<std::scientific << std::setprecision(7) <<metric_rr.c12<<" "<<energy_zanzotto(metric_rr,burgers)<<"\n";        
// 		// 		cella_stru arbib = stress_arbib(metric_a);
// 		// 		filestr4<<std::scientific << std::setprecision(7) <<alpha<<" "<<arbib.c12<<"\n";        
// 
// 		cella_stru sr, spr;
// // 		sr = stress_easy(metric_r, burgers, BLK1::beta_Var);
// // 		spr = stress_easy(metric_rr, burgers, BLK1::beta_Var);
// //         sr  =force_energy::calculation::getInstance().c.fptr3(metric_r,burgers,BLK1::beta_Var);
// // 		spr = force_energy::calculation::getInstance().c.fptr3(metric_rr,burgers,BLK1::beta_Var);
// 		spr= force_energy::calculation::getInstance().c.fptr3(metric_rr,burgers,BLK1::beta_Var);
// 
// 
// 		stress << std::scientific << std::setprecision(7) 
// 		<< alpha   << " " 
// 		<< spr.c11 << " "
// 		<< spr.c22 << " "
// 		<< spr.c12 <<endl;
// 
// 
// // 		stress_pitteri << std::scientific << std::setprecision(7) << alpha << " " << spr.c12 << "\n";
// 
// 
// 
// 
// 
// 		//    cout<< filename << "  \n";
// // 		pitteri << std::scientific << std::setprecision(7) << " " << metric_rr.c11 << " " << metric_rr.c22 << " " << metric_rr.c12 << endl;
// // 		angle << std::scientific << std::setprecision(7) << " " << metric_r.c11 << " " << metric_r.c22 << " " << metric_r.c12 << endl;
// // 		energy_s << std::scientific << std::setprecision(7) << " " << energy_zanzotto(metric_r, burgers, BLK1::beta_Var) << endl;
// // 		angle2 << std::scientific << std::setprecision(7) << " " << metric_a.c11 << " " << metric_a.c22 << " " << metric_a.c12 << endl;
// // 		m_field << std::scientific << std::setprecision(7) << " " << alpha << " " << m.m11 << " " << m.m22 << " " << m.m12 << " " << m.m21 << endl;
// 		
// 		//piola and cauchy calculations
// 		
// 		struct de_stru d1;
// // 		
// 		d1= piola_one(metric_rr,c,m2,burgers, BLK1::beta_Var);
// // 		c.cauchy[i].f12  = d1.reduced_stress.f12;
// // 		c.cauchy[i].f11  = d1.reduced_stress.f11;
// // 		c.cauchy[i].f22  = d1.reduced_stress.f22;
// 		piolaw << std::scientific << std::setprecision(7) << " " << alpha << " " 
// 		<< d1.reduced_stress.f12 << " " 
// 		<< d1.reduced_stress.f21 << " " 
// 		<< d1.reduced_stress.f11 << " " 
// 		<< d1.reduced_stress.f22 << endl;
// // 
// // 		d1= piola_one(metric_rr,r,m2,burgers, BLK1::beta_Var);
// // // 		c.piola_one[i].f12  = d1.reduced_stress.f12;
// // // 		c.piola_one[i].f11  = d1.reduced_stress.f11;
// // // 		c.piola_one[i].f22  = d1.reduced_stress.f22;
// // // 		c.piola_one[i].f21  = d1.reduced_stress.f21;
// // 		double P11,P22,P12,P21;
// // 		
// // // 		P11 = d1.reduced_stress.f11;
// // // 		P22 = d1.reduced_stress.f22;
// // // 		P12 = d1.reduced_stress.f12;
// // // 		P21 = d1.reduced_stress.f21;
// // 
// // 		//double newp11 = m.m11*(m.m11*P11 + m.m21*P21) + m.m21*(m.m11*P12 + m.m21*P22);
// // 		//double newp11 =m.m11 *(m.m11* P11 + m.m12 *P21) + m.m12* (m.m11 *P12 + m.m12 *P22)
// // 		piolaw << std::scientific << std::setprecision(7) << " " << alpha << " " << d1.reduced_stress.f12 << " " <<  " " << d1.reduced_stress.f21<< " " << d1.reduced_stress.f11 << " " << d1.reduced_stress.f22 << endl;
// 		
// 		//increase loading
// 		alpha += dx;
// 	}
// 
// 
// 	angle.close();
// 	angle2.close();
// 	pitteri.close();
// 	energy_s.close();
// 	m_field.close();
// 	filestr.close();
// 	filestr2.close();
// 	filestr3.close();
// 	filestr4.close();
// 	stress.close();
// 	stress_pitteri.close();
// 
// }


//it takes as argument real vectors
struct base_stru elastic_vectors(base_stru c){

		//to be returned
		struct base_stru rp;
	
		struct	base_stru r = riduci_vectors(c);
		
		//real matrics
		struct cella_stru metric_a  = faicella(c);

		//umut reduced metrics
		struct cella_stru metric_pr = riduci_umut(metric_a);

		struct matrix_stru m;
		
		std::vector<double> ve1(2);
		std::vector<double> ve2(2);

		if(metric_pr.c12 >= 0 && metric_pr.c11 <= metric_pr.c22){
			rp.e1[0] = r.e1[0];
			rp.e1[1] = r.e1[1];
			rp.e2[0] = r.e2[0];
			rp.e2[1] = r.e2[1];
		}

		//right up
		if(metric_pr.c12 >= 0 && metric_pr.c22 < metric_pr.c11 ){
			ve1[0] = r.e1[0];
			ve1[1] = r.e1[1];
			ve2[0] = r.e2[0];
			ve2[1] = r.e2[1];

			ve2.swap(ve1);	
			
			rp.e1[0] = ve1[0];
			rp.e1[1] = ve1[1];
			rp.e2[0] = ve2[0];
			rp.e2[1] = ve2[1];

		
		}	
	
		//left down

		if(metric_pr.c12 <= 0 && metric_pr.c11 < metric_pr.c22){
			rp.e2[0] = -r.e2[0];
			rp.e2[1] = -r.e2[1];
		}

		//right down
		if(metric_pr.c12 < 0 && metric_pr.c22 < metric_pr.c11 ){
			ve1[0] = r.e1[0];
			ve1[1] = r.e1[1];
			ve2[0] = r.e2[0];
			ve2[1] = r.e2[1];

			ve2.swap(ve1);	
			
			rp.e1[0] = ve1[0];
			rp.e1[1] = ve1[1];
			rp.e2[0] = -ve2[0];
			rp.e2[1] = -ve2[1];
		}

	return rp;




}




struct matrix_stru f_plastic(base_stru c){

		//to be returned
		struct base_stru rp;
	
		struct base_stru r = riduci_vectors(c);

 		struct base_stru  e_vec = elastic_vectors(c);

		struct matrix_stru m3 = find_integer_matrix(c, e_vec);


	return m3;




}

