#pragma once
#include "dlib.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "structures.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "custom_inline_functions.h"
#include "plot.h"
#include <iostream>
#include <map>
using namespace std;


//void calcstresses_CG_en_homogenous(struct conf_stru& c,double alpha,int k,ofstream& filestr);
void calcstresses_CG_en_homogenous(struct conf_stru& c,double alpha);

