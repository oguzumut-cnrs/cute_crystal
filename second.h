#pragma once
//  #define ARMA_ALLOW_FAKE_CLANG
//  #define ARMA_ALLOW_FAKE_GCC
// 
//  #define ARMA_DONT_USE_WRAPPER
// // 
//  #define ARMA_DONT_USE_HDF5


#include <armadillo>

using namespace arma; 
using namespace std; 

#include <iostream>
#include <vector>

#include "common.h"
#include "structures.h"
#include "namespaces.h"
#include "utilities.h"
#include "dlib.h"
#include "custom_inline_functions.h"

#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <GenEigsSolver.h>
#include <SymEigsSolver.h>
#include <MatOp/SparseGenMatProd.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>

using Eigen::MatrixXd;
typedef Eigen::SparseMatrix<double> SpMatEigen; // declares a column-major sparse matrix type of double

// #include <blaze/Math.h>

// using blaze::StaticVector;
// using blaze::DynamicVector;
int K_matrix(struct conf_stru& c);

void slicing_filling_global_stifness(sp_mat& squezed);

sp_mat Hessian_2D(struct conf_stru& c);

SpMatEigen  Hessian_2D_eigen(struct conf_stru& c);

void filling_global_stifness_eigen(SpMatEigen& squezed,std::vector < std::vector<std::vector< std::vector<double> > >>& Km,
int ii,int kk, struct punto_stru& p);

void  slicing_filling_global_stifness_eigen(sp_mat& squezed, SpMatEigen& squezed_eigen);

