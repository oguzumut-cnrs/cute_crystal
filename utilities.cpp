
#include "utilities.h"
#include <cmath>

using namespace std;

double maxval(std::vector<double>& m)
{
    double max;
    max  = *max_element(m.begin(), m.end());
    return max;

}

double minval(std::vector<double>& m)
{
    double min;
    min  = *min_element(m.begin(), m.end());
    return min;

}


int maxloc(std::vector<double>& m)
{
    int max;
    max  = max_element(m.begin(), m.end()) - m.begin();
    return max;

}

int minloc(std::vector<double>& m)
{
    int min;
    min  = min_element(m.begin(), m.end()) - m.begin();
    return min;

}



void single_data_to_a_file(double t1, string t2)
{
		fstream filestr;
		filestr.open (t2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16)   << " "<<t1<<endl ;
}


void all_data_to_a_file(double t1, double t3, string t2)
{
		fstream filestr;
		filestr.open (t2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(16) <<t3  << " "<<t1<<endl ;
		filestr.close();
}
void fourth_order_tensor_allocate(std::vector < std::vector<std::vector< std::vector<double> > >>& Am){

  Am.resize(2);
	for (int i = 0; i < 2; ++i)
		Am[i].resize(2);
	for (int i = 0; i < 2; ++i)
		for (int j= 0; j < 2; ++j)
		Am[i][j].resize(2);
	for (int i = 0; i < 2; ++i)
		for (int j= 0; j < 2; ++j)
			for (int k= 0; k < 2; ++k)
				Am[i][j][k].resize(2);


}
