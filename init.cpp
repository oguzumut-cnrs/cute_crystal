#include "calculations.h"
#include "rotation.h"
#include "energy_functions.h"
#include "vector_functions.h"
#include "init.h"
#include "dlib.h"
#include "input_output.h"
#include <cmath>
#include <vector>
using namespace std;
using namespace dlib;

std::vector<double> original_coordinates_hex()
{
	int ii,jj;
	ii=0;jj=0;
	
	double angle=BLK1::cutting_angle;
	angle = toRadians(angle);
	std::vector<double> xb,yb;
	xb.resize(0);yb.resize(0);
	
	
	auto real_coordinates_p0 = get_XY_for_rotated_hex(ii+1, jj, angle);	
	auto real_coordinates_p1 = get_XY_for_rotated_hex(ii, jj+1, angle);
	auto real_coordinates_p2 = get_XY_for_rotated_hex(ii-1, jj+1, angle);
	auto real_coordinates_p3 = get_XY_for_rotated_hex(ii-1, jj, angle);	
	auto real_coordinates_p4 = get_XY_for_rotated_hex(ii-1, jj-1, angle);
	auto real_coordinates_p5 = get_XY_for_rotated_hex(ii, jj-1, angle);

	xb.push_back(real_coordinates_p0.first);
	yb.push_back(real_coordinates_p0.second);

	xb.push_back(real_coordinates_p1.first);
	yb.push_back(real_coordinates_p1.second);

	xb.push_back(real_coordinates_p2.first);
	yb.push_back(real_coordinates_p2.second);

	xb.push_back(real_coordinates_p3.first);
	yb.push_back(real_coordinates_p3.second);

	xb.push_back(real_coordinates_p4.first);
	yb.push_back(real_coordinates_p4.second);

	xb.push_back(real_coordinates_p5.first);
	yb.push_back(real_coordinates_p5.second);

	std::vector<double> nb;
	nb.resize(7);
	for (int i=0; i<xb.size(); i++) {
		cout<<"local coo values "<<atan(yb[i]/abs(xb[i]))*180./dlib::pi<<endl;
		nb[i]=atan(yb[i]/abs(xb[i]))*180./dlib::pi;
	}
	nb[6]=nb[0];
		
	return nb;
	

}
void test_knn2(struct conf_stru& c ,int nn)
{
	std::vector<matrix<double,2,1> > samples;
	matrix<double,2,1> test;



	std::vector<double> nb = original_coordinates_hex();
	






	for (int i=0; i<c.p.size(); i++) 
	{
	  test = c.p[i].x,c.p[i].y;
	  samples.push_back(test);
	}

	std::vector<sample_pair> edges;
	clock_t t;
	t = clock();
	find_k_nearest_neighbors(samples, squared_euclidean_distance(0.9, 1.2), 6, edges);
    t = clock() - t;
    printf ("It took me  (%f seconds) to find_k_nearest_neighbors.\n",((float)t)/CLOCKS_PER_SEC);

	std::sort(edges.begin(), edges.end(), &order_by_index<sample_pair>);

	cout<<"edges size: "<<edges.size()<<endl;
	int index,index2,counter;
	
	int choice=c.p.size();
	for (int l=0; l<choice; l++){
		cout<<"----------"<<endl;
		counter=0;
		for(int i=0;i<edges.size();++i){

	// 		cout<<"i: "<<i<<endl;
			if(edges[i].index1()==l || edges[i].index2()==l ){
				counter++;
				if(edges[i].index1()==l){
					index=edges[i].index1();
					index2=edges[i].index2();
				
				}
				if(edges[i].index2()==l){
					index=edges[i].index2();
					index2=edges[i].index1();	
				}

				double res = std::atan((c.p[index2].y - c.p[index].y)/abs(c.p[index2].x - c.p[index].x))*180./dlib::pi;
				for (int nn=0; nn<6; ++nn){
					if( res/(nb[nn]+ 0.0001) > 0 && abs(res - nb[nn]) < 0.00001 ){
						c.p[l].nn[nn]  = index2;
						cout<<"*******"<<endl;
						cout<<"origin and angletan: "<<index<<" "<<res<<endl;
						cout<<"origin coox: "<<c.p[l].x<<endl;
						cout<<"origin cooy: "<<c.p[l].y<<endl;

						cout<<"nn: "<<nn<<endl;
						cout<<"index: "<<index2<<endl;
						cout<<"indice coox: "<<c.p[index2].x<<endl;
						cout<<"indice cooy: "<<c.p[index2].y<<endl;
						double distance = (c.p[index2].x-c.p[index].x)*(c.p[index2].x-c.p[index].x)+
										  (c.p[index2].y-c.p[index].y)*(c.p[index2].y-c.p[index].y);
						cout<<"distance: "<<distance<<endl;

						cout<<"******"<<endl;
					}
				}

			}
		}
		c.p[l].nn[6] = c.p[l].nn[0]; 
		
	}

 
}

void read_from_molecular_dynamics(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ifstream md;
	string  filename3;
	filename3= "/Users/umutsalman/mariusz/dir1530281960/dir_coo/for_ovito_10257.xyz";
	double dumb;

	md.open(filename3.c_str());

	ifstream file;
	file.open(filename3.c_str());

	string line;
	int count=0;
	while (getline(file, line))
		count++;
	cout << "Numbers of lines in the file : " << count << endl;
	file.close();


		for (int i = 0; i<n; ++i){
					
				md >> dumb >> c.p[i].x >> c.p[i].y;
				cout << dumb << " "<<c.p[i].x <<" "<< c.p[i].y << endl;

		}

	

	md.close();

}



std::pair<double, double> get_XY_for_rotated_square(int i, int j, double angle) {
	double beta = atan2(1.0*j , 1.0*i);
	double gamma = angle + beta;
	double r = sqrt(1.0*i*i + j*j);
	double x = r*cos(gamma);
	double y = r*sin(gamma);


	return std::pair<double, double>(x, y);
}


// 

void new_init_rotated_square(struct conf_stru& c, double angle) {
	int width  = dimensions::nx-1;
	int height = dimensions::ny-1;
	conf_stru temp_structure;
	punto_stru origin_point;
	origin_point.x = 0;
	origin_point.y = 0;
	origin_point.i = 0;
	origin_point.j = 0;
// 	temp_structure.p.push_back(origin_point);

	bool i_loop = true;
	bool jmax_loop = true;
	bool jmin_loop = true;
	int i = 0;
	int j = 0;

	angle = toRadians(angle);
	square_grid_creator t;
	t.set_values(width,height,angle);
        cout<<"while loop starts for rotation angle: "<<angle<<endl;
    bool added = true;
	while (added) {
			added = false;

			j = 0;
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0.0 && x - width <= 0.1) && (y - height <= 0.1)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}


		j = 1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0 && x - width <= 0.1) && (y - height <= 0.1 )) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i;
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (y >= height || x < -0.011 ) 
				break;
			j++;
		}

		j = -1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_square(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x - width <= 0.1) && (y >= -0.01)  && (y - height <= 0.1) ) {
// 				cout<<"we are inside j=-1 loop"<<endl;
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (x >= width || y < -0.011 ) {
				break;
			}
			j--;
		}

		i++;
	}
	
	t.set_size(temp_structure.p.size());
	t.nn_list_square(temp_structure);



                
                
	
    int n = temp_structure.p.size();

    force_energy::calculation::getInstance().setSize(n, width, height);
	cout << c.p.size() << endl;
	cout << n << endl;
        
	for (int k = 0; k < n; ++k) {
		c.p[k]    = temp_structure.p[k];
		c.pcons[k] = temp_structure.p[k];
		c.pfix[k]  = temp_structure.p[k];
		c.alloying[k][0]  = BLK1::beta_Var;
		c.alloying[k][1]  = BLK1::beta_Var;
		c.alloying[k][2]  = BLK1::beta_Var;

		c.disorder[k][0] = 1.*pow(BLK1::scale,-4);
		c.disorder[k][1] = 1.*pow(BLK1::scale,-4);
		c.disorder[k][2] = 1.*pow(BLK1::scale,-4);
	}
     
//         c.disorder = uniform_volumetric(0.9,1.1,1.,n);
//         cout<<"maxval of volumetric effect: "<<maxval(c.disorder)<<endl;
//         cout<<"minval of volumetric effect: "<<minval(c.disorder)<<endl	;

}



void init(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	//   int dnx = dimensions::nx/2. -1.;
	//   int dny = dimensions::ny/2. -1.;

	double dnx = 0.;//dimensions::nx;
	double dny =  0.;//c.dny;

	double dc = BLK1::symmetry_constantx;
	double dcx = BLK1::symmetry_constantx;
	double dcy = BLK1::symmetry_constanty;

	int ix, iy, ntot = c.p.size();

	for (int i = 0; i<c.p.size(); ++i)
	{
		int iy = i / nx;
		int ix = i % nx;

		double dix = double(ix - dnx);
		double diy = double(iy - dny);


		if (BLK1::crystal_symmetry.compare("hex") == 0)
		{
			double h=dimensions::dh;
			if (iy % 2 == 0)
			{
				c.p[i].x = h*dc*dix;
				c.p[i].y = h*dc*diy*sqrt(3) / 2;
				c.pfix[i].x = h*dc*dix;
				c.pfix[i].y = h*dc*diy*sqrt(3) / 2;

			}


			if (iy % 2 != 0)
			{

				c.p[i].x = h*dc*(dix + 0.5);
				c.p[i].y = h*dc*diy*sqrt(3) / 2;

				c.pfix[i].x = h*dc*(dix + 0.5);
				c.pfix[i].y = h*dc*diy*sqrt(3) / 2;
				

			}
	
// 				if(ix >=nx/2)
// 					c.p[i].y +=5*(dc*dc);
	
		}


		else if (BLK1::crystal_symmetry.compare("square") == 0)
		{
			
			double h=dimensions::dh;	
			//double h=1.;	

			c.p[i].x = (dix - dnx)*h;
			c.p[i].y = (diy - dny)*h;
 
		
			
			c.pfix[i].x = (dix - dnx)*h;
			c.pfix[i].y = (diy - dny)*h;
 
//  			if(ix >=nx/2+nx/4)
//  				c.p[i].y +=0;
//  

		}


		else if (BLK1::crystal_symmetry.compare("tilted") == 0)
		{

			if (iy % 2 == 0)
			{
				c.p[i].x = dcx*dix;
				c.p[i].y = dcy*diy;

				c.pfix[i].x = dcx*dix;
				c.pfix[i].y = dcy*diy;

			}


			if (iy % 2 != 0)
			{

				c.p[i].x = dcx*dix + dcx / 2.;
				c.pfix[i].x = dcx*dix + dcx / 2.;

				c.pfix[i].y = dcy*diy;
				c.p[i].y = dcy*diy;

			}
		}





		c.disp[i].x = 0;
		c.disp[i].y = 0;

		//  NEIGHBORDS OF POINTS TRIANGULATION

		c.p[i].nn[0] = (iy)*nx + ix + 1; //EAST
		c.p[i].nn[1] = (iy + 1)*nx + ix; //NORTH    
		c.p[i].nn[2] = (iy)*nx + ix - 1; //WEST
		c.p[i].nn[3] = (iy - 1)*nx + ix; //SOUTH     


						//IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS

		if (ix == nx - 1)
			c.p[i].nn[0] = (iy)*nx; 	         //EAST	    
		if (iy == ny - 1)
			c.p[i].nn[1] = ix;              //NORTH			
		if (ix == 0)
			c.p[i].nn[2] = (iy)*nx + nx - 1; 	 //WEST	    
		if (iy == 0)
			c.p[i].nn[3] = (iy + (ny - 1))*nx + ix; //SOUTH			

		c.p[i].nn[4] = c.p[i].nn[0];
		c.p[i].nn[4] = c.p[i].nn[0];

		if (BLK1::crystal_symmetry.compare("tilted") == 0 )
		{
			int no, ne, nw, se, sw;

			if (iy % 2 == 0)
			{
				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix;
				nw = (iy + 1)*nx + ix - 1;
				se = (iy - 1)*nx + ix;
				sw = (iy - 1)*nx + ix - 1;

				if (ix == 0)
				{
					nw = (iy + 1)*nx + (nx - 1);
					sw = (iy - 1)*nx + (nx - 1);


				}

				if (iy == 0)
				{
					se = (ny - 1)*nx + ix;
					sw = (ny - 1)*nx + ix - 1;

				}

				if (iy == 0 && ix == 0)
				{
					ne = (iy + 1)*nx + ix;
					nw = (iy + 1)*nx + nx - 1;
					se = (ny - 1)*nx + 0;
					sw = (ny - 1)*nx + nx - 1;



				}

			}


			if (iy % 2 != 0)
			{

				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix + 1;
				nw = (iy + 1)*nx + ix;
				se = (iy - 1)*nx + ix + 1;
				sw = (iy - 1)*nx + ix;

				if (iy == ny - 1)
				{
					ne = (0)*nx + ix + 1;
					nw = (0)*nx + ix;

				}

				if (ix == nx - 1)
				{
					ne = (iy + 1)*nx + 0;
					se = (iy - 1)*nx + 0;

				}

				if (iy == ny - 1 && ix == nx - 1)
				{
					ne = 0;
					nw = nx - 1;

				}

			}

			//  NEIGHBORDS OF POINTS TRIANGULATION FOR TILTED

			c.p[i].nn[0] = ne; //NORTHEAST
			c.p[i].nn[1] = nw; //NORTHWEST    
			c.p[i].nn[2] = sw; //SOUTHEAST
			c.p[i].nn[3] = se; //SOUTHWEST     


			//IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS


			c.p[i].nn[4] = c.p[i].nn[0];
			c.p[i].nn[4] = c.p[i].nn[0];


		}

		//alloying
		int cooxsq = (ix - nx / 2)*(ix - nx / 2);
		int cooysq = (iy - ny / 2)*(iy - ny / 2);
		c.alloying[i][0]  = BLK1::beta_Var;
		c.alloying[i][1]  = BLK1::beta_Var;
		c.alloying[i][2]  = BLK1::beta_Var;


		c.disorder[i][0] = 1.*pow(BLK1::scale,-4);
		c.disorder[i][1] = 1.*pow(BLK1::scale,-4);
		c.disorder[i][2] = 1.*pow(BLK1::scale,-4);


	}

	c.pfix  = c.p;
	c.pcons = c.pfix;




}

void assign_bc_MJ(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;


	int ix, iy, ntot = c.p.size();
	int k;
	string_to_integer bset;

	string  BC = BLK1::BC;
	string  lattice = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);

	bset.convert_new(BLK1::bc_x, BLK1::bc_y);
	std::vector<int> miss_ind;
	miss_ind.resize(0);
	cout<<"boundary conditions:"<<endl;
	cout<<"right: "<<bset.x <<endl;
	cout<<"left: "<< bset.x<<endl;
	cout<<"top: "<< bset.y<<endl;
	cout<<"bottom: "<<bset.y <<endl;

		for (int k = 0; k<c.p.size(); ++k)
			for (int s = 0; s < 4; s++){
				c.bc_cond[k][s] = 2; // full interaction
		}	



		for(int k = 0; k<c.p.size(); ++k){
			for (int s = 0; s < 4; s++){
				int ii = c.p[k].i;
				int jj = c.p[k].j;

				//this condition checks if the triangle exists
				//if the triangle does NOT exist, then a BC should be assigned 
				if(c.p[k].nn[s]==-1 || c.p[k].nn[s+1]==-1 ){
				
					if( c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
						c.bc_cond[k][s] =bset.x;
// 						std::vector<double> temp =  uniform_volumetric(0.7,1.3,1.,1);
// 						c.disorder[k] = temp[0];
					}
					else if( c.p[k].which_boundary ==4)
						c.bc_cond[k][s] =bset.y;
					else if( c.p[k].which_boundary ==3)
						c.bc_cond[k][s] =bset.y;

					else{
						cout<<"PROBLEM in assign_bc_MJ "<<endl;	
						cout<<"PROBLEM UNITS i  "<<k<<endl;
						cout<<"PROBLEM UNITS k  "<<s<<endl;
						cout<<"  c.p[k].which_boundary "<<  c.p[k].which_boundary<<endl;
						
						 exit (EXIT_FAILURE);				
					}				

				}
				
				//special treatment for upper boundary
				//body units are fixed
				//block boundary condition designed for compression
	// 			if(c.p[k].which_boundary ==3 )
// 					c.bc_cond[k][s] =bset.y;
	
			}			
			
		}
// 		for (int k = 0; k<c.p.size(); ++k)
// 		{
// // 			int counter2=0;
// // 			miss_ind.resize(0);
// 
// 		int ii = c.p[k].i;
// 		int jj = c.p[k].j;
// 		
// 		if(c.p[k].nn[0]==-1)
// 		{
// 			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 				c.bc_cond[k][0] = bset.x;
// 				c.bc_cond[k][3] = bset.x;
// 			}
// 			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// 				c.bc_cond[k][0] = bset.y;
// 				c.bc_cond[k][3] = bset.y;
// 			}							
// 		}
// 		if(c.p[k].nn[1]==-1)
// 		{
// 			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 				c.bc_cond[k][0] = bset.x;
// 				c.bc_cond[k][1] = bset.x;
// 			}
// 			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// 				c.bc_cond[k][0] = bset.y;
// 				c.bc_cond[k][1] = bset.y;
// 			}							
// 		}
// 		if(c.p[k].nn[2]==-1)
// 		{
// 			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 				c.bc_cond[k][1] = bset.x;
// 				c.bc_cond[k][2] = bset.x;
// 			}
// 			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// 				c.bc_cond[k][1] = bset.y;
// 				c.bc_cond[k][2] = bset.y;
// 			}							
// 		}
// 		
// 		if(c.p[k].nn[3]==-1)
// 		{
// 			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 				c.bc_cond[k][2] = bset.x;
// 				c.bc_cond[k][3] = bset.x;
// 			}
// 			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// 				c.bc_cond[k][2] = bset.y;
// 				c.bc_cond[k][3] = bset.y;
// 			}							
// 		}		
// 
// 		if(c.p[k].nn[4]==-1)
// 		{
// 			if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
// 				c.bc_cond[k][0] = bset.x;
// 				c.bc_cond[k][3] = bset.x;
// 			}
// 			if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 ){
// 				c.bc_cond[k][0] = bset.y;
// 				c.bc_cond[k][3] = bset.y;
// 			}							
// 		}
// 
// 		
// 		int c2= c.p[k].corner;
// 
// 		if(c2 ==1  )
// 			c.bc_cond[k][3] = bset.x;
// 	
// 		if(c2 ==2  )
// 			c.bc_cond[k][0] = bset.x;
// 		if(c2 ==3  )
// 			c.bc_cond[k][2] = bset.x;
// 			
// 		if(c2 ==4  )
// 			c.bc_cond[k][1] = bset.x;
// 
// 
// 					
// 	
// 				}			
			
// 		for (int k = 0; k<c.p.size(); ++k){
// 			for (int s = 0; s < 4; s++){
// 
// 				if(c.p[k].nn[s]==-1 || c.p[k].nn[s+1]==-1 ){
// 		
// 					if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 )
// 						cout<<"NO PROBLEM in assign_bc_MJ "<<endl;	
// 					else if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 )
// 						cout<<"PROBLEM in assign_bc_MJ "<<endl;	
// 					else{
// 						cout<<"PROBLEM in assign_bc_hex_MJ "<<endl;	
// 						cout<<"PROBLEM UNITS i  "<<k<<endl;
// 						cout<<"PROBLEM UNITS k  "<<s<<endl;
// 						cout<<"  c.p[k].which_boundary "<<  c.p[k].which_boundary<<endl;
// 				
// 		// 						 exit (EXIT_FAILURE);				
// 					}				
// 				}	
// 			}
// 		}
						

			//guarantees full interaction in each cell
		
	
}


//change this
void assign_bc_hex_MJ(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;


	int ix, iy, ntot = c.p.size();
	int k;
	string_to_integer bset;

	string  BC = BLK1::BC;
	string  lattice = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);

	bset.convert_new(BLK1::bc_x, BLK1::bc_y);
	std::vector<int> miss_ind;
	miss_ind.resize(0);
	cout<<"boundary conditions:"<<endl;
	cout<<"right: "<<bset.x <<endl;
	cout<<"left: "<< bset.x<<endl;
	cout<<"top: "<< bset.y<<endl;
	cout<<"bottom: "<<bset.y <<endl;

		for (int k = 0; k<c.p.size(); ++k)
			for (int s = 0; s < 6; s++){
				c.bc_cond[k][s] = 2; // full interaction
		}	
	
// 			d.p[0] = p;
// 			d.p[1] = c.p[p.nn[k]];
// 			d.p[2] = c.p[p.nn[k+1]];
		for(int k = 0; k<c.p.size(); ++k){
			for (int s = 0; s < 6; s++){
	// 			int counter2=0;
	// 			miss_ind.resize(0);
				int ii = c.p[k].i;
				int jj = c.p[k].j;
// 				cout<<"-------------"<<endl;
// 				cout<<"k= "<<k<< " and s= "<<s<<endl;
// 				cout<<"nn[s]= "<<c.p[k].nn[s]<< " and nn[s+1]= "<<c.p[k].nn[s+1]<<endl;
// 				cout<<"x coo nn[s]= "<<c.p[c.p[k].nn[s]].x<< " and coo nn[s+1]= "<<c.p[c.p[k].nn[s+1]].x<<endl;
// 				cout<<"y coo nn[s]= "<<c.p[c.p[k].nn[s]].y<< " and coo nn[s+1]= "<<c.p[c.p[k].nn[s+1]].y<<endl;

				if(c.p[k].nn[s]==-1 || c.p[k].nn[s+1]==-1 ){
				
					if(c.p[k].which_boundary ==1 ||  c.p[k].which_boundary ==2 ){
						c.bc_cond[k][s] =bset.x;
// 						std::vector<double> temp =  uniform_volumetric(0.7,1.3,1.,1);
// 						c.disorder[k] = temp[0];
						
					}
					else if(c.p[k].which_boundary ==3 ||  c.p[k].which_boundary ==4 )
						c.bc_cond[k][s] =bset.y;
					else{
						cout<<"PROBLEM in assign_bc_hex_MJ "<<endl;	
						cout<<"PROBLEM UNITS i  "<<k<<endl;
						cout<<"PROBLEM UNITS k  "<<s<<endl;
						cout<<"  c.p[k].which_boundary "<<  c.p[k].which_boundary<<endl;
						
// 						 exit (EXIT_FAILURE);				
					}				

				}
	
			}			
			
		}

			//guarantees full interaction in each cell
		
	
}



void assign_bc(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;


	int ix, iy,ntot=c.p.size();
	int k;
	string_to_integer bset;

	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);

	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
  

  
	if(BLK1::crystal_symmetry.compare("tilted") == 0)
	{
		for(int i =0;i<ntot;++i)
		{
			int iy = i / nx;
			int ix = i % nx;

			//guarantees full interaction in each cell
			c.bc_cond[i][0] = 2;
			c.bc_cond[i][1] = 2;
			c.bc_cond[i][2] = 2;
			c.bc_cond[i][3] = 2;

			//change the boundary condition on y
			if( (iy%2 ==0 && ix == 0)   )
			{
				
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][1] = bset.x;
				c.bc_cond[i][2] = bset.x;
			}
			
			if( (iy%2 !=0 && ix == nx-1) )
			{
				
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][2] = bset.x;
				c.bc_cond[i][3] = bset.x;
			}		
			
				//change the boundary condition on y 
			if( iy == ny-1 )
			{
				c.bc_cond[i][0] = bset.y;	
				c.bc_cond[i][1] = bset.y;	
				c.bc_cond[i][3] = bset.y;		
				
			}	
			
			if( iy == 0 )
			{
				c.bc_cond[i][1] = bset.y;	
				c.bc_cond[i][2] = bset.y;	
				c.bc_cond[i][3] = bset.y;		
				
			}
			//special points cavity
// 			if( pow(ix-nx/2,2.)+pow(iy-ny/2,2.) <= 100 ){
// 				c.bc_cond[i][0] = 1;
// 				c.bc_cond[i][1] = 1;
// 				c.bc_cond[i][2] = 1;
// 				c.bc_cond[i][3] = 1;				
// 			}
		}
	}


// 		else if(BLK1::crystal_symmetry.compare("tilted") != 0)
		else if(BLK1::crystal_symmetry.compare("square") == 0)
		{
			//guarantees full interaction in each cell
			
			for(int k =0;k<BLK1::triangle;++k)
			for(int i =0;i<ntot;++i)
			{
				int iy = i / nx;
				int ix = i % nx;
				c.bc_cond[i][k] = 2;
			
				//change the boundary condition on x
				if((ix == nx-1 && k == 0) || (ix == 0 && k == 2) || (ix == 0 && k == 1)  || (ix == nx-1 && k == 3) )
					c.bc_cond[i][k] = bset.x;
				//change the boundary condition on y, overrides on corners
				if((iy == ny-1 && k == 0) || (iy == 0 && k == 3) || (iy == ny-1 && k == 1) || (iy == 0 && k == 2))
					c.bc_cond[i][k] = bset.y;
// 					
// 				if( pow(ix-nx/2,2.)+pow(iy-ny/2,2.) <= 100 )
// 					c.bc_cond[i][k] = 1;
					
					
					
					

	// 			if( (iy == ny-2 && ix == nx-1 && k ==0) || (iy == ny-2 && ix == 0 && k==2) )
	// 				c.bc_cond[i][k] = bset.y;
	// 			if( (iy == 1 && ix == nx-1 && k ==0) || (iy == 1 && ix == 0 && k==2) )
	// 				c.bc_cond[i][k] = bset.y;

			}
			
		}
	//hex 6 NN
	if(BLK1::crystal_symmetry.compare("hex") == 0)
	{
		for(int i =0;i<ntot;++i)
		{
			int iy = i / nx;
			int ix = i % nx;

			//guarantees full interaction in each cell
			c.bc_cond[i][0] = 2;
			c.bc_cond[i][1] = 2;
			c.bc_cond[i][2] = 2;
			c.bc_cond[i][3] = 2;
			c.bc_cond[i][4] = 2;
			c.bc_cond[i][5] = 2;

			if( (iy%2 ==0 && ix == 0)   )
			{
				
				c.bc_cond[i][1] = bset.x;
				c.bc_cond[i][2] = bset.x;
				c.bc_cond[i][3] = bset.x;
				c.bc_cond[i][4] = bset.x;

			}


			if( (iy%2 ==0 && ix == nx-1)   )
			{
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][5] = bset.x;

			}
			
			if( (iy%2 !=0 && ix == nx-1) )
			{
				
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][1] = bset.x;
				c.bc_cond[i][4] = bset.x;
				c.bc_cond[i][5] = bset.x;
			}		

			if( iy%2 !=0 && ix == 0   )
			{
				c.bc_cond[i][2] = bset.x;
				c.bc_cond[i][3] = bset.x;

			}		
			//special points
			if( iy == ny-1 )
			{
				c.bc_cond[i][0] = bset.y;	
				c.bc_cond[i][1] = bset.y;	
				c.bc_cond[i][2] = bset.x;		
				
			}	
			
			if( iy == 0 )
			{
				c.bc_cond[i][3] = bset.x;	
				c.bc_cond[i][4] = bset.y;	
				c.bc_cond[i][5] = bset.x;		
				
			}
			if( iy == 0 && ix==nx-1 )
			{
				c.bc_cond[i][0] = bset.x;
				c.bc_cond[i][3] = bset.x;	
				c.bc_cond[i][4] = bset.y;	
				c.bc_cond[i][5] = bset.x;		
				
			}	
			if( iy == 0 && ix==0)
			{
				c.bc_cond[i][1] = bset.x;
				c.bc_cond[i][2] = bset.x;
				c.bc_cond[i][3] = bset.x;	
				c.bc_cond[i][4] = bset.y;	
				c.bc_cond[i][5] = bset.x;		
				
			}				
			

			if( iy == ny-1 && ix==0)
			{
				c.bc_cond[i][0] = bset.y;
				c.bc_cond[i][1] = bset.y;
				c.bc_cond[i][2] = bset.x;	
				c.bc_cond[i][3] = bset.x;	
				
			}	


			if( iy == ny-1 && ix==nx-1)
			{
				c.bc_cond[i][4] = bset.x;	
				c.bc_cond[i][5] = bset.x;	
				
			}	
						
				
		}
	}			
 	
   
    
       
}


std::pair<double, double> get_XY_for_rotated_hex(int ix, int iy, double angle) {
// 	double i = (iy % 2 == 0) ? ix : ix + 0.5;
// 	double j = iy * sqrt(3) / 2.0;

	double dc = BLK1::symmetry_constantx;
	double i,j;

		if (iy % 2 == 0)
		{
			i = dc*ix;
			j = dc*iy*sqrt(3.) / 2;

		}


		if (iy % 2 != 0)
		{

			i= dc*(ix + 0.5);

			j = dc*iy*sqrt(3.) / 2;
			
		}

	double beta = atan2(1.0*j , 1.0*i);
	double gamma = angle + beta;
// 	double r = sqrt(1.0*i*i + j*j);
// 	double x = r*cos(gamma);
// 	double y = r*sin(gamma);
	double r = sqrt(1.0*i*i + j*j);
	double x = r*cos(gamma);
	double y = r*sin(gamma);


	return std::pair<double, double>(x, y);
}
// 


std::pair<double, double> get_XY_for_rotated_tilted(int ix, int iy, double angle) {

// 	double i,j;
	double dc = BLK1::symmetry_constantx;
	double dcx = BLK1::symmetry_constantx;
	double dcy = BLK1::symmetry_constanty;


// 			if (iy % 2 == 0)
// 			{
// 				i = dcx*ix;
// 				j=  dcy*iy;
// 
// 			}
// 
// 
// 			if (iy % 2 != 0)
// 			{
// 
// 				i= dcx*ix + dcx / 2.;
// 				j= dcy*iy;
// 
// 			}

	double i = (iy % 2 == 0) ? dcx*ix : dcx*ix + dcx*0.5;
	double j = iy * dcy;


	double x = 1.0*i;
	double y = 1.0*j;


	return std::pair<double, double>(x, y);
}


int toIdx1D(int x, int y, int rowWidth) {
	return y*rowWidth + x;
}

void new_init_tilted_square(struct conf_stru& c) {
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dc = BLK1::symmetry_constantx;
	double dcx = BLK1::symmetry_constantx;
	double dcy = BLK1::symmetry_constanty;

	int ix, iy;

	for (int i = 0; i < nx*ny; ++i)
	{
		iy = i / nx;
		ix = i % nx;


		// dnx, dny - TO BE CHECKED; MAYBE IMPORTANT !!!!
		double hix, hiy;
// 		hix = (iy % 2 == 0) ? ix : ix + 0.5;
// 		hiy = iy * sqrt(3) / 2.0;

			if (iy % 2 == 0)
			{
				hix = dcx*ix;
				hiy=  dcy*iy;

			}


			if (iy % 2 != 0)
			{

				hix= dcx*ix + dcx / 2.;
				hiy= dcy*iy;

			}
		c.p[i].x = hix;
		c.p[i].y = hiy;



		//  NEIGHBORDS OF POINTS TRIANGULATION

		if (iy % 2 == 0)
		{
// 			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[0] = toIdx1D(ix, iy + 1, nx); //NORTH EAST
			c.p[i].nn[1] = toIdx1D(ix - 1, iy + 1, nx); //NORTH WEST
// 			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[2] = toIdx1D(ix - 1, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[3] = toIdx1D(ix, iy - 1, nx); //SOUTH EAST
		}
		else
		{
// 			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[0] = toIdx1D(ix + 1, iy + 1, nx); //NORTH EAST
			c.p[i].nn[1] = toIdx1D(ix, iy + 1, nx); //NORTH WEST
// 			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[2] = toIdx1D(ix, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[3] = toIdx1D(ix + 1, iy - 1, nx); //SOUTH EAST

		}

		c.p[i].nn[4] = c.p[i].nn[0];
		c.alloying[i][0]  = BLK1::beta_Var;
		c.alloying[i][1]  = BLK1::beta_Var;
		c.alloying[i][2]  = BLK1::beta_Var;
		c.disorder[i][0] = 1.*pow(BLK1::scale,-4);
		c.disorder[i][1] = 1.*pow(BLK1::scale,-4);
		c.disorder[i][2] = 1.*pow(BLK1::scale,-4);

	}


	for (int i = 0; i < nx*ny; ++i)
	{
			iy = i / nx;
			ix = i % nx;

			int no, ne, nw, se, sw;

			if (iy % 2 == 0)
			{
				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix;
				nw = (iy + 1)*nx + ix - 1;
				se = (iy - 1)*nx + ix;
				sw = (iy - 1)*nx + ix - 1;

				if (ix == 0)
				{
					nw = (iy + 1)*nx + (nx - 1);
					sw = (iy - 1)*nx + (nx - 1);


				}

				if (iy == 0)
				{
					se = (ny - 1)*nx + ix;
					sw = (ny - 1)*nx + ix - 1;

				}

				if (iy == 0 && ix == 0)
				{
					ne = (iy + 1)*nx + ix;
					nw = (iy + 1)*nx + nx - 1;
					se = (ny - 1)*nx + 0;
					sw = (ny - 1)*nx + nx - 1;



				}

			}


			if (iy % 2 != 0)
			{

				no = (iy)*nx + ix;

				ne = (iy + 1)*nx + ix + 1;
				nw = (iy + 1)*nx + ix;
				se = (iy - 1)*nx + ix + 1;
				sw = (iy - 1)*nx + ix;

				if (iy == ny - 1)
				{
					ne = (0)*nx + ix + 1;
					nw = (0)*nx + ix;

				}

				if (ix == nx - 1)
				{
					ne = (iy + 1)*nx + 0;
					se = (iy - 1)*nx + 0;

				}

				if (iy == ny - 1 && ix == nx - 1)
				{
					ne = 0;
					nw = nx - 1;

				}

			}

			//  NEIGHBORDS OF POINTS TRIANGULATION FOR TILTED

			c.p[i].nn[0] = ne; //NORTHEAST
			c.p[i].nn[1] = nw; //NORTHWEST    
			c.p[i].nn[2] = sw; //SOUTHEAST
			c.p[i].nn[3] = se; //SOUTHWEST     


			//IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS


			c.p[i].nn[4] = c.p[i].nn[0];
			c.p[i].nn[4] = c.p[i].nn[0];


		}

// 
// 	// PERIODIC CONDITIONS FOR X LINES (BOTTOM Y=0; TOP Y = NY-1) WITHOUT CORNERS
// 	for (int px = 1; px < nx - 1 - 1; ++px) {
// 		c.p[(ny - 1)*nx + px].nn[0] = toIdx1D(px + 1, 0, nx); //NORTH EAST
// 		c.p[(ny - 1)*nx + px].nn[1] = toIdx1D(px, 0, nx); //NORTH WEST
// 
// 		c.p[px].nn[2] = toIdx1D(px - 1, ny - 1, nx); //SOUTH WEST
// 		c.p[px].nn[3] = toIdx1D(px, ny - 1, nx); //SOUTH EAST
// 	}
// 
// 	// PERIODIC CONDITIONS FOR Y LINES (LEFT X=0; RIGHT X = NX-1) WITHOUT CORNERS
// 	for (int py = 2; py < ny - 1; py += 2) {
// 		c.p[toIdx1D(0, py, nx)].nn[1] = toIdx1D(nx - 1, py + 1, nx); //NORTH WEST
// // 		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST
// 		c.p[toIdx1D(0, py, nx)].nn[2] = toIdx1D(nx - 1, py - 1, nx); //SOUTH WEST
// 
// // 		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
// 	}
// 
// 	for (int py = 1; py < ny - 1; py += 2) {
// // 		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST
// 
// // 		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py + 1, nx); //NORTH EAST
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[3] = toIdx1D(0, py - 1, nx); //SOUTH EAST	
// 	}
// 
// 	// PERIODIC CONDITIONS -- CORNERS
// 
// 	// BOTTOM LEFT
// 	c.p[0].nn[1] = toIdx1D(nx - 1, 1, nx); //NORTH WEST
// // 	c.p[0].nn[3] = toIdx1D(nx - 1, 0, nx); //WEST
// 	c.p[0].nn[2] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH WEST
// 	c.p[0].nn[3] = toIdx1D(0, ny - 1, nx); //SOUTH EAST
// 
// 
// 	// BOTTOM RIGTH	
// // 	c.p[nx - 1].nn[0] = toIdx1D(0, 0, nx); //EAST
// 	c.p[nx - 1].nn[2] = toIdx1D(nx - 2, ny - 1, nx); //SOUTH WEST
// 	c.p[nx - 1].nn[3] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH EAST
// 
// 	// TOP RIGHT
// // 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[0] = toIdx1D(0, ny - 1, nx); //EAST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[0] = toIdx1D(0, 0, nx); //NORTH EAST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[1] = toIdx1D(nx - 1, 0, nx); //NORTH WEST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[3] = toIdx1D(0, ny - 2, nx); //SOUTH EAST
// 
// 	// TOP LEFT
// 	c.p[toIdx1D(0, ny - 1, nx)].nn[0] = toIdx1D(1, 0, nx); //NORTH EAST
// 	c.p[toIdx1D(0, ny - 1, nx)].nn[1] = toIdx1D(0, 0, nx); //NORTH WEST
// // 	c.p[toIdx1D(0, ny - 1, nx)].nn[3] = toIdx1D(nx - 1, ny - 1, nx); //WEST

	c.pfix = c.p;
	c.pcons = c.pfix;

 }




//classical hexagonal deal with periodic boundaries
void initHex6(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int ix, iy;
	double dc = BLK1::symmetry_constantx;
	double dcx = BLK1::symmetry_constantx;
	double dcy = BLK1::symmetry_constanty;
	double h=dimensions::dh;
	dc*=h;

	for (int i = 0; i < nx*ny; ++i)
	{
		iy = i / nx;
		ix = i % nx;


		// dnx, dny - TO BE CHECKED; MAYBE IMPORTANT !!!!
		double hix, hiy;
// 		hix = (iy % 2 == 0) ? ix : ix + 0.5;
// 		hiy = iy * sqrt(3) / 2.0;
		if (iy % 2 == 0)
		{
			hix = dc*ix;
			hiy = dc*iy*sqrt(3) / 2;

		}


		if (iy % 2 != 0)
		{

			hix= dc*(ix + 0.5);

			hiy = dc*iy*sqrt(3) / 2;
			
		}
			
			
			
		c.p[i].x = hix;
		c.p[i].y = hiy;

		//double dix = double(ix - dnx);
		//double diy = double(iy - dny);

		//c.p[i].x = dix;
		//c.p[i].y = diy;

		//  NEIGHBORDS OF POINTS TRIANGULATION

		if (iy % 2 == 0)
		{
			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[1] = toIdx1D(ix, iy + 1, nx); //NORTH EAST
			c.p[i].nn[2] = toIdx1D(ix - 1, iy + 1, nx); //NORTH WEST
			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[4] = toIdx1D(ix - 1, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[5] = toIdx1D(ix, iy - 1, nx); //SOUTH EAST
		}
		else
		{
			c.p[i].nn[0] = toIdx1D(ix + 1, iy, nx); //EAST
			c.p[i].nn[1] = toIdx1D(ix + 1, iy + 1, nx); //NORTH EAST
			c.p[i].nn[2] = toIdx1D(ix, iy + 1, nx); //NORTH WEST
			c.p[i].nn[3] = toIdx1D(ix - 1, iy, nx); //WEST
			c.p[i].nn[4] = toIdx1D(ix, iy - 1, nx); //SOUTH WEST
			c.p[i].nn[5] = toIdx1D(ix + 1, iy - 1, nx); //SOUTH EAST

		}

		c.p[i].nn[6] = c.p[i].nn[0];
	}

	for (int i = 0; i < nx*ny; ++i)
	{
			iy = i / nx;
			ix = i % nx;

			int no, ne, nw, se, sw,east,west;

			if (iy % 2 == 0)
			{
				no = (iy)*nx + ix;

				east = toIdx1D(ix + 1, iy, nx); //EAST
				ne = toIdx1D(ix, iy + 1, nx); //NORTH EAST
				nw = toIdx1D(ix - 1, iy + 1, nx); //NORTH WEST
				west = toIdx1D(ix - 1, iy, nx); //WEST
				sw = toIdx1D(ix - 1, iy - 1, nx); //SOUTH WEST
				se = toIdx1D(ix, iy - 1, nx); //SOUTH EAST

				if (ix == 0)
				{
					nw = (iy + 1)*nx + (nx - 1);
					sw = (iy - 1)*nx + (nx - 1);
					west = (iy )*nx + (nx - 1);


				}
				
				if (ix == nx-1)
				{
					east = (iy )*nx + 0;


				}				

				if (iy == 0)
				{
					se = (ny - 1)*nx + ix;
					sw = (ny - 1)*nx + ix - 1;

				}

				if (iy == 0 && ix == 0)
				{
					ne = (iy + 1)*nx + ix;
					nw = (iy + 1)*nx + nx - 1;
					se = (ny - 1)*nx + 0;
					sw = (ny - 1)*nx + nx - 1;
					west = (iy )*nx + (nx - 1);


				}

			}


			if (iy % 2 != 0)
			{

				east = toIdx1D(ix + 1, iy, nx); //EAST
				ne = toIdx1D(ix + 1, iy + 1, nx); //NORTH EAST
				nw = toIdx1D(ix, iy + 1, nx); //NORTH WEST
				west = toIdx1D(ix - 1, iy, nx); //WEST
				sw = toIdx1D(ix, iy - 1, nx); //SOUTH WEST
				se = toIdx1D(ix + 1, iy - 1, nx); //SOUTH EAST

				if (iy == ny - 1)
				{
					ne = (0)*nx + ix + 1;
					nw = (0)*nx + ix;

				}

				if (ix == nx - 1)
				{
					ne = (iy + 1)*nx + 0;
					se = (iy - 1)*nx + 0;
					east = (iy )*nx + 0;

				}
				
				if (ix == 0)
				{
					west = (iy )*nx + nx-1;

				}	
				
				
							

				if (iy == ny - 1 && ix == nx - 1)
				{
					ne = 0;
					nw = nx - 1;
					east = (iy )*nx + 0;

				}

			}

			//  NEIGHBORDS OF POINTS TRIANGULATION FOR TILTED
			c.p[i].nn[0] = east; //NORTHEAST
			c.p[i].nn[1] = ne; //NORTHEAST
			c.p[i].nn[2] = nw; //NORTHWEST    
			c.p[i].nn[3] = west; //NORTHWEST    
			c.p[i].nn[4] = sw; //SOUTHEAST
			c.p[i].nn[5] = se; //SOUTHWEST     


			//IF PERIODIC BOUNDARY CONDITION WANTED	 OF POINTS


			c.p[i].nn[6] = c.p[i].nn[0];
			
			c.disorder[i][0] = 1.*pow(BLK1::scale,-4);
			c.disorder[i][1] = 1.*pow(BLK1::scale,-4);
			c.disorder[i][2] = 1.*pow(BLK1::scale,-4);
			c.disorder[i][3] = 1.*pow(BLK1::scale,-4);
			c.alloying[i][0]  = BLK1::beta_Var;
			c.alloying[i][1]  = BLK1::beta_Var;
			c.alloying[i][2]  = BLK1::beta_Var;
			c.alloying[i][3]  = BLK1::beta_Var;




		}
// 	PERIODIC CONDITIONS FOR X LINES (BOTTOM Y=0; TOP Y = NY-1) WITHOUT CORNERS
// 	for (int px = 1; px < nx - 1 - 1; ++px) {
// 		c.p[(ny - 1)*nx + px].nn[1] = toIdx1D(px + 1, 0, nx); //NORTH EAST
// 		c.p[(ny - 1)*nx + px].nn[2] = toIdx1D(px, 0, nx); //NORTH WEST
// 
// 		c.p[px].nn[4] = toIdx1D(px - 1, ny - 1, nx); //SOUTH WEST
// 		c.p[px].nn[5] = toIdx1D(px, ny - 1, nx); //SOUTH EAST
// 	}
// 
// 	PERIODIC CONDITIONS FOR Y LINES (LEFT X=0; RIGHT X = NX-1) WITHOUT CORNERS
// 	for (int py = 2; py < ny - 1; py += 2) {
// 		c.p[toIdx1D(0, py, nx)].nn[2] = toIdx1D(nx - 1, py + 1, nx); //NORTH WEST
// 		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST
// 		c.p[toIdx1D(0, py, nx)].nn[4] = toIdx1D(nx - 1, py - 1, nx); //SOUTH WEST
// 
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
// 	}
// 
// 	for (int py = 1; py < ny - 1; py += 2) {
// 		c.p[toIdx1D(0, py, nx)].nn[3] = toIdx1D(nx - 1, py, nx); //WEST
// 
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[0] = toIdx1D(0, py, nx); //EAST
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[1] = toIdx1D(0, py + 1, nx); //NORTH EAST
// 		c.p[toIdx1D(nx - 1, py, nx)].nn[5] = toIdx1D(0, py - 1, nx); //SOUTH EAST	
// 	}
// 
// 	PERIODIC CONDITIONS -- CORNERS
// 
// 	BOTTOM LEFT
// 	c.p[0].nn[2] = toIdx1D(nx - 1, 1, nx); //NORTH WEST
// 	c.p[0].nn[3] = toIdx1D(nx - 1, 0, nx); //WEST
// 	c.p[0].nn[4] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH WEST
// 	c.p[0].nn[5] = toIdx1D(0, ny - 1, nx); //SOUTH EAST
// 
// 
// 	BOTTOM RIGTH	
// 	c.p[nx - 1].nn[0] = toIdx1D(0, 0, nx); //EAST
// 	c.p[nx - 1].nn[4] = toIdx1D(nx - 2, ny - 1, nx); //SOUTH WEST
// 	c.p[nx - 1].nn[5] = toIdx1D(nx - 1, ny - 1, nx); //SOUTH EAST
// 
// 	TOP RIGHT
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[0] = toIdx1D(0, ny - 1, nx); //EAST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[1] = toIdx1D(0, 0, nx); //NORTH EAST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[2] = toIdx1D(nx - 1, 0, nx); //NORTH WEST
// 	c.p[toIdx1D(nx - 1, ny - 1, nx)].nn[5] = toIdx1D(0, ny - 2, nx); //SOUTH EAST
// 
// 	TOP LEFT
// 	c.p[toIdx1D(0, ny - 1, nx)].nn[1] = toIdx1D(1, 0, nx); //NORTH EAST
// 	c.p[toIdx1D(0, ny - 1, nx)].nn[2] = toIdx1D(0, 0, nx); //NORTH WEST
// 	c.p[toIdx1D(0, ny - 1, nx)].nn[3] = toIdx1D(nx - 1, ny - 1, nx); //WEST
	c.pfix = c.p;
	c.pcons = c.pfix;


}









void new_init_rotated_hex(struct conf_stru& c, double angle) {
	int width  = dimensions::nx-1;
	int height = dimensions::ny-1;
	conf_stru temp_structure;
	punto_stru origin_point;
	origin_point.x = 0;
	origin_point.y = 0;
	origin_point.i = 0;
	origin_point.j = 0;
// 	temp_structure.p.push_back(origin_point);

	bool i_loop = true;
	bool jmax_loop = true;
	bool jmin_loop = true;
	int i = 0;
	int j = 0;

	angle = toRadians(angle);
	square_grid_creator t;
	t.set_values(width,height,angle);
        cout<<"while loop starts for rotation angle: "<<angle<<endl;
    bool added = true;
	while (added) {
			added = false;

			j = 0;
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= 0.0 && x - width <= 0.1) && (y - height <= 0.1)) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}


		j = 1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x >= -0.1 && x - width <= 0.1) && (y - height <= 0.1 )) {
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i;
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (y >= height || x < -0.011 ) 
				break;
			j++;
		}

		j = -1;
		while (true) {
			auto real_coordinates = get_XY_for_rotated_hex(i, j, angle);
			double x = real_coordinates.first;
			double y = real_coordinates.second;
			if ((x - width <= 0.1) && (y >= -0.01)  && (y - height <= 0.1) ) {
// 				cout<<"we are inside j=-1 loop"<<endl;
				punto_stru next_point;
				next_point.x = x;
				next_point.y = y;
				next_point.i = i; 
				next_point.j = j;
				temp_structure.p.push_back(next_point);
				added = true;
			}
			else if (x >= width || y < -0.011 ) {
				break;
			}
			j--;
		}

		i++;
	}
	
	t.set_size(temp_structure.p.size());
	t.nn_list_hex(temp_structure);



                
                
	
    int n = temp_structure.p.size();

    force_energy::calculation::getInstance().setSize(n, width, height);
	cout << c.p.size() << endl;
	cout << n << endl;
        
	for (int k = 0; k < n; ++k) {
		c.p[k]    = temp_structure.p[k];
		c.pcons[k] = temp_structure.p[k];
		c.pfix[k]  = temp_structure.p[k];
		c.alloying[k][0]  = BLK1::beta_Var;
		c.alloying[k][1]  = BLK1::beta_Var;
		c.alloying[k][2]  = BLK1::beta_Var;
		c.disorder[k][0] = 1.*pow(BLK1::scale,-4);
		c.disorder[k][1] = 1.*pow(BLK1::scale,-4);
		c.disorder[k][2] = 1.*pow(BLK1::scale,-4);
	}
     
        

}




