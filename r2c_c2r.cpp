#include <cmath>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#include "structures.h"

#include "r2c_c2r.h"

void r2c(double *x_real, MKL_Complex16 *x_cmplx, const int N)
{
    /* Size of 1D transform */
//     const int N = 6;	
 

    /* Arbitrary harmonic used to verify FFT */
    int H = -1;

    /* Execution status */
    MKL_LONG status = 0;
    
    std::vector<double> aaa;
    aaa.resize(N);

    /* Pointers to input and output data */
//     double *x_real = NULL;
//     MKL_Complex16 *x_cmplx = NULL;

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", version);
// 
//     printf("Example basic_dp_real_dft_1d\n");
//     printf("Forward-Backward double-precision real-to-complex"
//            " out-of-place 1D transform\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 1\n");
//     printf(" DFTI_LENGTHS                  = {%d}\n", N);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL,
                                  1, (MKL_LONG)N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Commit the  descriptor\n");
    status = DftiCommitDescriptor(hand);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Allocate data arrays\n");
//     x_real  = (double*)mkl_malloc(N*sizeof(double), 64);
//     x_cmplx = (MKL_Complex16*)mkl_malloc((N/2+1)*sizeof(MKL_Complex16), 64);
//     if (x_real == NULL || x_cmplx == NULL) goto failed;

//     printf("Initialize data for real-to-complex FFT\n");
//     init_r(x_real, N, H);

//     printf("Compute forward transform\n");
    status = DftiComputeForward(hand, x_real, x_cmplx);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Verify the result\n");
// //     status = verify_c(x_cmplx, N, H);
// //     if (status != 0) goto failed;
// 
// //     printf("Initialize data for complex-to-real FFT\n");
// //     init_c(x_cmplx, N, H);
// 
// //     printf("Compute backward transform\n");
// //     status = DftiComputeBackward(hand, x_cmplx, x_real);
// //     if (status != DFTI_NO_ERROR) goto failed;
// 
//     printf("Verify the result\n");
//     status = verify_r(x_real, N, H);
//     if (status != 0) goto failed;

 cleanup:

//     printf("Free DFTI descriptor\n");
    DftiFreeDescriptor(&hand);
    

//     printf("Free data arrays\n");
//     mkl_free(x_real);
//     mkl_free(x_cmplx);

//     printf("TEST %s\n", (status == 0) ? "PASSED" : "FAILED");
//     return status;

//  failed:
//     //printf(" ERROR, status = "LI"\n", status);
//     cout<<" ERROR, status = "<< status<<std::endl;
// 
//     status = 1;
//     goto cleanup;
}

void c2r( MKL_Complex16 *x_cmplx,double *x_real, const int N)
{
    /* Size of 1D transform */
//     const int N = 6;
 

    /* Arbitrary harmonic used to verify FFT */
    int H = -1;

    /* Execution status */
    MKL_LONG status = 0;

    /* Pointers to input and output data */
//     double *x_real = NULL;
//     MKL_Complex16 *x_cmplx = NULL;

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", version);
// 
//     printf("Example basic_dp_real_dft_1d\n");
//     printf("Forward-Backward double-precision real-to-complex"
//            " out-of-place 1D transform\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 1\n");
//     printf(" DFTI_LENGTHS                  = {%d}\n", N);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");
    double Scale = 1.0/(double)(N);

//     printf("Create DFTI descriptor\n");
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL,
                                  1, (MKL_LONG)N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);
	
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, Scale);
 
//     printf("Commit the  descriptor\n");
    status = DftiCommitDescriptor(hand);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Allocate data arrays\n");
//     x_real  = (double*)mkl_malloc(N*sizeof(double), 64);
//     x_cmplx = (MKL_Complex16*)mkl_malloc((N/2+1)*sizeof(MKL_Complex16), 64);
//     if (x_real == NULL || x_cmplx == NULL) goto failed;

//     printf("Initialize data for real-to-complex FFT\n");
//     init_r(x_real, N, H);

//     printf("Compute forward transform\n");
//     status = DftiComputeForward(hand, x_real, x_cmplx);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Verify the result\n");
//     status = verify_c(x_cmplx, N, H);
//     if (status != 0) goto failed;

//     printf("Initialize data for complex-to-real FFT\n");
//     init_c(x_cmplx, N, H);

//     printf("Compute backward transform\n");
    status = DftiComputeBackward(hand, x_cmplx, x_real);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Verify the result\n");
//     status = verify_r(x_real, N, H);
//     if (status != 0) goto failed;

 cleanup:

//     printf("Free DFTI descriptor\n");
    DftiFreeDescriptor(&hand);
    

//     printf("Free data arrays\n");
// //     mkl_free(x_real);
// //     mkl_free(x_cmplx);
// 
//     printf("TEST %s\n", (status == 0) ? "PASSED" : "FAILED");
//     return status;
// 
//  failed:
//     //printf(" ERROR, status = "LI"\n", status);
//     cout<<" ERROR, status = "<< status<<std::endl;
// 
//     status = 1;
//     goto cleanup;
}

/* Compute (K*L)%M accurately */
static double moda(int K, int L, int M)
{
    return (double)(((long long)K * L) % M);
}

/* Initialize array x(N) to produce unit peaks at y(H) and y(N-H) */
static void init_r(double *x, int N, int H)
{
    double TWOPI = 6.2831853071795864769, phase, factor;
    int n;

    factor = (2*(N-H)%N == 0) ? 1.0 : 2.0;
    for (n = 0; n < N; n++) {
        phase  = moda(n, H, N) / N;
        x[n] = factor * cos(TWOPI * phase) / N;
    }
}

/* Verify that x has unit peak at H */
static int verify_c(MKL_Complex16 *x, int N, int H)
{
    double err, errthr, maxerr;
    int n;

    /*
     * Note, this simple error bound doesn't take into account error of
     * input data
     */
    errthr = 2.5 * log((double) N) / log(2.0) * DBL_EPSILON;
    printf(" Check if err is below errthr %.3lg\n", errthr);

    maxerr = 0.0;
    for (n = 0; n < N/2+1; n++) {
        double re_exp = 0.0, im_exp = 0.0, re_got, im_got;

        if ((n-H)%N == 0 || (-n-H)%N == 0) re_exp = 1.0;

        re_got = x[n].real;
        im_got = x[n].imag;
        err  = fabs(re_got - re_exp) + fabs(im_got - im_exp);
        if (err > maxerr) maxerr = err;
        if (!(err < errthr)) {
            printf(" x[%i]: ", n);
            printf(" expected (%.17lg,%.17lg), ", re_exp, im_exp);
            printf(" got (%.17lg,%.17lg), ", re_got, im_got);
            printf(" err %.3lg\n", err);
            printf(" Verification FAILED\n");
            return 1;
        }
    }
    printf(" Verified,  maximum error was %.3lg\n", maxerr);
    return 0;
}

/* Initialize array x(N) to produce unit peak at y(H) */
static void init_c(MKL_Complex16 *x, int N, int H)
{
    double TWOPI = 6.2831853071795864769, phase;
    int n;

    for (n = 0; n < N/2+1; n++) {
        phase  = moda(n, H, N) / N;
        x[n].real =  cos(TWOPI * phase) / N;
        x[n].imag = -sin(TWOPI * phase) / N;
    }
}

/* Verify that x has unit peak at H */
static int verify_r(double *x, int N, int H)
{
    double err, errthr, maxerr;
    int n;

    /*
     * Note, this simple error bound doesn't take into account error of
     * input data
     */
    errthr = 2.5 * log((double) N) / log(2.0) * DBL_EPSILON;
    printf(" Check if err is below errthr %.3lg\n", errthr);

    maxerr = 0.0;
    for (n = 0; n < N; n++) {
        double re_exp = 0.0, re_got;

        if ((n-H)%N == 0) re_exp = 1.0;

        re_got = x[n];
        err  = fabs(re_got - re_exp);
        if (err > maxerr) maxerr = err;
        if (!(err < errthr)) {
            printf(" x[%i]: ", n);
            printf(" expected %.17lg, ", re_exp);
            printf(" got %.17lg, ", re_got);
            printf(" err %.3lg\n", err);
            printf(" Verification FAILED\n");
            return 1;
        }
    }
    printf(" Verified,  maximum error was %.3lg\n", maxerr);
    return 0;
}







void r2c_2D(double *data_real, MKL_Complex16 *data_cmplx, const int nx, const int ny)
{
    /* Size of 2D transform */
    int N1 = nx;
    int N2 = ny;
    MKL_LONG N[2] = {N2, N1};

    /* Arbitrary harmonic used to verify FFT */
    int H1 = -1, H2 = 2;

    /* Execution status */
    MKL_LONG status = 0;

    /* Pointers to input and output data */
//     double *data_real = NULL;
//     MKL_Complex16 *data_cmplx = NULL;

    /* Strides describe data layout in real and conjugate-even domain */
    MKL_LONG rs[3] = {0, N1, 1};
    MKL_LONG cs[3] = {0, N1/2+1, 1};

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", version);
// 
//     printf("Example basic_dp_real_dft_2d\n");
//     printf("Forward-Backward double-precision 2D real out-of-place FFT\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 2\n");
//     printf(" DFTI_LENGTHS                  = {%i, %i}\n", N2, N1);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, 2, N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set input  strides = ");
//     printf("{"LI", "LI", "LI"}\n", rs[0], rs[1], rs[2]);
    status = DftiSetValue(hand, DFTI_INPUT_STRIDES, rs);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set output strides = ");
//     printf("{"LI", "LI", "LI"}\n", cs[0], cs[1], cs[2]);
    status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, cs);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Commit the descriptor\n");
    status = DftiCommitDescriptor(hand);
//     if (status != DFTI_NO_ERROR) goto failed;

    status = DftiComputeForward(hand, data_real, data_cmplx);
//     if (status != DFTI_NO_ERROR) goto failed;



}




void c2r_2D(MKL_Complex16 *data_cmplx, double *data_real, const int nx, const int ny)
{
    /* Size of 2D transform */
    int N1 = nx; 
    int N2 = ny;
    MKL_LONG N[2] = {N2, N1};

    /* Arbitrary harmonic used to verify FFT */
    int H1 = -1, H2 = 2;

    /* Execution status */
    MKL_LONG status = 0;

    /* Pointers to input and output data */
//     double *data_real = NULL;
//     MKL_Complex16 *data_cmplx = NULL;

    /* Strides describe data layout in real and conjugate-even domain */
    MKL_LONG rs[3] = {0, N1, 1};
    MKL_LONG cs[3] = {0, N1/2+1, 1};

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", versison);

//     printf("Example basic_dp_real_dft_2d\n");
//     printf("Forward-Backward double-precision 2D real out-of-place FFT\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 2\n");
//     printf(" DFTI_LENGTHS                  = {%i, %i}\n", N2, N1);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, 2, N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//     if (status != DFTI_NO_ERROR) goto failed;
    double Scale = 1.0/(double)(nx*ny);
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, Scale);

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);

    status = DftiSetValue(hand, DFTI_INPUT_STRIDES, cs);
    status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, rs);
//     printf("Commit the descriptor\n");
    status = DftiCommitDescriptor(hand);

//     printf("Compute backward transform\n");
    status = DftiComputeBackward(hand, data_cmplx, data_real);

 
}


void r2c_3D(double *data_real, MKL_Complex16 *data_cmplx, const int nx, const int ny, const int nz)
{
    /* Size of 3D transform */
    int N1 = nx;
    int N2 = ny;
    int N3 = nz;

    MKL_LONG N[3] = {N3, N2, N1};

    /* Arbitrary harmonic used to verify FFT */
    int H1 = -1, H2 = 2, H3 = 4;

    /* Execution status */
    MKL_LONG status = 0;

    /* Pointers to input and output data */
//     double *data_real = NULL;
//     MKL_Complex16 *data_cmplx = NULL;

    /* Strides describe data layout in real and conjugate-even domain */
    MKL_LONG rs[4] = {0, N2*(N1/2)*2, (N1/2)*2, 1};
    MKL_LONG cs[4] = {0, N2*(N1/2+1), (N1/2+1), 1};

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
    printf("%s\n", version);
// 
//     printf("Example basic_dp_real_dft_2d\n");
//     printf("Forward-Backward double-precision 2D real out-of-place FFT\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 2\n");
//     printf(" DFTI_LENGTHS                  = {%i, %i}\n", N2, N1);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
//     if (status != DFTI_NO_ERROR) goto failed;
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, 3, N);

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set input  strides = ");
//     printf("{"LI", "LI", "LI"}\n", rs[0], rs[1], rs[2]);
    status = DftiSetValue(hand, DFTI_INPUT_STRIDES, rs);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set output strides = ");
//     printf("{"LI", "LI", "LI"}\n", cs[0], cs[1], cs[2]);
    status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, cs);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Commit the descriptor\n");
    status = DftiCommitDescriptor(hand);
//     if (status != DFTI_NO_ERROR) goto failed;

    status = DftiComputeForward(hand, data_real, data_cmplx);
//     if (status != DFTI_NO_ERROR) goto failed;
    DftiFreeDescriptor(&hand);


}




void c2r_3D(MKL_Complex16 *data_cmplx, double *data_real, const int nx, const int ny, const int nz)
{
    /* Size of 3D transform */
    int N1 = nx;
    int N2 = ny;
    int N3 = nz;
    MKL_LONG N[3] = {N3, N2, N1};

    /* Arbitrary harmonic used to verify FFT */
    int H1 = -1, H2 = 2, H3 = 4;

    /* Execution status */
    MKL_LONG status = 0;

    /* Pointers to input and output data */
//     double *data_real = NULL;
//     MKL_Complex16 *data_cmplx = NULL;

    /* Strides describe data layout in real and conjugate-even domain */
    MKL_LONG rs[4] = {0, N2*(N1/2)*2, (N1/2)*2, 1};
    MKL_LONG cs[4] = {0, N2*(N1/2+1), (N1/2+1), 1};

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];

    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", versison);

//     printf("Example basic_dp_real_dft_2d\n");
//     printf("Forward-Backward double-precision 2D real out-of-place FFT\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 2\n");
//     printf(" DFTI_LENGTHS                  = {%i, %i}\n", N2, N1);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
    status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, 3, N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
    
    double Scale = 1.0/(double)(nx*ny*nz);
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, Scale);

    
    
    
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);

    status = DftiSetValue(hand, DFTI_INPUT_STRIDES, cs);
    status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, rs);
//     printf("Commit the descriptor\n");
    status = DftiCommitDescriptor(hand);

//     printf("Compute backward transform\n");
    status = DftiComputeBackward(hand, data_cmplx, data_real);

     DftiFreeDescriptor(&hand);

}

#define MKL_Complex16 std::complex<double>

void r2c_3D_special(std::vector<double>& data_real, std::vector<MKL_Complex16>& data_cmplx, const int nx, const int ny, const int nz)
{
    /* Size of 3D transform */
    MKL_LONG N1 = nx;
    MKL_LONG N2 = ny;
    MKL_LONG N3 = nz;
    int n_tot = nx*ny*nz;
    int n_complex = (nx/2+1)*ny*nz;

    double dnx=nx;
    double dny=ny;
    double dnz=nz;
    int n_dim;    
 
//     double *x_real= NULL;
// 
//     MKL_Complex16 *x_cmplxt = NULL;
//   
// 	x_real= (double*)mkl_malloc(n_tot*sizeof(double), 64);
// 
//     x_cmplxt = (MKL_Complex16*)mkl_malloc(n_complex*sizeof(MKL_Complex16), 64);
 
//  	for(int i=0;i<n_tot;++i)
//  		x_real[i] = data_real[i];
 		

    if(nz != 1 && ny != 1 && nx!= 1)
		n_dim = 3;
    if(nz == 1 && ny != 1 && nx!= 1 )
    	n_dim = 2;
    if(nz == 1 && ny == 1 && nx!= 1)
    	n_dim = 1;
 
//       n_dim=1;
//     cout<<"dimension: "<<n_dim<<std::endl;

    MKL_LONG *N = NULL;
    MKL_LONG *rs = NULL;
    MKL_LONG *cs = NULL;

      N  = (MKL_LONG*)mkl_malloc(n_dim*sizeof(MKL_LONG), 64);
     rs  = (MKL_LONG*)mkl_malloc((n_dim+1)*sizeof(MKL_LONG), 64);
     cs  = (MKL_LONG*)mkl_malloc((n_dim+1)*sizeof(MKL_LONG), 64);
// 
// 	
     if(n_dim==1 ){
      N[0] =  N1;
     rs[0]= 0;
     rs[1]= 1;
     cs[0]= 0;
     cs[1]= 1;
  	} 

//     else if(n_dim==2 ){
//       N[0] = N2;  N[1] = N1;
//      rs[0] = 0; rs[1] = N1; rs[2] = 1;
// 	 cs[0] = 0; cs[1] = N1/2+1; cs[2] = 1;
// //      MKL_LONG rs[3] = {0, N1, 1};
// //     MKL_LONG cs[3] = {0, N1/2+1, 1};
//   	 
//   	} 	 
//     else if(n_dim==3 ){
//  	  N[0] = N1; N[1] = N2; N[2] = N3;
//      rs[3] = 1;             cs[3] = 1;
//      rs[2] = (N3/2+1)*2;    cs[2] = (N3/2+1);
//      rs[1] = N2*(N3/2+1)*2; cs[1] = N2*(N3/2+1);
//      rs[0] = 0;             cs[0] = 0;
// 
//    	}

    else if(n_dim==2 ){
      N[0] = N2;  N[1] = N1;
     rs[0] = 0; rs[1] = N1; rs[2] = 1;
	 cs[0] = 0; cs[1] = N1/2+1; cs[2] = 1;
 
//     MKL_LONG rs[3] = {0, N1, 1};
//     MKL_LONG cs[3] = {0, N1/2+1, 1};
	 
  	} 	 
    else if(n_dim==3 ){
//  	  N[0] = N1; N[1] = N2; N[2] = N3;
 	  N[0] = N3 ; N[1] =N2; N[2]  = N1;

     rs[3] = 1;     cs[3] = 1;
     rs[2] = N1;    cs[2] = (N1/2+1);
     rs[1] = N2*N1; cs[1] =  N2*(N1/2+1);
     rs[0] = 0;     cs[0] = 0;
// 

    /* Compute strides */
//     rs[3] = 1;             cs[3] = 1;
//     rs[2] = (N3/2+1)*2;    cs[2] = (N3/2+1);
//     rs[1] = N2*(N3/2+1)*2; cs[1] = N2*(N3/2+1);
//     rs[0] = 0;             cs[0] = 0;
//     MKL_LONG rs[4] = {0, N2*(N1/2+1)*2, (N1/2+1)*2, 1};
//     MKL_LONG cs[4] = {0, N2*(N1/2+1), (N1/2+1), 1};


   	}


    /* Execution status */
    MKL_LONG status = 0;

  
    



    DFTI_DESCRIPTOR_HANDLE hand = NULL;


    if(n_dim == 1) 
    	status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, n_dim, N1);
    if(n_dim > 1) 
    	status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, n_dim, N);


    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);
    if(n_dim != 1){ 
		status = DftiSetValue(hand, DFTI_INPUT_STRIDES, rs);
		status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, cs);
    }
//     double Scale = 1.0/(double)(dnx*dny*dnz);
// 	status = DftiSetValue(hand, DFTI_FORWARD_SCALE, Scale);

    status = DftiCommitDescriptor(hand);

    status = DftiComputeForward(hand, data_real.data(), data_cmplx.data() );
    
//      	for(int i=0;i<n_complex;++i){ 
// // 			struct lcomplex m1 ;
// // 			m1.re = x_cmplxt[i].real;
// // 			m1.im = x_cmplxt[i].imag;
// // 			complex<double> v1,v2,v3;	
// // 			v1 = complex<double>(m1.re, m1.im);
// // 			data_cmplx[i] = v1;
// 			data_cmplx[i] = x_cmplxt[i];
// 
// 						
//  		}

    DftiFreeDescriptor(&hand);
    mkl_free(N);
    mkl_free(cs);
    mkl_free(rs);
    // mkl_free(x_real);
//     mkl_free(x_cmplxt);



}



void c2r_3D_special(std::vector<MKL_Complex16>& data_cmplx,std::vector<double>& data_real, const int nx, const int ny, const int nz)
{
    /* Size of 3D transform */
    /* Size of 3D transform */
    MKL_LONG N1 = nx;
    MKL_LONG N2 = ny;
    MKL_LONG N3 = nz;
    double dnx=nx;
    double dny=ny;
    double dnz=nz;
    int n_dim;    
    
    int n_tot = nx*ny*nz;
    int n_complex = (nx/2+1)*ny*nz;
//     double *x_real= NULL;
// 
//     MKL_Complex16 *x_cmplxt = NULL;
// 
// 	x_real= (double*)mkl_malloc(n_tot*sizeof(double), 64);
// 
//     x_cmplxt = (MKL_Complex16*)mkl_malloc(n_complex*sizeof(MKL_Complex16), 64);
 
//  		
//      	for(int i=0;i<n_complex;++i){ 
// // 			x_cmplxt[i].real = data_cmplx[i].real();
// // 			x_cmplxt[i].imag = data_cmplx[i].imag();
// 			x_cmplxt[i]  = data_cmplx[i];
// 						
//  		}

    if(nz != 1 && ny != 1 && nx!= 1)
		n_dim = 3;
    if(nz == 1 && ny != 1 && nx!= 1 )
    	n_dim = 2;
    if(nz == 1 && ny == 1 && nx!= 1)
    	n_dim = 1;
    	
//      cout<<"dimension: "<<n_dim<< std::endl;
 
    MKL_LONG *N = NULL;
    MKL_LONG *rs = NULL;
    MKL_LONG *cs = NULL;

      N  = (MKL_LONG*)mkl_malloc(n_dim*sizeof(MKL_LONG), 64);
     rs  = (MKL_LONG*)mkl_malloc((n_dim+1)*sizeof(MKL_LONG), 64);
     cs  = (MKL_LONG*)mkl_malloc((n_dim+1)*sizeof(MKL_LONG), 64);
// 
// 	
     if(n_dim==1 ){
      N[0] =  N1;
     rs[0]= 0;
     rs[1]= 1;
     cs[0]= 0;
     cs[1]= 1;
  	} 

    else if(n_dim==2 ){
 
      N[0] = N2;  N[1] = N1;
     rs[0] = 0; rs[1] = N1; rs[2] = 1;
	 cs[0] = 0; cs[1] = N1/2+1; cs[2] = 1;
	 
  	} 	 
    else if(n_dim==3 ){
//  	  N[0] = N1; N[1] = N2; N[2] = N3;
//      rs[3] = 1;             cs[3] = 1;
//      rs[2] = (N3/2+1)*2;    cs[2] = (N3/2+1);
//      rs[1] = N2*(N3/2+1)*2; cs[1] = N2*(N3/2+1);
//      rs[0] = 0;             cs[0] = 0;
// 

    /* Compute strides */
 	  N[0] = N3 ; N[1] =N2; N[2]  = N1;
//     MKL_LONG rs[4] = {0, N2*(N1/2+1)*2, (N1/2+1)*2, 1};
//     MKL_LONG cs[4] = {0, N2*(N1/2+1), (N1/2+1), 1};

     rs[3] = 1;     cs[3] = 1;
     rs[2] = N1;    cs[2] = (N1/2+1);
     rs[1] = N2*N1; cs[1] =  N2*(N1/2+1);
     rs[0] = 0;     cs[0] = 0;


   	}

   


    /* Execution status */
    MKL_LONG status = 0;

  

    DFTI_DESCRIPTOR_HANDLE hand = NULL;

    char version[DFTI_VERSION_LENGTH];


    DftiGetValue(0, DFTI_VERSION, version);
//     printf("%s\n", versison);

//     printf("Example basic_dp_real_dft_2d\n");
//     printf("Forward-Backward double-precision 2D real out-of-place FFT\n");
//     printf("Configuration parameters:\n");
//     printf(" DFTI_PRECISION                = DFTI_DOUBLE\n");
//     printf(" DFTI_FORWARD_DOMAIN           = DFTI_REAL\n");
//     printf(" DFTI_DIMENSION                = 2\n");
//     printf(" DFTI_LENGTHS                  = {%i, %i}\n", N2, N1);
//     printf(" DFTI_PLACEMENT                = DFTI_NOT_INPLACE\n");
//     printf(" DFTI_CONJUGATE_EVEN_STORAGE   = DFTI_COMPLEX_COMPLEX\n");

//     printf("Create DFTI descriptor\n");
    if(n_dim == 1) 
    	status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, n_dim, N1);
    if(n_dim > 1) 
    	status = DftiCreateDescriptor(&hand, DFTI_DOUBLE, DFTI_REAL, n_dim, N);
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: out-of-place\n");
    status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
    
    double Scale = 1.0/(double)(nx*ny*nz);
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, Scale);

    
    
    
//     if (status != DFTI_NO_ERROR) goto failed;

//     printf("Set configuration: CCE storage\n");
    status = DftiSetValue(hand, DFTI_CONJUGATE_EVEN_STORAGE,
                          DFTI_COMPLEX_COMPLEX);

    if(n_dim != 1){ 
		status = DftiSetValue(hand, DFTI_INPUT_STRIDES, cs);
		status = DftiSetValue(hand, DFTI_OUTPUT_STRIDES, rs);
    }
//     printf("Commit the descriptor\n");
    status = DftiCommitDescriptor(hand);

//     printf("Compute backward transform\n");
    status = DftiComputeBackward(hand, data_cmplx.data(), data_real.data());

    
//      	for(int i=0;i<n_tot;++i)
//  			data_real[i] = x_real[i];

    DftiFreeDescriptor(&hand);
    mkl_free(N);
    mkl_free(cs);
    mkl_free(rs);
//     mkl_free(x_real);
//     mkl_free(x_cmplxt);




}


