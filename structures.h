#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <string>
struct de_stru;
#include "struct_functions.h"


using std::string;

struct matrix_stru
{
	double m11;
	double m12;
	double m22;
	double m21;

	matrix_stru& operator+(const matrix_stru& rhs)
	{
		m11 += rhs.m11;
		m22 += rhs.m22;
		m21 += rhs.m21;
		m12 += rhs.m12;

		return *this;
	}

	matrix_stru& operator-(const matrix_stru& rhs)
	{
		m11 -= rhs.m11;
		m22 -= rhs.m22;
		m21 -= rhs.m21;
		m12 -= rhs.m12;

		return *this;
	}

	matrix_stru& operator/(const matrix_stru& rhs)
	{
		m11 /= rhs.m11;
		m22 /= rhs.m22;
		m21 /= rhs.m21;
		m12 /= rhs.m12;

		return *this;
	}

// 	matrix_stru& operator=(const matrix_stru& rhs)
// 	{
// 		
// 		m11 = rhs;
// 		m22 = rhs;
// 		m21 = rhs;
// 		m12 = rhs;
// 
// 		return *this;
// 	}



	void print(void);
	int check(void);
	
	double det(void);

	matrix_stru transpose(void);
	matrix_stru inverse(const matrix_stru&);
	matrix_stru multiply(const matrix_stru&);
	matrix_stru zero(const double&);
	matrix_stru identity();

// 	matrix_stru curl1(const  de_stru& , const  de_stru& ,int );

};

void inline matrix_stru::print()
{
	using std::cout;
	using std::endl;
	cout << " m11= " << m11 << "\n" <<
		" m22= " << m22 << "\n" <<
		" m12= " << m12 << "\n" <<
		" m21= " << m21 << endl;
		
	cout <<"------------"<<endl;

}

int inline matrix_stru::check()
{
	int i = 0;
	if (m11 == 1 && m22 == 1 && m12 == 0 && m21 == 0)
		i = 1;

	return i;

}

matrix_stru inline matrix_stru::transpose()
{
	matrix_stru mt;
	
	mt.m11=m11;
	mt.m22=m22;
	mt.m12=m21;
	mt.m21=m12;

	return mt;

}

double inline matrix_stru::det()
{
	matrix_stru mt;
	
	mt.m11=m11;
	mt.m22=m22;
	mt.m12=m21;
	mt.m21=m12;
	
	double det =mt.m11*mt.m22-mt.m12*mt.m21;

	return det;

}
matrix_stru inline matrix_stru::inverse(const  matrix_stru& x)
{
	matrix_stru mt;
	double det =x.m11*x.m22-x.m12*x.m21;
	
	mt.m11=m22/det;
	mt.m22=m11/det;
	mt.m12=-m12/det;
	mt.m21=-m21/det;

	return mt;

}

matrix_stru inline matrix_stru::multiply(const matrix_stru& rhs)
{
	matrix_stru k;
	k.m11 = m11*rhs.m11+ m12*rhs.m21;
	k.m12 = m11*rhs.m12+ m12*rhs.m22;
	k.m21 = m21*rhs.m11+ m22*rhs.m21;
	k.m22 = m21*rhs.m12+ m22*rhs.m22;

	return k;
	
}



matrix_stru inline matrix_stru::zero(const double& rhs)
{
	matrix_stru k;
	k.m11 = rhs;
	k.m12 = rhs;
	k.m21 = rhs;
	k.m22 = rhs;

	return k;
	
}

matrix_stru inline matrix_stru::identity()
{
	matrix_stru k;
	k.m11 = 1.;
	k.m22 = 1.;
	k.m21 = 0.;
	k.m12 = 0.;

	return k;
	
}

struct base_stru
{
	double e1[2];
	double e2[2];
	struct matrix_stru matrix_form(void);

	//   std::array<double, 2> e1;
	//   std::array<double, 2> e2;
};

 struct matrix_stru inline base_stru::matrix_form()
{
	struct matrix_stru fp;
	
	fp.m11=e1[0];
	fp.m21=e1[1];
	fp.m12=e2[0];
	fp.m22=e2[1]; 

	return fp;

}


struct cella_stru
{
	double c11;
	double c22;
	double c12;
	struct matrix_stru m;
};

struct double_cella_stru 
{
  	cella_stru c11;
  	cella_stru c22;
  	cella_stru c12;
};

struct punto_stru
{
	double x;
	double y;
	int nn[7];  /* Indici dei primi vicini - qui c'e' gia' dentro che [4]=[]*/
// 				  int tipo;
//  	std::vector<int> nn(7); 
	int i, j; // MJ:: indexes for row and column
	int is_boundary; // MJ:: 1 for boundary, 0 for internal 
    int totnumber;// 
    int pu[4];// periodic unit index
    double tvx;// translation_vector
    double tvy;// translation_vector

    int which_boundary;// (0 active),(1 passive)
    int corner;// (0 active),(1 passive)

	//int id; // MJ:: position in the vector 
};

struct def_grad
{
	double f11;
	double f22;
	double f12;
	double f21;
	def_grad inverse(const def_grad&);
	def_grad multiply(const def_grad&);
};

def_grad inline def_grad::inverse(const  def_grad& x)
{
	def_grad mt;
	double det =x.f11*x.f22-x.f12*x.f21;
	
	mt.f11=f22/det;
	mt.f22=f11/det;
	mt.f12=-f12/det;
	mt.f21=-f21/det;

	return mt;

}

def_grad inline def_grad::multiply(const def_grad& rhs)
{
	def_grad k;
	k.f11 = f11*rhs.f11+ f12*rhs.f21;
	k.f12 = f11*rhs.f12+ f12*rhs.f22;
	k.f21 = f21*rhs.f11+ f22*rhs.f21;
	k.f22 = f21*rhs.f12+ f22*rhs.f22;

	return k;
	
}

struct de_stru
{
	struct punto_stru p[3];
	struct punto_stru gr[3];
	struct def_grad reduced_stress;

	double energy;
	struct base_stru return_v(void);
};


//learn this
//  
//  struct base_stru  de_stru::return_v()
// {
// 	struct base_stru vv;
// 	
// 	vv.e1[0]= p[1].x-p[0].x ;
// 	vv.e1[1]= p[1].y-p[0].y ;
// 	vv.e2[0]= p[2].x-p[0].x ;
// 	vv.e2[1]= p[2].y-p[0].y ;
// 
// 	return vv;
// 
//  };


struct spec_for_reduction
{
	struct base_stru  v;
	struct cella_stru c; 
};

struct bouandaries_stru
{
	int k1;
	int k2;
};


struct energy_stress
{
	double energy;
	struct cella_stru r_stress;

};


class coordinates {
  public:
    std::vector<double>   x;
    std::vector<double>   y;
    double length0_x;
    double length0_y;
    double x_range;
    double y_range;
 
};


class at_analysis {
  public:
    double detAc;
    double xsi;
 
};




struct conf_stru
{
	int npunti;
	int inc;
	double energy,load,soft,sum_F12,zeroing;
	double dnx, dny;
	double total_triangular_number;
	std::vector<punto_stru> p, pfix, pref, pcons,pold, disp, gr;
	std::vector<double> energy_local, beta_inhomo;
	std::vector<double> rfx, rfy, volume;
	std::vector<cella_stru>  non_reduced, reduced, pitteri;
	std::vector<def_grad>  reduced_stress,curl, piola_one, cauchy;

	std::vector <std::vector<punto_stru> > grp1;
	std::vector <std::vector<matrix_stru> > m_field_full,fo;
	std::vector <std::vector<cella_stru> > metric_full;
	std::vector <std::vector<cella_stru> > metricr_full;

	std::vector <std::vector<int> > bc_cond;
	std::vector <std::vector<double> > disorder,alloying;

	//   std::vector<punto_stru> top,bottom,left,right,left_per,right_per;
	std::vector<punto_stru> top, bottom, left, right, left_per, right_per, top_per, bottom_per;

	std::vector<punto_stru> full;
	std::vector<matrix_stru> m_field,grad_umut;
	std::map<string, double(*)(struct cella_stru& c, double &burgers, double alloy)> energy_map;
	std::map<string, cella_stru(*)(const struct cella_stru& c, double &burgers, double& alloy)> stress_map;
	 //fboundary conditions
    void (*fptr)( int, int,  int,   int,  struct de_stru& , struct conf_stru& ); //Declare a function pointer to voids with  params
    //function energy and stres
    struct energy_stress (*fptr2)(const struct cella_stru& c, double burgers, double alloy, const struct base_stru& v); //Declare a function pointer to double with  params
    //first derivatives
    struct cella_stru (*fptr3)(const struct cella_stru& c, double burgers, double alloy); //Declare a function pointer to cella with  params
	//second derivatives
    struct double_cella_stru (*fptr4)(const struct cella_stru& c,double burgers,double alloy, const struct base_stru& v); //Declare a function pointer to cella with  params

	//energy atomistic
    double  (*fptr5)(double r); //Declare a function pointer to cella with  params
	//derivative atomistic
    double  (*fptr6)(double r); //Declare a function pointer to cella with  params
	//second derivative atomistic
    double  (*fptr7)(double r); //Declare a function pointer to cella with  params



    double t1,t2;
    
    std::vector<double> j1,j2;

	// MJ:: this fun returns id of point related with (i,j) position in the row-column reference system
	// if there is no any point for particular (i,j) then -1 is returned
	int get_punto(int i, int j) {
		for (int idx = 0; idx < p.size(); idx++)
			if ((p[idx].i == i) && (p[idx].j == j))
				return idx;
		return -1;
	}
	// c.grp1 = (punto_stru**) calloc(ntot, sizeof(punto_stru*));
	// // c.grp2 = (punto_stru**) calloc(ntot, sizeof(punto_stru*));
	// 
	// for ( i = 0; i < ntot; i++ )
	// {
	//     c.grp1[i] = (punto_stru*) calloc(12, sizeof(punto_stru));
	// //     c.grp2[i] = (punto_stru*) calloc(4, sizeof(punto_stru));
	// 
	// }  
	
	
	//for atomistic calculations
	coordinates tilde_coo;
	coordinates       coo;
	

};




