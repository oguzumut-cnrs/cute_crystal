#ifndef SQUARE_GRID_CREATOR_H
#define SQUARE_GRID_CREATOR_H
#include "structures.h"
#include "namespaces.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

class square_grid_creator{
  private:
    int width, height,n;
    double angle;
    
  public:  
    
    void set_values(int,int,double);
    void set_size(int);

    void nn_list_square(struct conf_stru& );
    void nn_list_hex(struct conf_stru& );

    void periodicity_0_degree_square(struct conf_stru& );
    void per_x_searcher(struct conf_stru& );
    void per_y_searcher(struct conf_stru& );
    
};


class tilted_square_grid_creator{
  private:
    int width, height,n;
    double angle;
    
  public:  
    
    void set_values(double,double,double);
    void set_size(int);

    void nn_list_square(struct conf_stru& );
    void periodicity_tilted_square(struct conf_stru& );
    void per_x_searcher(struct conf_stru& );
    void per_y_searcher(struct conf_stru& );
    
};




#endif
