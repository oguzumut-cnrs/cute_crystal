// #include "rotation.h"
// #include "structures.h"
// #include "struct_functions.h"
// #include "namespaces.h"
// #include "string_to_integer.h"
#include "rotation_roberta.h"
#include "structures.h"
#include "acoustic_tensor.h"

using namespace std;
 using namespace arma;


void energy_landscape_gorbushin()
{


	boost::numeric::ublas::matrix<double> A(3,3), U, H;
	
	
	string filename;
	filename = "energy_landscape.dat";
	ofstream	 filestr;
	filestr.open(filename.c_str());

	double burgers = 1.;//pow(BLK1::scale,-4);
	struct	base_stru c,r,rp;

	
	struct cella_stru metric_a  ;//= faicella(c);
	struct cella_stru metric_r  ;//= faicella(r);
	struct cella_stru metric_rr ;
	struct energy_stress es;
	es.energy=0;

	double epsilon = 0.1;

	int iii=0;
	//for ovito
	int line_number = 0;
//Nikolai whole domain
// 	for(double d= .7;d<=3; d=d+0.01){
double d= 1;
	for(double t=0.;t<=1.2; t=t+0.01){
		for(double p=-dlib::pi;p<=dlib::pi; p=p+dlib::pi/128){

//Mert  line

// 	for(double d= .7;d<=3; d=d+0.025){
// 	for(double t=0.2;t<=sqrt(4./3.); t=t+0.005){

// 	for(double t= sqrt(3.)/2 - 4.*epsilon;t<=sqrt(3.)/2 + 4.*epsilon; t=t+0.001){
// 		for(double p= (1.)/2 - 4.*epsilon;p<= (1.)/2 + 4.*epsilon; p=p + 0.001){




// 		for(double p=-dlib::pi;p<=dlib::pi; p=p+dlib::pi/256.){
			line_number++;
		}
	}
// }	
	filestr << line_number << endl;
	filestr << " " << endl;
	//////////////////////////////////////////////////////////////
// {y, Sqrt[3]/2 - 2. \[Epsilon], Sqrt[3]/2 + 1.8 \[Epsilon]}, {x, 
//  1/2 - 1.5 \[Epsilon], 1/2 + 1.5 \[Epsilon]}


// 	for(double t=0.;t<=3.; t=t+0.005){
// 		for(double p=-dlib::pi;p<=dlib::pi; p=p+dlib::pi/256.){


// 	for(double d= .7;d<=3; d=d+0.01){

	for(double t=0.;t<=1.2; t=t+0.01){
		for(double p=-dlib::pi;p<=dlib::pi; p=p+dlib::pi/128){
// 		for(double p=-dlib::pi;p<=dlib::pi; p=p+dlib::pi/256.){

// 		for(double d= sqrt(.85);d<=1.2; d=d+0.05){

// 	for(double t= sqrt(3.)/2 - 4.*epsilon;t<=sqrt(3.)/2 + 4.*epsilon; t=t+0.001){

// 		for(double p= (1.)/2 - 4.*epsilon;p<= (1.)/2 + 4.*epsilon; p=p + 0.001){

			iii++;
//Nikolai whole domain
			metric_a.c11 = sqrt(d)*(cosh(t) + sinh(t)*sin(p)); 
			metric_a.c22 = sqrt(d)*(cosh(t) - sinh(t)*sin(p)); 
			metric_a.c12 =          sqrt(d)*( sinh(t)*cos(p)); 

// Terzi on the boundary of fondemental domain
// 			metric_a.c11 = sqrt(d)*t; 
// 			metric_a.c22 = sqrt(d)*t/4. + sqrt(d)*1/t; 
// 			metric_a.c12 = sqrt(d)*t/2; 

// Umut centered around triangular point
// 			metric_a.c11 = 1/t; 
// 			metric_a.c22 = (p*p+t*t)/t; 
// 			metric_a.c12 = p/t; 

			
			arma::vec eigval;
			arma::mat eigvec;
			arma::mat A(2,2,fill::zeros),Lp(2,1,fill::zeros),Lm(2,1,fill::zeros);
			

			A(0,0) = metric_a.c11 ;A(1,1) = metric_a.c22;
			A(0,1) = metric_a.c12; A(1,0) = metric_a.c12;
// 			A(1,2) = 0.;A(2,1) = 0.;A(0,2) = 0.;A(2,0) = 0.;
// 			A(2,2) = 1.;
		    arma::eig_sym(eigval, eigvec, A);
		    for(int l=0; l<eigvec.size();l++){
				Lp[l] = eigvec[l];
				Lm[l] = eigvec[l+2];
				
			}
// 			cout << "Matrix K::\n"<<endl;
			//sqrt of C
			arma::mat U =  sqrt(eigval[0])*kron(Lp,trans(Lp)) + sqrt(eigval[1])*kron(Lm,trans(Lm)); 
			struct base_stru v;
			v.e1[0] = U(0,0);
			v.e1[1] = U(1,0);
			v.e2[0] = U(0,1);
			v.e2[1] = U(1,1);
// 			mat K2 = kron(Lp,trans(Lp)); 

// 		    cout<< K << endl;
// 			cout << "Matrix C::\n"<<endl;
// 		    cout<< A << endl;
			

// 		    	cout<< eigvec[l] << endl;
// 		    mat K2 = kron(eigvec,eigvec); 
// 		    cout << "Matrix K2::\n"<<endl;
// 		    cout<< Lp << endl;
// 		    cout<< "----" << endl;
// 		    cout<< Lm << endl;


// 			polar::polar_decomposition(A, U, H);
// 		    std::cout <<"A= "<< A << std::endl;
// 	        //Q
// 	        std::cout <<"U= "<< U << std::endl;
//             std::cout <<"H= "<< H << std::endl;


// 			metric_rr = riduci_umut(metric_a);
			metric_a.m = metric_a.m.identity();
// 			metric_a.m.print();

			metric_r = riduci_matthieu(metric_a);
// 			metric_r.m.print();
// 			cout<< metric_r.c11<<" "<< metric_r.c22<<" "<< metric_r.c12<<endl;
// 			cout<< metric_r.c11*metric_r.c22-metric_r.c12*metric_r.c12<<endl;
// 
// 			cout<< metric_a.c11<<" "<< metric_a.c22<<" "<< metric_a.c12<<endl;
// 			cout<< metric_a.c11*metric_a.c22-metric_a.c12*metric_a.c12<<endl;

			es = force_energy::calculation::getInstance().c.fptr2(metric_r, burgers, BLK1::beta_Var,r);
 			struct at_analysis temp = 
 			acoustic_tensor(v,metric_r,metric_r.m,0, 0);	
 			double dumb;
 			if(temp.detAc<0)  dumb =-1.;
 			if(temp.detAc>=0) dumb = 0.;
//  			if(temp.detAc>=0 && temp.detAc<0.05) dumb= 1.;

 			
//  			if(temp.detAc<0)
//  				cout<<"temp.detAc: "<<temp.detAc<<endl;
			
			filestr << std::scientific << std::setprecision(16) 
			<< p<< " " 
			<< t<< " " 
			<< d<< " " 
			<< (metric_a.c11 - metric_a.c22)/sqrt(2.)<< " " 
			<<  metric_a.c12 << " " 	
			<<  eigval[0] << " " 
			<<  eigval[1] << " " 	
			<<  dumb << " " 
			<<  temp.detAc << " "  
			<<  es.energy   <<	" "	
			<<  metric_a.c11 << " " 
			<<  metric_a.c22 << 	

			endl;
// 			if(iii > 1) break;

 			}
		}
// 	}

	filestr.close();
}
// double example(struct matrix_stru& F)
// {
//    	double angle,temp;
//     boost::mt19937 seed(42);
//     boost::uniform_real<> dist(-10.0, 10.0);
//     boost::variate_generator<boost::mt19937&, boost::uniform_real<> > random(seed, dist);
// 
//     boost::numeric::ublas::matrix<double> A(3,3), U, H;
// //     std::generate(A.data().begin(), A.data().end(), 0);
// 
// // 	if ( abs(F.m11) < 0.000000000000001) F.m11 = 0;
// // 	if ( abs(F.m22) < 0.000000000000001) F.m22 = 0;
// // 	if ( abs(F.m12) < 0.000000000000001) F.m12 = 0;
// // 	if ( abs(F.m21) < 0.000000000000001) F.m21 = 0;
// 
//     
//     A(0,0) = F.m11;
//     A(1,1) = F.m22;
//     A(0,1) = F.m12;
//     A(1,0) = F.m21;
//     A(1,2) = 0.;
//     A(2,1) = 0.;
//     A(0,2) = 0.;
//     A(2,0) = 0.;
//     A(2,2) =1;
//     
// 
// //     for (std::size_t i = 0; i < 100000; ++i)
// //     {
// //         std::generate(A.data().begin(), A.data().end(), random);
//         polar::polar_decomposition(A, U, H);
// //     }
// 
// //     std::cout <<"A= "<< A << std::endl;
// //     std::cout <<"U= "<< U << std::endl;
// //     std::cout <<"H= "<< H << std::endl;
//     if(H(0,0)<1 && H(0,0) >0){ 
//     	angle=acos(H(0,0));
//     }
//     else{
//     	angle = 0;
//     }
//     angle=toDegrees(angle);
// //     std::cout <<"angle= "<<angle << std::endl;
//     
// // 
// //     std::cout <<"prod= "<< prod(U, trans(U)) << std::endl;
//     return angle;
// }


double rot_calcul_roberta(struct cella_stru& c, struct matrix_stru& f)
{
	//diagonalizzo!
	double discr,lambda_1,lambda_2,bb,cc;
	double angle,detf;
	base_stru UU1vec,UU1vecinv;
	matrix_stru rot1,rot2,UU,UU1,UU2,tempU,RR;
	bb=-(c.c11+c.c22);
    cc=(c.c11*c.c22-pow(c.c12,2));
	discr=pow(bb,2)-4.*cc;
	lambda_1=0.5*(-bb+sqrt(discr));
	lambda_2=0.5*(-bb-sqrt(discr));

     //~ std::cout << "EIGENVALUES are   " << lambda_1<<"   "<<lambda_2<<std::endl;  
	double chi_1= atan((lambda_1-c.c11)/c.c12);
	double chi_2= atan((c.c22-lambda_2)/c.c12);
	//~ std::cout << "angles chi are   " << chi_1<<"   "<<chi_2<<std::endl;  

	rot1.m11= cos(-1.*chi_1);
	rot1.m12=-sin(-1.*chi_1);
	rot1.m21= sin(-1.*chi_1);
	rot1.m22= cos(-1.*chi_1);
	rot2=rot1.transpose();
	//~ cout<<"rot1.m11  ="<< rot1.m11<<"    rot1.m21  ="<< rot1.m21<<"    rot1.m12  ="<< rot1.m12<<"    rot1.m22  ="<< rot1.m22<<endl;
    //~ cout<<"rot2.m11  ="<< rot2.m11<<"    rot2.m21  ="<< rot2.m21<<"    rot2.m12  ="<< rot2.m12<<"    rot2.m22  ="<< rot2.m22<<endl;

	tempU.m11=sqrt(lambda_1);
	tempU.m12=0;
	tempU.m21=0;
	tempU.m22=sqrt(lambda_2);
	//~ UU=tempU.multiply(rot1);
	UU.m11 = tempU.m11*rot1.m11+ tempU.m12*rot1.m21;
	UU.m12 = tempU.m11*rot1.m12+ tempU.m12*rot1.m22;
	UU.m21 = tempU.m21*rot1.m11+ tempU.m22*rot1.m21;
	UU.m22 = tempU.m21*rot1.m12+ tempU.m22*rot1.m22;
	//~ cout<<"tempU.m11  ="<< tempU.m11<<"    tempU.m21  ="<< tempU.m21<<"    tempU.m12  ="<< tempU.m12<<"    tempU.m22  ="<< tempU.m22<<endl;
	//~ cout<<"UU.m11  ="<< UU.m11<<"    UU.m21  ="<< UU.m21<<"    UU.m12  ="<< UU.m12<<"    UU.m22  ="<< UU.m22<<endl;
	UU1.m11 = rot2.m11*UU.m11+ rot2.m12*UU.m21;
	UU1.m12 = rot2.m11*UU.m12+ rot2.m12*UU.m22;
	UU1.m21 = rot2.m21*UU.m11+ rot2.m22*UU.m21;
	UU1.m22 = rot2.m21*UU.m12+ rot2.m22*UU.m22;
	//~ cout<<"UU1.m11  ="<< UU1.m11<<"    UU1.m21  ="<< UU1.m21<<"    UU1.m12  ="<< UU1.m12<<"    UU1.m22  ="<< UU1.m22<<endl;

	//~ UU.inverse(UU);
	//~ UU1vec.e1[0]=UU1.m11;
	//~ UU1vec.e1[1]=UU1.m21;
	//~ UU1vec.e2[0]=UU1.m12;
	//~ UU1vec.e2[1]=UU1.m22; 
	//~ UU1vecinv=inverse_F(UU1vec);
	//~ UU=UU1vecinv.matrix_form();
	 detf = (UU1.m11*UU1.m22-1*UU1.m12*UU1.m21);	
	 //~ cout<<"def  "<<detf<<endl;
	 UU2.m11 = (1./detf)*UU1.m22;
	 UU2.m12 = (1./detf)*(-1.)*UU1.m12;
	 UU2.m21=  (1./detf)*(-1.)*UU1.m21;
	 UU2.m22 = (1./detf)*UU1.m11;
     //~ double detf =    (vec_f.e1[0]*vec_f.e2[1] - vec_f.e2[0]*vec_f.e1[1]) ;
	//~ vec.e1[0] = (1/detf)*e1[0];
	//~ vec.e1[1] = (1/detf)*e1[1];
	//~ vec.e2[0] = (1/detf)*e2[0];
	//~ vec.e2[1] = (1/detf)*e2[1];
	//~ cout<<"UU.m11  ="<< UU.m11<<"    UU.m21  ="<< UU.m21<<"    UU.m12  ="<< UU.m12<<"    UU.m22  ="<< UU.m22<<endl;	
	//~ cout<<"UU2.m11  ="<< UU2.m11<<"    UU2.m21  ="<< UU2.m21<<"    UU2.m12  ="<< UU2.m12<<"    UU2.m22  ="<< UU2.m22<<endl;	

	//~ RR=UU.multiply(f);
	RR.m11 = f.m11*UU2.m11+ f.m12*UU2.m21;
	RR.m12 = f.m11*UU2.m12+ f.m12*UU2.m22;
	RR.m21 = f.m21*UU2.m11+ f.m22*UU2.m21;
	RR.m22 = f.m21*UU2.m12+ f.m22*UU2.m22;
	//~ cout<<"RR.m11  ="<< RR.m11<<"    RR.m21  ="<< RR.m21<<"    RR.m12  ="<< RR.m12<<"    RR.m22  ="<< RR.m22<<endl;
	angle=acos(RR.m11);
	//~ std::cout<<"angle= "<<angle<<"     angle_check= "<<asin(-1.*RR.m12)<<"     angle_check_2= "<<acos(RR.m22)<<endl;
	//calcolo l'autospazio di C
    angle=toDegrees(angle);
      if(abs(angle)>=180){angle=angle -1*std::copysign(180,angle);}
	return angle;
	}
