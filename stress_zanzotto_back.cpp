#include "stress_zanzotto.h"

struct de_stru  stress_zanzotto(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double energy;
	double theta_Var = 5.;
	double gamma_Var = 10.;
	//burgers = 1 for non defect
// 	     burgers   = 1.;
	double beta_Var = BLK1::beta_Var;

	double c11 = c.c11;
	double c22 = c.c22;
	double c12 = c.c12;

	struct cella_stru dtemp;

	    
// 	dtemp= stress_easy (c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);
	
	dtemp= force_energy::calculation::getInstance().c.fptr3(c,burgers,alloy);

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);


	struct de_stru  d;
	double a = force_energy::calculation::getInstance().c.t1;
	double b = force_energy::calculation::getInstance().c.t2;
	//phi,11
	d.gr[1].x =  a*(temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0]);
	//phi,21
	d.gr[1].y =  a*(temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1]);
	//phi,12
	d.gr[2].x =  b*(temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0]);
	//phi,22
	d.gr[2].y =  b*(temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1]);
//      
    d.gr[0].x = -d.gr[1].x - d.gr[2].x;
 	d.gr[0].y = -d.gr[1].y - d.gr[2].y;
	

	return d;



}


struct de_stru  piola_one(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

	dtemp= stress_easy (c,burgers,alloy);

// 	struct cella_stru temp2 = dtemp;
	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);


	struct de_stru  d,dt;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
	//phi,22
	d.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;


	double P11 = d.gr[1].x;
	double P12 = d.gr[2].x;
	double P21 = d.gr[1].y;
	double P22 = d.gr[2].y;
	
	
	d.reduced_stress.f11 = P11;
	d.reduced_stress.f12 = P12;
	d.reduced_stress.f21 = P21;
	d.reduced_stress.f22 = P22;

	return d;
}



struct de_stru  cauchy(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

	dtemp= stress_easy (c,burgers,alloy);

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);

	struct de_stru  d1,cauchy;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d1.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d1.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d1.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
	//phi,22
	d1.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;

    double detf =    (v1.e1[0]*v1.e2[1] - v1.e2[0]*v1.e1[1]) ;
	d1.reduced_stress.f11 = d1.gr[1].x;
	d1.reduced_stress.f12 = d1.gr[2].x;
	d1.reduced_stress.f21 = d1.gr[1].y;
	d1.reduced_stress.f22 = d1.gr[2].y;
	
	cauchy.reduced_stress.f12  = (d1.reduced_stress.f11*v1.e1[1]+d1.reduced_stress.f12*v1.e2[1])/detf;
	cauchy.reduced_stress.f11  = (d1.reduced_stress.f11*v1.e1[0]+d1.reduced_stress.f12*v1.e2[0])/detf;
	cauchy.reduced_stress.f22  = (d1.reduced_stress.f21*v1.e1[1]+d1.reduced_stress.f22*v1.e2[1])/detf;


	return cauchy;
}



struct de_stru  second_piola(const struct cella_stru& c,
	const struct base_stru& v1, const struct matrix_stru& m, double &burgers
	, double& alloy)
	//struct cella_stru& c MUST be the reduced one
{


	struct cella_stru dtemp;

	// 	string s;	
	// 	if(BLK1::first.compare("A") ==0)
	// 	{
	// 		s="C";
	// 	}
	// 	else if(BLK1::first.compare("B") ==0)
	// 	{
	// 		s="D";
	// 	}
	//     dtemp=force_energy::calculation::getInstance().c.stress_map[s](c,burgers,alloy);

// 	dtemp = force_energy::calculation::getInstance().c.stress_map[BLK1::first](c, burgers, alloy);

	dtemp= stress_easy (c,burgers,alloy);

	struct cella_stru temp2 = riduci_matrix_reductions_stress(dtemp, m);

	struct de_stru  d1,pk2;

	//it returns the components of Piola_Kirshoff
	//phi,11
	d1.gr[1].x = temp2.c11 * 2 * v1.e1[0] + temp2.c12*v1.e2[0];
	//phi,21
	d1.gr[1].y = temp2.c11 * 2 * v1.e1[1] + temp2.c12*v1.e2[1];
	//phi,12
	d1.gr[2].x = temp2.c22 * 2 * v1.e2[0] + temp2.c12*v1.e1[0];
	//phi,22
	d1.gr[2].y = temp2.c22 * 2 * v1.e2[1] + temp2.c12*v1.e1[1];


	// 	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	// 	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;

    double detf =    (v1.e1[0]*v1.e2[1] - v1.e2[0]*v1.e1[1]) ;
	d1.reduced_stress.f11 = d1.gr[1].x;
	d1.reduced_stress.f12 = d1.gr[2].x;
	d1.reduced_stress.f21 = d1.gr[1].y;
	d1.reduced_stress.f22 = d1.gr[2].y;
	double f11,f22,f21,f12;
	f11 = v1.e1[0];
	f22 = v1.e2[1];
	f12 = v1.e2[0];
	f21  =v1.e1[1];
	

	pk2.reduced_stress.f11  =  (d1.reduced_stress.f11*f22  -  d1.reduced_stress.f21*f12)/detf;
	pk2.reduced_stress.f22  = -(d1.reduced_stress.f12*f21  -  d1.reduced_stress.f22*f11)/detf;
	
	pk2.reduced_stress.f12  =  (d1.reduced_stress.f12*f22  -  d1.reduced_stress.f22*f12)/detf;
	pk2.reduced_stress.f21  = -(d1.reduced_stress.f11*f21  -  d1.reduced_stress.f21*f11)/detf;


	return pk2;
}