#pragma once
#include "structures.h"
#include "second.h"

#include "calculations_homogenous.h"
// #define ARMA_ALLOW_FAKE_CLANG
// #define ARMA_ALLOW_FAKE_GCC
// #define ARMA_DONT_USE_WRAPPER
// #define ARMA_DONT_USE_HDF5
// #define ARMA_DONT_USE_BLAS

#include "ap.h"
#include <armadillo>

using namespace arma; 
#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <GenEigsSolver.h>
#include <SymEigsSolver.h>
#include <MatOp/SparseGenMatProd.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>

using Eigen::MatrixXd;
typedef Eigen::SparseMatrix<double> SpMatEigen; // declares a column-major sparse matrix type of double

void newton_raphson(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point);
void newton_eigen(struct conf_stru& c, sp_mat& squezed,alglib::real_1d_array& starting_point,int t);
void newton_eigen_version(struct conf_stru& c,alglib::real_1d_array& starting_point,int t);
