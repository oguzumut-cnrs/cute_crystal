#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include<complex>
#include<cmath>

#include "mkl_service.h"
#include "mkl_dfti.h"

void r2c(double *x_real, MKL_Complex16 *x_cmplx, const int N);
void c2r( MKL_Complex16 *x_cmplx,double *x_real, const int N);
void r2c_2D(double *data_real, MKL_Complex16 *data_cmplx, const int nx, const int ny);
void c2r_2D(MKL_Complex16 *data_cmplx, double *data_real, const int nx, const int ny);
void r2c_3D(double *data_real, MKL_Complex16 *data_cmplx, const int nx, const int ny, const int nz);
void c2r_3D(MKL_Complex16 *data_cmplx, double *data_real, const int nx, const int ny, const int nz);
void r2c_3D_special(
std::vector<double>& , std::vector<std::complex<double>>& , const int nx, const int ny, const int nz);
void c2r_3D_special
( std::vector<std::complex<double>>& data_cmplx,std::vector<double>& data_real, 
const int nx, const int ny, const int nz);


/* Compute (K*L)%M accurately */
static double moda(int K, int L, int M);


/* Initialize array x(N) to produce unit peaks at y(H) and y(N-H) */
static void init_r(double *x, int N, int H);


/* Verify that x has unit peak at H */
static int verify_c(MKL_Complex16 *x, int N, int H);


/* Initialize array x(N) to produce unit peak at y(H) */
static void init_c(MKL_Complex16 *x, int N, int H);

/* Verify that x has unit peak at H */
static int verify_r(double *x, int N, int H);

