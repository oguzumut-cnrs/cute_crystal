#include "vector_functions.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
using namespace std;

#include "utilities.h"
// double maxval(std::vector<double>& m)
// {
// 	double max;
// 	max = *max_element(m.begin(), m.end());
// 	return max;
// 
// }
// 
// double minval(std::vector<double>& m)
// {
// 	double min;
// 	min = *min_element(m.begin(), m.end());
// 	return min;
// 
// }
std::vector<double>  wiener(double av, double std,int n)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
// 	int N = nx*ny;
	static int i = 0;

	std::vector<double> r;
	r.resize(n);
	static std::random_device rd;
	static std::default_random_engine generator;
// 	if (i == 0){
// 		generator.seed(12);   //Now this is seeded same each time.
// 		i++;
// 	}
	// 	generator.seed( rd() ); //Now this is seeded differently each time.

	if (i == 0){
		generator.seed( rd() );   //Now this is seeded differently each time.
		i++;
		string seed="seed.dat";
		fstream filestr;
		filestr.open (seed, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(7)   << " "<<rd()<<endl ;
		filestr.close();

	}


	std::normal_distribution<double> dis(av, std);
// 	std::uniform_real_distribution<double> dis(-0.001, 0.001);


	for (i = 0; i<n; ++i)
		r[i] = 0;

	for (i = 0; i<n; ++i)
		r[i] = dis(generator);


	// 	for (i=0; i<N;++i)
	// 	{
	// 		int iy = i / nx;
	// 		int ix = i % nx;
	// 		
	// 	    if(ix%2==0 && iy%2==0)
	// 	    	r[i]  = dis(generator) ;
	// 	    				
	// 	}       

		cout<<"max r= "<<maxval(r)<<endl;
		cout<<"min r= "<<minval(r)<<endl;

	return r;

}

std::vector<double>  uniform(double a, double b, double amp,int n)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
// 	int N = nx*ny;
	static int i = 0;

	std::vector<double> r;
	r.resize(n);
	static std::random_device rd;
	static std::default_random_engine generator;
	if (i == 0)
		generator.seed( rd() ); //Now this is seeded differently each time.

// 		generator.seed(12);   //Now this is seeded same each time.
	i++;
	//avoids recreation of seed



	std::uniform_real_distribution<double> dis(a, b);

	for (i = 0; i<n; ++i)
		r[i] = 0;

	for (i = 0; i<n; ++i)
		// 	for (i=0; i<N;++i) 
		r[i] = amp*dis(generator);

	cout << "max r= " << maxval(r) << endl;
	cout << "min r= " << minval(r) << endl;

	return r;

}

std::vector<double>  uniform_volumetric(double a, double b, double amp,int n)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
// 	int N = nx*ny;
	static int i = 0;

	std::vector<double> r;
	r.resize(n);
	static std::random_device rd;
	static std::default_random_engine generator;
	if (i == 0)
// 		generator.seed(12);   //Now this is seeded same each time.
		generator.seed( rd() ); //Now this is seeded differently each time.

	i++;



// 	std::uniform_real_distribution<double> dis(a, b);
	std::normal_distribution<double> dis(a, b);


	for (i = 0; i<n; ++i)
		r[i] = 0;

	for (i = 0; i<n; ++i)
		r[i] = amp*dis(generator);

// 	cout << "max r= " << maxval(r) << endl;
// 	cout << "min r= " << minval(r) << endl;

	return r;

}

std::vector<double> perturba(double& amp, int n)
{
	int i;
	time_t t;
	int nx = dimensions::nx;
	int ny = dimensions::ny;
// 	int n = nx*ny;

	time(&t);
	srand((unsigned int)t);
	std::vector<double> r;
	r.resize(n);

	for (i = 0; i<n; ++i)
		r[i] = 0.;



	for (i = 4 * nx; i<n - 4 * ny; ++i)
		r[i] = amp*(static_cast <float> (std::rand()) / static_cast <float> (RAND_MAX));

	cout << "max r= " << maxval(r) << endl;
	cout << "min r= " << minval(r) << endl;

	return r;
}


std::vector<double> perturba_precipate(double& amp, int n)
{
	int seed = 12;
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int iSecretx, iSecrety;
	int Max, Max_y;
	int Min;
// 	int n = nx*ny;
	int ntot = n;
	int n_defects = n/200;

	int *coo_x = (int*)calloc(n_defects, sizeof(int));
	int *coo_y = (int*)calloc(n_defects, sizeof(int));

	std::vector<double> r;
	r.resize(n);

	for (int i = 0; i<ntot; i++)
		r[i] = 1.;

	Max = nx - 15;
	Max_y = ny - 20;
	Min = 20;
	Max = nx - 1;
	Max_y = ny - 1;
	Min = 1;

	//initiate seed
	//srand((unsigned int)seed);
    std::srand(std::time(nullptr)); // use current time as seed for random generator

	if (n_defects > 1)
	{

		for (int j = 0; j< n_defects; j++)
		{

			iSecretx = ((double(std::rand()) / double(RAND_MAX)) * (Max - Min)) + Min;
			iSecrety = ((double(std::rand()) / double(RAND_MAX)) * (Max_y - Min)) + Min;

			coo_x[j] = iSecretx;
			coo_y[j] = iSecrety;
			//cout<<"coo_x"<<coo_x[j]<<" "<<coo_y[j]<<endl;
		}
	}

	if (n_defects == 1)
	{
		coo_x[0] = nx / 2;
		coo_y[0] = ny / 2;
	}


	for (int j = 0; j< n_defects; j++)
	{
		for (int i = 0; i<nx; i++)
		{
			for (int k = 0; k<ny; k++)
			{

				if (pow(i - coo_x[j], 2) + pow(k - coo_y[j], 2) <= 9)
					r[k*nx + i] = amp;

			}
		}
	}



	free(coo_x);
	free(coo_y);


	return r;
}
