#pragma once
// void acoustic_tensor(struct conf_stru& c);
// #define ARMA_ALLOW_FAKE_GCC 
// #define ARMA_DONT_USE_WRAPPER
// 
// #define ARMA_DONT_USE_HDF5


// #define ARMA_ALLOW_FAKE_CLANG
// #define ARMA_ALLOW_FAKE_GCC
// #define ARMA_DONT_USE_WRAPPER
// #define ARMA_DONT_USE_HDF5
// #define ARMA_DONT_USE_BLAS


#include "structures.h"
#include "struct_functions.h"
#include "namespaces.h"
#include "common.h"
#include "atomistic_grid.h"
#include "utilities.h"
#include "ap.h"
#include <armadillo>
using namespace arma; 
#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <GenEigsSolver.h>
#include <SymEigsSolver.h>
#include <MatOp/SparseGenMatProd.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>

using Eigen::MatrixXd;
typedef Eigen::SparseMatrix<double> SpMatEigen; // declares a column-major sparse matrix type of double

using namespace std; 


struct double_cella_stru dd_phi(const struct cella_stru& c,double burgers,double alloy,const struct base_stru& v);
struct double_cella_stru dd_phi_detc(const struct cella_stru& c,double burgers,double alloy,const struct base_stru& v);
struct double_cella_stru dd_phi_lj(const struct cella_stru& c,double burgers,double alloy,const struct base_stru& v);
struct double_cella_stru dd_phi_morse(const struct cella_stru& c,double burgers,double alloy,const struct base_stru& v);





 at_analysis acoustic_tensor(const struct base_stru& v, const struct cella_stru& c, struct matrix_stru& z,
					 double alpha, double theta );

 at_analysis min_det_acoustic_tensor(const struct base_stru& v, const struct cella_stru& c, struct matrix_stru&);

 void stifness_tensor( std::vector < std::vector<std::vector< std::vector<double> > >> Am,
const struct base_stru& v, const struct cella_stru& c, struct matrix_stru& z);
