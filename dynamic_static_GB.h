#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"

#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include "init.h"
void statique_GB(struct conf_stru& c);
std::pair<double, double> get_XY_for_rotated_square1(double x, double y, double angle, double interface,double keep_slip);


std::pair<double, double> get_XY_for_rotated_square2(double x, double y, double angle, double interface,double keep_slip);


void measurements_GB(struct conf_stru& c, double theta );






	