#pragma once
#include "common.h"
#include "dlib.h"
#include "string_to_integer.h"
#include "structures.h"
#include "r2c_c2r.h"
#include "utilities.h"
#include "namespaces.h"

#include <iostream>
#include <map>
#include "ap.h"

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "optimization.h"


void function1D_grad(const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void *ptr);

/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/
double rosen_phasefield(const column_vector& m);

/*!
ensures
- returns the gradient vector for the rosen function
!*/
const column_vector rosen_derivative_phasefield(const column_vector& m);


// This function computes the Hessian matrix for the rosen() fuction.  This is
// the matrix of second derivatives.
dlib::matrix<double> rosen_hessian_phasefield(const column_vector& m);

class test_function_phasefield
{
    /*
        This object is an example of what is known as a "function object" in C++.
        It is simply an object with an overloaded operator().  This means it can 
        be used in a way that is similar to a normal C function.  The interesting
        thing about this sort of function is that it can have state.  
        
        In this example, our test_function object contains a column_vector 
        as its state and it computes the mean squared error between this 
        stored column_vector and the arguments to its operator() function.

        This is a very simple function, however, in general you could compute
        any function you wanted here.  An example of a typical use would be 
        to find the parameters of some regression function that minimized 
        the mean squared error on a set of data.  In this case the arguments
        to the operator() function would be the parameters of your regression
        function.  You would loop over all your data samples and compute the output 
        of the regression function for each data sample given the parameters and 
        return a measure of the total error.   The dlib optimization functions 
        could then be used to find the parameters that minimized the error.
    */
public:

    test_function_phasefield (
        const column_vector& input
    )
    {
        target = input;
    }

    double operator() ( const column_vector& arg) const
    {
        // return the mean squared error between the target vector and the input vector
        return mean(squared(target-arg));
    }

private:
    column_vector target;
};

class rosen_model_phasefield 
{
    /*!
        This object is a "function model" whicsh can be used with the
        find_min_trust_region() routine.  
    !*/

public:
    typedef ::column_vector column_vector;
    typedef dlib::matrix<double> general_matrix;

    double operator() (
        const column_vector& x
    ) const { return rosen_phasefield (x); }

    void get_derivative_and_hessian (
        const column_vector& x,
        column_vector& der,
        general_matrix& hess
    ) const
    {
        der = rosen_derivative_phasefield (x);
        hess = rosen_hessian_phasefield(x);
    }
};



// std::vector<std::complex<float>> fft_complex(std::vector<std::complex<float>>& in){
//     std::vector<std::complex<float>> out(in.size());
// 
//     DFTI_DESCRIPTOR_HANDLE descriptor;
//     MKL_LONG status;
// 
//     status = DftiCreateDescriptor(&descriptor, DFTI_SINGLE, DFTI_COMPLEX, 1, in.size()); //Specify size and precision
//     status = DftiSetValue(descriptor, DFTI_PLACEMENT, DFTI_NOT_INPLACE); //Out of place FFT
//     status = DftiCommitDescriptor(descriptor); //Finalize the descriptor
//     status = DftiComputeForward(descriptor, in.data(), out.data()); //Compute the Forward FFT
//     status = DftiFreeDescriptor(&descriptor); //Free the descriptor
// 
//     return out;
// }
// 
// std::vector<std::complex<float>> fft_real(std::vector<float>& in_real){
//     std::vector<std::complex<float>> in(in_real.size());
// 
//     std::copy(in_real.begin(), in_real.end(), in.begin());
// 
//     return fft_complex(in);
// }
// 
// std::vector<float> ifft(std::vector<std::complex<float>>& in){
//     std::vector<std::complex<float>> out(in.size());
// 
//     DFTI_DESCRIPTOR_HANDLE descriptor;
//     MKL_LONG status;
// 
//     status = DftiCreateDescriptor(&descriptor, DFTI_SINGLE, DFTI_COMPLEX, 1, in.size()); //Specify size and precision
//     status = DftiSetValue(descriptor, DFTI_PLACEMENT, DFTI_NOT_INPLACE); //Out of place FFT
//     status = DftiSetValue(descriptor, DFTI_BACKWARD_SCALE, 1.0f / in.size()); //Scale down the output
//     status = DftiCommitDescriptor(descriptor); //Finalize the descriptor
//     status = DftiComputeBackward(descriptor, in.data(), out.data()); //Compute the Forward FFT
//     status = DftiFreeDescriptor(&descriptor); //Free the descriptor
// 
//     std::vector<float> output(out.size());
// 
//     for(std::size_t i = 0; i < out.size(); ++i){
//         output[i] = out[i].real();
//     }
// 
//     return output;
// }
