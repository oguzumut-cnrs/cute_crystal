#pragma once
#include "dlib.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "structures.h"
#include "namespaces.h"
#include "boundary_conditions.h"
#include "custom_inline_functions.h"
#include <iostream>
#include <map>



void energy_calculator(struct conf_stru& c, std::vector<punto_stru>&   coo);
double energy_for_avalanches(struct conf_stru& c, struct boundary_conditions& f,struct boundary_conditions& fold );


