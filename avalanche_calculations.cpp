#include "calculations.h"
#include "rotation.h"
#include "input_output.h"
#include "energy_functions.h"
#include "result_memorize.h"
#include "avalanche_calculations.h"
#include <cmath>
#include <vector>



double energy_for_avalanches(struct conf_stru& c, struct boundary_conditions& f,struct boundary_conditions& fold ){
	//designed to calculate the work done by the load before relaxing
	double dnx,dny,diy;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());
	
	def_grad temp;
	temp= f.f.multiply(fold.f.inverse(fold.f));

				
	for(int i =0;i<c.p.size();++i){
		int iy = i / nx;
		int ix = i % nx;

		temp_coo[i].x = c.disp[i].x + c.pfix[i].x * temp.f11 + c.pfix[i].y * temp.f12  ;				
		temp_coo[i].y = c.disp[i].y + c.pfix[i].x * temp.f21 + c.pfix[i].y * temp.f22  ;

	}

	energy_calculator(c,temp_coo);
	double energy=0;
	for(int i =0;i<c.p.size();++i)
		energy+=c.energy_local[i];

	return energy;

}



void energy_calculator(struct conf_stru& c,std::vector<punto_stru>&   coo){
//c is needed for whole grid connectivity, while ctemp is only loaded coordinates
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	
	c.energy = 0.;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);
	static int loop=2;
	int inc = force_energy::calculation::getInstance().c.inc ;
	
// 	void (*fptr)( int,  int,   int,  struct de_stru& , struct conf_stru& ); //Declare a function pointer to voids with no params

// 	c.fptr = &per_BC; //fptr -> one

	
	for (int i=0; i<npunti; ++i)
	{
		c.gr[i].x=c.gr[i].y=c.energy_local[i] =0.;
		
		for(int l=0;l<12;++l)
			c.grp1[i][l].x = c.grp1[i][l].y=0.;
	}

	int tt=6;
	
	
// 	cout<<"here"<<endl;
	std::vector<double> c1,c2;
// 	cout<<"here"<<endl;
	c1.resize(0);
// 	cout<<"here"<<endl;
	c2.resize(0);
// 	cout<<"here"<<endl;
// 	
	c1.push_back(1);
	c1.push_back(1);
	c1.push_back(-1);
	c1.push_back(-1);
// 	cout<<"herec2"<<endl;
// 
	c2.push_back(1);
	c2.push_back(-1);
	c2.push_back(-1);
	c2.push_back(1); 


	c.t1=1.;
	c.t2=1.;	

	
try
{
	//k -> loop on the triangles
	for (int k=0; k<BLK1::triangle; k = k+inc) 
// 	for (int k=0; k<3; k=k+2) 

	{
// 			c.t1=c1[k];
// 			c.t2=c2[k];		
		//this is the loop on triangle vertices			
// 		for (int i=0; i<nx*ny; i++) 
		dlib::parallel_for(BLK1::num_threads, 0,npunti, [&](int i)
		{
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
			struct punto_stru p=c.p[i];
			struct punto_stru pt=coo[i];
			
            
			struct de_stru d;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;
// 			cout<<"i: "<<i<<endl; 

			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  = c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			double mesh=0;
			struct cella_stru metric_pr;	
			struct matrix_stru minv,fl,fu,f;
			struct spec_for_reduction vandc;
			struct energy_stress es;
			
			



// 				if(allequal(c.bc_cond[i][k],0,1,5))	
			if(!check(c.bc_cond[i][k],2))	
				goto label3;


//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = pt;
			d.p[1] = coo[p.nn[k]];
			d.p[2] = coo[p.nn[k+1]];

//      		per_BC_hex(ix,iy,k,d,c);
 
//  		    per_BC(ix,iy,k,d,c);
//  		    fptr(ix,iy,k,d,c); //=> one()
 		    c.fptr(i,ix,iy,k,d,c);

//             per_BC_MJ(d,i,c,k);


//---------------------------------VECTORS OF CELLS------------------------------------		
// 			v=vectorize(d);
			
		v.e1[0]= (d.p[1].x-d.p[0].x) ;
		v.e1[1]= (d.p[1].y-d.p[0].y) ;
		v.e2[0]= (d.p[2].x-d.p[0].x) ;
		v.e2[1]= (d.p[2].y-d.p[0].y) ;			
//---------------------------------REAL METRICS------------------------------------		

		metrics  = faicella(v);		
//---------------------------------REAL METRICS------------------------------------		


			vecr=riduci_vectors(v);

			//FIND INTEGER MATRIX			
			m2=find_integer_matrix(v,vecr);

		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);



			es = c.fptr2(metric_rr,c.disorder[i][k],c.alloying[i][k],vecr);			
			energy_thread = es.energy;
				

			c.energy_local[i] += energy_thread;


// 			d1= stress_zanzotto(es.r_stress,v,m2,c.disorder[i],c.alloying[i]);
	

//----------------------------------------------------------------------------------------

			
			c.gr[i].x         += d1.gr[0].x;
			c.gr[i].y         += d1.gr[0].y;
// 
//  	  //no need to sum the cross terms
			c.grp1[i][l].x   = d1.gr[1].x ; 
			c.grp1[i][l].y   = d1.gr[1].y ; 
			c.grp1[i][l+1].x = d1.gr[2].x ; 
			c.grp1[i][l+1].y = d1.gr[2].y ; 
		
// 			c.gr[i].x         += d1.gr[0].x;
// 			c.gr[i].y         += d1.gr[0].y;
// 			
// 			c.gr[p.nn[k]].x   += d1.gr[1].x;
// 			c.gr[p.nn[k]].y   += d1.gr[1].y;
// 			c.gr[p.nn[k+1]].x += d1.gr[2].x;
// 			c.gr[p.nn[k+1]].y += d1.gr[2].y; 
			label3: ;

			
			
			
			
		});
// 		}
	}
}

catch (std::exception& e)
{
	cout << e.what() << endl;
}

    //THIS ONE CANNOT BE PARALLALIZED
    
	for (int k=0; k<BLK1::triangle; k = k+inc) 
//  	for (int k=0; k<3; k=k+2) 
    {
		 
		for (int i=0; i<npunti; ++i) 
		{ 
			int l=2*k;
			struct punto_stru p=c.p[i];
			if(!check(c.bc_cond[i][k],2))	
				goto label4;

			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
				cout<<"warning3"<<endl;

			c.gr[p.nn[k]].x   += c.grp1[i][l].x;
			c.gr[p.nn[k]].y   += c.grp1[i][l].y;
			c.gr[p.nn[k+1]].x += c.grp1[i][l+1].x;
			c.gr[p.nn[k+1]].y += c.grp1[i][l+1].y;  
			label4: ;  
    	} 
    }

}
