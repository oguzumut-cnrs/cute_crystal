de_stru stress_arbib(const struct cella_stru& c,const struct base_stru& v1)
//struct cella_stru& c MUST be the non-reduced one
{

	struct cella_stru stress;
 
	double c11 = c.c11;
	double c22 = c.c22;
	double c12 = c.c12;
	double Pi = dlib::pi;
	double E = exp(1.);
	
	
	stress.c12 = 2720000000000*(-2*c12 + (2*c12)/(-pow(c12,2) + c11*c22)) + 
   (3950000000*(((-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
                 pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
                (21493760*cos((4*c12*Pi)/c11))/
                 pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (864299970*cos((6*c12*Pi)/c11))/
                 pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (20245856256*cos((8*c12*Pi)/c11))/
                 pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (333202640600*cos((10*c12*Pi)/c11))/
                 pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4252023300096*cos((12*c12*Pi)/c11))/
                 pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (44656994071935*cos((14*c12*Pi)/c11))/
                 pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (401490886656000*cos((16*c12*Pi)/c11))/
                 pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (3176440229784420*cos((18*c12*Pi)/c11))/
                 pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (22567393309593600*cos((20*c12*Pi)/c11))/
                 pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (146211911499519294*cos((22*c12*Pi)/c11))/
                 pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (874313719685775360*cos((24*c12*Pi)/c11))/
                 pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4872010111798142520*cos((26*c12*Pi)/c11))/
                 pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.)*
           ((393768*c12*Pi*cos((2*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (2*c12*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                cos((2*c12*Pi)/c11))/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
             (85975040*c12*Pi*cos((4*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (5185799820*c12*Pi*cos((6*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (161966850048*c12*Pi*cos((8*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3332026406000*c12*Pi*cos((10*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (51024279601152*c12*Pi*cos((12*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (625197917007090*c12*Pi*cos((14*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (6423854186496000*c12*Pi*cos((16*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (57175924136119560*c12*Pi*cos((18*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (451347866191872000*c12*Pi*cos((20*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3216662052989424468*c12*Pi*cos((22*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (20983529272458608640*c12*Pi*cos((24*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (126672262906751705520*c12*Pi*cos((26*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (393768*Pi*sin((2*c12*Pi)/c11))/
              (c11*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (2*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                sin((2*c12*Pi)/c11))/c11 - 
             (85975040*Pi*sin((4*c12*Pi)/c11))/
              (c11*pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (5185799820*Pi*sin((6*c12*Pi)/c11))/
              (c11*pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (161966850048*Pi*sin((8*c12*Pi)/c11))/
              (c11*pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3332026406000*Pi*sin((10*c12*Pi)/c11))/
              (c11*pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (51024279601152*Pi*sin((12*c12*Pi)/c11))/
              (c11*pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (625197917007090*Pi*sin((14*c12*Pi)/c11))/
              (c11*pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (6423854186496000*Pi*sin((16*c12*Pi)/c11))/
              (c11*pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (57175924136119560*Pi*sin((18*c12*Pi)/c11))/
              (c11*pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (451347866191872000*Pi*sin((20*c12*Pi)/c11))/
              (c11*pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3216662052989424468*Pi*sin((22*c12*Pi)/c11))/
              (c11*pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (20983529272458608640*Pi*sin((24*c12*Pi)/c11))/
              (c11*pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (126672262906751705520*Pi*sin((26*c12*Pi)/c11))/
              (c11*pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))))/864. + 
        (((196884*sin((2*c12*Pi)/c11))/
              pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
             pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
             (21493760*sin((4*c12*Pi)/c11))/
              pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (864299970*sin((6*c12*Pi)/c11))/
              pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (20245856256*sin((8*c12*Pi)/c11))/
              pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (333202640600*sin((10*c12*Pi)/c11))/
              pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4252023300096*sin((12*c12*Pi)/c11))/
              pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (44656994071935*sin((14*c12*Pi)/c11))/
              pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (401490886656000*sin((16*c12*Pi)/c11))/
              pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (3176440229784420*sin((18*c12*Pi)/c11))/
              pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (22567393309593600*sin((20*c12*Pi)/c11))/
              pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (146211911499519294*sin((22*c12*Pi)/c11))/
              pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (874313719685775360*sin((24*c12*Pi)/c11))/
              pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4872010111798142520*sin((26*c12*Pi)/c11))/
              pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))*
           ((393768*Pi*cos((2*c12*Pi)/c11))/
              (c11*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (2*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                cos((2*c12*Pi)/c11))/c11 + 
             (85975040*Pi*cos((4*c12*Pi)/c11))/
              (c11*pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (5185799820*Pi*cos((6*c12*Pi)/c11))/
              (c11*pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (161966850048*Pi*cos((8*c12*Pi)/c11))/
              (c11*pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3332026406000*Pi*cos((10*c12*Pi)/c11))/
              (c11*pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (51024279601152*Pi*cos((12*c12*Pi)/c11))/
              (c11*pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (625197917007090*Pi*cos((14*c12*Pi)/c11))/
              (c11*pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (6423854186496000*Pi*cos((16*c12*Pi)/c11))/
              (c11*pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (57175924136119560*Pi*cos((18*c12*Pi)/c11))/
              (c11*pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (451347866191872000*Pi*cos((20*c12*Pi)/c11))/
              (c11*pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3216662052989424468*Pi*cos((22*c12*Pi)/c11))/
              (c11*pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (20983529272458608640*Pi*cos((24*c12*Pi)/c11))/
              (c11*pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (126672262906751705520*Pi*cos((26*c12*Pi)/c11))/
              (c11*pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (393768*c12*Pi*sin((2*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (2*c12*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                sin((2*c12*Pi)/c11))/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
             (85975040*c12*Pi*sin((4*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (5185799820*c12*Pi*sin((6*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (161966850048*c12*Pi*sin((8*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3332026406000*c12*Pi*sin((10*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (51024279601152*c12*Pi*sin((12*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (625197917007090*c12*Pi*sin((14*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (6423854186496000*c12*Pi*sin((16*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (57175924136119560*c12*Pi*sin((18*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (451347866191872000*c12*Pi*sin((20*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3216662052989424468*c12*Pi*sin((22*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (20983529272458608640*c12*Pi*sin((24*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (126672262906751705520*c12*Pi*sin((26*c12*Pi)/c11))/
              (c11*sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))))/1.492992e6))/
    sqrt(pow(-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
            pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
           (21493760*cos((4*c12*Pi)/c11))/
            pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (864299970*cos((6*c12*Pi)/c11))/
            pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (20245856256*cos((8*c12*Pi)/c11))/
            pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (333202640600*cos((10*c12*Pi)/c11))/
            pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4252023300096*cos((12*c12*Pi)/c11))/
            pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (44656994071935*cos((14*c12*Pi)/c11))/
            pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (401490886656000*cos((16*c12*Pi)/c11))/
            pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (3176440229784420*cos((18*c12*Pi)/c11))/
            pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (22567393309593600*cos((20*c12*Pi)/c11))/
            pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (146211911499519294*cos((22*c12*Pi)/c11))/
            pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (874313719685775360*cos((24*c12*Pi)/c11))/
            pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4872010111798142520*cos((26*c12*Pi)/c11))/
            pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.,2) + 
      pow((196884*sin((2*c12*Pi)/c11))/
          pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
         pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
         (21493760*sin((4*c12*Pi)/c11))/
          pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (864299970*sin((6*c12*Pi)/c11))/
          pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (20245856256*sin((8*c12*Pi)/c11))/
          pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (333202640600*sin((10*c12*Pi)/c11))/
          pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4252023300096*sin((12*c12*Pi)/c11))/
          pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (44656994071935*sin((14*c12*Pi)/c11))/
          pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (401490886656000*sin((16*c12*Pi)/c11))/
          pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (3176440229784420*sin((18*c12*Pi)/c11))/
          pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (22567393309593600*sin((20*c12*Pi)/c11))/
          pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (146211911499519294*sin((22*c12*Pi)/c11))/
          pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (874313719685775360*sin((24*c12*Pi)/c11))/
          pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4872010111798142520*sin((26*c12*Pi)/c11))/
          pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11),2)/2.985984e6);
          
  stress.c11 =2720000000000*(c22 - c22/(-pow(c12,2) + c11*c22)) + 
   (3950000000*(((-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
                 pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
                (21493760*cos((4*c12*Pi)/c11))/
                 pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (864299970*cos((6*c12*Pi)/c11))/
                 pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (20245856256*cos((8*c12*Pi)/c11))/
                 pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (333202640600*cos((10*c12*Pi)/c11))/
                 pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4252023300096*cos((12*c12*Pi)/c11))/
                 pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (44656994071935*cos((14*c12*Pi)/c11))/
                 pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (401490886656000*cos((16*c12*Pi)/c11))/
                 pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (3176440229784420*cos((18*c12*Pi)/c11))/
                 pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (22567393309593600*cos((20*c12*Pi)/c11))/
                 pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (146211911499519294*cos((22*c12*Pi)/c11))/
                 pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (874313719685775360*cos((24*c12*Pi)/c11))/
                 pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4872010111798142520*cos((26*c12*Pi)/c11))/
                 pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.)*
           (pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*
              ((c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) - 
                (2*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*cos((2*c12*Pi)/c11) + 
             (196884*(-((c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22))) + 
                  (2*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*cos((2*c12*Pi)/c11))
               /pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (21493760*((-2*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (4*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*cos((4*c12*Pi)/c11))
               /pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (864299970*((-3*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (6*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*cos((6*c12*Pi)/c11))
               /pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (20245856256*((-4*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (8*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*cos((8*c12*Pi)/c11))
               /pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (333202640600*((-5*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (10*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((10*c12*Pi)/c11))/pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (4252023300096*((-6*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (12*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((12*c12*Pi)/c11))/pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (44656994071935*((-7*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (14*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((14*c12*Pi)/c11))/pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (401490886656000*((-8*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (16*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((16*c12*Pi)/c11))/pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (3176440229784420*((-9*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (18*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((18*c12*Pi)/c11))/pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (22567393309593600*((-10*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (20*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((20*c12*Pi)/c11))/pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (146211911499519294*
                ((-11*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (22*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((22*c12*Pi)/c11))/pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (874313719685775360*
                ((-12*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (24*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((24*c12*Pi)/c11))/pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (4872010111798142520*
                ((-13*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (26*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                cos((26*c12*Pi)/c11))/pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (393768*c12*Pi*sin((2*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (2*c12*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                sin((2*c12*Pi)/c11))/pow(c11,2) + 
             (85975040*c12*Pi*sin((4*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (5185799820*c12*Pi*sin((6*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (161966850048*c12*Pi*sin((8*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3332026406000*c12*Pi*sin((10*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (51024279601152*c12*Pi*sin((12*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (625197917007090*c12*Pi*sin((14*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (6423854186496000*c12*Pi*sin((16*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (57175924136119560*c12*Pi*sin((18*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (451347866191872000*c12*Pi*sin((20*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (3216662052989424468*c12*Pi*sin((22*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (20983529272458608640*c12*Pi*sin((24*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (126672262906751705520*c12*Pi*sin((26*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))))/864. + 
        (((196884*sin((2*c12*Pi)/c11))/
              pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
             pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
             (21493760*sin((4*c12*Pi)/c11))/
              pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (864299970*sin((6*c12*Pi)/c11))/
              pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (20245856256*sin((8*c12*Pi)/c11))/
              pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (333202640600*sin((10*c12*Pi)/c11))/
              pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4252023300096*sin((12*c12*Pi)/c11))/
              pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (44656994071935*sin((14*c12*Pi)/c11))/
              pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (401490886656000*sin((16*c12*Pi)/c11))/
              pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (3176440229784420*sin((18*c12*Pi)/c11))/
              pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (22567393309593600*sin((20*c12*Pi)/c11))/
              pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (146211911499519294*sin((22*c12*Pi)/c11))/
              pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (874313719685775360*sin((24*c12*Pi)/c11))/
              pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4872010111798142520*sin((26*c12*Pi)/c11))/
              pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))*
           ((-393768*c12*Pi*cos((2*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (2*c12*pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*
                cos((2*c12*Pi)/c11))/pow(c11,2) - 
             (85975040*c12*Pi*cos((4*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (5185799820*c12*Pi*cos((6*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (161966850048*c12*Pi*cos((8*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3332026406000*c12*Pi*cos((10*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (51024279601152*c12*Pi*cos((12*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (625197917007090*c12*Pi*cos((14*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (6423854186496000*c12*Pi*cos((16*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (57175924136119560*c12*Pi*cos((18*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (451347866191872000*c12*Pi*cos((20*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3216662052989424468*c12*Pi*cos((22*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (20983529272458608640*c12*Pi*cos((24*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (126672262906751705520*c12*Pi*cos((26*c12*Pi)/c11))/
              (pow(c11,2)*pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*
              ((c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) - 
                (2*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*sin((2*c12*Pi)/c11) + 
             (196884*(-((c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22))) + 
                  (2*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*sin((2*c12*Pi)/c11))
               /pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (21493760*((-2*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (4*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*sin((4*c12*Pi)/c11))
               /pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (864299970*((-3*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (6*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*sin((6*c12*Pi)/c11))
               /pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (20245856256*((-4*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (8*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*sin((8*c12*Pi)/c11))
               /pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (333202640600*((-5*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (10*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((10*c12*Pi)/c11))/pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (4252023300096*((-6*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (12*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((12*c12*Pi)/c11))/pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (44656994071935*((-7*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (14*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((14*c12*Pi)/c11))/pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (401490886656000*((-8*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (16*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((16*c12*Pi)/c11))/pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (3176440229784420*((-9*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (18*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((18*c12*Pi)/c11))/pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (22567393309593600*((-10*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (20*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((20*c12*Pi)/c11))/pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (146211911499519294*
                ((-11*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (22*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((22*c12*Pi)/c11))/pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (874313719685775360*
                ((-12*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (24*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((24*c12*Pi)/c11))/pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)\
              + (4872010111798142520*
                ((-13*c22*Pi)/(c11*sqrt(-pow(c12,2) + c11*c22)) + 
                  (26*sqrt(-pow(c12,2) + c11*c22)*Pi)/pow(c11,2))*
                sin((26*c12*Pi)/c11))/pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))
           )/1.492992e6))/
    sqrt(pow(-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
            pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
           (21493760*cos((4*c12*Pi)/c11))/
            pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (864299970*cos((6*c12*Pi)/c11))/
            pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (20245856256*cos((8*c12*Pi)/c11))/
            pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (333202640600*cos((10*c12*Pi)/c11))/
            pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4252023300096*cos((12*c12*Pi)/c11))/
            pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (44656994071935*cos((14*c12*Pi)/c11))/
            pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (401490886656000*cos((16*c12*Pi)/c11))/
            pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (3176440229784420*cos((18*c12*Pi)/c11))/
            pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (22567393309593600*cos((20*c12*Pi)/c11))/
            pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (146211911499519294*cos((22*c12*Pi)/c11))/
            pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (874313719685775360*cos((24*c12*Pi)/c11))/
            pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4872010111798142520*cos((26*c12*Pi)/c11))/
            pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.,2) + 
      pow((196884*sin((2*c12*Pi)/c11))/
          pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
         pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
         (21493760*sin((4*c12*Pi)/c11))/
          pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (864299970*sin((6*c12*Pi)/c11))/
          pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (20245856256*sin((8*c12*Pi)/c11))/
          pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (333202640600*sin((10*c12*Pi)/c11))/
          pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4252023300096*sin((12*c12*Pi)/c11))/
          pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (44656994071935*sin((14*c12*Pi)/c11))/
          pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (401490886656000*sin((16*c12*Pi)/c11))/
          pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (3176440229784420*sin((18*c12*Pi)/c11))/
          pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (22567393309593600*sin((20*c12*Pi)/c11))/
          pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (146211911499519294*sin((22*c12*Pi)/c11))/
          pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (874313719685775360*sin((24*c12*Pi)/c11))/
          pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4872010111798142520*sin((26*c12*Pi)/c11))/
          pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11),2)/2.985984e6);
    
          
      stress.c22 = 2720000000000*(c11 - c11/(-pow(c12,2) + c11*c22)) + 
   (3950000000*((((-196884*Pi*cos((2*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) + 
             (pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*cos((2*c12*Pi)/c11))/
              sqrt(-pow(c12,2) + c11*c22) - 
             (42987520*Pi*cos((4*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (2592899910*Pi*cos((6*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (80983425024*Pi*cos((8*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (1666013203000*Pi*cos((10*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (25512139800576*Pi*cos((12*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (312598958503545*Pi*cos((14*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3211927093248000*Pi*cos((16*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (28587962068059780*Pi*cos((18*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (225673933095936000*Pi*cos((20*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (1608331026494712234*Pi*cos((22*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (10491764636229304320*Pi*cos((24*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (63336131453375852760*Pi*cos((26*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)))*
           (-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
                 pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
                (21493760*cos((4*c12*Pi)/c11))/
                 pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (864299970*cos((6*c12*Pi)/c11))/
                 pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (20245856256*cos((8*c12*Pi)/c11))/
                 pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (333202640600*cos((10*c12*Pi)/c11))/
                 pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4252023300096*cos((12*c12*Pi)/c11))/
                 pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (44656994071935*cos((14*c12*Pi)/c11))/
                 pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (401490886656000*cos((16*c12*Pi)/c11))/
                 pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (3176440229784420*cos((18*c12*Pi)/c11))/
                 pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (22567393309593600*cos((20*c12*Pi)/c11))/
                 pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (146211911499519294*cos((22*c12*Pi)/c11))/
                 pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (874313719685775360*cos((24*c12*Pi)/c11))/
                 pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
                (4872010111798142520*cos((26*c12*Pi)/c11))/
                 pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.))/864. + 
        (((196884*sin((2*c12*Pi)/c11))/
              pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
             pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
             (21493760*sin((4*c12*Pi)/c11))/
              pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (864299970*sin((6*c12*Pi)/c11))/
              pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (20245856256*sin((8*c12*Pi)/c11))/
              pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (333202640600*sin((10*c12*Pi)/c11))/
              pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4252023300096*sin((12*c12*Pi)/c11))/
              pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (44656994071935*sin((14*c12*Pi)/c11))/
              pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (401490886656000*sin((16*c12*Pi)/c11))/
              pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (3176440229784420*sin((18*c12*Pi)/c11))/
              pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (22567393309593600*sin((20*c12*Pi)/c11))/
              pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (146211911499519294*sin((22*c12*Pi)/c11))/
              pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (874313719685775360*sin((24*c12*Pi)/c11))/
              pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
             (4872010111798142520*sin((26*c12*Pi)/c11))/
              pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))*
           ((-196884*Pi*sin((2*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*Pi*sin((2*c12*Pi)/c11))/
              sqrt(-pow(c12,2) + c11*c22) - 
             (42987520*Pi*sin((4*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (2592899910*Pi*sin((6*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (80983425024*Pi*sin((8*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (1666013203000*Pi*sin((10*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (25512139800576*Pi*sin((12*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (312598958503545*Pi*sin((14*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (3211927093248000*Pi*sin((16*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (28587962068059780*Pi*sin((18*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (225673933095936000*Pi*sin((20*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (1608331026494712234*Pi*sin((22*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (10491764636229304320*Pi*sin((24*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)) - 
             (63336131453375852760*Pi*sin((26*c12*Pi)/c11))/
              (sqrt(-pow(c12,2) + c11*c22)*
                pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))))/1.492992e6))/
    sqrt(pow(-1 + (744 + (196884*cos((2*c12*Pi)/c11))/
            pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
           (21493760*cos((4*c12*Pi)/c11))/
            pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (864299970*cos((6*c12*Pi)/c11))/
            pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (20245856256*cos((8*c12*Pi)/c11))/
            pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (333202640600*cos((10*c12*Pi)/c11))/
            pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4252023300096*cos((12*c12*Pi)/c11))/
            pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (44656994071935*cos((14*c12*Pi)/c11))/
            pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (401490886656000*cos((16*c12*Pi)/c11))/
            pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (3176440229784420*cos((18*c12*Pi)/c11))/
            pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (22567393309593600*cos((20*c12*Pi)/c11))/
            pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (146211911499519294*cos((22*c12*Pi)/c11))/
            pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (874313719685775360*cos((24*c12*Pi)/c11))/
            pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4872010111798142520*cos((26*c12*Pi)/c11))/
            pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.,2) + 
      pow((196884*sin((2*c12*Pi)/c11))/
          pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
         pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
         (21493760*sin((4*c12*Pi)/c11))/
          pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (864299970*sin((6*c12*Pi)/c11))/
          pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (20245856256*sin((8*c12*Pi)/c11))/
          pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (333202640600*sin((10*c12*Pi)/c11))/
          pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4252023300096*sin((12*c12*Pi)/c11))/
          pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (44656994071935*sin((14*c12*Pi)/c11))/
          pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (401490886656000*sin((16*c12*Pi)/c11))/
          pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (3176440229784420*sin((18*c12*Pi)/c11))/
          pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (22567393309593600*sin((20*c12*Pi)/c11))/
          pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (146211911499519294*sin((22*c12*Pi)/c11))/
          pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (874313719685775360*sin((24*c12*Pi)/c11))/
          pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4872010111798142520*sin((26*c12*Pi)/c11))/
          pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11),2)/2.985984e6);  
          
     stress.c11/=pow(10,12.);      
     stress.c22/=pow(10,12.);      
     stress.c12/=pow(10,12.);      
          
 	struct de_stru  d;
	
	d.gr[1].x = stress.c11*2*v1.e1[0]+stress.c12*v1.e2[0];
	d.gr[1].y = stress.c11*2*v1.e1[1]+stress.c12*v1.e2[1];
	d.gr[2].x = stress.c22*2*v1.e2[0]+stress.c12*v1.e1[0];
	d.gr[2].y = stress.c22*2*v1.e2[1]+stress.c12*v1.e1[1];
	d.gr[0].x = -d.gr[1].x - d.gr[2].x ;
	d.gr[0].y = -d.gr[1].y - d.gr[2].y ;
// 	d.gr[0].x = +1;
// 	d.gr[0].y = +1;

	return d;
                 
}


double energy_arbib(struct cella_stru& c)
//struct cella_stru& c MUST be the non-reduced one
{


	double energy;
 
	double c11 = c.c11;
	double c22 = c.c22;
	double c12 = c.c12;
	double Pi = dlib::pi;
	double E = exp(1.);
	
	
	energy = 2720000000000*(-pow(c12,2) + c11*c22 - log(-pow(c12,2) + c11*c22)) + 
   7900000000*sqrt(pow(-1 + (744 + 
           (196884*cos((2*c12*Pi)/c11))/
            pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*cos((2*c12*Pi)/c11) + 
           (21493760*cos((4*c12*Pi)/c11))/
            pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (864299970*cos((6*c12*Pi)/c11))/
            pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (20245856256*cos((8*c12*Pi)/c11))/
            pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (333202640600*cos((10*c12*Pi)/c11))/
            pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4252023300096*cos((12*c12*Pi)/c11))/
            pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (44656994071935*cos((14*c12*Pi)/c11))/
            pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (401490886656000*cos((16*c12*Pi)/c11))/
            pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (3176440229784420*cos((18*c12*Pi)/c11))/
            pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (22567393309593600*cos((20*c12*Pi)/c11))/
            pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (146211911499519294*cos((22*c12*Pi)/c11))/
            pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (874313719685775360*cos((24*c12*Pi)/c11))/
            pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
           (4872010111798142520*cos((26*c12*Pi)/c11))/
            pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11))/1728.,2) + 
      pow((196884*sin((2*c12*Pi)/c11))/
          pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) - 
         pow(E,(2*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11)*sin((2*c12*Pi)/c11) + 
         (21493760*sin((4*c12*Pi)/c11))/
          pow(E,(4*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (864299970*sin((6*c12*Pi)/c11))/
          pow(E,(6*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (20245856256*sin((8*c12*Pi)/c11))/
          pow(E,(8*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (333202640600*sin((10*c12*Pi)/c11))/
          pow(E,(10*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4252023300096*sin((12*c12*Pi)/c11))/
          pow(E,(12*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (44656994071935*sin((14*c12*Pi)/c11))/
          pow(E,(14*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (401490886656000*sin((16*c12*Pi)/c11))/
          pow(E,(16*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (3176440229784420*sin((18*c12*Pi)/c11))/
          pow(E,(18*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (22567393309593600*sin((20*c12*Pi)/c11))/
          pow(E,(20*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (146211911499519294*sin((22*c12*Pi)/c11))/
          pow(E,(22*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (874313719685775360*sin((24*c12*Pi)/c11))/
          pow(E,(24*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11) + 
         (4872010111798142520*sin((26*c12*Pi)/c11))/
          pow(E,(26*sqrt(-pow(c12,2) + c11*c22)*Pi)/c11),2)/2.985984e6);
        
        return energy/pow(10,12.);
}