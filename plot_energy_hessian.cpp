
#include "plot_energy_hessian.h"
     
     
void energy_hessian_plotter(struct conf_stru& c){




	string fileovitoh = "ovitohessian.dat";
	std::fstream ovitostreamh;
	ovitostreamh.open (fileovitoh, std::fstream::in | std::fstream::out | std::fstream::app);

	string fileovitohrs = "ovitohessian_reduced_space.dat";
	std::fstream ovitostreamhrs;
	ovitostreamhrs.open (fileovitohrs, std::fstream::in | std::fstream::out | std::fstream::app);


	string fileovito = "ovitoenergy.dat";
	std::fstream ovitostream;
	ovitostream.open (fileovito, std::fstream::in | std::fstream::out | std::fstream::app);


	struct cella_stru metrics,metric_rr;
	struct base_stru dumb;

	double div1=0.5;
	double div2=0.5*0.5;
	int counter=0;
	int counter2=0;

	double gamma = pow(4. / 3., 1. / 4.);
	double gammasq = pow(4. / 3., 1. / 4.)*pow(4. / 3., 1. / 4.);


	double inc = 0.0025;
	double rayon_hex = 0.005;
	double c12lim=.8;
	double c12limn=.4;
	double c22limmax=1.4;
	double c11limmax=.6;


	for(double z=c12limn;z<=c12lim;z=z+inc)
		for(double y=c11limmax;y<=c22limmax;y=y+inc )
			counter2++;

//---------------------------------------------------------------------	
	for(double z=c12limn;z<=c12lim;z=z+inc ){
		for(double y=c11limmax;y<=c22limmax;y=y+inc ){
			double x = (z*z+1.)/y;
			if( abs(pow(x-gammasq,2) + pow(y-gammasq,2) +  pow(z-gammasq/2,2)) < rayon_hex )
				++counter;
		}
	}


	for(double z=c12limn;z<=c12lim;z=z+inc){
		for(double x=c11limmax;x<=c22limmax;x=x+inc){
			double y = (z*z+1.)/x;
			if( pow(x-gammasq,2) + pow(y-gammasq,2) +  pow(z-gammasq/2,2) < rayon_hex )
				++counter;
		}
	}
//---------------------------------------------------------------------	

	ovitostream << std::scientific 
				<< 2*counter2 << "\n" 
				<< endl;
			

// 	ovitostreamh << std::scientific 
// 				<< counter << "\n" 
// 				<< endl;
				
				
// ovitostreamhrs	<< std::scientific 
// 				<< counter << "\n" 
// 				<< endl;			
	double temp= -1000000;
	
	for(double z=c12limn;z<=c12lim;z=z+inc){
		for(double y=c11limmax;y<=c22limmax;y=y+inc ){
		
			metrics.c12=z;
			metrics.c22=y;		
			double x = metrics.c11 = (metrics.c12*metrics.c12 + 1.)/metrics.c22;
			double x_li = metrics.c12 + metrics.c22 *(-(1/4) - 1/(gammasq*gammasq)) + 2/gammasq;
			double x_li_sq = 2 -  metrics.c22;
 			
			metric_rr = riduci(metrics);
			struct energy_stress es     =  c.fptr2(metric_rr,1.,BLK1::beta_Var,dumb);
			struct double_cella_stru dd =  c.fptr4(metric_rr,1.,BLK1::beta_Var,dumb);
			
			double fxx = dd.c11.c11;
			double fyy = dd.c22.c22;
			double fzz = dd.c12.c12;
			double fxz = dd.c11.c12;
			double fxy = dd.c11.c22;
			double fyz = dd.c22.c12;
			matrix<double> M(2,2);
			M(0,0) = metrics.c11;
			M(1,1) = metrics.c22;
			M(0,1) = metrics.c12;
			M(1,0) = metrics.c12;

			matrix<double> eig = real_eigenvalues(M);
			
//  			 std::cout<<real_eigenvalues(M)<<std::endl;
//  			 std::cout<<eig.size()<<std::endl;


			
			double deth   = -(pow(fxz,2)*fyy) + 2*fxy*fxz*fyz - fxx*pow(fyz,2) - pow(fxy,2)*fzz + fxx*fyy*fzz;
			double detd2  = fxx*fyy-fxy*fxy;
			double detd1  = fxx;			
			


			
			ovitostream << std::scientific << std::setprecision(7) 
				<< metrics.c11 << " " 
				<< metrics.c22 << " " 
				<< metrics.c12 << " "
// 				<< eig(0) << " "
// 				<< eig(1) << " "				
// 				<<x_li 			<< " "
// 				<<x_li_sq 			<< " "
// 				<< deth << " " 
// 				<< detd2 << " " 
// 				<< detd1 << " " 
				<<  es.energy << " "  
				<< endl;
// 			
// 			ovitostreamhrs << std::scientific << std::setprecision(7) 
// 				<< metric_rr.c12 << " "
// 				<< metric_rr.c11 << " "
// 				<< metric_rr.c22 << " "
// 				<< es.energy << " "  
// 				<< endl;


			if( sqrt(pow(x-gammasq,2) + pow(y-gammasq,2) +  pow(z-gammasq/2,2)) < rayon_hex ){
					ovitostreamh << std::scientific << std::setprecision(7) 
						<< metrics.c12 << " "
						<<x_li 			<< " "
// 						<< metric_rr.c11 << " "
// 						<< metric_rr.c22 << " "
// 						<< deth << " " 
// 						<< detd2 << " " 
// 						<< detd1 << " " 
						<< es.energy << " "  
						<< endl;
			}
		}
	}
 		


	for(double z=c12limn;z<=c12lim;z=z+inc){
		for(double x=c11limmax;x<=c22limmax;x=x+inc){
		
			metrics.c12=z;
			metrics.c11=x;		
			double y = metrics.c22=(metrics.c12*metrics.c12 + 1.)/metrics.c11;
			double y_li = metrics.c12 + metrics.c11 *(-(1/4) - 1./(gammasq*gammasq)) + 2/gammasq;
			double y_li_sq = 2 -  metrics.c11;
 			
			metric_rr = riduci(metrics);
			struct energy_stress es     =  c.fptr2(metric_rr,1.,BLK1::beta_Var,dumb);
			struct double_cella_stru dd =  c.fptr4(metric_rr,1.,BLK1::beta_Var,dumb);
			
			double fxx = dd.c11.c11;
			double fyy = dd.c22.c22;
			double fzz = dd.c12.c12;
			double fxz = dd.c11.c12;
			double fxy = dd.c11.c22;
			double fyz = dd.c22.c12;

			
			
			double deth   = -(pow(fxz,2)*fyy) + 2*fxy*fxz*fyz - fxx*pow(fyz,2) - pow(fxy,2)*fzz + fxx*fyy*fzz;
			double detd2  = fxx*fyy-fxy*fxy;
			double detd1  = fxx;
			
			

// 			 matrix<double> M(3,3);
// 			 
// 			 M(0,0) = fxx;
// 			 M(1,1) = fyy;
// 			 M(2,2) = fzz;
// 			 M(0,1) = fxy;
// 			 M(0,2) = fxz;
// 			 M(1,2) = fyz;
//  			 
//  			 M(1,0) = M(0,1);
//  			 M(2,0) = M(0,2);
//  			 M(2,1) = M(1,2);
// 			 std::cout<<"writing eigenvalues of hessian"<<std::endl;
// 			 std::cout<<real_eigenvalues(M)<<std::endl;

			matrix<double> M(2,2);
			M(0,0) = metrics.c11;
			M(1,1) = metrics.c22;
			M(0,1) = metrics.c12;
			M(1,0) = metrics.c12;

			matrix<double> eig = real_eigenvalues(M);

			ovitostream << std::scientific << std::setprecision(7) 
				<< metrics.c11 << " " 
				<< metrics.c22 << " " 
				<< metrics.c12 << " "
// 				<< eig(0) << " "
// 				<< eig(1) << " "				
// 				<<y_li 			<< " "
// 				<<y_li_sq 			<< " "
// 				<< deth << " " 
// 				<< detd2 << " " 
// 				<< detd1 << " " 
				<<  es.energy << " "  
				<< endl;
// 
// 			ovitostreamhrs << std::scientific << std::setprecision(7) 
// 				<< metric_rr.c12 << " "
// 				<< metric_rr.c11 << " "
// 				<< metric_rr.c22 << " "
// 				<< es.energy << " "  
// 				<< endl;


			if( sqrt(pow(x-gammasq,2) + pow(y-gammasq,2) +  pow(z-gammasq/2,2)) < rayon_hex ){
					ovitostreamh << std::scientific << std::setprecision(7) 
						<< metrics.c12 << " "
						<<y_li 			<< " "
// 						<< metric_rr.c11 << " "
// 						<< metric_rr.c22 << " "
// 						<< deth << " " 
// 						<< detd2 << " " 
// 						<< detd1 << " " 
						<< es.energy << " "  
						<< endl;

				}
			}
		}
		

		ovitostreamhrs.close();
		ovitostream.close();
		ovitostreamh.close();

			
}	

void energy_hessian_plotter_v2(struct conf_stru& c){





	string fileovito = "ovitoenergy.dat";
	std::fstream ovitostream;
	ovitostream.open (fileovito, std::fstream::in | std::fstream::out | std::fstream::app);


	struct cella_stru metrics,metric_rr;
	struct base_stru dumb;

	double div1=0.5;
	double div2=0.5*0.5;
	int counter=0;
	int counter2=0;

	double gamma = pow(4. / 3., 1. / 4.);
	double gammasq = pow(4. / 3., 1. / 4.)*pow(4. / 3., 1. / 4.);


	double inc = 0.0035;
	double rayon_hex = 0.01;
	double c12lim=2.5;
//	double c12limn=-gammasq/2-gammasq/4;
	double c12limn=0;

	double c22limmax=1.8;
	double c11limmax=.4;




	for(double z=c12limn;z<=c12lim;z=z+inc){
		for(double y=c11limmax;y<=c22limmax;y=y+inc ){
				for(double x=c11limmax;x<=c22limmax;x=x+inc ){
	
					metrics.c12=z/(sqrt(abs(x*y-z*z)));
					metrics.c22=y/(sqrt(abs(x*y-z*z)));		
					metrics.c11=x/(sqrt(abs(x*y-z*z)));
			        double x_li = metrics.c12 + metrics.c22 *(-(1./4.) - 1./(gammasq*gammasq)) + 2./gammasq;
					
					//if(metrics.c11*metrics.c22-metrics.c12*metrics.c12 >0 && abs(metrics.c12) <c12lim){	
			//	if( sqrt(pow(x-gammasq,2) + pow(y-gammasq,2) +  pow(z-gammasq/2,2)) < rayon_hex )
			//	if( metrics.c12>=0.566407 && metrics.c12<=0.59 &&  x_li>=1.145 && x_li<=1.167 )
				if( metrics.c12>=0.55 && metrics.c12<=0.6 &&  x_li>=1.14 && x_li<=1.17 )

					if(metrics.c11*metrics.c22-metrics.c12*metrics.c12 >0 
					&& metrics.c12 <c12lim
					&& metrics.c12 >c12limn){	

						++counter;
						
					}
			}

		}
	}

   cout<<"number of lines= "<<counter<<endl;

// 	ovitostream << std::scientific 
// 				<< counter << "\n" 
// 				<< endl;
			

		
	
	for(double z=c12limn;z<=c12lim;z=z+inc){
		for(double y=c11limmax;y<=c22limmax;y=y+inc ){
				for(double x=c11limmax;x<=c22limmax;x=x+inc ){
	
					metrics.c12=z/(sqrt(abs(x*y-z*z)));
					metrics.c22=y/(sqrt(abs(x*y-z*z)));		
					metrics.c11=x/(sqrt(abs(x*y-z*z)));
	
	
			        double x_li = metrics.c12 + metrics.c22 *(-(1./4.) - 1./(gammasq*gammasq)) + 2./gammasq;

				
				if( metrics.c12>=0.55 && metrics.c12<=0.6 &&  x_li>=1.14 && x_li<=1.17 )
					if(metrics.c11*metrics.c22-metrics.c12*metrics.c12 >0 
					&& metrics.c12 <c12lim
					&& metrics.c12 >c12limn){	
		
						metric_rr = riduci(metrics);
						struct energy_stress es     =  c.fptr2(metric_rr,1.,BLK1::beta_Var,dumb);
			
			
// 						ovitostream << std::scientific << std::setprecision(7) 
// 							<< metrics.c11 << " " 
// 							<< metrics.c22 << " " 
// 							<< metrics.c12 << " "
// 							<< es.energy << " "  
// 							<< endl;


						ovitostream << std::scientific << std::setprecision(7) 
							<< x_li << " " 
							<< metrics.c12  << " " 
							<< es.energy << " "  
							<< endl;

					}
			}

		}
	}
 		


		

		ovitostream.close();

			
}	

	
