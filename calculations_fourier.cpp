#include "calculations_fourier.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "mkl_service.h"
#include "mkl_dfti.h"
#include <cmath>
#include <vector>

using std::cout;
using std::endl;

void function1D_grad(const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void *ptr) {

	int N = dimensions::nx*dimensions::ny*dimensions::nz;
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	int nz=dimensions::nz;
	int boundary = 0.;
	double l_reg = 2.;
	double khi_elasticity = 769.;
	double khi_substrat = khi_elasticity/2000.;
	double khi_defect = 1.;
	double sum=0;
	sum=0;


	for(int i=0;i<N;++i){ 
		fourier::fourier_operator::getInstance().dft.alpha[i]=x[i];
		fourier::fourier_operator::getInstance().dft.dispu[i]=x[i+N];		
    }
    
    if(boundary == 1){
    //boundary connections
    fourier::fourier_operator::getInstance().dft.dispu[0]=0.;		
    fourier::fourier_operator::getInstance().dft.dispu[N-1]=    
    fourier::fourier_operator::getInstance().dft.load;

    //boundary connections 
    fourier::fourier_operator::getInstance().dft.alpha[0]=
    fourier::fourier_operator::getInstance().dft.alpha[1];		
    fourier::fourier_operator::getInstance().dft.alpha[N-1]=
    fourier::fourier_operator::getInstance().dft.alpha[N-2];
 	}	   		

    	
    // TF ALPHA
// 	for(int i=0;i<N;++i){ 
// 		fourier::fourier_operator::getInstance().dft.work[i]=fourier::fourier_operator::getInstance().dft.alpha[i];
//     }    
//     r2c_3D_special(fourier::fourier_operator::getInstance().dft.work, 
//     fourier::fourier_operator::getInstance().dft.tf_work, nx,ny,nz);
        r2c(fourier::fourier_operator::getInstance().dft.alpha, 
        fourier::fourier_operator::getInstance().dft.tf_alpha, N);
// 	for(int i=0;i<N/2+1;++i){ 
// 		fourier::fourier_operator::getInstance().dft.tf_alpha[i].real = (double) fourier::fourier_operator::getInstance().dft.tf_work[i].real();
// 		fourier::fourier_operator::getInstance().dft.tf_alpha[i].imag = (double) fourier::fourier_operator::getInstance().dft.tf_work[i].imag();
// 		
// 
//     }    
    // TF DIPSLACEMENT
    r2c(fourier::fourier_operator::getInstance().dft.dispu, 
    fourier::fourier_operator::getInstance().dft.tf_dispu, N);
    
    //calculate epsilon
    multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxp,
    fourier::fourier_operator::getInstance().dft.tf_dispu,
    fourier::fourier_operator::getInstance().dft.x_cmplxt,N);
    
    if(boundary == 0){
    
    fourier::fourier_operator::getInstance().dft.x_cmplxt[0].real  = 
    fourier::fourier_operator::getInstance().dft.load;

    fourier::fourier_operator::getInstance().dft.x_cmplxt[0].imag  = 
    fourier::fourier_operator::getInstance().dft.load;
	}


    c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
    fourier::fourier_operator::getInstance().dft.epsilon, N);
    
    //boundary connections
     if(boundary == 1){
   	 fourier::fourier_operator::getInstance().dft.epsilon[N-1]=0.;   
   	 } 


    //calculate gradient of alpha
    multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxp,
    fourier::fourier_operator::getInstance().dft.tf_alpha,
    fourier::fourier_operator::getInstance().dft.x_cmplxt,N);

    c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
    fourier::fourier_operator::getInstance().dft.grad_alpha, N);
 
 //boundary connections
    if(boundary == 1){
    fourier::fourier_operator::getInstance().dft.grad_alpha[0]=0.;		
    fourier::fourier_operator::getInstance().dft.grad_alpha[N-1]=0.;    
    }

 
 
 	//calculate the total energy 
 	sum=0.;
 	
 	for(int i=0; i<N; ++i){
 	
 	   sum +=pow(fourier::fourier_operator::getInstance().dft.alpha[i],2.)/(4.*l_reg) + 
       khi_elasticity*
       pow(1.-fourier::fourier_operator::getInstance().dft.alpha[i],2.)*
       pow(fourier::fourier_operator::getInstance().dft.epsilon[i],2.)*
       khi_defect*fourier::fourier_operator::getInstance().dft.defect[i] + 
       l_reg*pow(fourier::fourier_operator::getInstance().dft.grad_alpha[i],2.)
       +0.5*khi_substrat*pow(fourier::fourier_operator::getInstance().dft.dispu[i],2.);
       
 	}
 
 
 
 	 //laplacian alpha
     multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxx,
     fourier::fourier_operator::getInstance().dft.tf_alpha,
     fourier::fourier_operator::getInstance().dft.x_cmplxt,N);
     
     c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
     fourier::fourier_operator::getInstance().dft.laplacian_alpha,N);

 	//boundary connections
	 if(boundary == 1){

     fourier::fourier_operator::getInstance().dft.laplacian_alpha[0]=0.;		
     fourier::fourier_operator::getInstance().dft.laplacian_alpha[N-1]=0.;  
     }  
	 

	 //elastic part
	 
	 for(int i=0; i<N; ++i)
	 fourier::fourier_operator::getInstance().dft.elastic_part[i] = 
	 khi_elasticity*pow(1.-fourier::fourier_operator::getInstance().dft.alpha[i],2.)*
	 fourier::fourier_operator::getInstance().dft.epsilon[i]*
	 fourier::fourier_operator::getInstance().dft.defect[i];
	 
	 //multiply with qxn
     r2c(fourier::fourier_operator::getInstance().dft.elastic_part, 
     fourier::fourier_operator::getInstance().dft.tf_elastic_part, N);
	 multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxn,
     fourier::fourier_operator::getInstance().dft.tf_elastic_part,
     fourier::fourier_operator::getInstance().dft.x_cmplxt,N);

	 //derivated elastic part
	 c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
     fourier::fourier_operator::getInstance().dft.elastic_part, N);
 	 
 	 for(int i=0; i<N; ++i){
		 grad[i+N] = -
		 fourier::fourier_operator::getInstance().dft.elastic_part[i]+
		 khi_substrat*fourier::fourier_operator::getInstance().dft.dispu[i];
 
		 grad[i] = 
		 fourier::fourier_operator::getInstance().dft.alpha[i]/(2.*l_reg) -
		 l_reg*fourier::fourier_operator::getInstance().dft.laplacian_alpha[i] -
		 khi_elasticity*(1-fourier::fourier_operator::getInstance().dft.alpha[i])*
		 pow(fourier::fourier_operator::getInstance().dft.epsilon[i],2.)*
		 khi_defect*fourier::fourier_operator::getInstance().dft.defect[i];
		 
		 fourier::fourier_operator::getInstance().dft_3D.eulerlagrange[i] =  grad[i] ;
		 fourier::fourier_operator::getInstance().dft_3D.eulerlagrange[i+N] =  grad[i+N] ;

	 }

	 func =sum;
 
}

double rosen_phasefield(const column_vector& m)
/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/
{
	double sum;
	int N = dimensions::nx;
	double l_reg = 2.;
	double khi_elasticity = 769.;
	double khi_substrat = 0.*khi_elasticity/5000.;
	double khi_defect = 1.;

// 	column_vector defect,temp;
// 	defect.set_size(N);
	int boundary = 0.;

// 	double mu = N*1.0/2.-1.;
// 	double sig =sqrt(1.);
// 
// 	for(int i=0;i<N;++i){
// 		double x=i*1.0;
// 		fourier::fourier_operator::getInstance().dft.defect[i] = 1.-exp(-0.5*pow((x-mu)/sig,2.)  );
// 	}
// 	fourier::fourier_operator::getInstance().dft.defect[N/2-1] = 0.8;


	for(int i=0;i<N;++i){ 
		fourier::fourier_operator::getInstance().dft.alpha[i]=m(i);
		fourier::fourier_operator::getInstance().dft.dispu[i]=m(i+N);		
    }
    
    if(boundary == 1){
    //boundary connections
    fourier::fourier_operator::getInstance().dft.dispu[0]=0.;		
    fourier::fourier_operator::getInstance().dft.dispu[N-1]=    
    fourier::fourier_operator::getInstance().dft.load;

    //boundary connections 
    fourier::fourier_operator::getInstance().dft.alpha[0]=
    fourier::fourier_operator::getInstance().dft.alpha[1];		
    fourier::fourier_operator::getInstance().dft.alpha[N-1]=
    fourier::fourier_operator::getInstance().dft.alpha[N-2];
 	}	   		

    	
    // TF ALPHA
    r2c(fourier::fourier_operator::getInstance().dft.alpha, 
    fourier::fourier_operator::getInstance().dft.tf_alpha, N);
    // TF DIPSLACEMENT
    r2c(fourier::fourier_operator::getInstance().dft.dispu, 
    fourier::fourier_operator::getInstance().dft.tf_dispu, N);
    
    //calculate epsilon
    multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxp,
    fourier::fourier_operator::getInstance().dft.tf_dispu,
    fourier::fourier_operator::getInstance().dft.x_cmplxt,N);
    
    if(boundary == 0){
    
    fourier::fourier_operator::getInstance().dft.x_cmplxt[0].real  = 
    fourier::fourier_operator::getInstance().dft.load;

    fourier::fourier_operator::getInstance().dft.x_cmplxt[0].imag  = 
    fourier::fourier_operator::getInstance().dft.load;
	}


    c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
    fourier::fourier_operator::getInstance().dft.epsilon, N);
    
    //boundary connections
     if(boundary == 1){
   	 fourier::fourier_operator::getInstance().dft.epsilon[N-1]=0.;   
   	 } 


    //calculate gradient of alpha
    multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxp,
    fourier::fourier_operator::getInstance().dft.tf_alpha,
    fourier::fourier_operator::getInstance().dft.x_cmplxt,N);

    c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
    fourier::fourier_operator::getInstance().dft.grad_alpha, N);
 
 //boundary connections
    if(boundary == 1){
    fourier::fourier_operator::getInstance().dft.grad_alpha[0]=0.;		
    fourier::fourier_operator::getInstance().dft.grad_alpha[N-1]=0.;    
    }

 
 
 	//calculate the total energy 
 	sum=0.;
 	
 	for(int i=0; i<N; ++i){
 	
 	   sum +=pow(fourier::fourier_operator::getInstance().dft.alpha[i],2.)/(4.*l_reg) + 
       khi_elasticity*
       pow(1.-fourier::fourier_operator::getInstance().dft.alpha[i],2.)*
       pow(fourier::fourier_operator::getInstance().dft.epsilon[i],2.)*
       khi_defect*fourier::fourier_operator::getInstance().dft.defect[i] + 
       l_reg*pow(fourier::fourier_operator::getInstance().dft.grad_alpha[i],2.)
       +0.5*khi_substrat*pow(fourier::fourier_operator::getInstance().dft.dispu[i],2.);
       
 	}
 
 
 
 	 //laplacian alpha
     multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxx,
     fourier::fourier_operator::getInstance().dft.tf_alpha,
     fourier::fourier_operator::getInstance().dft.x_cmplxt,N);
     
     c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
     fourier::fourier_operator::getInstance().dft.laplacian_alpha,N);

 	//boundary connections
	 if(boundary == 1){

     fourier::fourier_operator::getInstance().dft.laplacian_alpha[0]=0.;		
     fourier::fourier_operator::getInstance().dft.laplacian_alpha[N-1]=0.;  
     }  
	 

	 //elastic part
	 
	 for(int i=0; i<N; ++i)
	 fourier::fourier_operator::getInstance().dft.elastic_part[i] = 
	 khi_elasticity*pow(1.-fourier::fourier_operator::getInstance().dft.alpha[i],2.)*
	 fourier::fourier_operator::getInstance().dft.epsilon[i]*
	 fourier::fourier_operator::getInstance().dft.defect[i];
	 
	 //multiply with qxn
     r2c(fourier::fourier_operator::getInstance().dft.elastic_part, 
     fourier::fourier_operator::getInstance().dft.tf_elastic_part, N);
	 multiply_complex_vector_MKL(fourier::fourier_operator::getInstance().dft.qxn,
     fourier::fourier_operator::getInstance().dft.tf_elastic_part,
     fourier::fourier_operator::getInstance().dft.x_cmplxt,N);

	 //derivated elastic part
	 c2r(fourier::fourier_operator::getInstance().dft.x_cmplxt, 
     fourier::fourier_operator::getInstance().dft.elastic_part, N);
 	 
 	 for(int i=0; i<N; ++i){
	 fourier::fourier_operator::getInstance().dft.grad[i+N] = -
	 fourier::fourier_operator::getInstance().dft.elastic_part[i]+
	 khi_substrat*fourier::fourier_operator::getInstance().dft.dispu[i];
	 
	 fourier::fourier_operator::getInstance().dft.grad[i] = 
	 fourier::fourier_operator::getInstance().dft.alpha[i]/(2.*l_reg) -
	 l_reg*fourier::fourier_operator::getInstance().dft.laplacian_alpha[i] -
	 khi_elasticity*(1-fourier::fourier_operator::getInstance().dft.alpha[i])*
	 pow(fourier::fourier_operator::getInstance().dft.epsilon[i],2.)
	 ;

	 }

 
	 

 
 

	return sum;

// 	return sum/(2.0*n);

}

const column_vector rosen_derivative_phasefield(const column_vector& m)
/*!
ensures
- returns the gradient vector for the rosen function
!*/
{

	int N = dimensions::nx;

	column_vector res;
	res.set_size(2*N);

	
	for(int i=0; i<2*N; ++i){
	
	res(i) = fourier::fourier_operator::getInstance().dft.grad[i];
// 	if(i<N)
// 	cout<<res(i)<<endl;
	
	}
	
//  	res(0) =0.;
// 	res(N-1) =0.;
// 	res(N) =0.;
// 	res(2*N-1) =0.;
	
    return res;
}


// This function computes the Hessian matrix for the rosen() fuction.  This is
// the matrix of second derivatives.
dlib::matrix<double> rosen_hessian_fourier(const column_vector& m)
{
	const double x = m(0);
	const double y = m(1);

	dlib::matrix<double> res(2, 2);

	// now compute the second derivatives 
	res(0, 0) = 1200 * x*x - 400 * y + 2; // second derivative with respect to x
	res(1, 0) = res(0, 1) = -400 * x;   // derivative with respect to x and y
	res(1, 1) = 200;                 // second derivative with respect to y
	return res;
}







