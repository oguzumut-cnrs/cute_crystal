#include "dynamic_static.h"
#include "namespaces.h"
#include "dlib.h"
#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"

#include "calculations_alglib.h"
#include "newton_raphson.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "atomistic_grid.h"
#include "utilities.h"
#include <iostream>
#include <fstream>
using std::ofstream;
using std::fstream;
using std::cout;
using std::endl;
using namespace dlib;

bool exist(const std::string& name)
{
	std::ifstream infile(name);
	return infile.good();
}

// #include <Eigen/Core>
// #include <LBFGS.h>
// using Eigen::VectorXd;
// using namespace LBFGSpp;	
// 
// #include <Eigen/Core>
// #include <iostream>
// #include <LBFGS.h>
// 
// using Eigen::VectorXd;
// using namespace LBFGSpp;
// 
// 
// class Rosenbrock
// {
// private:
//     int n;
// public:
//     Rosenbrock(int n_) : n(n_) {}
//     double operator()(const VectorXd& m, VectorXd& grad)
//     {
// 	double x, y,sum;
// 	int i,ix,iy,nx,ny;
// 	double dnx,dny,size,sum2;
// 	nx=dimensions::nx ;
// 	ny=dimensions::ny ;
// 	int npunti =  force_energy::calculation::getInstance().c.p.size();
// 	int n =  force_energy::calculation::getInstance().c.p.size();
// 	double h=dimensions::dh;	
// 
// // 	double load=loading::testNum ;
// 	double load=BLK1::load;
// 	double theta = BLK1::theta;
// 	double dt=BLK1::dt*10;
// 	static int step = 1;
// 	static int step2 = 0;
// 
// 	static int t = 0;
// 	
// 	int n_special=force_energy::calculation::getInstance().c.total_triangular_number;
// 	
// // 	struct conf_stru * c_local;
// // 	c_local = &force_energy::calculation::getInstance().c;
// 
// 	dnx=nx;
// 	dny=ny;
// 	size=dnx*dny;
// 
// 
// 
// 
// 	for(int i=0;i<npunti;++i)
// 	{ 
//         force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + m[i];
//         force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + m[i+npunti];
// 	} 
// 
// 	if(fexists("roule") == false)
// 	{ 
// 		cout<<"CODE HAS BEEN STOPPED IN THE CG"<<endl;
// 		exit (EXIT_FAILURE); 
// 	}
// 	
// 
// 
//     	if((step%BLK1::cg_freq ==0 && BLK1::cg_save == 1)){
//     		step =1;
// //     		exit(1);
// 
// //     		ps_file(force_energy::calculation::getInstance().c,t);
// //     		slicer(force_energy::calculation::getInstance().c,t);
//     		string CG = "CG";
//     		result_memorize(force_energy::calculation::getInstance().c);
// //     		write_to_a_file(force_energy::calculation::getInstance().c, t);
//  	    	string answer = "l";
// // 	    	ps_file(force_energy::calculation::getInstance().c,t+999, answer);
// 			int tprime  = t + 9999;
// // 			do_delaunay(force_energy::calculation::getInstance().c,tprime,theta, load);
// 
// 			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);
// 
// //     		cout<<"iteration:  "<<step<<" energy: "<<sum<<endl;
// 
//     	}
// 
// 	step2++;
// 	
//   
//     ++step;
// 	if(BLK1::constraint_min.compare("no") ==0)
// 		apply_BC_CG(force_energy::calculation::getInstance().c,load);
// 	calcstresses_CG_en(force_energy::calculation::getInstance().c);
// 	if(BLK1::constraint_min.compare("no") ==0)
// 		apply_BC_CG(force_energy::calculation::getInstance().c,load);
// 	
// 	sum = 0.;
// 	double func=0.;
// 	
// 
// 
// 
// 		for(int i=0;i<n;++i)
// 		{ 
// 			func+= h*h*force_energy::calculation::getInstance().c.energy_local[i];
// 			double x = force_energy::calculation::getInstance().c.gr[i].x;
// 			double y = force_energy::calculation::getInstance().c.gr[i].y; 
// 
//  			grad[i]     = h*x;
//  			grad[i+n]   = h*y;
//     	}
//     
// 
//         return func;
//     }
// };







std::vector<double>  edge_dislocation(double dx, double dy)
{
//dx and dy are the coo of the dislocation core	
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=nx*ny;
    double c1 = 0.159155*.6;
    double c2 = 1.;
    
	std::vector<double> ux;
	ux.resize(n);
// 	double dx = double(nx) / 2.;
// 	double dy = double(ny) / 2.;

	for (int iy=0; iy<ny; iy++) 
	for (int ix=0; ix<nx; ix++) 
	{    
		int i=(ix)*ny  + iy;


		ux[i] = c1*((c2*(-dx + ix)*(-dy + iy))/(pow(-dx + ix,2) + pow(-dy + iy,2)) + 
               atan((-dy + iy)/(-dx + ix)));
     	double diy =iy;
     	double dix =ix;
     	double dnx =nx;
     	double dny =ny;

/* 
		 if(iy >ny/2)
         ux[i] =  (1.-2*(diy/dny))*sin(dlib::pi*(dix/dnx));
		 if(iy <ny/2)
         ux[i] = -(1.+2*(diy/dny))*sin(dlib::pi*(dix/dnx));
 */

        if(ix == dx || iy == dy)
        	ux[i] = 0;
   //       if(ix <=dy || ix >= 3*dy )
  //        	ux[i] = 0;
        
//         cout<<ux[i]<<endl;
        
    }
    
    
              
    return ux;

}

std::vector<double>  edge_dislocation2(double dx, double dy)
{
//dx and dy are the coo of the dislocation core	
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=nx*ny;
    double c1 = 0.159155*.6;
    double c2 = 1.;
    
	std::vector<double> ux;
	ux.resize(n);
// 	double dx = double(nx) / 2.;
// 	double dy = double(ny) / 2.;

	for (int iy=0; iy<ny; iy++) 
	for (int ix=0; ix<nx; ix++) 
	{    
		int i=(ix)*ny  + iy;


		ux[i] = c1*((c2*(-dx + ix)*(-dy + iy))/(pow(-dx + ix,2) + pow(-dy + iy,2)) + 
               atan((-dx + ix)/(-dy + iy)));
        if(ix == dx || iy == dy)
        	ux[i] = 0;
//         if(ix <=dy || ix >= 3*dy )
//         	ux[i] = 0;
        
//         cout<<ux[i]<<endl;
        
    }
    
    
              
    return ux;

}

void initial_perturbation(conf_stru& c)
{
	// 	//THE QUENCHED DISORDER or INITIAL PERTURBATION FOR STATIQUE CASE


	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	if (BLK1::IC.compare("defect") == 0)
	{
		double amp = 0.5;
		cout << "applying " << BLK1::IC << endl;
		//c.disorder = perturba_precipate(amp,n);
	}
	else if (BLK1::IC.compare("wiener") == 0)
	{
		double av = 0.;
		double std = .04;
		cout << "applying " << BLK1::IC << endl;
		c.rfx = wiener(av, std,n);
		c.rfy = wiener(av, std,n);
	}
	else if (BLK1::IC.compare("uniform") == 0)
	{
		double a = -1;
		double b = 1;
		double amp = 0.01;
		cout << "applying " << BLK1::IC << endl;
		c.rfx = uniform(a, b, amp,n);
		c.rfy = uniform(a, b, amp,n);
	}
	else
	{
		cout << "applying  noise: " << BLK1::IC << endl;
		for (int i = 0; i<c.p.size(); ++i)
		{
			c.rfx[i] = 0.;
			c.rfy[i] = 0.;

		}

	}

}



void homogenous_frame_load(struct conf_stru& c){

		double load=BLK1::load;
		double initial_strain  =BLK1::load;
		double theta=BLK1::theta;


		static int n_direction = 2;




// 		temperature+=0.01;
        boundary_conditions set1;
		string filenamepiola,filenamecauchy;
		std::ofstream piolaw,cauchyw;
// 		for(int k=0;k<4;k++){
// 		
// 			filenamepiola  = "alpha_piola_frame"  + IntToStr(k) + ".dat";
// 			filenamecauchy = "alpha_cauchy_frame" + IntToStr(k) + ".dat";
// 			piolaw.open (filenamepiola.c_str());
// 			cauchyw.open(filenamecauchy.c_str());
// 			load  = 0;
// 			BLK1::setNum(load);
// 			initial_strain  =BLK1::load;
// 			int t=1;

		
		
			for(int iter=0;iter<BLK1::total_iteration;++iter){
			
			cout<<"homogenous iteration number"<<iter<<endl;
			cout<<"load"<<load<<endl;

				set1.setParameters(theta,load);
				choose_bc(set1); 		
// 				if(BLK1::macro_def.compare("shear") ==0)
// 					set1.macro_shear();
// 				if(BLK1::macro_def.compare("tension") ==0)
// 					set1.macro_compression();
// 				 if(BLK1::macro_def.compare("uni_tension") ==0)
// 					set1.uni_macro_compression();
	
// 					
	// 			if(BLK1::bc_mode.compare("frame") ==0)
	// 				set1.def_frame(c,set1);
			
				apply_BC_generale(c,set1);

					for(int i =0;i<c.p.size();++i){
						c.p[i].x = c.pfix[i].x * set1.f.f11 + c.pfix[i].y  * set1.f.f12;				
						c.p[i].y = c.pfix[i].x * set1.f.f21 + c.pfix[i].y  * set1.f.f22;
					}
//				if(k==0)
//					write_to_a_ovito_file(c, t++);
				calcstresses_CG_en_homogenous(c, load);
				initial_strain+=BLK1::strain_rate*pow(-1.,n_direction);
				load  = initial_strain;
				BLK1::setNum(load);

			
			}
			
//				piolaw.close();
//				cauchyw.close();

//		}	
			

}


void dynamic(struct conf_stru& c)
{
   
   
   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();
	double load=BLK1::load;
	int t=1;
	const char* filename = "roule";
    ofstream myfile;
    myfile.open ("roule");



   //These are displacements needed in CG
	dlib::parallel_for(1, 0,nx*ny, [&](int i)
	{
		c.disp[i].x     = c.p[i].x-c.pfix[i].x ;
		
		c.disp[i].y     = c.p[i].y-c.pfix[i].y ;   	
	});
     
    
    
    //FIRST CALCULATE FORCES
    struct conf_stru guess;
    column_vector starting_point;
	starting_point.set_size(2*n);

    for(int iter=0;iter<20000;iter++)
    {	
		//THEN GUESS THE SOLUTION USING EULER TIME STEP
		guess  = timestep_euler(c, load);
		
		for(int i=0;i<n;i++)
		{
			c.p[i].x     = guess.disp[i].x+c.pfix[i].x ;
		
			c.p[i].y     = guess.disp[i].y+c.pfix[i].y ;   	

			c.disp[i].x  = guess.disp[i].x ;
		
			c.disp[i].y  = guess.disp[i].y ;   	
		}

	
      

		   //INITIATE the STARTING POINT; USING INITIAL EULER GUESS
			dlib::parallel_for(1, 0,n, [&](int i)
			{
				starting_point(i)     = guess.disp[i].x ;
				starting_point(i+n)   = guess.disp[i].y ;   	
			});   
			 cout<<"conjugate gradient starts"<<endl;
			double wall0 = get_wall_time();
			double cpu0  = get_cpu_time();
			double theta;
				 find_min(lbfgs_search_strategy(10),  // The 20 here is basically a measure of how much memory L-BFGS will use.
						 objective_delta_stop_strategy(1e-6),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
						 rosen, rosen_derivative, starting_point, -nx*ny);

			double wall1 = get_wall_time();
			double cpu1  = get_cpu_time();
	
			cout << "Wall Time = " << wall1 - wall0 << endl;
			cout << "CPU Time  = " << cpu1  - cpu0  << endl;


			 dlib::parallel_for(1, 0,n, [&](int i)
			 { 
				 double xx =  starting_point(i);
				 double yy =  starting_point(i+n); 
				 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x + xx;
				 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y + yy;
				 c.disp[i].x     = xx ;
				 c.disp[i].y     = yy ;

			 });
			 
			 if(iter%100 ==0)
				ps_file2(force_energy::calculation::getInstance().c, t++);
		    
		    
		     if(fexists(filename) == false){ cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; }
   
			 
	}
}

void statique(struct conf_stru& c)
{
   
   
   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();
	double load=BLK1::load;
	double initial_strain  =BLK1::load;
	double temperature = BLK1::beta_Var;
	int k1,k2;
	k1=0;
	
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;

	int t=0;
	static int n_direction = 2;
 	if(BLK1::strain_rate <0)
 		n_direction = 3;
		
	
	const char* filename = "roule";

	ofstream myfile;
	myfile.open ("roule");
	
	//SHEARING ANGLE
 	double theta=BLK1::theta;

	int temp_counter=0;
	//for(int k=0;k<2;k=k+c.inc)
	for(int k=0;k<dimensions::triangle_limit;k=k+c.inc)
	for(int i=0;i<n;i++)
	{
		if(c.bc_cond[i][k] == 2)
			temp_counter++;
	}  
	
	c.total_triangular_number = temp_counter;
	cout<<"c.total_triangular_number= "<<c.total_triangular_number<<endl;
	write_to_a_ovito_file(c, 999);


   //IF FRAME DEFORMATION ACTIVATED, C_PFIX STARTS AS A DEFORMED CONFIGURATION
		boundary_conditions set0,setold;
		set0.setParameters(theta,load);

	if(BLK1::bc_mode.compare("frame") ==0)
		set0.def_frame(force_energy::calculation::getInstance().c,set0);
	else
		set0.macro_shear();
	setold=set0;
   //These are displacements needed in CG

	for(int i=0;i<n;i++)
	{
		c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		c.disp[i].y  = c.p[i].y -c.pfix[i].y ;   

	}  

	////dipole v


//    std::vector<double > v3 = edge_dislocation(ny/2-1    ,  40);    
//    std::vector<double > v4 = edge_dislocation(ny/2 ,nx/2+nx/4	);


    std::vector<double > v3 = edge_dislocation(ny/2-1    ,nx/2-20);    
    std::vector<double > v4 = edge_dislocation(ny/2      ,nx-8);


	//dipole v
//  
     std::vector<double > v1 = edge_dislocation2( 70 ,nx-16  );    
     std::vector<double > v2 = edge_dislocation2(ny-80,nx-17 );
 



    column_vector starting_point,sp_max,sp_min,starting_point2;
	starting_point.set_size(2*n);
	starting_point2.set_size(2*n);

	sp_max.set_size(2*n);
	sp_min.set_size(2*n);

	dlib::parallel_for(1, 0,n, [&](int i){
		starting_point(i)     = c.disp[i].x  ;
		starting_point(i+n)   = c.disp[i].y  ;   	
	});

//to make the model scalar

	if(BLK1::scalar.compare("yes") ==0){
		dlib::parallel_for(1, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0. ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = 0.  ;   	


		});  
	} 


	else if(BLK1::scalar.compare("semi") ==0){
		dlib::parallel_for(1, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0.3 ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = -0.3  ;   	


		});  
	} 

	else if(BLK1::scalar.compare("no") ==0){
		dlib::parallel_for(1, 0,n, [&](int i){
			sp_max(i)     =  1000.  ;
			sp_max(i+n)   =  1000. ;   	
			sp_min(i)     = -1000.  ;
			sp_min(i+n)   = -1000.  ;   	

				double h=1;	
				starting_point(i)     =  0*dimensions::dh*1.1*(1.1*v3[i] - 1.1*v4[i])	;
				starting_point(i+n)   = +0*1.1*(1.1*v1[i] - 1.1*v2[i]) ;   	

				 int iy = i / nx;
				 int ix = i % nx;
//  				 if(iy==ny/2-1 && (ix>nx/8 && ix< nx- nx/8)  )
//  				 	starting_point(i) = .5001;
//  				 if(iy==ny/2 && (ix>nx/8 && ix< nx- nx/8)  )
//  				 	starting_point(i) = -.5001;


// 				 if(iy==ny/2 && ix>nx/4 )
// 				 	starting_point(i) = .35;


				c.disp[i].x +=starting_point(i) ;
				c.disp[i].y +=starting_point(i+n)   ;   	


		});  	
		
	} 
	
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
	if(BLK1::IC.compare("none") !=0 ){

		std::vector<double > u_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );    
		std::vector<double > v_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );  
		for(int i=0;i<n;i++){
			  c.disp[i].x +=u_dis[i];
			  c.disp[i].y +=v_dis[i];
		}
	}


	if(BLK1::IC.compare("volume") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(1., BLK1::d2,c.p.size());
	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;


		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][3] =dv4[i];


	}
	
	
	if(BLK1::IC.compare("alloying") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());

	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
				c.alloying[i][k]  = BLK1::beta_Var;


		for(int i=0;i<n;i++)
			  c.alloying[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][3] =dv4[i];


	}	
	
	
	if(BLK1::IC.compare("precip") ==0 || BLK1::IC.compare("precipx") ==0  ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 amp=BLK1::d1;
	 std::vector<double > dv1,dv2,dv3;
	  
	  dv1 =   perturba_precipate(amp,c.p.size());
	  dv2 =   perturba_precipate(amp,c.p.size());
	  dv3 =   perturba_precipate(amp,c.p.size());

		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;
		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv3[i];
		
	}	
	
	

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());

	for(int i=0;i<n;i++)
	{
		int iy = i / nx;
		int ix = i % nx;
		
		

		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ; 
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
		starting_point2(i)   = starting_point(i);
		starting_point2(i+n) = starting_point(i+n);

	}  


	write_to_a_ovito_file(c, 99);

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

	
	string dissipation,av_sizes,av_sizes2,av_sizes3,size_vs_energy,strain_vs_energy,slip;
	string  av_sizespitteri = "int_total_slip_vs_stress_drop.dat";
	string half_stress =  "half_stress_drop.dat";
	string full_stress =  "full_stress_drop.dat";
	string piola_stress =  "well_drop.dat";
	string piola_stress_full =  "well_drop_vs_energy_drop.dat";

	dissipation="dissipation.dat" ;
	slip="alpha_vs_drop.dat" ;

	av_sizes="avalanches.dat" ;
	av_sizes2="stress_drop.dat" ;
	av_sizes3="strain_jump.dat" ;
	size_vs_energy="well_drop_vs_stress_drop.dat.dat" ;
	strain_vs_energy="alpha_vs_dissipation.dat" ;

	string slip_grad  = "total_slip.dat";

	
	fstream filestr,filestrav,filestrav2,filestrav3,filestrse;
	
	fstream peng1,peng2,peng3,peng4;
	string penge =  "peng_energy.dat";
	string pengs =  "peng_stress.dat";
	string pengde =  "peng_dstress.dat";
	string pengds =  "peng_denergy.dat";



	string fulldata =  "full_data.dat";
	fstream fsfullstream;

	
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	std::vector <std::vector<matrix_stru> > m_field;
	m_field.resize(n);
	for (int i = 0; i < n; ++i)
		m_field[i].resize(6);
                    
                
	std::vector <std::vector<int> > well_locator,well_locator_old;
	well_locator.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator[i][k]=0;


	well_locator_old.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator_old[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator_old[i][k]=0;

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////


	
		dlib::parallel_for(1, 0,n, [&](int i)
		{
			starting_point(i)     = c.disp[i].x  ;
			starting_point(i+n)   = c.disp[i].y  ;   	
		});  
		
		double drop =0;
		double drop3 =0;

		double stress_initial=0;
		double stress_final =0;
		double strain_initial =initial_strain;
		double strain_final   =initial_strain;
		double energy_initial =0;
		double energy_final   =0;
		double slip_initial =0;
		double slip_final =0;
		double stress_final_pitteri=0;
		double stress_initial_pitteri=0;
		double total_slip =0;
		double stress_initial_full=0;
		double stress_final_full=0;
		double stress_initial_piola=0;
		double stress_final_piola=0;
		double stress_initial_piola_full=0;
		double stress_final_piola_full=0;


		c.load = load;
		c.soft = 0.0;
		boundary_conditions set1;

		

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	int memorynumber =BLK1::memory; //0 memory //-1normal
    for(int iter=0;iter<BLK1::total_iteration;++iter){	
		

// 	    if(iter>475)
// 	    	BLK1::setCg_save(1,5);
	
		if(iter> memorynumber){
	    result_memorize(c);
	    if(iter%1 == 0){
	    	string answer = "l";
	    	//ps_file(c,t, answer);
	    }

		if(BLK1::constraint_min.compare("yes") ==0)
			apply_BC_box(c,sp_min,sp_max); 
		
// 		double energy_initial = energy_for_avalanches(c,set0,setold);
		//recall here that is is the energy and stress of loaded but not relaxed system
		energy_initial = rosen(starting_point)*(1.0);
		stress_initial = avalanche_sizes(c);
		stress_initial_pitteri = avalanche_sizes_half(c);
		stress_initial_full = avalanche_sizes_full(c);
		stress_initial_piola = avalanche_sizes_piola(c);
		stress_initial_piola_full = avalanche_sizes_piola_full(c);

			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(7)  << " " << load<<" "<<energy_initial/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(7)  << " " << load<<" "<<stress_initial/(1.*c.total_triangular_number)<<endl ;
				peng2.close();
 			}



		cout<<"conjugate gradient starts"<<endl;
		
		double wall0 = get_wall_time();
		double cpu0  = get_cpu_time();



		if(BLK1::constraint_min.compare("no") ==0){
		find_min(lbfgs_search_strategy(14),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, -n);
		}

// 		if(t>=117)
// 				BLK1::setCg_save(1,10);


		if(BLK1::constraint_min.compare("yes") ==0){
// 			find_min_box_constrained(lbfgs_search_strategy(7),  // The 10 here is basically a measure of how much memory L-BFGS will use.
// 			gradient_norm_stop_strategy(BLK1::precision).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 			rosen, rosen_derivative, starting_point, sp_min,sp_max);

			find_min_box_constrained(lbfgs_search_strategy(12),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, sp_min,sp_max);

    		// Set up parameters
//     		LBFGSParam<double> param;
//     		param.epsilon = 5e-3;
//     		param.max_iterations = 10000;
// 
//     		// Create solver and function object
//     		LBFGSSolver<double> solver(param);
//     		Rosenbrock fun(2*n);
// // 
//     		// Initial guess
//     		VectorXd x = VectorXd::Zero(2*n);
//     		// x will be overwritten to be the best point found
//     		double fx;
//     		for (int i = 0; i < n; ++i){
//     			x[i] = starting_point(i);
// 	    		x[i+n] = starting_point(i+n);
//     		}
//     		int niter = solver.minimize(fun, x, fx);
// // 
//     		std::cout << niter << " iterations" << std::endl;
// //     		std::cout << "x = \n" << x.transpose() << std::endl;
// //     		std::cout << "f(x) = " << fx << std::endl;
//             for (int i = 0; i < n; ++i){
//     			starting_point(i)  = x[i];
//     			starting_point(i+n)= x[i+n];
//     		}


		}
		

		cout<<"conjugate gradient ends"<<endl;



		double wall1 = get_wall_time();
		double cpu1  = get_cpu_time();

		cout << "Wall Time = " << wall1 - wall0 << endl;
		cout << "CPU Time  = " << cpu1  - cpu0  << endl;
		
//  		cout<<rosen(starting_point);
		double sizes=0;
		energy_final = rosen(starting_point)*(1.0);
		



		 //it updates the  solution for measurements and keeps the old solution
		 for(int i=0;i<n;i++)
		 { 
			 double xx =  starting_point(i);
			 double yy =  starting_point(i+n); 



			 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
			 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;

		 }

		 

		 result_memorize(c);
		 measurements(c);
		 double total_slips = slip_calculations(c);
		 int total_slips_int = slip_calculations_integer(c);

		// cout <<"total_slips= "<<total_slips<<endl;
		// cout <<"total_slips_integer= "<<total_slips_int<<endl;




		 stress_final = avalanche_sizes(c);
		 stress_final_pitteri = avalanche_sizes_half(c);
		 stress_final_full = avalanche_sizes_full(c);
		 stress_final_piola = avalanche_sizes_piola(c);
		 stress_final_piola_full = avalanche_sizes_piola_full(c);
		 
			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(7)  << " " << load<<" "<<energy_final/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				
				
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(7)  << " " << load<<" "<<stress_final/(1.*c.total_triangular_number)<<endl ;
				peng2.close();

// 				double drop_energy=energy_initial-energy_final;
// 
// 				peng3.open (pengde, fstream::in | fstream::out | fstream::app);
// 				peng3 << std::scientific << std::setprecision(7)   << " "<< load<<" "<<drop_energy<<endl ;
// 				peng3.close();
// 				
// 				double drop_stress=stress_initial-stress_final;
// 				peng4.open (pengds, fstream::in | fstream::out | fstream::app);
// 				peng4 << std::scientific << std::setprecision(7)  << " " << load<<" "<<drop_stress<<endl ;
// 				peng4.close();
// 				
// 				cout<<energy_initial<<" "<<energy_final<<" "<<energy_initial-energy_final<<" "<<drop_energy<<endl;


			}


		 //EULERIAN OR LAGRANGIAN
		 assign_solution(c,starting_point);
		//keep the solution against any change
// 		for (int i=0; i<n; ++i){	
// 	  	  starting_point2(i)     = starting_point(i);
// 	 	  starting_point2(i+n)   = starting_point(i+n);			 
// 		}

		well_locator_old = well_locator;
		actual_m_matrix_calculations(c,m_field,well_locator); 
		int well_jumps=0;;
		for(int k=0;k<dimensions::triangle_limit;k=k+c.inc){
			for(int i=0;i<n;i++){
				well_jumps +=	abs(well_locator[i][k] - well_locator_old[i][k]);
	
		}
			}
			
		cout<<"well_jumps= "<<well_jumps<<endl;


		 //drop of stress
		 drop = (stress_initial-stress_final)*pow(-1.,n_direction);
		 //drop of  half stress
		 double drop4 = (stress_initial_pitteri-stress_final_pitteri)*pow(-1.,n_direction);
		 double dropfull = (stress_initial_full-stress_final_full)*pow(-1.,n_direction);
		 double droppiola = (stress_initial_piola-stress_final_piola)*pow(-1.,n_direction);
		 double droppiolafull = (stress_initial_piola_full-stress_final_piola_full)*pow(-1.,n_direction);

		 cout<<"total_slips_int= "<<total_slips_int<<endl;
		 cout<<"drop= "<<drop<<endl;

		if(iter>1  && well_jumps>=0 ){
			//cout<< energy_initial-energy_final << endl;
			
			//energy dissipation
			filestr.open (dissipation, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(7)   << " "<<energy_initial-energy_final<<endl ;
			filestr.close();

			//total slip
			filestrav.open (av_sizes, fstream::in | fstream::out | fstream::app);
			filestrav << std::scientific << std::setprecision(7)   << " "<<total_slips<<endl ;
			filestrav.close();

			//stress drop
			filestrav2.open (av_sizes2, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<drop<<endl ;
			filestrav2.close();

			//half stress drop
			filestrav2.open (half_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<<load<<" "<<drop4<<endl ;
			filestrav2.close();

			//full stress drop
			filestrav2.open (full_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<dropfull<<endl ;
			filestrav2.close();
			//piola stress drop
			filestrav2.open (piola_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<endl ;
			filestrav2.close();

			//piola stress drop
			filestrav2.open (piola_stress_full, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<" "<<energy_initial-energy_final<<endl ;
			filestrav2.close();



			//distance to jumps
			double drop2  = abs(load - strain_initial);
			strain_initial = load;

			filestrav3.open (av_sizes3, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(drop)<< " "<<energy_initial-energy_final <<endl ;
			filestrav3.close();

			//total_slips_int versus strain
			filestrav3.open (av_sizespitteri, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips_int)<< " "<<abs(drop) <<endl ;
			filestrav3.close();

			
			//total_slip_integer versus energy dissipation
			filestrse.open (size_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<< " "<<drop <<endl ;
			filestrse.close();
			
			//total_slip versus energy dissipation
			filestrse.open (strain_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<load<< " "<<energy_initial-energy_final <<endl ;
			filestrse.close();			
			
			//total_slip integer
			filestrse.open (slip, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<load<< " "<<drop<<endl ;
			filestrse.close();	
			
			fsfullstream.open (fulldata, fstream::in | fstream::out | fstream::app);
			fsfullstream << std::scientific << std::setprecision(7)   
			<< " "<<load<< " "<<well_jumps<<" " <<drop<<" "<< energy_initial-energy_final << 
			" "<< drop4<<" "<< stress_initial/c.total_triangular_number<< 
			" "<< stress_initial_pitteri/c.total_triangular_number<<
			" "<<dropfull<<
			" "<< stress_final/c.total_triangular_number<<
			" "<< stress_final_pitteri/c.total_triangular_number<<
			" "<< stress_initial_full/c.total_triangular_number<<
			" "<< stress_final_full/c.total_triangular_number<<
			" "<< energy_initial/c.total_triangular_number<<
			" "<< energy_final/c.total_triangular_number<<endl ;
			fsfullstream.close();			
					


		}

		
		 
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
		set1.setParameters(theta,load);
		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
// 		if(BLK1::macro_def.compare("macro_shear_rhombic") ==0)
// 			set1.uni_macro_compression();

		
		
		//keep the previous solution
		for(int i =0;i<c.p.size();++i){
			temp_coo[i].x = c.p[i].x   ;				
			temp_coo[i].y = c.p[i].y   ;
			c.pold[i].x = c.p[i].x ;
			c.pold[i].y = c.p[i].y ;

		}

// 		def_grad temp;	
// 		temp= set1.f.multiply(setold.f.inverse(setold.f));
	// 
	// 	//guess the next solution incrementally		

		int inc = force_energy::calculation::getInstance().c.inc ;	
	

			//output operations
// 			ps_file(c,t); 
		if(iter%BLK1::cg_freq == 0 || well_jumps > 50*nx/200){
			//slicer(c,t);
			write_to_a_ovito_file(c,t);
// 			write_to_a_ovito_file_special(c,t);
			string filename2 = "all_load.dat";
			all_data_to_a_file(t,load,filename2);

			write_to_a_file(c,t);
			write_to_a_ovito_file_remove_Fmacro(c,t++,load);
			string filename = "last_load.dat";
			single_data_to_a_file(load,filename);
			if(BLK1::IC.compare("precipx") ==0 && iter==0){
				for(int k=0;k<3;k++)
					for(int i=0;i<n;i++)
			 		 c.disorder[i][k] =1;

			}
		//	do_delaunay(force_energy::calculation::getInstance().c,t,theta,load);
 		}
  		}
 		
 		if(iter == memorynumber){
 			string filename2  =  BLK1::config ;
			if(exist(filename2) == false){ 
		 		cout<<"no file sorry  "<<filename2<<"\n"; 
			}

			ifstream disp;

		//    cout<< filename << "  \n";
			disp.open(filename2.c_str());
			double temp;
			int nol;
			disp >> nol;
			cout<<"number of lines in the file"<<nol<<endl;
			for (int i = 0; i<n; ++i){
			disp >> std::scientific >> std::setprecision(16)>> c.p[i].x >> c.p[i].y >> temp >> temp >> temp ;
// 			if(i<2 || i>nol-2)
// 				cout << std::scientific << std::setprecision(14)<< c.p[i].x << " "<<c.p[i].y << " "<<temp << " "<< temp << " "<<temp <<endl;
			c.pfix[i].x = c.p[i].x;
			c.pfix[i].y = c.p[i].y; 
			c.pold[i].x  = c.p[i].x  ; 
			c.pold[i].y  = c.p[i].y  ; 		

			}

			disp.close();
			
			//string filename3  =  "../all_load.dat" ;
			string filename3  =  BLK1::config_u ;
			disp.open(filename3.c_str());


			for (int i = 0; i<=BLK1::memory_number; ++i)
				disp >> std::scientific >> std::setprecision(16)>> initial_strain>> temp  ;
			
			disp.close();

		    c.load = load = initial_strain ;
		   	BLK1::setNum(load);
		   	cout<<"initial strain memory= "<<initial_strain<<endl;
 		
   			set1.setParameters(theta,load);
   			choose_bc(set1);

        
        	apply_BC_generale(c,set1);
 			apply_BC(c,load);

		   	actual_m_matrix_calculations(c,m_field,well_locator); 
		   	result_memorize(c);
		   	measurements(c);
		   	double dpda = dphidalpha(c,true);


 		
 		}
		




	


		 if(fexists(filename) == false){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
				 
 	


	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////C is changing here due to the loading///////////////////////////
//         apply_BC_generale(c,set1);
//  	apply_BC(c,load);
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

//		double calculated_rate = BLK1::strain_rate;
//		double calculated_rate = m_matrix_calculations(c, load,theta,m_field,well_locator,n_direction);
 		double calculated_rate = BLK1::strain_rate;
		 
	


		if(n_direction % 2 == 0 && load>   BLK1::loading_limit ){
			n_direction++;
// 			goto label44 ;
		}
// 		if(n_direction % 2 != 0 && load< - BLK1::loading_limit ){
		if(n_direction % 2 != 0 && load< - 0 ){
		    printf ("Error opening file");
    		exit (EXIT_FAILURE);
			n_direction++;
		}

		
//		initial_strain+=calculated_rate;
// 		calculated_rate = BLK1::strain_rate;
 // 		initial_strain+=calculated_rate*pow(-1.,n_direction);
 		initial_strain+=calculated_rate;

// 		temperature+=0.01;
		load  = initial_strain;
		c.load = load;
		//c.soft = 0.;
		BLK1::setNum(load);
// 		BLK1::setBeta(temperature);
 		
   		set1.setParameters(theta,load);
   		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
			
		setold=set0;
		set0=set1;
        
        apply_BC_generale(c,set1);
 		apply_BC(c,load);
 		
        struct conf_stru guess;
		struct boundary_conditions setnew;
		setnew.setParameters(theta,calculated_rate);
   		choose_bc(setnew);




	    if(BLK1::guess_method.compare("boundary")==0){	
			for (int i=0; i<n; ++i){
			
				//fix boundary condition
				if(check(c.bc_cond[i][k1],0)  || check(c.bc_cond[i][k2],0) ){
					 //u_min and v_min
					starting_point(i)     =  c.pfix[i].x * setnew.f.f11 + c.pfix[i].y * setnew.f.f12 - c.pfix[i].x;
					starting_point(i+n)   =  c.pfix[i].x * setnew.f.f21 + c.pfix[i].y * setnew.f.f22 - c.pfix[i].y;
					c.p[i].x = starting_point(i)   + c.pfix[i].x;
					c.p[i].y = starting_point(i+n) + c.pfix[i].y;
				 
				}

			 }
		 }

	    else if(BLK1::guess_method.compare("bulk")==0){	

			for (int i=0; i<n; ++i){
				starting_point(i)     =  c.pfix[i].x * setnew.f.f11 + c.pfix[i].y * setnew.f.f12 - c.pfix[i].x;
				starting_point(i+n)   =  c.pfix[i].x * setnew.f.f21 + c.pfix[i].y * setnew.f.f22 - c.pfix[i].y;
				c.p[i].x = starting_point(i)   + c.pfix[i].x;
				c.p[i].y = starting_point(i+n) + c.pfix[i].y;

			 }

		}

		if(BLK1::IC.compare("wienerc") ==0){
			//0.005 for square
			std::vector<double > u_dis = wiener( 0 ,BLK1::d1,c.p.size()  );    
			std::vector<double > v_dis = wiener( 0 ,BLK1::d1,c.p.size()  );  
			for(int i=0;i<n;i++){
				if(check(c.bc_cond[i][k1],2)  && check(c.bc_cond[i][k2],2) ){
				  starting_point(i)   +=u_dis[i];
				  starting_point(i+n) +=v_dis[i];
				}
			}
		}	
		


		 if(fexists(filename) == false || load >= BLK1::loading_limit){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
		 	

				 
 	}

	label44: ; 
 	
}

std::pair<double, double> get_XY_for_rotated_square1(double x, double y, double angle, double interface,double keep_slip) {
    double nx=dimensions::nx;
    double ny=dimensions::ny;
    double cx = interface-1;
    double cy = keep_slip-1;
	
	double temp = (x-cx)*cos(angle) - (y-cy)*sin(angle) + cx;  
	          y = (x-cx)*sin(angle) + (y-cy)*cos(angle) + cy;
    x=temp;


	return std::pair<double, double>(x, y);
}


std::pair<double, double> get_XY_for_rotated_square2(double x, double y, double angle, double interface,double keep_slip) {
    double nx=dimensions::nx;
    double ny=dimensions::ny;
    double cx = interface;
    double cy = keep_slip-1;
	
	double temp = (x-cx)*cos(angle) - (y-cy)*sin(angle) + cx;  
	          y = (x-cx)*sin(angle) + (y-cy)*cos(angle) + cy ;
    x=temp;

	return std::pair<double, double>(x, y);
}
void grain_boundary(struct conf_stru& c, alglib::real_1d_array& starting_point ){

   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();

	double interface=nx/2-1;
	double keep_slip,keep_slip2;
	double anglee,anglee2;
	double theta_cr2=2;
	double theta_cr=BLK1::theta	;

	std::vector<int> jump_index;
	int jj=0;
	int counter=0;
	int li=0;

	for (int iy = 1; iy<ny; ++iy){
		int i= iy*nx + 0;
		int hh = round(c.pfix[i].y*2.*sin(0.5*theta_cr*dlib::pi/180.));
// 		cout<<"round= "<<hh<<" at "<<jj<<endl;

			if(hh	 >2*jj ){
		// 		cout<<"adding dislocation at: "<< counter<<endl;
				jump_index.push_back(i);
				for (int iyy = iy; iyy<ny; ++iyy)
				for (int ix = 0; ix<nx; ++ix){
					int i= iyy*nx + ix;
		// 			cout<<"adding dislocation"<<endl;
					if(c.pfix[i].y> jj){
		// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
						if(c.pfix[i].x<interface ){
							starting_point[i]   =  -1*(jj+1);
							li=i;	
						}	
			
		// 				if(c.pfix[i].x>=interface	){
		// 					starting_point(i)   =  1.0*(jj+1);
		// 				}
					}
				}
				if(jj==0){
						keep_slip	=c.pfix[i].y;
				}

				jj++;
				counter++;
			}

	}



	double phase = (c.pfix[jump_index[1]].y-c.pfix[jump_index[0]].y);
		cout<<phase/2<<endl;


	jj=0;
	for (int iy = 1; iy<ny; ++iy){
	
		int i= iy*nx + 0;
		int hh = round((c.pfix[i].y-phase/2.)*2.*sin(0.5*theta_cr*dlib::pi/180.));
		// cout<<"round= "<<hh<<" at "<<jj<<endl;
		// cout<<"j= "<<c.pfix[i].y<<endl;

			if(hh	 >2*jj	){
		// 		cout<<"adding dislocation at: "<< counter<<endl;
				for (int iyy = iy; iyy<ny; ++iyy)
				for (int ix = 0; ix<nx; ++ix){
					int i= iyy*nx + ix;
		// 			cout<<"adding dislocation"<<endl;
					if(c.pfix[i].y> jj){
		// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
		// 				if(c.pfix[i].x<interface ){
		// 					starting_point(i)   =  -1.0*(jj+1);
		// 					li=i;	
		// 				}	
				
						if(c.pfix[i].x>=interface	){
							starting_point[i]   = 1*(jj+1);
						}
					}
				}

				if(jj==0){
						keep_slip2	=c.pfix[i].y;
				}
				jj++;
				counter++;
			}

	}


	cout<<"keep_slip= "<<keep_slip2<<endl;
	double yy = c.p[(ny-1)*nx+interface-1].y;
// 		double xx = (interface-1.)-(c.pfix[(ny-1)*nx+interface-1].x + starting_point((ny-1)*nx+interface-1) );
	double xx =-starting_point[(ny-1)*nx+interface-1]+0.25;

	anglee = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
	cout<<"yy= "<<yy<<endl;
	cout<<"xx= "<<xx<<endl;
	cout<<"li= "<<li<<endl;

	cout<<"angle= "<<anglee<<endl;



	yy = c.p[(ny-1)*nx+interface].y;
	xx =-starting_point[(ny-1)*nx+interface]-0.25;

	anglee2 = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
	cout<<"yy= "<<yy<<endl;
	cout<<"xx= "<<xx<<endl;
	cout<<"li= "<<li<<endl;

	cout<<"angle2= "<<anglee2<<endl;




	for (int i = 0; i<n; ++i){
			c.disp[i].x =starting_point[i] ;
			c.disp[i].y =starting_point[i+n];   	
	}  	
		
	for(int i=0;i<n;i++){
		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ;
	} 

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c, -4);
// 	anglee = theta_cr/2.;
//     anglee2 = anglee2;
	for(int i=0;i<n;i++){
		int iy = i / nx;
		int ix = i % nx;
		double x;
		double y;

		if(c.pfix[i].x<interface){
			auto real_coordinates = get_XY_for_rotated_square1(c.p[i].x, c.p[i].y, -(anglee)*dlib::pi/180.,interface,keep_slip);
			x = real_coordinates.first;
			y = real_coordinates.second;
			c.p[i].x  = x ; 
			c.p[i].y  = y ; 
		}
 
		if(c.pfix[i].x>= interface){
			auto real_coordinates = get_XY_for_rotated_square2(c.p[i].x, c.p[i].y, -(anglee2)*dlib::pi/180.,interface,keep_slip2);
			x = real_coordinates.first;
			y = real_coordinates.second;
			c.p[i].x  = x ; 
			c.p[i].y  = y ; 
		}
		
		
		
	} 
	
	   cout<<"writing"<<endl;
	   write_to_a_ovito_file(c, -3);
	   
	   struct punto_stru p = c.p[(ny/2)*nx+(nx/2)+5];
	   struct de_stru d;
	   d.p[0] = p;
	   d.p[1] = c.p[p.nn[0]];
	   d.p[2] = c.p[p.nn[0+1]];
	   struct base_stru vv1;
	   	vv1.e1[0]= 0 ;
		vv1.e1[1]= 1 ;
		vv1.e2[0]= (d.p[2].x-d.p[0].x) ;
		vv1.e2[1]= (d.p[2].y-d.p[0].y) ;
		
	struct cella_stru cc;

	cc.c11 = sqrt(vv1.e1[0] * vv1.e1[0] + vv1.e1[1] * vv1.e1[1]);
	cc.c22 = sqrt(vv1.e2[0] * vv1.e2[0] + vv1.e2[1] * vv1.e2[1]);
	cc.c12 = vv1.e1[0] * vv1.e2[0] + vv1.e1[1] * vv1.e2[1];
		
	double costheta= (cc.c12)/(cc.c11*cc.c22);
	 cout<<"angle rotation= "<<acos(costheta)*180/dlib::pi<<endl;;	


	double distance = +100;
	double distance_max = -100;

	double vx,vx2;
	double vy,vy2;
	int tiy=ny/2-1;
	vx =c.p[tiy*nx+interface+5].x -c.p[tiy*nx+interface+4].x;
	vy =c.p[tiy*nx+interface+5].y -c.p[tiy*nx+interface+4].y;


	vx2 =c.p[tiy*nx+interface-5].x -c.p[tiy*nx+interface-6].x;
	vy2 =c.p[tiy*nx+interface-5].y -c.p[tiy*nx+interface-6].y;

	


	for(int iy=0;iy<ny;iy++){
		 double temp_distance = sqrt(pow(c.p[iy*nx+interface].x -c.p[iy*nx+interface-1].x,2.) +
							   pow(c.p[iy*nx+interface].y -c.p[iy*nx+interface-1].y,2.) );



// 		cout<<"distance= "<<temp_distance<<endl;
// 		cout<<"vx= "<<vx<<endl;
// 		cout<<"vy= "<<vy<<endl;
		
		if(temp_distance < distance)
			distance= temp_distance;
		if(temp_distance > distance_max)
			distance_max= temp_distance;

	}

			cout<<"final distance min= "<<distance<<endl;
			cout<<"final distance max= "<<distance_max<<endl;
	
	if(BLK1::theta > 4.){

		for(int i=0;i<n;i++){

			if(c.pfix[i].x>= interface && distance_max > 1.8){
				c.p[i].x -= vx*(.12);
				c.p[i].y -= vy*(.12);
			}	

			if(c.pfix[i].x < interface && distance_max > 1.8){
				c.p[i].x += vx2*(.12);
				c.p[i].y += vy2*(.12);
			}	



			if(c.pfix[i].x>= interface && distance < 0.6){
				c.p[i].x += vx2*(1-distance)*0.5;
				c.p[i].y += vy2*(1-distance)*0.5;
			}	
	
			if(c.pfix[i].x< interface && distance < 0.6){
				c.p[i].x -= vx2*(1-distance)*0.5;
				c.p[i].y -= vy2*(1-distance)*0.5;
			}	
		} 
	}


	for(int i=0;i<n;i++){
// 		c.pfix[i].x  = c.p[i].x  ; 
// 		c.pfix[i].y  = c.p[i].y  ; 	
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
	}




	 
	for(int i=0;i<n;i++){
		starting_point[i]     = c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		starting_point[i+n]  =  c.disp[i].y = c.p[i].y -c.pfix[i].y ;   

	
	} 
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c,-2);
	string answer = "l";
	ps_file(c,-2, answer);




}


void grain_boundary_v2(struct conf_stru& c, alglib::real_1d_array& starting_point ){

   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();

	double interface=nx/2-1;
	double keep_slip,keep_slip2;
	double anglee,anglee2;
	double theta_cr2=2;
	double theta_cr=BLK1::theta	;

	std::vector<int> jump_index;
	int jj=0;
	int counter=0;
	int li=0;
// 
// 	for (int iy = 0; iy<ny; ++iy){
// 		int i= iy*nx + 0;
// 		int hh = round(c.pfix[i].y*2.*sin(0.5*theta_cr*dlib::pi/180.));
// 		cout<<"round= "<<hh<<" at "<<jj<<endl;
// 
// 			if(hh	 >2*jj ){
// 		// 		cout<<"adding dislocation at: "<< counter<<endl;
// 				jump_index.push_back(i);
// 				for (int iyy = iy; iyy<ny; ++iyy)
// 				for (int ix = 0; ix<nx; ++ix){
// 					int i= iyy*nx + ix;
// 		// 			cout<<"adding dislocation"<<endl;
// 					if(c.pfix[i].y> jj){
// 		// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
// 						if(c.pfix[i].x<interface ){
// 							starting_point[i]   =  -1*(jj+1);
// 							li=i;	
// 						}	
// 			
// 		// 				if(c.pfix[i].x>=interface	){
// 		// 					starting_point(i)   =  1.0*(jj+1);
// 		// 				}
// 					}
// 				}
// 				if(jj==0){
// 						keep_slip	=c.pfix[i].y;
// 				}
// 
// 				jj++;
// 				counter++;
// 			}
// 
// 	}



	double phase = 0;// (c.pfix[jump_index[1]].y-c.pfix[jump_index[0]].y);
		cout<<phase/2<<endl;


	jj=0;
	for (int iy = 0; iy<ny; ++iy){
	
		int i= iy*nx + 0;
		int hh = round((c.pfix[i].y-phase/2.)*2.*sin(0.5*theta_cr*dlib::pi/180.));
		// cout<<"round= "<<hh<<" at "<<jj<<endl;
		// cout<<"j= "<<c.pfix[i].y<<endl;

			if(hh	 >2*jj	){
		// 		cout<<"adding dislocation at: "<< counter<<endl;
				for (int iyy = iy; iyy<ny; ++iyy)
				for (int ix = 0; ix<nx; ++ix){
					int i= iyy*nx + ix;
		// 			cout<<"adding dislocation"<<endl;
					if(c.pfix[i].y> jj){
		// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
		// 				if(c.pfix[i].x<interface ){
		// 					starting_point(i)   =  -1.0*(jj+1);
		// 					li=i;	
		// 				}	
				
						if(c.pfix[i].x>=interface	){
							starting_point[i]   = 1*(jj+1);
						}
					}
				}

				if(jj==0){
						keep_slip2	=c.pfix[i].y;
				}
				jj++;
				counter++;
			}

	}


// 	cout<<"keep_slip= "<<keep_slip2<<endl;
// 	double yy = c.p[(ny-1)*nx+interface-1].y;
// // 		double xx = (interface-1.)-(c.pfix[(ny-1)*nx+interface-1].x + starting_point((ny-1)*nx+interface-1) );
// 	double xx =-starting_point[(ny-1)*nx+interface-1]+0.25;
// 
// 	anglee = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
// 	cout<<"yy= "<<yy<<endl;
// 	cout<<"xx= "<<xx<<endl;
// 	cout<<"li= "<<li<<endl;
// 
// 	cout<<"angle= "<<anglee<<endl;



	double yy = c.p[(ny-1)*nx+interface].y;
	double xx =-starting_point[(ny-1)*nx+interface]-0.25;

	anglee2 = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
	cout<<"yy= "<<yy<<endl;
	cout<<"xx= "<<xx<<endl;
	cout<<"li= "<<li<<endl;

	cout<<"angle2= "<<anglee2<<endl;




	for (int i = 0; i<n; ++i){
			c.disp[i].x =starting_point[i] ;
			c.disp[i].y =starting_point[i+n];   	
	}  	
		
	for(int i=0;i<n;i++){
		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ;
	} 

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c, -4);
// 	anglee = theta_cr/2.;
//     anglee2 = anglee2;
	for(int i=0;i<n;i++){
		int iy = i / nx;
		int ix = i % nx;
		double x;
		double y;

// 		if(c.pfix[i].x<interface){
// 			auto real_coordinates = get_XY_for_rotated_square1(c.p[i].x, c.p[i].y, -(anglee)*dlib::pi/180.,interface,keep_slip);
// 			x = real_coordinates.first;
// 			y = real_coordinates.second;
// 			c.p[i].x  = x ; 
// 			c.p[i].y  = y ; 
// 		}
 
		if(c.pfix[i].x>= interface){
			auto real_coordinates = get_XY_for_rotated_square2(c.p[i].x, c.p[i].y, -(anglee2)*dlib::pi/180.,interface,keep_slip2);
			x = real_coordinates.first;
			y = real_coordinates.second;
			c.p[i].x  = x ; 
			c.p[i].y  = y ; 
		}
		
		
		
	} 
	
	   cout<<"writing"<<endl;
	   write_to_a_ovito_file(c, -3);
	   
	   struct punto_stru p = c.p[(ny/2)*nx+(nx/2)+5];
	   struct de_stru d;
	   d.p[0] = p;
	   d.p[1] = c.p[p.nn[0]];
	   d.p[2] = c.p[p.nn[0+1]];
	   struct base_stru vv1;
	   	vv1.e1[0]= 0 ;
		vv1.e1[1]= 1 ;
		vv1.e2[0]= (d.p[2].x-d.p[0].x) ;
		vv1.e2[1]= (d.p[2].y-d.p[0].y) ;
		
	struct cella_stru cc;

	cc.c11 = sqrt(vv1.e1[0] * vv1.e1[0] + vv1.e1[1] * vv1.e1[1]);
	cc.c22 = sqrt(vv1.e2[0] * vv1.e2[0] + vv1.e2[1] * vv1.e2[1]);
	cc.c12 = vv1.e1[0] * vv1.e2[0] + vv1.e1[1] * vv1.e2[1];
		
	double costheta= (cc.c12)/(cc.c11*cc.c22);
	 cout<<"angle rotation= "<<acos(costheta)*180/dlib::pi<<endl;;	


	double distance = +100;
	double distance_max = -100;

	double vx,vx2;
	double vy,vy2;
	int tiy=ny/2-1;
	vx =c.p[tiy*nx+interface+5].x -c.p[tiy*nx+interface+4].x;
	vy =c.p[tiy*nx+interface+5].y -c.p[tiy*nx+interface+4].y;


	vx2 =c.p[tiy*nx+interface-5].x -c.p[tiy*nx+interface-6].x;
	vy2 =c.p[tiy*nx+interface-5].y -c.p[tiy*nx+interface-6].y;

	


	for(int iy=0;iy<ny;iy++){
		 double temp_distance = sqrt(pow(c.p[iy*nx+interface].x -c.p[iy*nx+interface-1].x,2.) +
							   pow(c.p[iy*nx+interface].y -c.p[iy*nx+interface-1].y,2.) );



// 		cout<<"distance= "<<temp_distance<<endl;
// 		cout<<"vx= "<<vx<<endl;
// 		cout<<"vy= "<<vy<<endl;
		
		if(temp_distance < distance)
			distance= temp_distance;
		if(temp_distance > distance_max)
			distance_max= temp_distance;

	}

			cout<<"final distance min= "<<distance<<endl;
			cout<<"final distance max= "<<distance_max<<endl;
	
	if(BLK1::theta > 4.){

		for(int i=0;i<n;i++){

			if(c.pfix[i].x>= interface && distance_max > 1.8){
				c.p[i].x -= vx*(.12);
				c.p[i].y -= vy*(.12);
			}	

			if(c.pfix[i].x < interface && distance_max > 1.8){
				c.p[i].x += vx2*(.12);
				c.p[i].y += vy2*(.12);
			}	



			if(c.pfix[i].x>= interface && distance < 0.6){
				c.p[i].x += vx2*(1-distance)*0.5;
				c.p[i].y += vy2*(1-distance)*0.5;
			}	
	
			if(c.pfix[i].x< interface && distance < 0.6){
				c.p[i].x -= vx2*(1-distance)*0.5;
				c.p[i].y -= vy2*(1-distance)*0.5;
			}	
		} 
	}


	for(int i=0;i<n;i++){
// 		c.pfix[i].x  = c.p[i].x  ; 
// 		c.pfix[i].y  = c.p[i].y  ; 	
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
	}




	 
	for(int i=0;i<n;i++)
	{
		starting_point[i]     = c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		starting_point[i+n]  =  c.disp[i].y = c.p[i].y -c.pfix[i].y ;   

	
	} 
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c,-2);
	string answer = "l";
	ps_file(c,-2, answer);




}
void statique_alglib(struct conf_stru& c)
{
   
   
   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();
	double load=BLK1::load;
	double initial_strain  =BLK1::load;
	double temperature = BLK1::beta_Var;
	int k1,k2;
	k1=0;
	
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;


	int t=0;
	static int n_direction = 2;
 	if(BLK1::strain_rate <0)
 		n_direction = 3;
		
	string neigbor_list  =  "neigbor_list.txt";

	ofstream file_neigbor_list;
	file_neigbor_list.open(neigbor_list.c_str());
	for(int i=0;i<force_energy::calculation::getInstance().c.p.size();++i){
		file_neigbor_list << std::scientific << std::setprecision(16)<<i<<" "<<
		force_energy::calculation::getInstance().c.p[i].nn[0]<<" "<<
		force_energy::calculation::getInstance().c.p[i].nn[1]<<" "<<
		force_energy::calculation::getInstance().c.p[i].nn[2]<<" "<<
		force_energy::calculation::getInstance().c.p[i].nn[3]<<endl;
	
	}

	file_neigbor_list.close();
	
	const char* filename = "roule";

	ofstream myfile;
	myfile.open ("roule");
	
	//SHEARING ANGLE
 	double theta=BLK1::theta;

	int temp_counter=0;
	//for(int k=0;k<2;k=k+c.inc)
	for(int k=0;k<dimensions::triangle_limit;k=k+c.inc)
	for(int i=0;i<n;i++)
	{
		if(c.bc_cond[i][k] == 2)
			temp_counter++;
	}  
	
	c.total_triangular_number = temp_counter;
	cout<<"c.total_triangular_number= "<<c.total_triangular_number<<endl;
	write_to_a_ovito_file(c, 999);


   //IF FRAME DEFORMATION ACTIVATED, C_PFIX STARTS AS A DEFORMED CONFIGURATION
		boundary_conditions set0,setold;
		set0.setParameters(theta,load);

	if(BLK1::bc_mode.compare("frame") ==0)
		set0.def_frame(force_energy::calculation::getInstance().c,set0);
	else
		set0.macro_shear();
	setold=set0;
   //These are displacements needed in CG

	for(int i=0;i<n;i++)
	{
		c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		c.disp[i].y  = c.p[i].y -c.pfix[i].y ;  
// 		cout<<"u: "<<c.disp[i].x<<endl;
// 		cout<<"v: "<<c.disp[i].y<<endl;

	}  
	
	def_grad_fix(c);

	////dipole v


//    std::vector<double > v3 = edge_dislocation(ny/2-1    ,  40);    
//    std::vector<double > v4 = edge_dislocation(ny/2 ,nx/2+nx/4	);


    std::vector<double > v3 = edge_dislocation(ny/2-1    ,nx/2-20);    
    std::vector<double > v4 = edge_dislocation(ny/2      ,nx-8);


	//dipole v
//  
     std::vector<double > v1 = edge_dislocation2( 70 ,nx-16  );    
     std::vector<double > v2 = edge_dislocation2(ny-80,nx-17 );
 



//     column_vector starting_point,sp_max,sp_min,starting_point2;
// 	starting_point.set_size(2*n);
// 	starting_point2.set_size(2*n);
// 
// 	sp_max.set_size(2*n);
// 	sp_min.set_size(2*n);

  	cout<<"----initiate alglib---"<<endl;
  	alglib::setglobalthreading(alglib::parallel);

    alglib::real_1d_array starting_point,starting_point2 ;
    alglib::real_1d_array grad,sp,sp_grad;


    std::vector<double> temp_vector;
    temp_vector.resize(2*n);
 
	starting_point.setcontent(temp_vector.size(), &(temp_vector[0]));
	starting_point2.setcontent(temp_vector.size(), &(temp_vector[0]));
	grad.setcontent(temp_vector.size(), &(temp_vector[0]));

	 int mm;
	 if(BLK1::bc_x.compare("fix") ==0)
		mm=2*(nx-2)*(ny-2);
	 else if(BLK1::bc_x.compare("pbc") ==0)
		mm=2*n;

    temp_vector.resize(mm);
    sp.setcontent(temp_vector.size(), &(temp_vector[0]));
    sp_grad.setcontent(temp_vector.size(), &(temp_vector[0]));




	double func=0;
	void *ptr;



	//imprtant; it is null when eulerian; nonnul when Lagrangian
	dlib::parallel_for(1, 0,n, [&](int i){
		starting_point[i]     = c.disp[i].x  ;
		starting_point[i+n]   = c.disp[i].y  ;   	
	});

//to make the model scalar

 


	 if(BLK1::scalar.compare("no") ==0){
		dlib::parallel_for(1, 0,n, [&](int i){

				double h=1;	
// 				starting_point[i]     =  0*dimensions::dh*1.1*(1.1*v3[i] - 1.1*v4[i])	;
// 				starting_point[i+n]   = +0*1.1*(1.1*v1[i] - 1.1*v2[i]) ;   	
// 
// 				 int iy = i / nx;
// 				 int ix = i % nx;
// 
// 
// 				c.disp[i].x +=starting_point[i] ;
// 				c.disp[i].y +=starting_point[i+n]   ;   


		});  	
		
		cout<<"creating grain boundary"<<endl; 
		grain_boundary(c,starting_point);	
	
	} 
	
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
	if(BLK1::IC.compare("none") !=0 ){

		std::vector<double > u_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );    
		std::vector<double > v_dis = wiener( 0.,dimensions::dh*BLK1::d3,c.p.size()  );  
		for(int i=0;i<n;i++){
			if(check(c.bc_cond[i][k1],2)  && check(c.bc_cond[i][k2],2) ){
				  c.disp[i].x +=u_dis[i];
				  c.disp[i].y +=v_dis[i];
			}
		}
	}


	if(BLK1::IC.compare("volume") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(1., BLK1::d2,c.p.size());
	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;


		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][3] =dv4[i];


	}
	
	
	if(BLK1::IC.compare("alloying") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv4 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());

	  
		for(int k=0;k<4;k++)
			for(int i=0;i<n;i++)
				c.alloying[i][k]  = BLK1::beta_Var;


		for(int i=0;i<n;i++)
			  c.alloying[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][2] =dv3[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][3] =dv4[i];


	}	
	
	
	if(BLK1::IC.compare("precip") ==0 || BLK1::IC.compare("precipx") ==0  ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 amp=BLK1::d1;
	 std::vector<double > dv1,dv2,dv3;
	  
	  dv1 =   perturba_precipate(amp,c.p.size());
	  dv2 =   perturba_precipate(amp,c.p.size());
	  dv3 =   perturba_precipate(amp,c.p.size());

		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;
		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv3[i];
		
	}	
	
	

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());

	for(int i=0;i<n;i++)
	{
		int iy = i / nx;
		int ix = i % nx;
		
		

		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ; 
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
		starting_point2[i]   = starting_point[i];
		starting_point2[i+n] = starting_point[i+n];

	}  


	write_to_a_ovito_file(c, 99);

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

	
	string dissipation,av_sizes,av_sizes2,av_sizes3,size_vs_energy,strain_vs_energy,slip;
	string  av_sizespitteri = "int_total_slip_vs_stress_drop.dat";
	string half_stress =  "half_stress_drop.dat";
	string full_stress =  "full_stress_drop.dat";
	string piola_stress =  "well_drop.dat";
	string piola_stress_full =  "well_drop_vs_energy_drop.dat";

	dissipation="dissipation.dat" ;
	slip="alpha_vs_drop.dat" ;

	av_sizes="avalanches.dat" ;
	av_sizes2="stress_drop.dat" ;
	av_sizes3="strain_jump.dat" ;
	size_vs_energy="well_drop_vs_stress_drop.dat.dat" ;
	strain_vs_energy="alpha_vs_dissipation.dat" ;

	string slip_grad  = "total_slip.dat";

	
	fstream filestr,filestrav,filestrav2,filestrav3,filestrse;
	
	fstream peng1,peng2,peng3,peng4;
	string penge =  "peng_energy.dat";
	string pengs =  "peng_stress.dat";
	string pengde =  "peng_dstress.dat";
	string pengds =  "peng_denergy.dat";

	fstream gorbushin_avalanches;
	string gorbushin =  "gorbushin_energy_stress.dat";
	string gorbushin2 =  "gorbushin_energy_stress_onlyaval.dat";

	string fulldata =  "full_data.dat";
	string fulldata2 =  "full_data_short.dat";

	fstream fsfullstream;

	
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	std::vector <std::vector<matrix_stru> > m_field;
	m_field.resize(n);
	for (int i = 0; i < n; ++i)
		m_field[i].resize(6);
                    
                
	std::vector <std::vector<int> > well_locator,well_locator_old;
	well_locator.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator[i][k]=0;


	well_locator_old.resize(n);
	for (int i = 0; i < n; ++i)
		well_locator_old[i].resize(4);
		
	for (int i = 0; i < n; ++i)
		for (int k = 0; k <4 ; ++k)
			well_locator_old[i][k]=0;

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////


	
		for (int i = 0; i < n; ++i){
			starting_point[i]     = c.disp[i].x  ;
			starting_point[i+n]   = c.disp[i].y  ;   	
		};  
		
		double drop =0;
		double drop3 =0;

		double stress_initial=0;
		double stress_final =0;
		double strain_initial =initial_strain;
		double strain_final   =initial_strain;
		double energy_initial =0;
		double energy_final   =0;
		double dpda_final = 0;
		double slip_initial =0;
		double slip_final =0;
		double stress_final_pitteri=0;
		double stress_initial_pitteri=0;
		double total_slip =0;
		double stress_initial_full=0;
		double stress_final_full=0;
		double stress_initial_piola=0;
		double stress_final_piola=0;
		double stress_initial_piola_full=0;
		double stress_final_piola_full=0;


		c.load = load;
		c.soft = 0.0;
		boundary_conditions set1;

		

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	int memorynumber =BLK1::memory; //0 memory //-1normal
    for(int iter=0;iter<BLK1::total_iteration;++iter){	
		

// 	    if(iter>475)
// 	    	BLK1::setCg_save(1,5);
	
	if(iter> memorynumber){
		    result_memorize(c);
		
		// 	    	if(iter%1 == 0){
		// 	    		string answer = "l";
		// 	    		ps_file(c,t, answer);
		// 	    	}

		// 		if(BLK1::constraint_min.compare("yes") ==0)
		// 			apply_BC_box_alglib(c,sp_min,sp_max); 

				
		// 		double energy_initial = energy_for_avalanches(c,set0,setold);
			//recall here that is is the energy and stress of loaded but not relaxed system
			//c_field is reconstruced here
			rosen_alglib(starting_point,func,grad,ptr);
			energy_initial = func;
			stress_initial = avalanche_sizes(c);
			stress_initial_pitteri = avalanche_sizes_half(c);
			stress_initial_full = avalanche_sizes_full(c);
			stress_initial_piola = avalanche_sizes_piola(c);
			stress_initial_piola_full = avalanche_sizes_piola_full(c);
			double dpda_initial= dphidalpha(c,false);

			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(16)  << " " << load<<" "<<energy_initial/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(16)  << " " << load<<" "<<stress_final/(1.*c.total_triangular_number)<< " "<<dpda_initial<<endl ;
				peng2.close();
 			}



		
		double wall0 = get_wall_time();
		double cpu0  = get_cpu_time();


		

		double epsg =  0.;
		double epsf =  0.;
		double epsx =  0;
		alglib::ae_int_t maxits = 0;
		alglib::minlbfgsstate state;
		alglib::minlbfgsreport rep;


		if(BLK1::method.compare("statique")==0 ||  BLK1::method.compare("statique_newton")==0){
			cout<<"conjugate gradient starts"<<endl;

			
			int n_temp;
			if(BLK1::BC.compare("eulerian") == 0){
				n_temp=0;
				for(int i=0;i<n;i++){ 
 
					 int iy = i / nx;
					 int ix = i % nx;
					 if(BLK1::bc_x.compare("fix") ==0)
					 //This line should be done better but it works so far
					 //may be assign a flag to fix nodes ??? 
					 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
						continue;


					 double xx =   starting_point[i]       ;
					 double yy =   starting_point[i+n]  ; 


					 sp[n_temp]      = xx;
					 sp[n_temp+mm/2]    = yy;
					 n_temp++;
	
			 }
		 }


			if(BLK1::BC.compare("eulerian") == 0){
				minlbfgscreate(8, sp, state);
				minlbfgssetcond(state, epsg, epsf, epsx, maxits);
	// 			minlbfgssetscale(state, s)	
				alglib::minlbfgsoptimize(state, rosen_alglib_2);
				minlbfgsresults(state, sp, rep);
			}
			if(BLK1::BC.compare("eulerian") != 0){
				minlbfgscreate(8, starting_point, state);
				minlbfgssetcond(state, epsg, epsf, epsx, maxits);
	// 			minlbfgssetscale(state, s)	
				alglib::minlbfgsoptimize(state, rosen_alglib);
				minlbfgsresults(state, starting_point, rep);
			}
			

			if(BLK1::BC.compare("eulerian") == 0){
				n_temp=0;
				for(int i=0;i<n;i++){ 
 
					 int iy = i / nx;
					 int ix = i % nx;
					 if(BLK1::bc_x.compare("fix") ==0)
					 //This line should be done better but it works so far
					 //may be assign a flag to fix nodes ??? 
					 if(ix == nx - 1 || ix == 0 || iy == 0 || iy == ny-1)
						continue;


					 double xx =   sp[n_temp]       ;
					 double yy =   sp[n_temp+mm/2]  ; 


					 starting_point[i]      = xx;
					 starting_point[i+n]    = yy;
					 n_temp++;
			 }
		 }


		}
		
		double wall1 = get_wall_time();
		double cpu1  = get_cpu_time();

		cout<<"conjugate gradient ends"<<endl;




		cout << "Wall Time = " << wall1 - wall0 << endl;
		cout << "CPU Time  = " << cpu1  - cpu0  << endl;
		
//  		cout<<rosen(starting_point);
		 double sizes=0;


		 rosen_alglib(starting_point,func,grad,ptr);
		 double energy_final_tminus = energy_final;
		 energy_final = func;
		 double grad_sum=0.;
		 for(int i=0;i<n;++i)
		 	grad_sum += pow(force_energy::calculation::getInstance().c.gr[i].x,2.) + pow(force_energy::calculation::getInstance().c.gr[i].y,2);
		 	
		 cout<<"epsg ALGLIB: "<<sqrt(grad_sum)<<" termination type: "<<rep.terminationtype<<endl;
		 	

		 //it updates the  solution for measurements and keeps the old solution
		 for(int i=0;i<n;i++)
		 { 
			 double xx =  starting_point[i];
			 double yy =  starting_point[i+n]; 


			 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
			 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;

		 }

		double walleigen = get_wall_time();
		double cpu1eigen  = get_cpu_time();

		 
		if(BLK1::method.compare("statique_eigen")==0){	
		 sp_mat squezed(2*n,2*n);   
		 newton_eigen(c,squezed,starting_point,t);
		 double wall11 = get_wall_time();
		 double cpu11  = get_cpu_time();

		cout << "Wall Time eigen = " << wall11 - walleigen << endl;
		cout << "CPU Time eigen  = " << cpu11  - cpu1eigen  << endl;
		cout << "CPU Time eigen performance  = " << (cpu11  - cpu1eigen)/ ( wall11 - walleigen )  << endl;


		}
		
		else if(BLK1::method.compare("statique_eigen_ve")==0){	
		 sp_mat squezed(2*n,2*n);   
		 newton_eigen_version(c,starting_point,iter);
		 double wall11 = get_wall_time();
		 double cpu11  = get_cpu_time();

		 cout << "Wall Time eigen2 = " << wall11 - walleigen << endl;
		 cout << "CPU Time eigen2  = " << cpu11  - cpu1eigen  << endl;
		 cout << "CPU Time eigen2 performance  = " << (cpu11  - cpu1eigen)/ ( wall11 - walleigen )  << endl;

		}		
				 

		else if(BLK1::method.compare("statique_newton")==0){	

		 sp_mat squezed(2*n,2*n);   
// 		 if(sqrt(grad_sum) > pow(10,-6))
			 newton_raphson(c,squezed,starting_point);
		}		 



		 result_memorize(c);
		 measurements(c);
		 double dpda_final_tminus= dpda_final;

		 dpda_final= dphidalpha(c,true);
		 double total_slips = slip_calculations(c);
		 int total_slips_int = slip_calculations_integer(c);

		// cout <<"total_slips= "<<total_slips<<endl;
		// cout <<"total_slips_integer= "<<total_slips_int<<endl;




		 stress_final = avalanche_sizes(c);
		 stress_final_pitteri = avalanche_sizes_half(c);
		 stress_final_full = avalanche_sizes_full(c);
		 stress_final_piola = avalanche_sizes_piola(c);
		 stress_final_piola_full = avalanche_sizes_piola_full(c);



		//EULERIAN OR LAGRANGIAN
		assign_solution_alglib(c,starting_point);

		well_locator_old = well_locator;
		actual_m_matrix_calculations(c,m_field,well_locator); 
		int well_jumps=0;;
		for(int k=0;k<dimensions::triangle_limit;k=k+c.inc){
			for(int i=0;i<n;i++){
				well_jumps +=	abs(well_locator[i][k] - well_locator_old[i][k]);
	
		}
			}
			
		cout<<"well_jumps= "<<well_jumps<<endl;

		 
			if(iter>0){
				peng1.open (penge, fstream::in | fstream::out | fstream::app);
				peng1 << std::scientific << std::setprecision(16)  << " " << load<<" "<<energy_final/(1.*c.total_triangular_number)<<endl ;
				peng1.close();
				
				
				peng2.open (pengs, fstream::in | fstream::out | fstream::app);
				peng2 << std::scientific << std::setprecision(16)  << " " << load<<" "<<stress_final/(1.*c.total_triangular_number)<< " "<<dpda_final<<endl ;
				peng2.close();

				gorbushin_avalanches.open (gorbushin, fstream::in | fstream::out | fstream::app);
				gorbushin_avalanches << std::scientific << std::setprecision(16)  << 
				" " << load<<" "<<
				well_jumps<<" "<<
				energy_final<<" "<<
				dpda_final*(1.*c.total_triangular_number)<<" "<<
				(energy_final_tminus-energy_final)<<" "<<
				(dpda_final_tminus-dpda_final)*(1.*c.total_triangular_number)<<
				endl ;
				gorbushin_avalanches.close();
				
				if(energy_final_tminus-energy_final > 0){
					gorbushin_avalanches.open (gorbushin2, fstream::in | fstream::out | fstream::app);
					gorbushin_avalanches << std::scientific << std::setprecision(16)  << 
					" " << load<<" "<<
					well_jumps<<" "<<
					(energy_final_tminus-energy_final)<<" "<<
					(dpda_final_tminus-dpda_final)*(1.*c.total_triangular_number)*pow(-1.,n_direction)<<
					endl ;
					gorbushin_avalanches.close();
				}


			}

		 //drop of stress
		 drop = (stress_initial-stress_final)*pow(-1.,n_direction);
		 //drop of  half stress
		 double drop4 = (stress_initial_pitteri-stress_final_pitteri)*pow(-1.,n_direction);
		 double dropfull = (stress_initial_full-stress_final_full)*pow(-1.,n_direction);
		 double droppiola = (stress_initial_piola-stress_final_piola)*pow(-1.,n_direction);
		 double dropcontraction= (1.0*c.total_triangular_number)*(dpda_initial-dpda_final)*pow(-1.,n_direction);

		 cout<<"total_slips_int= "<<total_slips_int<<endl;
		 cout<<"drop= "<<dropcontraction<<endl;

		if(iter>1  && well_jumps>=0 ){
			//cout<< energy_initial-energy_final << endl;
			
			//energy dissipation
			filestr.open (dissipation, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(7)   << " "<<energy_initial-energy_final<<endl ;
			filestr.close();

			//total slip
			filestrav.open (av_sizes, fstream::in | fstream::out | fstream::app);
			filestrav << std::scientific << std::setprecision(7)   << " "<<total_slips<<endl ;
			filestrav.close();

			//stress drop
			filestrav2.open (av_sizes2, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<drop<<endl ;
			filestrav2.close();

			//half stress drop
			filestrav2.open (half_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<<load<<" "<<drop4<<endl ;
			filestrav2.close();

			//full stress drop
			filestrav2.open (full_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<dropfull<<endl ;
			filestrav2.close();
			//piola stress drop
			filestrav2.open (piola_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<endl ;
			filestrav2.close();

			//piola stress drop
			filestrav2.open (piola_stress_full, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<<" "<<energy_initial-energy_final<<endl ;
			filestrav2.close();



			//distance to jumps
			double drop2  = abs(load - strain_initial);
			strain_initial = load;

			filestrav3.open (av_sizes3, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(drop)<< " "<<energy_initial-energy_final <<endl ;
			filestrav3.close();

			//total_slips_int versus strain
			filestrav3.open (av_sizespitteri, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips_int)<< " "<<abs(drop) <<endl ;
			filestrav3.close();

			
			//total_slip_integer versus energy dissipation
			filestrse.open (size_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<well_jumps<< " "<<drop <<endl ;
			filestrse.close();
			
			//total_slip versus energy dissipation
			filestrse.open (strain_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<load<< " "<<energy_initial-energy_final <<endl ;
			filestrse.close();			
			
			//total_slip integer
			filestrse.open (slip, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<load<< " "<<drop<<endl ;
			filestrse.close();	
			
			fsfullstream.open (fulldata, fstream::in | fstream::out | fstream::app);
			fsfullstream << std::scientific << std::setprecision(16)   
			<< " "<<load<< 
			" "<<well_jumps<<
			" " <<drop<<
			" "<< energy_initial-energy_final << 
			" "<< drop4<<
			" "<< stress_initial/c.total_triangular_number << 
			" "<< stress_initial_pitteri/c.total_triangular_number<<
			" "<<dropfull<<
			" "<< stress_final/c.total_triangular_number<<
			" "<< stress_final_pitteri/c.total_triangular_number<<
			" "<< stress_initial_full/c.total_triangular_number<<
			" "<< stress_final_full/c.total_triangular_number<<
			" "<< energy_initial/c.total_triangular_number<<
			" "<< energy_final/c.total_triangular_number<<
			" "<< energy_final_tminus/c.total_triangular_number<<
			" "<<(energy_final_tminus-energy_final)<<
			" "<< dpda_final_tminus<<			
			" "<<(dpda_final_tminus-dpda_final)*(1.*c.total_triangular_number)<<			
			" "<< dpda_initial<<
			" "<< dpda_final<<
			" "<<dropcontraction<<
			endl ;

			fsfullstream.close();			

			fsfullstream.open (fulldata2, fstream::in | fstream::out | fstream::app);
			fsfullstream << std::scientific << std::setprecision(16)   
			<< " "<<load<< 
			" "<<well_jumps<<
			" "<<dropcontraction<<
			" "<< energy_initial-energy_final << 
			" "<< dpda_final<<
			" "<< dpda_initial<<
			" "<< energy_initial/c.total_triangular_number<<
			" "<< energy_final/c.total_triangular_number<<endl ;
			fsfullstream.close();					





		}

		
		 
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
		set1.setParameters(theta,load);
		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
// 		if(BLK1::macro_def.compare("macro_shear_rhombic") ==0)
// 			set1.uni_macro_compression();

		
		
		//keep the previous solution
		for(int i =0;i<c.p.size();++i){
			temp_coo[i].x = c.p[i].x   ;				
			temp_coo[i].y = c.p[i].y   ;
			c.pold[i].x = c.p[i].x ;
			c.pold[i].y = c.p[i].y ;

		}

// 		def_grad temp;	
// 		temp= set1.f.multiply(setold.f.inverse(setold.f));
	// 
	// 	//guess the next solution incrementally		

		int inc = force_energy::calculation::getInstance().c.inc ;	
	

			//output operations
// 			ps_file(c,t); 
		if(iter%(int) BLK1::dt == 0 || well_jumps > 50*nx/200 || iter < 10){
			//slicer(c,t);
			write_to_a_ovito_file(c,t);

			string filename2 = "all_load.dat";
			all_data_to_a_file(t,load,filename2);
			string answer = "l";
	    	ps_file(c,t, answer);

			write_to_a_file(c,t);
			//t increases here
			write_to_a_ovito_file_remove_Fmacro(c,t++,load);
			string filename = "last_load.dat";
			single_data_to_a_file(load,filename);
			if(BLK1::IC.compare("precipx") ==0 && iter==0){
				for(int k=0;k<3;k++)
					for(int i=0;i<n;i++)
			 		 c.disorder[i][k] =1;

			}

		//	do_delaunay(force_energy::calculation::getInstance().c,t,theta,load);
 		}
  	}
 		
 		//MEMORY
 		else if(iter == memorynumber){
 		
 			string filename2  =  BLK1::config ;
			
			if(exist(filename2) == false){ 
		 		cout<<"no file sorry  "<<filename2<<"\n"; 
		 		exit (EXIT_FAILURE); 
			}

			ifstream disp;

			disp.open(filename2.c_str());
			double temp;
			double load_value_from_cauchy;
			int nol;
			disp >> nol;
			cout<<"number of lines in the file: "<<filename2<<" "<<nol<<endl;
			
			for (int i = 0; i<n; ++i){
				disp >> std::scientific >> std::setprecision(16)>> c.p[i].x >> c.p[i].y >> temp >> temp >> temp>> load_value_from_cauchy ;
	// 			if(i<2 || i>nol-2)
	// 				cout << std::scientific << std::setprecision(14)<< c.p[i].x << " "<<c.p[i].y << " "<<temp << " "<< temp << " "<<temp <<endl;
				if(BLK1::BC.compare("eulerian") == 0){
					c.pfix[i].x  = c.p[i].x;
					c.pfix[i].y  = c.p[i].y; 
					c.pold[i].x  = c.p[i].x; 
					c.pold[i].y  = c.p[i].y; 		
				}
				if(BLK1::BC.compare("eulerian") != 0){
					c.pold[i].x  = c.p[i].x; 
					c.pold[i].y  = c.p[i].y; 		
// 					starting_point[i]      = + c.p[i].x - c.pfix[i].x; 
// 					starting_point[i+n]    = + c.p[i].y - c.pfix[i].y; 		

				}
			}

			disp.close();
			
			//string filename3  =  "../all_load.dat" ;
			string filename3  =  BLK1::config_u ;
			disp.open(filename3.c_str());

			cout<<"memory number: "<<BLK1::memory_number<<endl;
			for (int i = 0; i<=BLK1::memory_number; ++i)
				disp >> std::scientific >> std::setprecision(16)>> initial_strain>> temp  ;
			
			disp.close();

			
			cout<<"comparing the memory values of the initial strain between 2 methods:"<<endl;
			cout<<"method classic: "<<initial_strain<<endl;
			cout<<"method new: "<<load_value_from_cauchy<<endl;


		    c.load = load = initial_strain ;
		   	BLK1::setNum(load);
		   	cout<< std::setprecision(16)<<"initial strain memory= "<<initial_strain<<endl;
 		
   			set1.setParameters(theta,load);
   			choose_bc(set1);

        
        	apply_BC_generale(c,set1);
//  			apply_BC(c,load);

		   	actual_m_matrix_calculations(c,m_field,well_locator); 
		   	result_memorize(c);
		   	measurements(c);
		   	dphidalpha(c,true);
		   	def_grad_fix(c);
 		
 		}
		//MEMORY		



		 if(fexists(filename) == false){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
				 
 	


//		double calculated_rate = BLK1::strain_rate;
//		double calculated_rate = m_matrix_calculations(c, load,theta,m_field,well_locator,n_direction);
 		double calculated_rate = BLK1::strain_rate;
		 
	

		
			if(n_direction % 2 == 0 && load>   BLK1::loading_limit ){
				n_direction++;
		// 			goto label44 ;
			}
		// 		if(n_direction % 2 != 0 && load< - BLK1::loading_limit ){
			if(n_direction % 2 != 0 && load< - 0 ){
				printf ("Error opening file");
				exit (EXIT_FAILURE);
				n_direction++;
			}


		//		initial_strain+=calculated_rate;
		// 		calculated_rate = BLK1::strain_rate;
		// 		initial_strain+=calculated_rate*pow(-1.,n_direction);
			initial_strain+=calculated_rate;

		// 		temperature+=0.01;
			load  = initial_strain;
			c.load = load;
			//c.soft = 0.;
			BLK1::setNum(load);
		// 		BLK1::setBeta(temperature);

			set1.setParameters(theta,load);
			choose_bc(set1);

			setold=set0;
			set0=set1;

			apply_BC_generale(c,set1);
			//This makes it evertyhing wrong
		//  		apply_BC(c,load);

			struct conf_stru guess;
			struct boundary_conditions setnew;
			setnew.setParameters(theta,calculated_rate);

		// 		if(BLK1::BC.compare("eulerian") != 0)
		// 			setnew.setParameters(theta,initial_strain);


			choose_bc(setnew);

			//guess methods for initial conditions


			if(BLK1::guess_method.compare("boundary")==0){	

				if(BLK1::BC.compare("eulerian") == 0){
					for (int i=0; i<n; ++i){

						//fix boundary condition
						if(check(c.bc_cond[i][k1],0)  || check(c.bc_cond[i][k2],0) ){
							 //u_min and v_min
							starting_point[i]     =  c.pfix[i].x * setnew.f.f11 + c.pfix[i].y * setnew.f.f12 - c.pfix[i].x;
							starting_point[i+n]   =  c.pfix[i].x * setnew.f.f21 + c.pfix[i].y * setnew.f.f22 - c.pfix[i].y;
							c.p[i].x = starting_point[i]   + c.pfix[i].x;
							c.p[i].y = starting_point[i+n] + c.pfix[i].y;

						}

					 }
				}

				if(BLK1::BC.compare("eulerian") != 0){
					for (int i=0; i<n; ++i){

						//fix boundary condition
						if(check(c.bc_cond[i][k1],0)  || check(c.bc_cond[i][k2],0) ){
							 //u_min and v_min
							starting_point[i]     =  c.p[i].x * setnew.f.f11 + c.p[i].y * setnew.f.f12 - c.pfix[i].x;
							starting_point[i+n]   =  c.p[i].x * setnew.f.f21 + c.p[i].y * setnew.f.f22 - c.pfix[i].y;
							c.p[i].x = starting_point[i]   + c.pfix[i].x;
							c.p[i].y = starting_point[i+n] + c.pfix[i].y;

						}

					 }
				}			


			 }

			else if(BLK1::guess_method.compare("bulk")==0){	

				if(BLK1::BC.compare("eulerian") == 0){
					for (int i=0; i<n; ++i){
						starting_point[i]     =  c.pfix[i].x * setnew.f.f11 + c.pfix[i].y * setnew.f.f12 - c.pfix[i].x;
						starting_point[i+n]   =  c.pfix[i].x * setnew.f.f21 + c.pfix[i].y * setnew.f.f22 - c.pfix[i].y;
						c.p[i].x = starting_point[i]   + c.pfix[i].x;
						c.p[i].y = starting_point[i+n] + c.pfix[i].y;

					 }
				 }

				if(BLK1::BC.compare("eulerian") != 0){
					for (int i=0; i<n; ++i){
						starting_point[i]     =  c.p[i].x * setnew.f.f11 + c.p[i].y * setnew.f.f12 - c.pfix[i].x;
						starting_point[i+n]   =  c.p[i].x * setnew.f.f21 + c.p[i].y * setnew.f.f22 - c.pfix[i].y;
						c.p[i].x = starting_point[i]   + c.pfix[i].x;
						c.p[i].y = starting_point[i+n] + c.pfix[i].y;

					 }
				 }


			}


			else if(BLK1::guess_method.compare("none")==0){	

				if(BLK1::BC.compare("eulerian") == 0){
					for (int i=0; i<n; ++i){
						starting_point[i]     =  0.;
						starting_point[i+n]   =  0.;
						c.p[i].x = starting_point[i]   + c.pfix[i].x;
						c.p[i].y = starting_point[i+n] + c.pfix[i].y;

					 }
				 }

				if(BLK1::BC.compare("eulerian") != 0){
					for (int i=0; i<n; ++i){
						c.p[i].x = starting_point[i]   + c.pfix[i].x;
						c.p[i].y = starting_point[i+n] + c.pfix[i].y;

					 }
				 }


			}



			if(BLK1::IC.compare("wienerc") ==0){
				//0.005 for square
				std::vector<double > u_dis = wiener( 0 ,BLK1::d1,c.p.size()  );    
				std::vector<double > v_dis = wiener( 0 ,BLK1::d1,c.p.size()  );  
				for(int i=0;i<n;i++){
					if(check(c.bc_cond[i][k1],2)  && check(c.bc_cond[i][k2],2) ){
					  starting_point[i]   +=u_dis[i];
					  starting_point[i+n] +=v_dis[i];
					}
				}
			}	
		
		
		 if(fexists(filename) == false || load >= BLK1::loading_limit){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
		 	

				 
 	}

	label44: ; 
 	
}



double dphidalpha(struct conf_stru& c, bool on_off  ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
	    int n=c.p.size();
    
		int k1,k2;
		k1=0;

		if(BLK1::crystal_symmetry.compare("square") == 0)
			k2=2;
		if(BLK1::crystal_symmetry.compare("hex") == 0)
			k2=3;
			
		double load = BLK1::load;
		double sum=0;
		
	 	struct de_stru d1;
	
		for(int i=0;i<n;++i){ 
			int iy = i / nx;
			int ix = i % nx;
// 			if(!check(c.bc_cond[i][k1],2) || !check(c.bc_cond[i][k2],2))
// 				continue;
// 			   
			 d1.reduced_stress.f11 =   c.piola_one[i].f11;
			 d1.reduced_stress.f22 =   c.piola_one[i].f22;
			 d1.reduced_stress.f12 =   c.piola_one[i].f12;
			 d1.reduced_stress.f21 =   c.piola_one[i].f21;
			 double dpda  =  contraction_with_shear(d1,BLK1::theta);
			 sum+=dpda;

			 
		}
		


		sum /=  c.total_triangular_number;			
		if(on_off == true){
			fstream filestr;
			string filename;
			filename="alpha_dphi.dat" ;
			filestr.open (filename, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(16)  <<" "<<load << " "<<sum<<endl ;
			filestr.close();
  		}
  		
  		return sum;
	    



}


void measurements(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		fstream filestr;
		string filename2,filename3,filename4,filename5,filename6,filename7;
		filename2="alpha_energy.dat" ;
		string filename2_2="alpha_energy_GB.dat" ;

// 		energy.open(filename2.c_str());
    
    
// 	    filestr.open (filename2, fstream::in | fstream::out | fstream::trunc);
		//goes to the end
	    filestr.open (filename2, fstream::in | fstream::out | fstream::app);

		double load = BLK1::load;
		double sum=0;
		int n=c.p.size();
		
		for(int i=0;i<n;++i){ 
			sum+= c.energy_local[i];
		}

		sum /= c.total_triangular_number;
		filestr << std::scientific << std::setprecision(7)  <<" "<<load << " "<<sum<<endl ;
				
  		filestr.close();

  		
  		filename2="alpha_stress_C12.dat" ;
  		filename3="alpha_stress_C11.dat" ;
  		filename4="alpha_stress_C22.dat" ;

  		filename5="alpha_stress_C12cauchy.dat" ;
  		filename6="alpha_stress_C11cauchy.dat" ;
  		filename7="alpha_stress_C22cauchy.dat" ;

		sum=0;
		double sum2=0;
		double sum3=0;
		double sum4=0;
		double sum5=0;
		double sum6=0;
		
		for(int i=0;i<n;++i){ 
			int iy = i / nx;
			int ix = i % nx;
			if(!check(c.bc_cond[i][0],2))
				continue;
			sum+=  c.piola_one[i].f12;
			sum2+= c.piola_one[i].f11;
			sum3+= c.piola_one[i].f11;
			sum4+= c.cauchy[i].f12;
			sum5+= c.cauchy[i].f11;
			sum6+= c.cauchy[i].f22;

		}

		sum /=  c.total_triangular_number;			
		sum2 /= c.total_triangular_number;			
		sum3 /= c.total_triangular_number;			
		sum4 /= c.total_triangular_number;			
		sum5 /= c.total_triangular_number;			
		sum6 /= c.total_triangular_number;			

		filestr.open (filename2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<abs(load) << " "<<sum<<endl ;
  		filestr.close();
	    
	    filestr.open (filename3, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<abs(load) << " "<<sum2<<endl ;
  		filestr.close();

	    filestr.open (filename4, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<abs(load) << " "<<sum3<<endl ;
  		filestr.close();

	    filestr.open (filename5, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<load << " "<<sum4<<endl ;
  		filestr.close();
	    
	    filestr.open (filename6, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<load << " "<<sum5<<endl ;
  		filestr.close();
	    
	    filestr.open (filename7, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(15)  <<" "<<load << " "<<sum6<<endl ;
  		filestr.close();


}




double avalanche_sizes(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.cauchy[i].f12;
			//}
		}


		return sum4;


}


double def_frad_calcul(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.piola_one[i].f12;
			//}
		}

		sum4  /= c.total_triangular_number;
		return sum4;


}


double avalanche_sizes_half(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.pitteri[i].c12;
			//}
		}


		return sum4;


}


double avalanche_sizes_full(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.cauchy[i].f12;
				sum4+= c.cauchy[i].f21;
				sum4+= c.cauchy[i].f11;
				sum4+= c.cauchy[i].f22;

			//}
		}


		return sum4;


}


double avalanche_sizes_piola(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.piola_one[i].f12;
				sum4+= c.piola_one[i].f21;

			//}
		}


		return sum4;


}


double avalanche_sizes_piola_full(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= c.piola_one[i].f12;
				sum4+= c.piola_one[i].f21;
				sum4+= c.piola_one[i].f11;
				sum4+= c.piola_one[i].f22;

			//}
		}


		return sum4;


}


double slip_sizes(struct conf_stru& c ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;

		for(int i=0;i<n;++i){
 			//if(iy==ny/2-1){
				sum4+= abs(c.non_reduced[i].c12);
			//}
		}


		return sum4;

}



double update_m(struct conf_stru& c,std::vector <std::vector<matrix_stru> >& m_old ){

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		int n=c.p.size();
		


		double sum4=0;
		
		for (int k=0; k<1; k=k+c.inc)
		for(int i=0;i<n;++i){ 
			if(!check(c.bc_cond[i][k],2))
				continue;

			sum4+=  
			abs(c.m_field_full[i][k].m12-m_old[i][k].m12)+
			abs(c.m_field_full[i][k].m11-m_old[i][k].m11)+
			abs(c.m_field_full[i][k].m22-m_old[i][k].m22)+
			abs(c.m_field_full[i][k].m21-m_old[i][k].m21);
		}

		return sum4;


}


	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////save m_field before c changes///////////////////////////
	////////////*****************///////////////////////////

// 		for (int k=0; k<1; k=k+c.inc)
// 		for(int i=0;i<n;++i){ 
// 			if(!check(c.bc_cond[i][k],2))
// 				continue;
// 			
// 			m_field[i][k] = c.m_field_full[i][k];
// 			
// 		}







