
#include "boundary_conditions.h"
#include "namespaces.h"
#include "dlib.h"

struct def_grad boundary_conditions::macro_shear()
{

	string  BC = BLK1::BC;
	double perturb = 0 * dlib::pi / 180;
	// 			std::vector<double> rx,ry;
	// 			rx.resize(nx*ny);
	// 			ry.resize(nx*ny);
	// 			double av= 0;
	// 			double std=0.01;
	// 			rx = wiener(av, std);
	// 			ry = wiener(av, std);


	f.f11 = (1. - load_in*cos(theta_d + perturb)*sin(theta_d + perturb));
	f.f22 = (1. + load_in*cos(theta_d - perturb)*sin(theta_d - perturb));
	f.f12 = load_in* pow(cos(theta_d + perturb), 2.);
	f.f21 = -load_in* pow(sin(theta_d - perturb), 2.);



	return f;


}


struct def_grad boundary_conditions::macro_shear_rhombic()
{

	string  BC = BLK1::BC;
	double perturb = 0 * dlib::pi / 180;
	// 			std::vector<double> rx,ry;
	// 			rx.resize(nx*ny);
	// 			ry.resize(nx*ny);
	// 			double av= 0;
	// 			double std=0.01;
	// 			rx = wiener(av, std);
	// 			ry = wiener(av, std);
	f.f11 = cosh(load_in);
	f.f22 = cosh(load_in);
	f.f12 = sinh(load_in);
	f.f21 = sinh(load_in);
// 
// 	f.f11 =  (1./sqrt(cosh(load_in)))*cosh(load_in);
// 	f.f21  =  0;
// 	f.f12 =  (1./sqrt(cosh(load_in)))*sinh(load_in);
// 	f.f22 =  (1./sqrt(cosh(load_in)));

	return f;


}

struct def_grad boundary_conditions::macro_shear_rectangular()
{

	string  BC = BLK1::BC;
	double perturb = 0 * dlib::pi / 180;

	f.f11 = cosh(load_in/2.) - sinh(load_in/2.);
	f.f22 = cosh(load_in/2.) + sinh(load_in/2.);
	f.f12 = 0.;
	f.f21 = 0.;


	return f;


}





struct def_grad boundary_conditions::macro_compression()
{

	string  BC = BLK1::BC;



	
	f.f11 = sqrt(load_in);
// 	f.f22 = 1+load_in;
	
	f.f22 = sqrt(load_in);
	f.f12 = 0.;
	f.f21 = 0.;




	return f;

}

struct def_grad boundary_conditions::macro_rhombic_compression()
{

	string  BC = BLK1::BC;


	// 	f.f11 = 1 +0*load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
	// 	f.f22 = 1 +load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
	// 	f.f12 = 0.;
	// 	f.f21 = 0.;

	double load = 1 + load_in;

/* 
	f.f11 = 1./load;
	f.f22 = load;
	f.f12 = 0.;
	f.f21 = 0.;
	
 */
	def_grad temp,temp2;
	temp2.f11 = cosh(BLK1::initial_strain2);
	temp2.f22 = cosh(BLK1::initial_strain2);
	temp2.f12 = sinh(BLK1::initial_strain2);
	temp2.f21 = sinh(BLK1::initial_strain2);

	

	
	f.f11 = 1.;
	f.f22 = 1+load_in;
	f.f12 = 0.;
	f.f21 = 0.;
	temp= temp2.multiply(f);
	f=temp;


// 	double load_in2 = 0.05;
	// 	f.f11 = (1. -  load_in2*cos(theta_d)*sin(theta_d))			;
	// 	f.f22 = (1. +  load_in2*cos(theta_d)*sin(theta_d))  +load_in ;
	// 	f.f12 =   load_in2* pow(cos(theta_d),2.);
	// 	f.f21 =  -load_in2* pow(sin(theta_d),2.);

	return f;

}


struct def_grad boundary_conditions::macro_multipled_loading()
{

	string  BC = BLK1::BC;


   	boundary_conditions set1;
   	//0.19.
   	set1.setParameters(BLK1::theta_init,BLK1::initial_strain2);
   	set1.macro_shear();
	

	
	def_grad temp,temp2;
	temp2 = set1.f;
// 	temp2.f11 = (1. - load_in*cos(theta_d )*sin(theta_d ));
// 	temp2.f22 = (1. + load_in*cos(theta_d )*sin(theta_d));
// 	temp2.f12 =  load_in* pow(cos(theta_d ), 2.);
// 	temp2.f21 =  -load_in* pow(sin(theta_d ), 2.);
// 	cout<<"---------"<<endl;
// 
// 	cout<<"load_in= "<<load_in<<endl;
// 
// 	cout<<"load_in2= "<<set1.load_in<<endl;
// 
// 
// 	cout<<"theta_d= "<<theta_d<<endl;
// 
// 	cout<<"theta_d2= "<<set1.theta_d<<endl;
// 
// 	cout<<"---------"<<endl;


	f.f11 = (1. - load_in*cos(theta_d )*sin(theta_d  ));
	f.f22 = (1. + load_in*cos(theta_d  )*sin(theta_d  ));
	f.f12 = load_in* pow(cos(theta_d  ), 2.);
	f.f21 = -load_in* pow(sin(theta_d  ), 2.);
	
	temp= temp2.multiply(f);
	f=temp;


// 	double load_in2 = 0.05;
	// 	f.f11 = (1. -  load_in2*cos(theta_d)*sin(theta_d))			;
	// 	f.f22 = (1. +  load_in2*cos(theta_d)*sin(theta_d))  +load_in ;
	// 	f.f12 =   load_in2* pow(cos(theta_d),2.);
	// 	f.f21 =  -load_in2* pow(sin(theta_d),2.);

	return f;

}

struct def_grad boundary_conditions::uni_macro_compression()
{

	string  BC = BLK1::BC;


	// 	f.f11 = 1 +0*load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
	// 	f.f22 = 1 +load_in * ( pow(cos(theta_d),2.) + pow(sin(theta_d),2.) ) ;
	// 	f.f12 = 0.;
	// 	f.f21 = 0.;

	double load =  load_in;


	f.f11 = 1./load;
	f.f22 = load;
	f.f12 = 0.;
	f.f21 = 0.;
	
 

// 	double load_in2 = 0.05;
	// 	f.f11 = (1. -  load_in2*cos(theta_d)*sin(theta_d))			;
	// 	f.f22 = (1. +  load_in2*cos(theta_d)*sin(theta_d))  +load_in ;
	// 	f.f12 =   load_in2* pow(cos(theta_d),2.);
	// 	f.f21 =  -load_in2* pow(sin(theta_d),2.);

	return f;

}

void  boundary_conditions::setParameters(double value1, double value2)
{
	theta_d = value1*dlib::pi / 180.0;
	load_in = value2;

	// 	return f;

}




//it deforms c.pfix

void boundary_conditions::def_frame(struct conf_stru& c, struct boundary_conditions& f)

{
	double dny, diy, dnx;
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double theta_d = f.theta_d;
	double load_in = f.load_in;




	//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
	// 
	//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;

	//    	struct def_grad f = combiner(load_in,theta_d,macro_shear);

	double k, m;
	//----------------------periodic boundaries------------------------------------



	//----------------------deform the reference state------------------------------------



	for (int i = 0; i<c.p.size(); ++i)
	{
		// 				int iy = i / nx;
		// 				int ix = i % nx;
		// 				c.pfix[i].x = ((double) ix-c.dnx) * f.f.f11 + ((double) iy-c.dny) * f.f.f12;
		// 				
		// 				c.pfix[i].y = ((double) ix-c.dnx) * f.f.f21 + ((double) iy-c.dny) * f.f.f22;

		int iy = i / nx;
		int ix = i % nx;
		c.pfix[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;

		c.pfix[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;


	}
}	
void  boundary_conditions::triangle_number(int k)
{

	BLK1::setTriangle(k);


}	
	

