#include "namespaces.h"
#include <cmath>
string reading::filename;
string reading::filename2;
//when namespaces are replaced by static  fields in a class
//each of them has to be declared in a separate cpp file

int dimensions::nx;
int dimensions::ny;

int dimensions::sub_nx;
int dimensions::sub_ny;


int dimensions::triangle_limit;
double dimensions::dh;


void dimensions::setNum(int value1, int value2)
{
	nx = value1;
	ny = value2;
}

void dimensions::setNumsub(int value1, int value2)
{
	sub_nx = value1;
	sub_ny = value2;
}


void dimensions::setNumparam(int value1, double value2)
{
	triangle_limit = value1;
	dh = value2;
}

    double atomistic::cut_off;
    double atomistic::r_eq;

	void atomistic::setNum(double value1,double value2)
	{
		cut_off = value1;
		r_eq    = value2;
	}
	string  BLK1::model;
	double  BLK1::beta_Var;
	double  BLK1::theta;
	double  BLK1::theta_init;

	//number of parallel threads
	int BLK1::num_threads;
	
	//initial strain 
	//    double load=0.01 *ny;
	double  BLK1::dt;
	
	//statique or dynamic or analysis
	string  BLK1::method;
	
	//pbc or free
	string  BLK1::BC;

	//wiener, uniform, defect or none
	string  BLK1::IC;
	
	double  BLK1::load;
	double  BLK1::initial_strain;
	double  BLK1::initial_strain2;

	double  BLK1::strain_rate;
	double  BLK1::total_iteration;

	string  BLK1::macro_def;
	string  BLK1::crystal_symmetry;
	double  BLK1::symmetry_constantx; 
	double  BLK1::symmetry_constanty;

	double  BLK1::loading_limit;
	string  BLK1::bc_x;
	string  BLK1::bc_y;
	string  BLK1::bc_mode;
	string  BLK1::first;
	int  BLK1::memory;
	int  BLK1::memory_number;
	string  BLK1::config;
	string  BLK1::config_u;
	int     BLK1::triangle;
	string  BLK1::grid_creator;
	int     BLK1::cg_save;
	int     BLK1::cg_freq;

	string  BLK1::scalar;

	double  BLK1::cutting_angle;

	string  BLK1::constraint_min;

	double  BLK1::scale;

	double  BLK1::precision;
	double  BLK1::d1;
	double  BLK1::d2;
	double  BLK1::d3;

	string  BLK1::guess_method;
	int     BLK1::req;


	void BLK1::setNum(double value)
	{
		load = value;
	}

	void BLK1::setMethod(string value)
	{
		model = value;
	}


	void BLK1::setThread(int value)
	{
		num_threads = value;
	}

	void BLK1::setBeta(double value1)
	{
		beta_Var = value1;
	}

	void BLK1::setTheta(double value1,double value2)
	{
		theta = value1;
		theta_init = value2;

	}

	void BLK1::setTimestep(double value)
	{
		dt = value;
	}

	void BLK1::setAnalysis(string value)
	{
		method = value;
	}

	void BLK1::setBCandIC(string value1, string value2)
	{
		BC = value1;
		IC = value2;
	}

	void BLK1::setISandSR(double value1, double value2,double value3)
	{
		initial_strain = value1;
		strain_rate = value2;
		initial_strain2 = value3;

	}

	void BLK1::setITER(int value)
	{
		total_iteration = value;

	}

	void BLK1::setMacro_def(string value)
	{
		macro_def = value;

	}

	void BLK1::setCrystal_symmetry(string value)
	{
		crystal_symmetry = value;
		if (BLK1::crystal_symmetry.compare("hex") == 0)
		{
			symmetry_constantx = pow(4. / 3., 1. / 4.);
			symmetry_constanty = pow(4. / 3., 1. / 4.)*sqrt(3.) / 2.;
		}
		else if (BLK1::crystal_symmetry.compare("square") == 0)
		{
			symmetry_constantx = 1.;
			symmetry_constanty = 1.;
		}

		else if (BLK1::crystal_symmetry.compare("tilted") == 0)
		{
			symmetry_constantx = sqrt(2.);
			symmetry_constanty = sqrt(2.) / 2.;
		}


	}

	void BLK1::setloading_limit(double value)
	{
		loading_limit = value;

	}

	void BLK1::setBCnew(string value1, string value2, string value3)
	{
		bc_x = value1;
		bc_y = value2;
		bc_mode = value3;

	}


	void BLK1::setPoly(string value1)
	{
		first = value1;

	}

	void BLK1::setMemory(int value1, int value2, string value3, string value4)
	{
		memory = value1;
		memory_number = value2;
		config = value3;
		config_u = value4;

	}



	void BLK1::setTriangle(int value1)
	{
		triangle = value1;

	}


	void BLK1::setGrid_creator(string value1)
	{
		grid_creator = value1;

	}

	void BLK1::setCg_save(int value1,int value2)
	{
		cg_save = value1;
		cg_freq = value2;

	}

	void BLK1::setScalar(string value1)
	{
		scalar = value1;

	}

	void BLK1::setCutting_angle(double value1)
	{
		cutting_angle = value1;

	}

	void BLK1::setConstraint_min(string value1)
	{
		constraint_min = value1;

	}


	void BLK1::setScale(double value1)
	{
		scale = value1;

	}


	void BLK1::setPrecision(double value1)
	{
		precision = value1;

	}


	void BLK1::setDisorder(double value1,double value2,double value3)
	{
		d1 = value1;
		d2 = value2;
		d3 = value3;

	}

	void BLK1::setGuess_method(string value1)
	{
		guess_method = value1;
	}


	void BLK1::setEigenvalues(int value1)
	{
		req = value1;
	}

