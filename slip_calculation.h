#pragma once

#include "structures.h"
#include "struct_functions.h"
#include "namespaces.h"
#include "string_to_integer.h"
#include "stress_zanzotto.h"
#include "calculations.h"
double slip_calculations(struct conf_stru& c);
int slip_calculations_integer(struct conf_stru& c);
void actual_m_matrix_calculations(struct conf_stru& c, std::vector <std::vector<matrix_stru> >& m,std::vector <std::vector<int> >& well_locator );
double m_matrix_calculations(struct conf_stru& c, double load, double theta, 
std::vector <std::vector<matrix_stru> >&  m_field,std::vector <std::vector<int> >& well_locator, int n_direction );

