#include "plot.h"
#include <algorithm>    // std::min
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include "namespaces.h"
#include "vector_functions.h"
using std::stringstream;
using std::ofstream;
using std::vector;
using std::cout;
using std::endl;
using std::setw;
using std::setprecision;
string IntToStr(int n)
{
	stringstream result;
	result << 10000 + n;
	return result.str();
}


string DoubleToStr(double x)
{
	stringstream result;
	result << x;
	return result.str();
}


void ps_file2(struct conf_stru& c, int t)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;

	ofstream metrics;
	string filename;

	filename = "dir_images/config_" + IntToStr(t) + ".ps";

	cout << filename << "  \n";
	metrics.open(filename.c_str());


	double fact_multiplicatif = 5 * 72;

	double x_min = c.p[0].x;
	double y_min = c.p[0].y;

	double x_max = c.p[c.p.size() - 1].x;
	double y_max = c.p[c.p.size() - 1].y;

	//  double range = max( x_max - x_min, y_max - y_min )

	double range = x_max - x_min;
	//     cout<<"range of scaling= "<<range<<endl;

	std::vector<double> x_trace, y_trace, ener;
	x_trace.resize(c.p.size());
	y_trace.resize(c.p.size());
	ener.resize(c.p.size());


	for (int i = 0; i<c.p.size(); i++)
		ener[i] = c.reduced[i].c12;

	double maxval_ener = maxval(ener);
	double minval_ener = minval(ener);

	if (maxval_ener == 0)
		maxval_ener = 1;

	for (int i = 0; i<c.p.size(); i++)
	{
		x_trace[i] = (c.p[i].x - x_min) / range;
		y_trace[i] = (c.p[i].y - y_min) / range;
		ener[i] = (c.reduced[i].c12 - minval_ener) / maxval_ener;

		// 		x_trace[i] = ( c[i].x  ) / range;
		// 		y_trace[i] = ( c[i].y  ) / range;

	}

	double linewidth = 0.2; //  ! epaisseur des liens (en inch)
	double rayon = 0.003; //  ! rayon des cercles (en inch)
	double dec_x = 0.05;
	double dec_y = 0.05;

	// !------- creation du fichiers Postscript ---------------------


	double bx_min = 0;
	double by_min = 0;
	double bx_max = 550;
	double by_max = bx_max;

	// 	write(50,2000)
	// 	write(50,2010) bx_min, by_min, bx_max,by_max
	// 
	// 	2000 format('%!PS-Adobe-2.0 EPSF-1.2')
	// 	2010 format('%%BoundingBox: ',4(1x,i4))
	metrics << "%!PS-Adobe-2.0 EPSF-1.2" << endl;
	metrics << "%%BoundingBox: " << setw(4) << bx_min << setw(4) << by_min << setw(4) << bx_max << setw(4) << by_max << endl;
	metrics << "/Times-Roman findfont   % Get the basic font" << endl;
	metrics << "20 scalefont            % Scale the font to 20 points" << endl;
	metrics << "setfont" << endl;
	metrics << "/inch { " << fact_multiplicatif << " mul} def  % Convert inches->points (1/72 inch)" << endl;
	metrics << linewidth << "  setlinewidth " << endl;

	// 	write(50,*)'/Times-Roman findfont   % Get the basic font'
	// 	write(50,*)'20 scalefont            % Scale the font to 20 points'
	// 	write(50,*)'setfont'  
	// 	write(50,*)'/inch {', fact_multiplicatif ,' mul} def      % Convert inches->points (1/72 inch)'
	// 	write(50,*) linewidth,'  setlinewidth '




	// !########### creation des cercles (en bleu) ##############################

	double red = 1;
	double green = 0;
	double blue = 0;
	metrics << "newpath" << endl;
	//defines the color
	metrics << std::scientific << std::setprecision(2) << " " << red << " " << green << " " << blue << " setrgbcolor" << endl;

	//lines
	// 	c.p[i].nn[0] = (iy)*nx       + ix + 1; //EAST
	// 	c.p[i].nn[1] = (iy+1)*nx     + ix    ; //NORTH    
	// 	c.p[i].nn[2] = (iy)*nx       + ix -1 ; //WEST
	// 	c.p[i].nn[3] = (iy-1)*nx     + ix    ; //SOUTH     


	int iy, ix;
	for (int n = 0; n<c.p.size(); n++)
	{
		int iy = n / nx;
		int ix = n % nx;
		if (ix == 0 || ix == nx - 1 || iy == 0 || iy == ny - 1)
			continue;

		int east = (iy)*nx + ix + 1; //EAST
		int north = (iy + 1)*nx + ix; //NORTH    
		int west = (iy)*nx + ix - 1; //WEST
		int south = (iy - 1)*nx + ix; //SOUTH     
		int north_east = (iy + 1)*nx + ix + 1; //NORTH    EAST

		metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[n] << " inch " << dec_y + y_trace[n] << "  inch moveto" << endl;
		metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[east] << " inch " << dec_y + y_trace[east] << "  inch lineto" << endl;
		metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[north_east] << " inch " << dec_y + y_trace[north_east] << "  inch moveto" << endl;
		metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[north] << " inch " << dec_y + y_trace[north] << "  inch lineto" << endl;
		// 		 metrics<<std::scientific << std::setprecision(7)<<" "<<ener[n]<<"  setgray"<<endl;
		red = 1 - ener[n];
		green = 1 - ener[n];
		blue = 1;
		metrics << std::scientific << std::setprecision(2) << " " << red << " " << green << " " << blue << " setrgbcolor" << endl;

		metrics << "fill" << endl;


	}
	metrics << "stroke " << endl;

	//circles           
	red = 1;
	green = 0;
	blue = 0;

	metrics << std::scientific << std::setprecision(2) << " " << red << " " << green << " " << blue << " setrgbcolor" << endl;

	for (int n = 0; n<c.p.size(); n++)
	{
		int iy = n / nx;
		int ix = n % nx;
		if (ix == 0 || ix == nx - 1 || iy == 0 || iy == ny - 1)
		{
			metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[n] << " inch " << dec_y + y_trace[n] << "  inch moveto" << endl;
			metrics << std::scientific << std::setprecision(7) << " " << dec_x + x_trace[n] << " inch " << dec_y + y_trace[n] << " inch " << rayon << "  inch  0 360 arc" << endl;
			metrics << "fill" << endl;
		}

	}
	//     metrics <<"fill "<<endl;
	metrics << "stroke " << endl;


	// !##############################################################

	// 100 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,'moveto')
	// 101 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,e14.7,'  inch  0 360 arc')
	// 110 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,'lineto')
	// 120 format(1x,f4.2,1x,f4.2,1x,f4.2,3x,'setrgbcolor')
	// 
	// 
	// 
	// close(50)
	// 

}


#include <algorithm>    // std::min




void slicer(struct conf_stru& c, int t)
{

	// !----- calcul des min et max ----------------------
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = nx*ny;


	double r_min = -0.5;
	double r_max = 0.5;


	// !------- creation du fichiers PPM ---------------------

	ofstream stream_v;
	string filename;

	filename = "dir_slicer/config_" + IntToStr(t) + ".jpg";

	cout << filename << "  \n";
	stream_v.open(filename.c_str());

	stream_v << std::scientific << std::setprecision(7)
		<< "P3" << "\n" <<
		"# CREATOR: XV Version 3.10  Rev: 12/16/94" << "\n" <<
		"# CREATOR: XV Version 3.10  Rev: 12/16/94" << "\n" <<
		"# R_min = " << r_min << "  R_max = " << r_max << "\n" <<
		nx << " " << ny << "\n" <<
		"255" << endl;




	double inter = r_max - r_min;

	double half = inter / 2. + r_min;

	double sigma = 0.;

	double slope = 2. / (r_max - r_min);

	for (int i = 0; i<n; ++i)
	{

		int ir = 255 * (c.reduced[i].c12 - r_min) / inter;
		int ib = 255 * (r_max - c.reduced[i].c12) / inter;
		int ig = 255 * slope*std::min(c.reduced[i].c12 - r_min, r_max - c.reduced[i].c12);

		int zero = 0;
		int ikbb = 255;

		ir = std::min(ir, ikbb); ir = std::max(zero, ir);
		ig = std::min(ig, ikbb); ig = std::max(zero, ig);
		ib = std::min(ib, ikbb); ib = std::max(zero, ib);
		stream_v << ir << " " << ig << " " << ib << endl;

	}


	stream_v.close();




}