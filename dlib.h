#pragma once

#include "dlib/matrix.h"
#include "dlib/threads.h"
#include "dlib/optimization.h"
#include "dlib/graph_utils_threaded.h"
#include "dlib/threads.h"
// #include "dlib/gui_widgets.h"
// #include "dlib/image_transforms.h"

#include "dlib/optimization.h"
// #include "dlib/matrix/matrix_abstract.h"
// #include "dlib/svm/sparse_vector_abstract.h"
// #include "dlib/statistics.h"

// dlib overrides grp1 member of conf_stru on windows!!
// by #define grp1 
// so it has to be undefined
#ifdef grp1
#undef grp1
#endif 


typedef dlib::matrix<double, 0, 1> column_vector;