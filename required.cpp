#include "required.h"
   void required(){
    //assign which energy will be used
    
	if(BLK1::first.compare("poly") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &energy_zanzotto;
		force_energy::calculation::getInstance().c.fptr3 = &stress_easy;
		force_energy::calculation::getInstance().c.fptr4 = &dd_phi_detc;
		
		struct base_stru v;

		double_cella_stru temp_AT;
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);

		if(BLK1::crystal_symmetry.compare("square") == 0){
			v.e1[0]=1;
			v.e1[1]=0;
			v.e2[0]=0.;
			v.e2[1]=1.;
			metric_temp.c11=1.;
			metric_temp.c22=1.;
			metric_temp.c12=0;
		}

		if(BLK1::crystal_symmetry.compare("hex") == 0){
			v.e1[0]=gamma;
			v.e1[1]=0;
			v.e2[0]=gamma/2;
			v.e2[1]=sqrt(3.)*gamma/2;
			metric_temp  = faicella(v);
		}

		cout<<"calling coarse_grain_fast_AT"<<endl;
		force_energy::calculation::getInstance().c.zeroing =0;
		struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);			

 		temp_AT= force_energy::calculation::getInstance().c.fptr4(metric_temp, 1, BLK1::beta_Var, v);
		cout<<"energy= "<<es.energy<<endl;
		force_energy::calculation::getInstance().c.zeroing = es.energy;

		cout<<"temp_AT.c11.c11= "<<temp_AT.c11.c11<<endl;
		cout<<"temp_AT.c22.c22= "<<temp_AT.c22.c22<<endl;
		cout<<"temp_AT.c12.c12= "<<temp_AT.c12.c12<<endl;
		cout<<"temp_AT.c11.c22= "<<temp_AT.c11.c22<<endl;
		cout<<"temp_AT.c11.c12= "<<temp_AT.c11.c12<<endl;
		cout<<"temp_AT.c22.c12= "<<temp_AT.c11.c12<<endl;

		cout<<"using polynomial energy"<<endl;
	}
	else if(BLK1::first.compare("log") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &energy_log;
		force_energy::calculation::getInstance().c.fptr3 = &stress_log;
		force_energy::calculation::getInstance().c.fptr4 = &dd_phi;
		cout<<"using polynomial energy with logarithmic volume change"<<endl;
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=1.;
		metric_temp.c22=1.;
		metric_temp.c12=0;
		struct base_stru v;
		if(BLK1::crystal_symmetry.compare("square") == 0){
			v.e1[0]=1;
			v.e1[1]=0;
			v.e2[0]=0.;
			v.e2[1]=1.;
			metric_temp.c11=1.;
			metric_temp.c22=1.;
			metric_temp.c12=0;
		}

		if(BLK1::crystal_symmetry.compare("hex") == 0){
			v.e1[0]=gamma;
			v.e1[1]=0;
			v.e2[0]=gamma/2;
			v.e2[1]=sqrt(3.)*gamma/2;
			metric_temp  = faicella(v);
		}

		double_cella_stru temp_AT;
		cout<<"calling coarse_grain_fast_AT"<<endl;
		force_energy::calculation::getInstance().c.zeroing =0;
		struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);			

 		temp_AT= force_energy::calculation::getInstance().c.fptr4(metric_temp, 1, BLK1::beta_Var, v);
		cout<<"energy= "<<es.energy<<endl;
		force_energy::calculation::getInstance().c.zeroing = 0.;
		force_energy::calculation::getInstance().c.zeroing = es.energy;

		cout<<"temp_AT.c11.c11= "<<temp_AT.c11.c11<<endl;
		cout<<"temp_AT.c22.c22= "<<temp_AT.c22.c22<<endl;
		cout<<"temp_AT.c12.c12= "<<temp_AT.c12.c12<<endl;
		cout<<"temp_AT.c11.c22= "<<temp_AT.c11.c22<<endl;
		cout<<"temp_AT.c11.c12= "<<temp_AT.c11.c12<<endl;
		cout<<"temp_AT.c22.c12= "<<temp_AT.c11.c12<<endl;

	}	
	
	else if(BLK1::first.compare("lj") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &energy_lj;
		force_energy::calculation::getInstance().c.fptr3 = &stress_lj;
		force_energy::calculation::getInstance().c.fptr4 = &dd_phi_lj;
		cout<<"using polynomial energy with LJ volume change"<<endl;
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=1.;
		metric_temp.c22=1.;
		metric_temp.c12=0;
		struct base_stru v;
		v.e1[0]=1;
		v.e2[1]=0;
		v.e2[0]=0.;
		v.e1[1]=1.;
		double_cella_stru temp_AT;
		cout<<"calling coarse_grain_fast_AT"<<endl;
		force_energy::calculation::getInstance().c.zeroing =0;
		struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);			

 		temp_AT= force_energy::calculation::getInstance().c.fptr4(metric_temp, 1, BLK1::beta_Var, v);
		cout<<"energy= "<<es.energy<<endl;
		force_energy::calculation::getInstance().c.zeroing = es.energy;

		cout<<"temp_AT.c11.c11= "<<temp_AT.c11.c11<<endl;
		cout<<"temp_AT.c22.c22= "<<temp_AT.c22.c22<<endl;
		cout<<"temp_AT.c12.c12= "<<temp_AT.c12.c12<<endl;
		cout<<"temp_AT.c11.c22= "<<temp_AT.c11.c22<<endl;
		cout<<"temp_AT.c11.c12= "<<temp_AT.c11.c12<<endl;
		cout<<"temp_AT.c22.c12= "<<temp_AT.c11.c12<<endl;
	}	
	
	
	else if(BLK1::first.compare("morse") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &energy_morse;
		force_energy::calculation::getInstance().c.fptr3 = &stress_morse;
		force_energy::calculation::getInstance().c.fptr4 = &dd_phi_morse;
		cout<<"using polynomial energy with Morse volume change"<<endl;
	}			
    

	else if(BLK1::first.compare("niko") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &energy_nikolai;
		force_energy::calculation::getInstance().c.fptr3 = &stress_niko;
		force_energy::calculation::getInstance().c.fptr4 = &dd_phi_morse;
		cout<<"using Kirshoff energy with (detC-1)^2/2. volume change"<<endl;
	}			
    



	else if(BLK1::first.compare("atomistic_sq") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &coarse_grain_fast;
		//
		force_energy::calculation::getInstance().c.fptr3 = &coarse_grain_fast_stress;
		force_energy::calculation::getInstance().c.fptr4 = &coarse_grain_fast_AT;
		//
		
		force_energy::calculation::getInstance().c.fptr5 = &square_energy;
		force_energy::calculation::getInstance().c.fptr6 = &square_energy_der;
		force_energy::calculation::getInstance().c.fptr7 = &square_energy_sder;

		cout<<"using atomistic energy ->square"<<endl;

		cout<<"now writing some homogenous paths ->square"<<endl;
		//atomistic grid
		atomistic_grid_square(force_energy::calculation::getInstance().c.coo);
		struct def_grad F_temp;
		F_temp.f11=1;
		F_temp.f21=0;
		F_temp.f12=0;
		F_temp.f22=1;
		transform_to_tilde_coordinates(force_energy::calculation::getInstance().c.coo
		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
// 		transform_to_original_coordinates(force_energy::calculation::getInstance().c.coo
// 		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
	
		std::vector<double>  angles;
		
		angles.push_back(0. );
		
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=1.;
		metric_temp.c22=1.;
		metric_temp.c12=0;
		struct base_stru v;
		v.e1[0]=1;
		v.e2[1]=1;
		v.e2[0]=0.;
		v.e1[1]=0.;
		double_cella_stru temp;
		cout<<"calling coarse_grain_fast_AT"<<endl;
		//activate print to see elastic constants
		force_energy::calculation::getInstance().c.energy=0;
 		temp= coarse_grain_fast_AT(metric_temp, 1, 1, v);
        struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);	
        cout<<	"minimum of the energy: "<<es.energy<<endl;	
		force_energy::calculation::getInstance().c.zeroing = 0.;
		force_energy::calculation::getInstance().c.zeroing = es.energy;
		
	}	


	else if(BLK1::first.compare("atomistic_sq_v2") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &coarse_grain_fast;
		//
		force_energy::calculation::getInstance().c.fptr3 = &coarse_grain_fast_stress;
		force_energy::calculation::getInstance().c.fptr4 = &coarse_grain_fast_AT;
		//
		
		force_energy::calculation::getInstance().c.fptr5 = &square_energy_v2;
		force_energy::calculation::getInstance().c.fptr6 = &square_energy_der_v2;
		force_energy::calculation::getInstance().c.fptr7 = &square_energy_sder_v2;

		cout<<"using atomistic energy ->square_v2"<<endl;

		cout<<"now writing some homogenous paths ->square_v2"<<endl;
		//atomistic grid
		atomistic_grid_square(force_energy::calculation::getInstance().c.coo);
		struct def_grad F_temp;
		F_temp.f11=1;
		F_temp.f21=0;
		F_temp.f12=0;
		F_temp.f22=1;
		transform_to_tilde_coordinates(force_energy::calculation::getInstance().c.coo
		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
// 		transform_to_original_coordinates(force_energy::calculation::getInstance().c.coo
// 		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
	
		std::vector<double>  angles;
		
		angles.push_back(0. );
		
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=1.;
		metric_temp.c22=1.;
		metric_temp.c12=0;
		struct base_stru v;
		v.e1[0]=1;
		v.e2[1]=1;
		v.e2[0]=0.;
		v.e1[1]=0.;
		double_cella_stru temp;
		cout<<"calling coarse_grain_fast_AT"<<endl;
		//activate print to see elastic constants
 		temp= coarse_grain_fast_AT(metric_temp, 1, 1, v);
        struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);	
        cout<<	"minimum of the energy: "<<es.energy<<endl;	
		force_energy::calculation::getInstance().c.zeroing = 0.;
		force_energy::calculation::getInstance().c.zeroing = es.energy;
		
		
	}	

	else if(BLK1::first.compare("atomistic_lj") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &coarse_grain_fast;
		//
		force_energy::calculation::getInstance().c.fptr3 = &coarse_grain_fast_stress;
		force_energy::calculation::getInstance().c.fptr4 = &coarse_grain_fast_AT;
		//
		force_energy::calculation::getInstance().c.fptr5 = &lennard_jones_energy;
		force_energy::calculation::getInstance().c.fptr6 = &lennard_jones_energy_der;
		force_energy::calculation::getInstance().c.fptr7 = &lennard_jones_energy_sder;

// 		

		cout<<"using atomistic energy ->LJ"<<endl;
		
		cout<<"now writing some homogenous paths ->LJ"<<endl;
		
		//atomistic grid
		atomistic_grid_square(force_energy::calculation::getInstance().c.coo);
		struct def_grad F_temp;
		F_temp.f11=1;
		F_temp.f21=0;
		F_temp.f12=0;
		F_temp.f22=1;
		transform_to_tilde_coordinates(force_energy::calculation::getInstance().c.coo
		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		

		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=gamma*gamma;
		metric_temp.c22=gamma*gamma;
		metric_temp.c12=gamma*gamma/2.;
		struct base_stru v;
		v.e1[0]=gamma;
		v.e2[1]=0;
		v.e2[0]=gamma/2.;
		v.e1[1]=gamma*sqrt(3.)/2.;
		double_cella_stru temp;
		cout<<"calling coarse_grain_fast_AT"<<endl;
 		temp= coarse_grain_fast_AT(metric_temp, 1, 1, v);
        struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);	
        cout<<	"minimum of the energy: "<<es.energy<<endl;	
		force_energy::calculation::getInstance().c.zeroing = 0.;
		force_energy::calculation::getInstance().c.zeroing = es.energy;

// 	}		




}

	else if(BLK1::first.compare("atomistic_lj_v2") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &coarse_grain_fast;
		//
		force_energy::calculation::getInstance().c.fptr3 = &coarse_grain_fast_stress;
		force_energy::calculation::getInstance().c.fptr4 = &coarse_grain_fast_AT;
		//
		force_energy::calculation::getInstance().c.fptr5 = &lennard_jones_energy_v2;
		force_energy::calculation::getInstance().c.fptr6 = &lennard_jones_energy_der_v2;
		force_energy::calculation::getInstance().c.fptr7 = &lennard_jones_energy_sder_v2;

// 		

		cout<<"using atomistic energy ->LJ"<<endl;
		
		cout<<"now writing some homogenous paths ->LJ"<<endl;
		
		//atomistic grid
		atomistic_grid_square(force_energy::calculation::getInstance().c.coo);
		struct def_grad F_temp;
		F_temp.f11=1;
		F_temp.f21=0;
		F_temp.f12=0;
		F_temp.f22=1;
		transform_to_tilde_coordinates(force_energy::calculation::getInstance().c.coo
		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
// 		transform_to_original_coordinates(force_energy::calculation::getInstance().c.coo
// 		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
		std::vector<double>  angles;
		angles.push_back(0. );
// 		angles.push_back(30. );
//  		angles.push_back(45. );
//  		angles.push_back(135. );
// 		angles.push_back(26);
// 		angles.push_back(60. );
// 		angles.push_back(90. );
// 		angles.push_back(120. );
// 		angles.push_back(150. );
// 		angles.push_back(180. );
	
// 		for(int i =0; i<angles.size() ;++i)
// 		coarse_grain(force_energy::calculation::getInstance().c.tilde_coo
// 		,force_energy::calculation::getInstance().c.coo, angles[0] );
		
		cella_stru metric_temp;
		double gamma = pow(4. / 3., 1. / 4.);
		metric_temp.c11=gamma*gamma;
		metric_temp.c22=gamma*gamma;
		metric_temp.c12=gamma*gamma/2.;
		struct base_stru v;
		v.e1[0]=gamma;
		v.e2[1]=0;
		v.e2[0]=gamma/2.;
		v.e1[1]=gamma*sqrt(3.)/2.;
		double_cella_stru temp;
		cout<<"calling coarse_grain_fast_AT"<<endl;
 		temp= coarse_grain_fast_AT(metric_temp, 1, 1, v);
        struct energy_stress es = force_energy::calculation::getInstance().c.fptr2(metric_temp, 1, BLK1::beta_Var, v);	
        cout<<	"minimum of the energy: "<<es.energy<<endl;	
		force_energy::calculation::getInstance().c.zeroing = 0.;
		force_energy::calculation::getInstance().c.zeroing = es.energy;

// 	}		




}

else if(BLK1::first.compare("atomistic_eam") ==0){
		force_energy::calculation::getInstance().c.fptr2 = &coarse_grain_fast_EAM;
		//
		force_energy::calculation::getInstance().c.fptr3 = &stress_morse;
		force_energy::calculation::getInstance().c.fptr4 = &coarse_grain_fast_AT_EAM;
		//
		force_energy::calculation::getInstance().c.fptr5 = &EAM_energy;
		force_energy::calculation::getInstance().c.fptr6 = &EAM_energy_der;
		force_energy::calculation::getInstance().c.fptr7 = &EAM_energy_sder;

// 		

		cout<<"using atomistic energy ->LJ"<<endl;
		
		cout<<"now writing some homogenous paths ->LJ"<<endl;
		
		//atomistic grid
		atomistic_grid_square(force_energy::calculation::getInstance().c.coo);
		struct def_grad F_temp;
		F_temp.f11=1;
		F_temp.f21=0;
		F_temp.f12=0;
		F_temp.f22=1;
		transform_to_tilde_coordinates(force_energy::calculation::getInstance().c.coo
		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
// 		transform_to_original_coordinates(force_energy::calculation::getInstance().c.coo
// 		,force_energy::calculation::getInstance().c.tilde_coo, F_temp );		
		std::vector<double>  angles;
		angles.push_back(0. );
		angles.push_back(30. );
 		angles.push_back(45. );
 		angles.push_back(135. );
		angles.push_back(26);
		angles.push_back(60. );
		angles.push_back(90. );
		angles.push_back(120. );
		angles.push_back(150. );
		angles.push_back(180. );
	
		for(int i =0; i<angles.size() ;++i)
		coarse_grain_eam(force_energy::calculation::getInstance().c.tilde_coo
		,force_energy::calculation::getInstance().c.coo, angles[i] );
		

// 	}		




}


    if(BLK1::crystal_symmetry.compare("square") == 0 ||
        BLK1::crystal_symmetry.compare("tilted") == 0 ){

		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);

		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);
		
		force_energy::calculation::getInstance().c.j1.push_back(-1);
		force_energy::calculation::getInstance().c.j2.push_back(-1);
		
		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);

	
// 		force_energy::calculation::getInstance().c.j1.push_back(1);
// 		force_energy::calculation::getInstance().c.j2.push_back(-1);
// 	
// 		force_energy::calculation::getInstance().c.j1.push_back(-1);
// 		force_energy::calculation::getInstance().c.j2.push_back(-1);
// 	
// 	
// 		force_energy::calculation::getInstance().c.j1.push_back(-1);
// 		force_energy::calculation::getInstance().c.j2.push_back(1); 
	}


    if(BLK1::crystal_symmetry.compare("hex") == 0 ){
// 		force_energy::calculation::getInstance().c.j1.push_back(1);
// 		force_energy::calculation::getInstance().c.j2.push_back(1);
// 		//special
// 		force_energy::calculation::getInstance().c.j1.push_back(1);
// 		force_energy::calculation::getInstance().c.j2.push_back(1);
// 	
// 		force_energy::calculation::getInstance().c.j1.push_back(1);
// 		force_energy::calculation::getInstance().c.j2.push_back(-1);
// 	
// 		force_energy::calculation::getInstance().c.j1.push_back(-1);
// 		force_energy::calculation::getInstance().c.j2.push_back(-1);
// 		//special 2
// 		force_energy::calculation::getInstance().c.j1.push_back(-1);
// 		force_energy::calculation::getInstance().c.j2.push_back(-1);	
// 	
// 		force_energy::calculation::getInstance().c.j1.push_back(-1);
// 		force_energy::calculation::getInstance().c.j2.push_back(1); 

		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);
		//special
		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);
	
		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);
	
		force_energy::calculation::getInstance().c.j1.push_back(-1);
		force_energy::calculation::getInstance().c.j2.push_back(-1);
		//special 2
		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1);	
	
		force_energy::calculation::getInstance().c.j1.push_back(1);
		force_energy::calculation::getInstance().c.j2.push_back(1); 

	}
		



	}	
