
#include "structures.h"
#include "namespaces.h"
#include "vector_functions.h"
#include "init.h"
#include "periodicity.h"
#include <iostream>
#include <map>
using namespace std;


void square_grid_creator::set_values (int x, int y, double z) {
  width = x;
  height = y;
  angle =z;
}

void square_grid_creator::set_size (int x) {
  n = x;
}





void square_grid_creator::nn_list_square(struct conf_stru& temp_in){

//      int n = temp_in.p.size();// > c.p.size() ? c.p.size() : temp_in.p.size();

// 	test_knn2(temp_in ,4);

	/* neighbors*/
	for (int k = 0; k < n; ++k) {
		for (int s = 0; s < 5; s++) {
			temp_in.p[k].nn[s] = 0;
			temp_in.p[k].is_boundary = 0; //it is not a boundary
		}
		int ii = temp_in.p[k].i;
		int jj = temp_in.p[k].j;
		temp_in.p[k].nn[0] = temp_in.get_punto(ii + 1, jj);
		temp_in.p[k].nn[1] = temp_in.get_punto(ii, jj + 1);
		temp_in.p[k].nn[2] = temp_in.get_punto(ii - 1, jj);
		temp_in.p[k].nn[3] = temp_in.get_punto(ii, jj - 1);
		temp_in.p[k].nn[4] = temp_in.p[k].nn[0];
        temp_in.p[k].which_boundary = 0;
        temp_in.p[k].corner = 0;

// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].nn[0]<<" "<<temp_in.p[k].nn[1]<<" "<<temp_in.p[k].nn[2]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].nn[3]<<" "<<temp_in.p[k].nn[4]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].x<<" "<<temp_in.p[k].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[temp_in.p[k].nn[0]].x<<" "<<temp_in.p[temp_in.p[k].nn[0]].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[temp_in.p[k].nn[1]].x<<" "<<temp_in.p[temp_in.p[k].nn[1]].y<<endl;
        

// 		temp_in.p[k].which_boundary = 4-counter2;


		for (int s = 0; s < 5; s++)
			if (temp_in.p[k].nn[s] == -1) {
				temp_in.p[k].is_boundary = 1;
				continue;					
		}

		double c =2;
		double ctop =4;
		if(temp_in.p[k].is_boundary == 1){
			if(abs(width-temp_in.p[k].x) < c)        
					temp_in.p[k].which_boundary = 1; //right
			if(abs(temp_in.p[k].x) < c)  			
					temp_in.p[k].which_boundary = 2; //left
			if(abs(height-temp_in.p[k].y) < c)		
					temp_in.p[k].which_boundary = 3; //top
			if(abs(temp_in.p[k].y) < c) 				
					temp_in.p[k].which_boundary = 4; //bottom
			
// 			if(abs(width-temp_in.p[k].x) < c && abs(height-temp_in.p[k].y) < c ) 				
// 					temp_in.p[k].corner = 1; //right top
// 
// 			if(abs(width-temp_in.p[k].x) < c && abs(temp_in.p[k].y) < c) 				
// 					temp_in.p[k].corner = 2; //right bottum
// 
// 			if(abs(temp_in.p[k].x)  < c && abs(height-temp_in.p[k].y) < c ) 				
// 					temp_in.p[k].corner = 3; //left top
// 
// 			if(abs(temp_in.p[k].x) < c && abs(temp_in.p[k].y) < c) 				
// 					temp_in.p[k].corner = 4; //left bottum							
					
		}
		
		//special treatment for upper boundary
// 		if(temp_in.p[k].is_boundary == 1 || temp_in.p[k].is_boundary == 0 ){
// 			if(abs(height-temp_in.p[k].y) < ctop)		
// 					temp_in.p[k].which_boundary = 3; //top				
// 		}		
// 		//special treatment for lower boundary
// 		if(temp_in.p[k].is_boundary == 1 || temp_in.p[k].is_boundary == 0 ){
// 			if(abs(temp_in.p[k].y) < ctop)		
// 					temp_in.p[k].which_boundary = 4; //top				
// 		}			
		
		
	}
     //periodicity efforts
      
	  cout<<"ax calculation starts for a system size "<<n<<endl;

       for (int k = 0; k < n; ++k) {
          	temp_in.p[k].pu[0]  = -1;
          	temp_in.p[k].pu[1]  = -1;
          	temp_in.p[k].pu[2]  = -1;
          	temp_in.p[k].pu[3]  = -1;
        }

	
	// periodicity is  working for 0 degree rotations,use the classic method
// 	periodicity_0_degree_square(temp_in);
		
}

void square_grid_creator::nn_list_hex(struct conf_stru& temp_in){

//      int n = temp_in.p.size();// > c.p.size() ? c.p.size() : temp_in.p.size();

// 	test_knn2(temp_in ,4);
	/* neighbors*/
	for (int k = 0; k < n; ++k) {
		for (int s = 0; s < 7; s++) {
			temp_in.p[k].nn[s] = 0;
			temp_in.p[k].is_boundary = 0; //it is not a boundary
		}
		int ii = temp_in.p[k].i;
		int jj = temp_in.p[k].j;
// 		temp_in.p[k].nn[0] = temp_in.get_punto(ii + 1, jj);
// 		temp_in.p[k].nn[1] = temp_in.get_punto(ii, jj + 1);
// 		temp_in.p[k].nn[2] = temp_in.get_punto(ii - 1, jj);
// 		temp_in.p[k].nn[3] = temp_in.get_punto(ii, jj - 1);
// 		temp_in.p[k].nn[4] = temp_in.p[k].nn[0];
 
 
 

 
 		if (jj % 2 == 0)
		{
			temp_in.p[k].nn[0] = temp_in.get_punto(ii + 1, jj); //EAST
			temp_in.p[k].nn[1] = temp_in.get_punto(ii, jj + 1); //NORTH EAST
			temp_in.p[k].nn[2] = temp_in.get_punto(ii - 1, jj + 1); //NORTH WEST
			temp_in.p[k].nn[3] = temp_in.get_punto(ii - 1, jj); //WEST
			temp_in.p[k].nn[4] = temp_in.get_punto(ii - 1, jj - 1); //SOUTH WEST
			temp_in.p[k].nn[5] = temp_in.get_punto(ii, jj - 1); //SOUTH EAST
			temp_in.p[k].nn[6] = temp_in.p[k].nn[0];

		}
		else
		{
			temp_in.p[k].nn[0] = temp_in.get_punto(ii + 1, jj); //EAST
			temp_in.p[k].nn[1] = temp_in.get_punto(ii + 1, jj + 1); //NORTH EAST
			temp_in.p[k].nn[2] = temp_in.get_punto(ii, jj + 1); //NORTH WEST
			temp_in.p[k].nn[3] = temp_in.get_punto(ii - 1, jj); //WEST
			temp_in.p[k].nn[4] = temp_in.get_punto(ii, jj - 1); //SOUTH WEST
			temp_in.p[k].nn[5] = temp_in.get_punto(ii + 1, jj - 1); //SOUTH EAST
			temp_in.p[k].nn[6] = temp_in.p[k].nn[0];

		}

 
        temp_in.p[k].which_boundary = 0;
        temp_in.p[k].corner = 0;
    }

// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].nn[0]<<" "<<temp_in.p[k].nn[1]<<" "<<temp_in.p[k].nn[2]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].nn[3]<<" "<<temp_in.p[k].nn[4]<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[k].x<<" "<<temp_in.p[k].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[temp_in.p[k].nn[0]].x<<" "<<temp_in.p[temp_in.p[k].nn[0]].y<<endl;
// 		cout<<"s: "<<k<<" "<<"k: "<< k <<"-> "<<temp_in.p[temp_in.p[k].nn[1]].x<<" "<<temp_in.p[temp_in.p[k].nn[1]].y<<endl;
        

// 		temp_in.p[k].which_boundary = 4-counter2;
for (int k = 0; k < n; ++k) {

		for (int s = 0; s < 7; s++)
			if (temp_in.p[k].nn[s] == -1) {
				temp_in.p[k].is_boundary = 1;
				continue;					
		}

		double c =0.99;
		double dcx = 2*BLK1::symmetry_constantx;
		double dcy = 2*BLK1::symmetry_constanty;

		if(temp_in.p[k].is_boundary == 1)
		{
			if(abs(width-temp_in.p[k].x) < dcx)        
					temp_in.p[k].which_boundary = 1; //right
			if(abs(temp_in.p[k].x) < dcx)  			
					temp_in.p[k].which_boundary = 2; //left
			if(abs(height-temp_in.p[k].y) < dcy)		
					temp_in.p[k].which_boundary = 3; //top
			if(abs(temp_in.p[k].y) < dcy) 				
					temp_in.p[k].which_boundary = 4; //bottom
			
// 			if(abs(width-temp_in.p[k].x) < c && abs(height-temp_in.p[k].y) < c ) 				
// 					temp_in.p[k].corner = 1; //right top
// 
// 			if(abs(width-temp_in.p[k].x) < c && abs(temp_in.p[k].y) < c) 				
// 					temp_in.p[k].corner = 2; //right bottum
// 
// 			if(abs(temp_in.p[k].x)  < c && abs(height-temp_in.p[k].y) < c ) 				
// 					temp_in.p[k].corner = 3; //left top
// 
// 			if(abs(temp_in.p[k].x) < c && abs(temp_in.p[k].y) < c) 				
// 					temp_in.p[k].corner = 4; //left bottum							
					
		}
	}
     //periodicity efforts
      
	  cout<<"ax calculation starts for a system size "<<n<<endl;

       for (int k = 0; k < n; ++k) {
          	temp_in.p[k].pu[0]  = -1;
          	temp_in.p[k].pu[1]  = -1;
          	temp_in.p[k].pu[2]  = -1;
          	temp_in.p[k].pu[3]  = -1;
        }

	
	// periodicity is  working for 0 degree rotations,use the classic method
// 	periodicity_0_degree_square(temp_in);
		
}






void  square_grid_creator::periodicity_0_degree_square(struct conf_stru& temp_in){
    
	per_x_searcher(temp_in);

}


void  square_grid_creator::per_x_searcher(struct conf_stru& temp_in){

	int ax;
	for (int k = 0; k < n; ++k) {
			int c1= temp_in.p[k].which_boundary;
			int c2= temp_in.p[k].corner;

           if(c1==2 || (c2== 3 || c2==4)){ // find the left b.atoms
               	int ii = temp_in.p[k].i;
				int jj = temp_in.p[k].j;
				int limit = sqrt(width*width+height*height*1.0)+width;
				ax = -100;
				for(int l=ii + 1;l<=limit;++l){
					int g  = temp_in.get_punto(l, jj); // check if nn right ind exists
					int c3= temp_in.p[g].which_boundary;
					int c4= temp_in.p[g].corner;
				    if(g!=-1)
				    if(c3==1 || c4== 1 || c4==2){					
						ax= g;
						//index of periodic unit ax of unit k 
						temp_in.p[k].pu[1]  = ax;
						temp_in.p[k].pu[2]  = ax;
						temp_in.p[k].nn[2]  = ax;

						//index of periodic unit k of unit ax 

						temp_in.p[ax].pu[0] = k;
						temp_in.p[ax].pu[3] = k;
						temp_in.p[ax].nn[0] = k;
						temp_in.p[ax].nn[4] = k;
// 				    	cout<<"warning unit: "<<temp_in.p[k].i<<"  " <<temp_in.p[k].j<<" left: "<<k<<endl;
// 				    	cout<<"warning unit: "<<temp_in.p[ax].i<<"  " <<temp_in.p[ax].j<<" right: "<<ax<<endl;
// 				    	cout<<"conditions: "<<c3<<"  " <<c4<<endl;
// 
// 				    	cout<<"------"<<endl;

						//west coordinates of  unit of which we look for its pu
						// (used to calculated the translation vector)
// 						auto real_coordinates  = get_XY_for_rotated_square(width/2, height/2, angle);
						//coordinates of periodic unit
// 						auto real_coordinates2 = get_XY_for_rotated_square(width/2+1, height/2, angle);

// 						double r = sqrt(1.0*i*i + j*j);
// 						double x = r*cos(gamma);
// 						double y = r*sin(gamma);



						double x4 =  temp_in.p[ax].x;
						double y4 =  temp_in.p[ax].y;
// 						
						double x3 =  temp_in.p[k].x;
						double y3 =  temp_in.p[k].y;


						//translated coordinates
// 						temp_in.p[ax].x -= (x4-x3)+cos(angle);
// 						temp_in.p[ax].y -= (y4-y3)+sin(angle);
						
						//it will come with minus
						temp_in.p[k].tvx = (x4-x3)+cos(angle);
						temp_in.p[k].tvy = (y4-y3)+sin(angle);

						//it will come with plus

						temp_in.p[ax].tvx = (x4-x3)+cos(angle);
						temp_in.p[ax].tvy = (y4-y3)+sin(angle);

// 						cout<<"ax= "<<ax<<endl;
					}
				}

            }                
		}
}
//so far font use it
//apply bc has to be changed
void  square_grid_creator::per_y_searcher(struct conf_stru& temp_in){

	int ax;
	for (int k = 0; k < n; ++k) {
			int c1= temp_in.p[k].which_boundary;
			int c2= temp_in.p[k].corner;

           if(c1==2 || (c2== 2 || c2==4)){ // find the bottom b.atoms
               	int ii = temp_in.p[k].i;
				int jj = temp_in.p[k].j;
				int limit = sqrt(width*width+height*height*1.0)+width;
				ax = -100;
				for(int l=jj + 1;l<=limit;++l){
					int g  = temp_in.get_punto(ii, l); // check if nn right ind exists
					int c3= temp_in.p[g].which_boundary;
					int c4= temp_in.p[g].corner;
				    if(g!=-1)
				    if(c3==3 || c4== 1 || c4==3){					
						ax= g;
						//index of periodic unit ax of unit k 
						temp_in.p[k].pu[1]  = ax;
						temp_in.p[k].pu[2]  = ax;
						temp_in.p[k].nn[2]  = ax;

						//index of periodic unit k of unit ax 

						temp_in.p[ax].pu[0] = k;
						temp_in.p[ax].pu[3] = k;
						temp_in.p[ax].nn[0] = k;
						temp_in.p[ax].nn[4] = k;
// 				    	cout<<"warning unit: "<<temp_in.p[k].i<<"  " <<temp_in.p[k].j<<" left: "<<k<<endl;
// 				    	cout<<"warning unit: "<<temp_in.p[ax].i<<"  " <<temp_in.p[ax].j<<" right: "<<ax<<endl;
// 				    	cout<<"conditions: "<<c3<<"  " <<c4<<endl;
// 
// 				    	cout<<"------"<<endl;

						//west coordinates of  unit of which we look for its pu
						// (used to calculated the translation vector)
// 						auto real_coordinates  = get_XY_for_rotated_square(width/2, height/2, angle);
						//coordinates of periodic unit
// 						auto real_coordinates2 = get_XY_for_rotated_square(width/2+1, height/2, angle);

// 						double r = sqrt(1.0*i*i + j*j);
// 						double x = r*cos(gamma);
// 						double y = r*sin(gamma);



						double x4 =  temp_in.p[ax].x;
						double y4 =  temp_in.p[ax].y;
// 						
						double x3 =  temp_in.p[k].x;
						double y3 =  temp_in.p[k].y;


						//translated coordinates
// 						temp_in.p[ax].x -= (x4-x3)+cos(angle);
// 						temp_in.p[ax].y -= (y4-y3)+sin(angle);
						
						//it will come with minus
						temp_in.p[k].tvx = (x4-x3)+cos(angle);
						temp_in.p[k].tvy = (y4-y3)+sin(angle);

						//it will come with plus

						temp_in.p[ax].tvx = (x4-x3)+cos(angle);
						temp_in.p[ax].tvy = (y4-y3)+sin(angle);

// 						cout<<"ax= "<<ax<<endl;
					}
				}

            }                
		}
}

