#include "calculations.h"
#include "rotation.h"
#include "input_output.h"
#include "energy_functions.h"
#include "result_memorize.h"
#include "atomistic_grid.h"
#include "delaunay.h"
#include "assign_solution.h"
#include <cmath>
#include <vector>

using std::cout;
using std::endl;

#define ARMA_ALLOW_FAKE_GCC 
#define ARMA_DONT_USE_WRAPPER

#define ARMA_DONT_USE_HDF5


void def_grad_fix(struct conf_stru& c){

		
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int npunti = c.p.size();

	double h=dimensions::dh;

	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc){
			c.t1 = force_energy::calculation::getInstance().c.j1[k];
			c.t2 = force_energy::calculation::getInstance().c.j2[k];
			for (int i=0; i<nx*ny; i++) {
					
					int iy = i / nx;
					int ix = i % nx;
					struct de_stru dfix;
					struct punto_stru pfix = c.pfix[i];

					if(!check(c.bc_cond[i][k],2))	
						continue;


			//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
					dfix.p[0] = pfix;
					dfix.p[1] = c.pfix[pfix.nn[k]];
					dfix.p[2] = c.pfix[pfix.nn[k+1]];

					c.fptr(i,ix,iy,k,dfix,c);
					
					c.fo[i][k].m11    = c.t1*(dfix.p[1].x-dfix.p[0].x)/h ; //F_11
					c.fo[i][k].m21    = c.t1*(dfix.p[1].y-dfix.p[0].y)/h ; //F_21
					c.fo[i][k].m12    = c.t2*(dfix.p[2].x-dfix.p[0].x)/h ; //F_12
					c.fo[i][k].m22    = c.t2*(dfix.p[2].y-dfix.p[0].y)/h ; //F_22	


			}

	}
}

void calcstresses_CG_en_UL(struct conf_stru& c, const alglib::real_1d_array& sp)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	
// 	c.energy = 0.;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);
	static int loop=2;
	double force_c=1.;
	

	
	for (int i=0; i<npunti; ++i)
	{
		c.gr[i].x=c.gr[i].y=c.energy_local[i] =0.;
		
		for(int l=0;l<9;++l)
			c.grp1[i][l].x = c.grp1[i][l].y=0.;
	}

	int tt=6;
	

	
	//k -> loop on the triangles
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 

	{
// 			c.t1=c1[k];
// 			c.t2=c2[k];		
			c.t1 = force_energy::calculation::getInstance().c.j1[k];
			c.t2 = force_energy::calculation::getInstance().c.j2[k];
// 			c.t1=1.;
// 			c.t2=1.;		

		//this is the loop on triangle vertices			
// 		for (int i=0; i<nx*ny; i++) 
#pragma omp parallel
#pragma omp for
		for (int i=0; i<nx*ny; i++) {

			
			int l=2*k;
			int tr=nx*ny;
			int iy = i / nx;
			int ix = i % nx;
            struct punto_stru p = c.p[i];
            struct punto_stru pfix = c.pfix[i];

			struct de_stru d,dfix;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;

			//REAL BASIS VECTORS
			struct base_stru v,vecrp,vfix;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			double burgers  =c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			double mesh=0;
			struct cella_stru metric_pr;	
			struct matrix_stru minv,fl,fu,f;
			struct spec_for_reduction vandc;
			double scale=2;
			struct energy_stress es;
			double rot=0*dlib::pi/180;
			struct matrix_stru Q;
			double h=dimensions::dh;
			double extra=0;
			double num=1;
			double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;
			struct matrix_stru fc,fo,ftot,ftot_inverse;
			double detF=1;



			if(!check(c.bc_cond[i][k],2))	
				goto label3;


//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
// 			dfix.p[0] = pfix;
// 			dfix.p[1] = c.pfix[pfix.nn[k]];
// 			dfix.p[2] = c.pfix[pfix.nn[k+1]];
// 
//  		    c.fptr(i,ix,iy,k,dfix,c);


			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

 		    c.fptr(i,ix,iy,k,d,c);




//---------------------------------VECTORS OF CELLS------------------------------------		
//---------------------------------deformation gradient------------------------------------		

// 			v=vectorize(d);
			
		//F
		fo.m11  = c.fo[i][k].m11;// vfix.e1[0]= c.t1*(dfix.p[1].x-dfix.p[0].x)/h ; //F_11
		fo.m21  = c.fo[i][k].m21;//vfix.e1[1]= c.t1*(dfix.p[1].y-dfix.p[0].y)/h ; //F_21
		fo.m12  = c.fo[i][k].m12;//vfix.e2[0]= c.t2*(dfix.p[2].x-dfix.p[0].x)/h ; //F_12
		fo.m22  = c.fo[i][k].m22;//vfix.e2[1]= c.t2*(dfix.p[2].y-dfix.p[0].y)/h ; //F_22	




		ftot.m11 = v.e1[0] = fo.m11  + (-sp[i] + sp[p.nn[k]])*c.t1;
		ftot.m21 = v.e1[1] = fo.m21  + (-sp[i+tr] + sp[p.nn[k]+tr])*c.t1 ;
		ftot.m12 = v.e2[0] = fo.m12  + (-sp[i]    + sp[p.nn[k+1]])*c.t2 ;
		ftot.m22 = v.e2[1] = fo.m22  + (-sp[i+tr] + sp[p.nn[k+1]+tr])*c.t2;
// 		cout<<v.e1[0]<<endl;
// 		cout<<v.e1[1]<<endl;
// 		cout<<v.e2[0]<<endl;
// 		cout<<v.e2[1]<<endl;
// 		cout<<"*****"<<k<<endl;	
	
		fc=ftot.multiply(fo.inverse(fo));
		ftot_inverse = ftot.inverse(ftot);
		detF = fo.det();
// 		v.e1[0] = fc.m11 ;
// 		v.e1[1]=  fc.m21 ;
// 		v.e2[0] = fc.m12 ;
// 		v.e2[1] = fc.m22 ;
		
		
// 		ftot_inverse = ftot.inverse(ftot);
// 		cout<<v.e1[0]<<endl;
// 		cout<<v.e1[1]<<endl;
// 		cout<<v.e2[0]<<endl;
// 		cout<<v.e2[1]<<endl;
// 		cout<<"------"<<k<<endl;	
// 		cout<<fc.m11<<endl;
// 		cout<<fc.m22<<endl;
// 		cout<<fc.m12<<endl;
// 		cout<<fc.m21<<endl;
// 		cout<<"------"<<endl;	
// 		cout<<fo.det()<<endl;


// 		
//---------------------------------REAL METRICS------------------------------------		

		    metrics  = faicella(v);
 
			 
			vecr=riduci_vectors(v);

			//FIND INTEGER MATRIX			
			m2=find_integer_matrix(v,vecr);

		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);
		    

			es = c.fptr2(metric_rr,c.disorder[i][k],c.alloying[i][k],vecr);			
			energy_thread = es.energy;

			c.energy_local[i] += energy_thread ;
			d1= stress_UL(es.r_stress,v,m2,ftot_inverse);
// 			d1= stress_zanzotto(es.r_stress,v,m2,ftot_inverse);
	
			
// 			detF = 1.;
			c.gr[i].x         += d1.gr[0].x *detF;
			c.gr[i].y         += d1.gr[0].y *detF;
// 
//  	  //no need to sum the cross terms
			c.grp1[i][l].x   = d1.gr[1].x  *detF; 
			c.grp1[i][l].y   = d1.gr[1].y  *detF; 
			c.grp1[i][l+1].x = d1.gr[2].x  *detF; 
			c.grp1[i][l+1].y = d1.gr[2].y  *detF; 
			label3: ;


	 		}
	}

    //THIS ONE CANNOT BE PARALLALIZED
    	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 

    {
		 
		for (int i=0; i<npunti; ++i) 
		{ 
			int l=2*k;
			struct punto_stru p=c.p[i];
			if(!check(c.bc_cond[i][k],2))	
				goto label4;

			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
				cout<<"warning3"<<endl;

			c.gr[p.nn[k]].x   += c.grp1[i][l].x;
			c.gr[p.nn[k]].y   += c.grp1[i][l].y;
			c.gr[p.nn[k+1]].x += c.grp1[i][l+1].x;
			c.gr[p.nn[k+1]].y += c.grp1[i][l+1].y;  
			label4: ;  
    	} 
    }
    


}


void calcstresses_CG_en(struct conf_stru& c)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;

	int npunti = c.p.size();
	struct cella_stru lengths;
	double dny = ny;
	double dnx = nx;
	double dix,diy,en,sum;
	int a,b;
	double load=BLK1::load;
	int flag1 = 0;
	int flag2 = 0;
	
// 	c.energy = 0.;
	string_to_integer bset;
	string  BC  = BLK1::BC;
	string  lattice  = BLK1::crystal_symmetry;
	bset.convert(BC);
	bset.convert_lattice(lattice);
	static int loop=2;
	double force_c=1.;
	
// 	void (*fptr)( int,  int,   int,  struct de_stru& , struct conf_stru& ); //Declare a function pointer to voids with no params

// 	c.fptr = &per_BC; //fptr -> one

	
	for (int i=0; i<npunti; ++i)
	{
		c.gr[i].x=c.gr[i].y=c.energy_local[i] =0.;
		
		for(int l=0;l<9;++l)
			c.grp1[i][l].x = c.grp1[i][l].y=0.;
	}

	int tt=6;
	

	
// 	 double sum_F12 = def_frad_calcul(force_energy::calculation::getInstance().c);
// 	 c.sum_F12 = sum_F12;
	
	

// 	c.t1=1;
// 	c.t2=1;

// 	c.c1.resize(1);
// 	c.c2.resize(1);


// 	c.t1=1.;
// 	c.t2=1.;	
// try
// {
	//k -> loop on the triangles
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 
// 	for (int k=0; k<3; k=k+2) 

	{
// 			c.t1=c1[k];
// 			c.t2=c2[k];		
			c.t1 = force_energy::calculation::getInstance().c.j1[k];
			c.t2 = force_energy::calculation::getInstance().c.j2[k];
// 			c.t1=1.;
// 			c.t2=1.;		

		//this is the loop on triangle vertices			
// 		for (int i=0; i<nx*ny; i++) 
#pragma omp parallel
#pragma omp for
		for (int i=0; i<nx*ny; i++) {

			
			int l=2*k;
			int iy = i / nx;
			int ix = i % nx;
// 			struct punto_stru p=c.p[i];
            struct punto_stru p = c.p[i];
			struct de_stru d;
			double diy=iy;
			double dix=ix;
			double dny = ny;
			double dnx = nx;
// 			cout<<"i: "<<i<<endl; 

			//REAL BASIS VECTORS
			struct base_stru v,vecrp;
			//REAL METRICS
			struct cella_stru metrics;
			struct matrix_stru m,m2;
			struct cella_stru metric_rr,tempm;
			//burgers = 1 if there is no precipitate
			double burgers  =c.disorder[i][k];
			double energy_thread=0.;	
			struct de_stru d1;
			struct base_stru vecr,vecru;
			double mesh=0;
			struct cella_stru metric_pr;	
			struct matrix_stru minv,fl,fu,f;
			struct spec_for_reduction vandc;
			double scale=2;
			struct energy_stress es;
			double rot=0*dlib::pi/180;
			struct matrix_stru Q;
			double h=dimensions::dh;
			double extra=0;
			double num=1;
			double n_tri = force_energy::calculation::getInstance().c.total_triangular_number;


// 			if(i%2!=0)
// 				goto label3;

// 				if(allequal(c.bc_cond[i][k],0,1,5))	
			if(!check(c.bc_cond[i][k],2))	
				goto label3;

// 			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
// 				cout<<"warningin"<<endl;

//---------------------------------CONSTRUCT THE TRIANGLE------------------------------------				
			d.p[0] = p;
			d.p[1] = c.p[p.nn[k]];
			d.p[2] = c.p[p.nn[k+1]];

//      		per_BC_hex(ix,iy,k,d,c);
 
//  		    per_BC(ix,iy,k,d,c);
//  		    fptr(ix,iy,k,d,c); //=> one()
 		    c.fptr(i,ix,iy,k,d,c);

//             per_BC_MJ(d,i,c,k);


//---------------------------------VECTORS OF CELLS------------------------------------		
// 			v=vectorize(d);
			
		v.e1[0]= c.t1*(d.p[1].x-d.p[0].x)/h ;
		v.e1[1]= c.t1*(d.p[1].y-d.p[0].y)/h ;
		v.e2[0]= c.t2*(d.p[2].x-d.p[0].x)/h ;
		v.e2[1]= c.t2*(d.p[2].y-d.p[0].y)/h ;			
//---------------------------------REAL METRICS------------------------------------		

		metrics  = faicella(v);



// 			if(k>=0 && abs(metrics.c11*metrics.c22-metrics.c12*metrics.c12 -1) >0.001){
// 			if(k>=0 && i == npunti/2){
// 			if(k==2  && iy==ny-1 ){
// 
// // 				double distance1 = (c.p[p.nn[k]].x-c.p[i].x)*(c.p[p.nn[k]].x-c.p[i].x)+
// // 					  (c.p[p.nn[k]].y-c.p[i].y)*(c.p[p.nn[k]].y-c.p[i].y);
// // 				double distance2 = (c.p[p.nn[k+1]].x-c.p[i].x)*(c.p[p.nn[k+1]].x-c.p[i].x)+
// // 					  (c.p[p.nn[k+1]].y-c.p[i].y)*(c.p[p.nn[k+1]].y-c.p[i].y);
// 
// 				double distance1 = (c.p[p.nn[k]].x-c.p[i].x)*(c.p[p.nn[k]].x-c.p[i].x)+
// 					  (c.p[p.nn[k]].y-c.p[i].y)*(c.p[p.nn[k]].y-c.p[i].y);
// 				double distance2 = (c.p[p.nn[k+1]].x-c.p[i].x)*(c.p[p.nn[k+1]].x-c.p[i].x)+
// 					  (c.p[p.nn[k+1]].y-c.p[i].y)*(c.p[p.nn[k+1]].y-c.p[i].y);
// 				double distance3 = (c.p[p.nn[k+1]].x-c.p[i].x)*(c.p[p.nn[k]].x-c.p[i].x)+
// 					  (c.p[p.nn[k+1]].y-c.p[i].y)*(c.p[p.nn[k]].y-c.p[i].y);
// 
// 
// // 				if(round(metrics.c11*metrics.c22-metrics.c12*metrics.c12) > 1)
// 				cout<<"i: "<<i<<" k: "<<k 
// 							  <<" c11: "<<metrics.c11 
// 							  <<" c22: "<<metrics.c22 
// 							  <<" c12: "<<metrics.c12
// 							  <<" detC: "<<metrics.c11*metrics.c22-metrics.c12*metrics.c12 <<"\n"
// 							  <<" ix "<<ix
// 							  <<" iy "<<iy<<"\n"
// // 							  <<" xcoo1 "<<c.p[p.nn[k]].x 
// // 							  <<" ycoo1 "<<c.p[p.nn[k]].y
// // 							  <<" xcoo2 "<<c.p[p.nn[k+1]].x 
// // 							  <<" ycoo2 "<<c.p[p.nn[k+1]].y<<"\n"
// // 							  <<" c11: "<<distance1
// // 						  	  <<" c22: "<<distance2
// // 						  	  <<" c12: "<<distance3
// 
// 
// 	// 						  <<" v1_x "<<p.nn[k] 
// 	// 						  <<" v1_y "<<p.nn[k] 
// 	// 						  <<" v2_x "<<p.nn[k+1] 
// 	// 						  <<" v2_y "<<p.nn[k+1] 
// 							  << endl;
// 			
// 
// 			}

			 //REDUCED VECTOR VECR; NON REDUCED V
// 			 cout<<"reducing: "<<endl;
			 
			 
			vecr=riduci_vectors(v);
// 			vandc  =  riduci_full(v,metrics);

			//FIND INTEGER MATRIX			
// 			m2=find_integer_matrix(v,vandc.v);
			m2=find_integer_matrix(v,vecr);

		    //REDUCED METRIC
		    metric_rr  = faicella(vecr);
// 		    metric_rr  = riduci(metrics);
		    
		    
// 		    if( pow(ix-nx/2,2.) + pow(iy-ny/2,2.) <=100 ){
// 		    if( iy <=ny/2 ){

// 		    	Q.m11	=  cos(rot);
// 		    	Q.m22	=  cos(rot);
// 		    	Q.m12	=- sin(rot);
// 		    	Q.m21	=  sin(rot);
		    
		    
// 		    }
		    
// 		    else{
// 		    
// 		    	Q.m11	=  cos(0);
// 		    	Q.m22	=  cos(0);
// 		    	Q.m12	= -sin(0);
// 		    	Q.m21	=  sin(0);
// 
// 		    }
		    
// 		    metric_rr =   rotation_sym(metric_rr,Q);

// 			energy_thread = c.fptr2(metric_rr,c.disorder[i],c.alloying[i]);	
			
// 			es  = coarse_grain_fast(metric_rr,c.disorder[i],c.alloying[i]);


			es = c.fptr2(metric_rr,c.disorder[i][k],c.alloying[i][k],vecr);			
			energy_thread = es.energy;

// 			extra = c.soft*0.5*pow(-sum_F12 + force_energy::calculation::getInstance().c.load,2.) ;
			c.energy_local[i] += energy_thread ;
// 			if((ix-nx/2)*(ix-nx/2) + (iy-ny/2)*(iy-ny/2) <=100 )
// 			if( iy <= ny/2-1 )
// 				rot=30.;
			d1= stress_zanzotto(es.r_stress,v,m2,Q);
	


			
				
				
			
			c.gr[i].x         += d1.gr[0].x;
			c.gr[i].y         += d1.gr[0].y;
// 
//  	  //no need to sum the cross terms
			c.grp1[i][l].x   = d1.gr[1].x ; 
			c.grp1[i][l].y   = d1.gr[1].y ; 
			c.grp1[i][l+1].x = d1.gr[2].x ; 
			c.grp1[i][l+1].y = d1.gr[2].y ; 
		
// 			c.gr[i].x         += d1.gr[0].x;
// 			c.gr[i].y         += d1.gr[0].y;
// 			
// 			c.gr[p.nn[k]].x   += d1.gr[1].x;
// 			c.gr[p.nn[k]].y   += d1.gr[1].y;
// 			c.gr[p.nn[k+1]].x += d1.gr[2].x;
// 			c.gr[p.nn[k+1]].y += d1.gr[2].y; 
			label3: ;


	 		}
	}
// }




// catch (std::exception& e)
// {
// 	cout << e.what() << endl;
// }

//     exit (EXIT_FAILURE);

    //THIS ONE CANNOT BE PARALLALIZED
    	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 

// 	for (int k=0; k<BLK1::triangle; k=k+c.inc) 
//  	for (int k=0; k<3; k=k+2) 
    {
		 
		for (int i=0; i<npunti; ++i) 
		{ 
			int l=2*k;
			struct punto_stru p=c.p[i];
			if(!check(c.bc_cond[i][k],2))	
				goto label4;

			if((p.nn[k] ==-1 || p.nn[k+1] ==-1) )	
				cout<<"warning3"<<endl;

			c.gr[p.nn[k]].x   += c.grp1[i][l].x;
			c.gr[p.nn[k]].y   += c.grp1[i][l].y;
			c.gr[p.nn[k+1]].x += c.grp1[i][l+1].x;
			c.gr[p.nn[k+1]].y += c.grp1[i][l+1].y;  
			label4: ;  
    	} 
    }
    

//       for (int i=0; i<npunti; ++i)
//     	cout<<c.gr[i].x<<endl; 
//     for (int i=0; i<npunti; ++i)
//     	cout<<c.gr[i].y<<endl;   

}

void fix_y_and_periodic_x_BC(int&ix, int&iy, int& k, struct de_stru& d, double& load)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;

	if (ix == nx - 1 && k == 0)    d.p[1].x += dnx;
	if (ix == nx - 1 && k == 3)    d.p[2].x += dnx;
	if (ix == 0 && k == 1)     d.p[2].x -= dnx;
	if (ix == 0 && k == 2)     d.p[1].x -= dnx;

	// 			if(iy == ny-1 && k ==0)    d.p[2].y += dny;
	// 			if(iy == ny-1 && k ==1)    d.p[1].y += dny;
	// 			if(iy == 0 && k   ==2)     d.p[2].y -= dny;
	// 			if(iy == 0 && k   ==3)     d.p[1].y -= dny;  


	//THIS PART IS NOT IMPORTANT SINCE IT CONCERNS ONLY FIXED BOUNDARY UNIT
	//THEY DO NOT MOVE
	//WE ONLY CALCULATE THEM NOT TO BREAK THE LOOP

	//BECAUSE OF LOADING, WE NEED ALSO TO FIX THE X COORDINATES; THE BOX CHANGES ITS SHAPE

	// 		if(iy == ny-1 && k ==0)    d.p[2].y = dny;
	// 		if(iy == ny-1 && k ==1)    d.p[1].y = dny;
	// 		if(iy == 0 && k   ==2)     d.p[2].y = -1.;
	// 		if(iy == 0 && k   ==3)     d.p[1].y = -1.;  
	// 
	// 
	// 		if(iy == ny-1 && k ==0)    d.p[2].x =  dix + load*(diy+1-(dny-1)/2)/(dny-1.);
	// 		if(iy == ny-1 && k ==1)    d.p[1].x =  dix + load*(diy+1-(dny-1)/2)/(dny-1.);
	// 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + load*(diy-1-(dny-1)/2)/(dny-1.);
	// 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + load*(diy-1-(dny-1)/2)/(dny-1.);        

	//THIS PART IS NOT IMPORTANT SINCE IT CONCERNS ONLY FIXED BOUNDARY UNIT
	//THEY DO NOT MOVE
	//WE ONLY CALCULATE THEM NOT TO BREAK THE LOOP
	// 		if(iy == ny-1 && k ==0)    d.p[2].x =  dix + load*(diy+1)/(dny-1.);
	// 		if(iy == ny-1 && k ==1)    d.p[1].x =  dix + load*(diy+1)/(dny-1.);
	// 		
	// // 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + load*(diy-1)/(dny-1.);
	// // 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + load*(diy-1)/(dny-1.);     
	// 
	// 		//IT IS NOT NECESSARY TO MULTIPLY WITH LOAD HERE SINCE u(iy=0) = 0
	// 		if(iy == 0 && k   ==2)     d.p[2].x =  dix + (diy-1)/(dny-1.);
	// 		if(iy == 0 && k   ==3)     d.p[1].x =  dix + (diy-1)/(dny-1.);     

}
//  				per_BC(ix,iy,k,d,c);


void write_to_a_file_with_location(struct conf_stru& c, int t, const string f1);
double rosen(const column_vector& m)
/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/
{
	double x, y,sum;
	int i,ix,iy,nx,ny;
	double dnx,dny,size,sum2;
	nx=dimensions::nx ;
	ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

// 	double load=loading::testNum ;
	double load=BLK1::load;
	double theta = BLK1::theta;
	double dt=BLK1::dt*10;
	static int step = 1;
	static int step2 = 0;

	static int t = 0;
	
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number;
	
// 	struct conf_stru * c_local;
// 	c_local = &force_energy::calculation::getInstance().c;

	dnx=nx;
	dny=ny;
	size=dnx*dny;




	for(int i=0;i<npunti;++i)
	{ 
        force_energy::calculation::getInstance().c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + m(i);
        force_energy::calculation::getInstance().c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + m(i+npunti);
	} 

	if(fexists("roule") == false)
	{ 
		cout<<"CODE HAS BEEN STOPPED IN THE CG"<<endl;
		exit (EXIT_FAILURE); 
	}
	


    	if((step%BLK1::cg_freq ==0 && BLK1::cg_save == 1)){
    		step =1;
//     		exit(1);

//     		ps_file(force_energy::calculation::getInstance().c,t);
//     		slicer(force_energy::calculation::getInstance().c,t);
    		string CG = "CG";
    		result_memorize(force_energy::calculation::getInstance().c);
//     		write_to_a_file(force_energy::calculation::getInstance().c, t);
 	    	string answer = "l";
// 	    	ps_file(force_energy::calculation::getInstance().c,t+999, answer);
			int tprime  = t + 9999;
// 			do_delaunay(force_energy::calculation::getInstance().c,tprime,theta, load);

			write_to_a_file_with_location(force_energy::calculation::getInstance().c,t++,CG);

//     		cout<<"iteration:  "<<step<<" energy: "<<sum<<endl;

    	}

	step2++;
	
  
    ++step;
	if(BLK1::constraint_min.compare("no") ==0)
		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	calcstresses_CG_en(force_energy::calculation::getInstance().c);
	if(BLK1::constraint_min.compare("no") ==0)
		apply_BC_CG(force_energy::calculation::getInstance().c,load);
	
	sum = 0.;
	
	if(BLK1::method.compare("dynamic") ==0)
	{
		for(int i=0;i<n;++i)
		{ 

			sum+=0.5*pow(m(i)   - force_energy::calculation::getInstance().c.disp[i].x + (dt)*force_energy::calculation::getInstance().c.rfx[i] ,2.) 
								+ dt*force_energy::calculation::getInstance().c.energy_local[i];
			sum+=0.5*pow(m(i+n) - force_energy::calculation::getInstance().c.disp[i].y + (dt)*force_energy::calculation::getInstance().c.rfy[i] ,2.) 
								+ dt*force_energy::calculation::getInstance().c.energy_local[i];
		} 	
	}

	if(BLK1::method.compare("statique") ==0)
	{

		for(int i=0;i<n;++i)
		{ 
			sum+= force_energy::calculation::getInstance().c.energy_local[i];
// 			cout<<"energy at node i= "<<i<< " " <<force_energy::calculation::getInstance().c.energy_local[i]<<endl;
    	}
    

    }



// 	cout<<"sum="<<sum/(BLK1::triangle*1.0*n)<<endl;
	//double h=1./dnx;	
	double h=dimensions::dh;
// 	cout<<	h*h*sum<<endl;
	return h*h*sum;
	//return sum;

// 	return sum/(2.0*n);

}

const column_vector rosen_derivative(const column_vector& m)
/*!
ensures
- returns the gradient vector for the rosen function
!*/
{

	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int npunti =  force_energy::calculation::getInstance().c.p.size();
	int n =  force_energy::calculation::getInstance().c.p.size();

	double dt=BLK1::dt*10;
	int n_special=force_energy::calculation::getInstance().c.total_triangular_number;


	column_vector res;
	res.set_size(2*n);
	double dnx = nx;
	//double h=1./dnx;	
	double h=dimensions::dh;	

	if(BLK1::method.compare("statique") ==0)
	{
		for(int i=0;i<n;i++)
		{ 
			double x = force_energy::calculation::getInstance().c.gr[i].x;
			double y = force_energy::calculation::getInstance().c.gr[i].y; 
// 			res(i)     = x;
// 			res(i+n)   = y;

 			res(i)     = h*x;
 			res(i+n)   = h*y;
// 			cout<<"forces x at node i= "<<i<< " " <<res(i) <<endl;
// 			cout<<"forces y at node i= "<<i<< " " <<res(i+n) <<endl;

//  			res(i)     = x;
//  			res(i+n)   = y;

		}
	}

	
	if(BLK1::method.compare("dynamic") ==0)
	{
		for(int i=0;i<n;i++)
		{
			res(i)  =(m(i)   - force_energy::calculation::getInstance().c.disp[i].x 
							+ dt*force_energy::calculation::getInstance().c.gr[i].x
							+ (dt)*force_energy::calculation::getInstance().c.rfx[i]);
		
			res(i+n)=(m(i+n) - force_energy::calculation::getInstance().c.disp[i].y 
							+ dt*force_energy::calculation::getInstance().c.gr[i].y
							+ (dt)*force_energy::calculation::getInstance().c.rfy[i]);


		}
	}
    return res;
}


// This function computes the Hessian matrix for the rosen() fuction.  This is
// the matrix of second derivatives.
dlib::matrix<double> rosen_hessian(const column_vector& m)
{
	const double x = m(0);
	const double y = m(1);

	dlib::matrix<double> res(2, 2);

	// now compute the second derivatives 
	res(0, 0) = 1200 * x*x - 400 * y + 2; // second derivative with respect to x
	res(1, 0) = res(0, 1) = -400 * x;   // derivative with respect to x and y
	res(1, 1) = 200;                 // second derivative with respect to y
	return res;
}

void per_BC_hex(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;

	boundary_conditions f;
	f.setParameters(BLK1::theta,BLK1::load);
	choose_bc(f);
	
		//deform translation vectors
	double h=dimensions::dh;
	double m=h*(double) nx *   BLK1::symmetry_constantx;
	double dx  =  ( + m) * f.f.f11;
	double dy  =  ( + m) * f.f.f21;


//*********************************************

		if (iy%2 != 0 && ix == 0 && k == 2)
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;
		}
		if (iy%2 != 0 && ix == 0 && k == 3)
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
		}


//*********************************************

		if (iy%2 == 0 && ix == nx-1  && k == 0)
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}
		if (iy%2 == 0 && ix == nx-1 && k == 5)
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
		}


//*********************************************

		if (iy%2 != 0 && ix ==  nx-1 && k == 0 )
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
			d.p[2].x += dx;
			d.p[2].y += dy;

		}
		if (iy%2 != 0 && ix ==  nx-1 && k == 1 )
		{
			d.p[1].x += dx;
			d.p[1].y += dy;
		}

		if (iy%2 != 0 && ix ==  nx-1 && k == 4 )
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
		}
		if (iy%2 != 0 && ix ==  nx-1 && k == 5 )
		{
			d.p[2].x += dx;
			d.p[2].y += dy;
			d.p[1].x += dx;
			d.p[1].y += dy;

		}



//*********************************************


		if (iy%2 == 0 && ix == 0 && k == 1 )
		{
			d.p[2].x -= dx;
			d.p[2].y -= dy;
		}
		if (iy%2 == 0 && ix ==  0 && k == 2 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
			d.p[2].x -= dx;
			d.p[2].y -= dy;

		}

		if (iy%2 == 0 && ix ==  0 && k == 3 && iy >0 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
			d.p[2].x -= dx;
			d.p[2].y -= dy;


		}
		if (iy%2 == 0 && ix ==  0 && k == 4 )
		{
			d.p[1].x -= dx;
			d.p[1].y -= dy;
		}


//*********************************************

	double m2=h*(double) ny *   BLK1::symmetry_constanty;
	double dx2  =  ( + m2) * f.f.f12;
	double dy2  =  ( + m2) * f.f.f22;
	
	
		if (iy%2 != 0 && iy ==  ny-1 && k == 0 )
		{
			d.p[2].x += dx2;
			d.p[2].y += dy2;
		}	

		if (iy%2 != 0 && iy ==  ny-1 && k == 1 )
		{
			d.p[2].x += dx2;
			d.p[2].y += dy2;

			d.p[1].x += dx2;
			d.p[1].y += dy2;

		}	


		if (iy%2 != 0 && iy ==  ny-1 && k == 2 )
		{

			d.p[1].x += dx2;
			d.p[1].y += dy2;

		}	



		if (iy ==  0 && ix ==0 && k == 3)
		{

			d.p[1].x -= dx;
			d.p[1].y -= dy;

			d.p[2].x -= dx2+dx;
			d.p[2].y -= dy2+dy;


		}	

		if (iy ==  0 && ix >0 && k == 3 )
		{

			d.p[2].x -= dx2;
			d.p[2].y -= dy2;

		}	


		if (iy ==  0 && ix >=0 && k == 4 )
		{

			d.p[1].x -= dx2;
			d.p[1].y -= dy2;

			d.p[2].x -= dx2;
			d.p[2].y -= dy2;

		}	


		if (iy ==  0 && ix >=0 && k == 5 )
		{

			d.p[1].x -= dx2;
			d.p[1].y -= dy2;


		}	


}
//void per_BC_MJ(struct de_stru& d, int i, struct conf_stru& c,int k)
void per_BC_MJ(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{
		double load =BLK1::load;
		double theta=BLK1::theta;
		double dx,dy;


// 		boundary_conditions f;
// 		f.setParameters(theta,load);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			f.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			f.macro_compression();
// 		//deform translation vectors
// 		
// 
// 
// 		if( c.p[i].pu[k] !=-1){
// 		    dx =  c.p[c.p[i].pu[k]].tvx * f.f.f11  + c.p[c.p[i].pu[k]].tvy * f.f.f12;
// 		    dy =  c.p[c.p[i].pu[k]].tvx * f.f.f21  + c.p[c.p[i].pu[k]].tvy * f.f.f22;
// 		}
// 
// 
// 
// 		//left
// 
// 
// 		if (c.p[i].which_boundary == 2 && k ==1)
// 		{
// 			d.p[2].x -= dx;
// 			d.p[2].y -= dy;
// 
// 		}
// 
// 
// 
// 		if (c.p[i].which_boundary == 2 && k==2 )
// 		{
// 	
// 			d.p[1].x -= dx;
// 			d.p[1].y -= dy;
// 
// 		}
// 
// 		//right
// 
// 		if (c.p[i].which_boundary == 1 && k==0 )
// 		{
// 			d.p[1].x += dx;
// 			d.p[1].y += dy;
// 		}
// 
// 
// 
// 		if (c.p[i].which_boundary == 1 && k ==3)
// 		{
// 	
// 			d.p[2].x += dx;
// 			d.p[2].y += dy;
// 		}
// 
// 
// 		//corners
// 		int c2= c.p[i].corner;
// // 		std::cout<<"c2: "<<c2<<std::endl;
// 
// 		//right top
// 		if(c2 ==1 && k==3  )
// 		{
// 			d.p[2].x += dx;
// 			d.p[2].y += dy;
// 		}
// 	
// 
// 		if(c2 ==2  && k==0)
// 		{
// 			d.p[1].x += dx;
// 			d.p[1].y += dy;
// 		}
// 		
// 		//left top
// 		if(c2 ==3  && k==2)
// 		{
// 			d.p[1].x -= dx;
// 			d.p[1].y -= dy;
// 		}
// 		
// 		if(c2 ==4  && k==1)
// 		{
// 			d.p[2].x -= dx;
// 			d.p[2].y -= dy;
// 		}

}


void per_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;
	//this is periodic because d.p[].x and d.p[].y s keeps displacement of periodic friend
	if (BLK1::crystal_symmetry.compare("tilted") != 0)
	{


		// 		if(BC.compare("pbc")  ==0 || BC.compare("pbcy")  ==0 )
		// 	    {
		if (ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
		}
		if (ix == nx - 1 && k == 3)
		{
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}



		if (ix == 0 && k == 2)
		{
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;
		}

		if (ix == 0 && k == 1)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
		}

		// 		}




		//PERIODIC IN Y
		// 		if(BC.compare("pbcy") ==0 || BC.compare("pbcyfx") ==0 )
		// 	    {
		if (iy == ny - 1 && k == 0)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;
		}


		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
		}

		if (iy == 0 && k == 2)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;
		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
		}

		// 		}

	}


	else if (BLK1::crystal_symmetry.compare("tilted") == 0)
	{

		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 0)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
		}

		if (iy % 2 == 0 && ix == 0 && k == 2)
		{
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;
		}

		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 2)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 2)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}


		//ok---------------------------------

		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
		}

		if (iy % 2 != 0 && ix == nx - 1 && k == 2)
		{
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}

		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 0)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 0)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}




		//ok---------------------------------


		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 1)
		{
			d.p[2].x -= c.left_per[iy].x;
			d.p[2].y -= c.left_per[iy].y;
			d.p[1].x -= c.left_per[iy].x;
			d.p[1].y -= c.left_per[iy].y;

		}



		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 1)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 1)
		{
			d.p[2].x -= c.bottom_per[ix].x;
			d.p[2].y -= c.bottom_per[ix].y;

		}

		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x += c.top_per[ix].x;
			d.p[1].y += c.top_per[ix].y;

		}


		//ok---------------------------------
		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 3)
		{
			d.p[1].x += c.right_per[iy].x;
			d.p[1].y += c.right_per[iy].y;
			d.p[2].x += c.right_per[iy].x;
			d.p[2].y += c.right_per[iy].y;
		}



		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 3)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 3)
		{
			d.p[2].x += c.top_per[ix].x;
			d.p[2].y += c.top_per[ix].y;

		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x -= c.bottom_per[ix].x;
			d.p[1].y -= c.bottom_per[ix].y;

		}



	}
}

void fix_BC(int i, int ix, int iy, int  k, struct de_stru& d, struct conf_stru& c)
{

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	double dnx = nx;
	double dny = ny;
	double diy = iy;
	double dix = ix;
	string  BC = BLK1::BC;
	//this is periodic because d.p[].x and d.p[].y s keeps displacement of periodic friend
	if (BLK1::crystal_symmetry.compare("tilted") != 0)
	{


		// 		if(BC.compare("pbc")  ==0 || BC.compare("pbcy")  ==0 )
		// 	    {
		if (ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.right[iy].x;
			d.p[1].y = c.right[iy].y;
		}
		if (ix == nx - 1 && k == 3)
		{
			d.p[2].x = c.right[iy].x;
			d.p[2].y = c.right[iy].y;
		}



		if (ix == 0 && k == 2)
		{
			d.p[1].x = c.left[iy].x;
			d.p[1].y = c.left[iy].y;
		}

		if (ix == 0 && k == 1)
		{
			d.p[2].x = c.left[iy].x;
			d.p[2].y = c.left[iy].y;
		}

		// 		}




		//PERIODIC IN Y
		// 		if(BC.compare("pbcy") ==0 || BC.compare("pbcyfx") ==0 )
		// 	    {
		if (iy == ny - 1 && k == 0)
		{
			d.p[2].x = c.top[ix].x;
			d.p[2].y = c.top[ix].y;
		}


		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x = c.top[ix].x;
			d.p[1].y = c.top[ix].y;
		}

		if (iy == 0 && k == 2)
		{
			d.p[2].x = c.bottom[ix].x;
			d.p[2].y = c.bottom[ix].y;
		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x = c.bottom[ix].x;
			d.p[1].y = c.bottom[ix].y;
		}

		// 		}

	}


	else if (BLK1::crystal_symmetry.compare("tilted") == 0)
	{

		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 0)
		{
			d.p[2].x = c.left_per[iy].x;
			d.p[2].y = c.left_per[iy].y;
		}

		if (iy % 2 == 0 && ix == 0 && k == 2)
		{
			d.p[1].x = c.left_per[iy].x;
			d.p[1].y = c.left_per[iy].y;
		}

		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 2)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 2)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}


		//ok---------------------------------

		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.right_per[iy].x;
			d.p[1].y = c.right_per[iy].y;
		}

		if (iy % 2 != 0 && ix == nx - 1 && k == 2)
		{
			d.p[2].x = c.right_per[iy].x;
			d.p[2].y = c.right_per[iy].y;
		}

		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 0)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 0)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}




		//ok---------------------------------


		//ok---------------------------------
		if (iy % 2 == 0 && ix == 0 && k == 1)
		{
			d.p[2].x = c.left_per[iy].x;
			d.p[2].y = c.left_per[iy].y;
			d.p[1].x = c.left_per[iy].x;
			d.p[1].y = c.left_per[iy].y;

		}



		//bottom left corner additional correction
		if (iy == 0 && ix == 0 && k == 1)
		{
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		//bottom right corner No need to additional correction


		if (iy == 0 && ix >0 && k == 1)
		{
			d.p[2].x = c.bottom_per[ix].x;
			d.p[2].y = c.bottom_per[ix].y;

		}

		if (iy == ny - 1 && k == 1)
		{
			d.p[1].x = c.top_per[ix].x;
			d.p[1].y = c.top_per[ix].y;

		}


		//ok---------------------------------
		//ok---------------------------------
		if (iy % 2 != 0 && ix == nx - 1 && k == 3)
		{
			d.p[1].x = c.right_per[iy].x;
			d.p[1].y = c.right_per[iy].y;
			d.p[2].x = c.right_per[iy].x;
			d.p[2].y = c.right_per[iy].y;
		}



		//top rigth corner additional correction
		if (iy == ny - 1 && ix == nx - 1 && k == 3)
		{
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		//COMPLETED top rigth corner additional correction

		//top left corner No need to additional correction

		if (iy == ny - 1 && ix <nx - 1 && k == 3)
		{
			d.p[2].x = c.top_per[ix].x;
			d.p[2].y = c.top_per[ix].y;

		}

		if (iy == 0 && k == 3)
		{
			d.p[1].x = c.bottom_per[ix].x;
			d.p[1].y = c.bottom_per[ix].y;

		}



	}
}

void apply_BC(struct conf_stru& c, double& load)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();
	int inc = force_energy::calculation::getInstance().c.inc ;

	dny = ny;
	string_to_integer bset;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
    if(bset.x ==0 ||  bset.y ==0){	
		for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 
			for (int i=0; i<n; ++i){
				if(check(c.bc_cond[i][k],0)){
					 //THIS PART IS IMPORTANT!!!
					 //IF FIXED BC APPLIED AND FORCES ARE NOT PUT TO ZERO 
					 //IT FAILS
					 //FOR CONSTRAINT MINIMIZATION FORCES MAY NOT HAVE TO BE ZERO
					 //THE DLIB TAKES CARE OF IT
					 c.p[i].x     = c.full[i].x; 
					 c.gr[i].x = 0;
					 c.p[i].y     = c.full[i].y; 
					 c.gr[i].y = 0;
				}

// 				if(check(c.bc_cond[i][k],4)){
// 					  c.gr[i].y = load;
// 				}
			 }
	}
		
	if(BLK1::scalar.compare("yes") ==0){
		for (int i=0; i<n; ++i)
			c.gr[i].y=0;
	}
	
}

void apply_BC_CG(struct conf_stru& c, double& load)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();
	int inc = force_energy::calculation::getInstance().c.inc ;
	int k1,k2;
	k1=0;
	
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;

	dny = ny;
	string_to_integer bset;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
    if(bset.x ==0 ||  bset.y ==0){	
// 		for (int k=0; k<dimensions::triangle_limit; k=k+c.inc) 
			for (int i=0; i<n; ++i){
				if(check(c.bc_cond[i][k1],0) || check(c.bc_cond[i][k2],0)){
					 //THIS PART IS IMPORTANT!!!
					 //IF FIXED BC APPLIED AND FORCES ARE NOT PUT TO ZERO 
					 //IT FAILS
					 //FOR CONSTRAINT MINIMIZATION FORCES MAY NOT HAVE TO BE ZERO
					 //THE DLIB TAKES CARE OF IT
					 c.p[i].x     = c.full[i].x; 
					 c.gr[i].x = 0;
					 c.p[i].y     = c.full[i].y; 
					 c.gr[i].y = 0;
				}

// 				if(check(c.bc_cond[i][k],4)){
// 					  c.gr[i].y = load;
// 				}
			 }
	}
		
	if(BLK1::scalar.compare("yes") ==0){
		for (int i=0; i<n; ++i)
			c.gr[i].y=0;
	}
	
}

void apply_BC_box(struct conf_stru& c,column_vector& sp_min, column_vector& sp_max)
{
	int k;
	double dny,ratio;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();
	int inc = force_energy::calculation::getInstance().c.inc ;

	dny = ny;
	string_to_integer bset;
	double minu,maxu,minv,maxv,minvp,maxvp;


	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
	

	//this should be generalized for non cutting also
// 	if(bset.x ==1 && BLK1::grid_creator.compare("cutting") ==0){
// 		
// 		//location of top boundary
// 		std::vector<double> temp;
// 		for (int i=0; i<n; ++i){
// 			if(c.p[i].which_boundary == 3){
// 				temp.push_back(c.p[i].y);
// 			}
// 		}
// 	
// 		double maxdisp = maxval(temp);
// 	
// 		cout<<"find max position:"<<maxdisp<<endl;
// 	
// 		for (int k=0; k<BLK1::triangle; k=k+inc){
// 			for (int i=0; i<n; ++i){
// 				 if(check(c.bc_cond[i][k],1)){
// 					sp_max(i+n) =maxdisp - c.pfix[i].y - 0.;
// // 					sp_max(i )  = +50;
// 		// 		 	cout<<"find max displacement:"<< sp_max(i+n)<<endl;	
// 				 }	
// 	
// 			}
// 		}
// 	
// 		//location of bottom boundary
// 		std::vector<double> temp2;
// 		for (int i=0; i<n; ++i){
// 			if(c.p[i].which_boundary == 4)
// 				temp2.push_back(c.p[i].y);
// 		}	
// 	
// 		double mindisp = minval(temp2);
// 		cout<<"find min position:"<<mindisp<<endl;
// 	
// 		for (int k=0; k<BLK1::triangle; k=k+inc){
// 			for (int i=0; i<n; ++i){
// 			 if(check(c.bc_cond[i][k],1)){
// 				sp_min(i+n) =mindisp - c.pfix[i].y + 0.;
// // 				sp_min(i) =  -50;
// 	// 		 	cout<<"find max displacement:"<< sp_max(i+n)<<endl;	
// 			 }	
// 	
// 			}
// 		}
// 	}

	
	
	for (int k=0; k<BLK1::triangle; k=k+inc) 
		for (int i=0; i<n; ++i){
			
			//fix boundary condition
			if(check(c.bc_cond[i][k],0)){
				 //u_min and v_min
				 sp_min(i)    = c.full[i].x -c.pfix[i].x ;
				 sp_min(i+n)  = c.full[i].y -c.pfix[i].y ;
				 //u_max and v_max
				 sp_max(i)    = c.full[i].x -c.pfix[i].x ;
				 sp_max(i+n)  = c.full[i].y -c.pfix[i].y ;
				 
			}


		 }
					
	
}


void apply_BC_full_generale(struct conf_stru& c, struct boundary_conditions& f)
{
	
	//it should be called once to change initial material coo
	double dnx,dny,diy;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
//  	dny = ny/2-1;
//  	dnx = nx/2-1;
 	dny = c.dny;
 	dnx = c.dnx;

				
	int n= c.p.size();
				
			for(int i =0;i<n;++i)
			{
				int iy = i / nx;
				int ix = i % nx;
// 				c.p[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.p[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;

// 				c.pfix[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
// 				
// 				c.pfix[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y  * f.f.f22;

				if(BLK1::BC.compare("eulerian") != 0)
				{
					c.p[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;				
					c.p[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;

				}


				if(BLK1::BC.compare("eulerian") == 0)
				{
					c.p[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;				
					c.p[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;

					c.pfix[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;				
					c.pfix[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y * f.f.f22;


				}

		
			}



      			
   
}

void apply_BC_generale(struct conf_stru& c, struct boundary_conditions& f)
{
	double dny,diy,dnx;
	int nx=dimensions::nx ;
	int ny=dimensions::ny ;
	int n = c.p.size();

//  	dny = ny/2-1;
//  	dnx = nx/2-1;

 	dny = c.dny;
 	dnx = c.dnx;

 	double theta_d = f.theta_d;
 	double load_in = f.load_in;


//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
// 
//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;
   	
//    	struct def_grad f = combiner(load_in,theta_d,macro_shear);

			double k,m;
//----------------------periodic boundaries------------------------------------
				
			for(int i =0;i<ny;++i)
			{				
				k=0;
				m=(double) nx *  BLK1::symmetry_constantx*dimensions::dh  / BLK1::scale  ;
				c.left_per[i].x  = (-k + m) * f.f.f11;
				c.left_per[i].y  = (-k + m) * f.f.f21;
				
			}

			
			for(int i =0;i<ny;++i)
			{				
				k=0;
				m=(double) nx *   BLK1::symmetry_constantx*dimensions::dh   / BLK1::scale;
				c.right_per[i].x  =  (-k + m) * f.f.f11;
				c.right_per[i].y  =  (-k + m) * f.f.f21;
			}
			
			for(int i =0;i<nx;++i)
			{				
				k=0;
				m=(double) ny *   BLK1::symmetry_constanty*dimensions::dh   / BLK1::scale;
				c.top_per[i].x  = (-k + m)* f.f.f12;
				c.top_per[i].y  = (-k + m)* f.f.f22;
				
			}

			
			for(int i =0;i<nx;++i)
			{				
				k=0;
				m=(double) ny*   BLK1::symmetry_constanty*dimensions::dh   / BLK1::scale;
				c.bottom_per[i].x  = (-k + m) * f.f.f12;
				c.bottom_per[i].y  = (-k + m) * f.f.f22;
			}			
			
			
//----------------------fix boundaries------------------------------------
			

// 			for(int i =0;i<ny;++i)
// 			{
// 				
// 				int iy;
// 				iy = i;
// 				int ix;
// 				ix=nx;
// 
// 				int k = iy*nx + ix;
// 
// 
// // 				c.right[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12;
// // 				
// // 				c.right[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22;
// 				
// 				c.right[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
// 				
// 				c.right[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;
// 
// 				
// 				ix=-1;
// 				
// 				k = iy*nx + ix;
// 				
// // 				c.left[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12;
// // 				
// // 				c.left[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22;
// 
// 				c.left[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
// 				
// 				c.left[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;
// 	
// 			}



// 			for(int i =0;i<nx;++i)
// 			{
// 				int ix;
// 				ix = i;
// 				int iy;
// 				iy = -1;
// 				
// 				int k = iy*nx + ix;
// 				
// 				
// 				c.bottom[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
// 				
// 				c.bottom[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;
// 
// 
// // 				c.bottom[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12  ;
// // 				
// // 				c.bottom[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22  ;
// 
// 			    iy = ny;
// 			    k = iy*nx + ix;
// 				
// // 				c.top[i].x = ((double) ix) * f.f.f11 + ((double) iy) * f.f.f12 ;
// // 				
// // 				c.top[i].y = ((double) ix) * f.f.f21 + ((double) iy) * f.f.f22 ;
// 				
// 				c.top[i].x = c.pcons[k].x * f.f.f11 + c.pcons[k].y * f.f.f12;
// 				
// 				c.top[i].y = c.pcons[k].x * f.f.f21 + c.pcons[k].y  * f.f.f22;
// 
// 			}
			
			
// 			for(int i =0;i<nx;++i)
// 			{
// 				int ix;
// 				ix = i;
// 				int iy=ny-1;
// 
// // 				c.top[i].x = ((double) ix-dnx) * (1. - load_in* cos(theta_d)*sin(theta_d)) +
// // 						     ((double) iy-dny) * load_in* pow(cos(theta_d),2.);
// // 				c.top[i].y =-((double) ix-dnx) * load_in* pow(sin(theta_d),2.) +
// // 						     ((double) iy-dny) * (1. +  load_in*cos(theta_d)*sin(theta_d));
// 
// 				c.top[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.top[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;
// 
// 
// 
// 			}						
// 			

			for(int i =0;i<n;++i)
			{	
					int iy = i / nx;
					int ix = i % nx;

// 					c.full[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
// 			
// 					c.full[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y  * f.f.f22;

					
					
					c.full[i].x = c.pcons[i].x * f.f.f11 + c.pcons[i].y * f.f.f12;
			
					c.full[i].y = c.pcons[i].x * f.f.f21 + c.pcons[i].y* f.f.f22;
					

      			}

}

// void apply_BC_generale(struct conf_stru& c, struct boundary_conditions& f)
// 
// {
// 	double dny,diy,dnx;
// 	int nx=dimensions::nx ;
// 	int ny=dimensions::ny ;
//  	dny = ny/2-1;
//  	dnx = nx/2-1;
//  	double theta_d = f.theta_d;
//  	double load_in = f.load_in;
// 
// 
//     cout<<"load_d= "<<load_in<<" load_in= "<<load_in<<endl;
// 
//    	cout<<"theta_d= "<<theta_d<<" theta= "<<theta_d<<endl;
//    	
// //    	struct def_grad f = combiner(load_in,theta_d,macro_shear);
// 
// 			double k,m;
// //----------------------periodic boundaries------------------------------------
// 
// 			
// 			
// //----------------------fix boundaries------------------------------------
// 			
// 
// 			for(int i =0;i<nx*ny;++i)
// 			{
// 
// 				c.pfix[i].x = ((double) ix-dnx) * f.f.f11 + ((double) iy-dny) * f.f.f12;
// 				
// 				c.pfix[i].y = ((double) ix-dnx) * f.f.f21 + ((double) iy-dny) * f.f.f22;
// 
// 				
// 	
// 			}
// 
// }

struct conf_stru  timestep_euler(struct conf_stru& c, double& load)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int npunti = c.p.size();
	double dt = BLK1::dt;



	apply_BC(c, load);
	calcstresses_CG_en(c);
	apply_BC(c, load);

	struct conf_stru guess;
	guess.p.resize(c.p.size());
	guess.disp.resize(c.p.size());


	for (int i = 0; i<npunti; i++)
	{

		guess.disp[i].x = c.disp[i].x - dt*(c.gr[i].x) + dt*c.rfx[i];
		guess.disp[i].y = c.disp[i].y - dt*(c.gr[i].y) + dt*c.rfy[i];

		//KEEP DATA AT OLDER TIME BEFORE UPDATING
		//UPDATE 	  	
	}

	// 	double sum=0; 
	// 	for (int i=0; i<nx*ny; i++) 	
	// 		sum+=c.energy_local[i];


	// 	cout<<"energy= "<<sum<<endl;
	return guess;

}


void  timestep_euler2(struct conf_stru& c, double& load)
{
	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int npunti = c.p.size();
	double dt = BLK1::dt;



	apply_BC(c, load);
	calcstresses_CG_en(c);
	apply_BC(c, load);

	//     struct conf_stru guess;
	//     guess.p.resize(nx*ny);


	for (int i = 0; i<npunti; i++)
	{

		c.p[i].x -= dt*(c.gr[i].x);
		c.p[i].y -= dt*(c.gr[i].y);

		//KEEP DATA AT OLDER TIME BEFORE UPDATING
		//UPDATE 	  	
	}


}



