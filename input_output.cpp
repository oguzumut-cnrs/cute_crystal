#include "input_output.h"
#include "namespaces.h"
#include "plot.h"
#include "structures.h"
#include "atomistic_grid.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

void write_to_a_file(struct conf_stru& c, int t){

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ofstream metrics, disp, nn;
	string filename, filename2, filename3;

	//    filename="dir_config/config_" + IntToStr(t) +".txt";
	filename = "dir_config/config_" + IntToStr(t);
	filename2 = "dir_config_u/config_" + IntToStr(t);
	filename3 = "dir_config_nn/config_" + IntToStr(t);

	//    cout<< filename << "  \n";
	metrics.open(filename.c_str());
	disp.open(filename2.c_str());

	for (int k = 0; k<3; k++)
	{
		for (int i = 0; i<n; ++i)
		{
			if (k == 0)
			{
				metrics << std::scientific << std::setprecision(7) << c.cauchy[i].f12 << endl;
				disp << std::scientific << std::setprecision(7)    << c.p[i].x << endl;
			}

			if (k == 1)
			{
				metrics << std::scientific << std::setprecision(7) <<  c.piola_one[i].f12 << endl;
				disp << std::scientific << std::setprecision(7) << c.p[i].y << endl;
			}

			if (k == 2)
			{
				metrics << std::scientific << std::setprecision(7) << c.cauchy[i].f11 	<< endl;
			}
		}
	}

// 	nn.open(filename3.c_str());
// 	
// 	for (int i = 0; i<n; ++i){
// 		for (int k=0; k<BLK1::triangle; k=k+c.inc){
// 			nn << std::scientific << std::setprecision(7)    << c.p[i].nn[k] << endl;
// 		}
// 	}


	metrics.close();
	disp.close();
// 	nn.close();

}

void read_from_a_file(struct conf_stru& c){

	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ifstream metrics, disp;
	string filename, filename2;


	filename = reading::filename;
	filename2 = reading::filename2;

	//    cout<< filename << "  \n";
	metrics.open(filename.c_str());
	disp.open(filename2.c_str());

	for (int k = 0; k<3; k++)
	{
		for (int i = 0; i<n; ++i)
		{

			if (k == 0)
			{
				metrics >> c.reduced[i].c11;
				disp >> c.p[i].x;
			}

			if (k == 1)
			{
				metrics >> c.reduced[i].c22;
				disp >> c.p[i].y;
			}

			if (k == 2)
				metrics >> c.reduced[i].c12;

		}
	}


	metrics.close();
	disp.close();

}

//for INSIDE CG steps, very expensive don't use

void write_to_a_file_with_location(struct conf_stru& c, int t, const string f1){


	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ofstream metrics, disp;
//	string filename,filename2,filename3,filename4;

	string filename = f1 + "/for_ovito_"   + IntToStr(t);
	string filename2 = f1 + "/cauchy_"   + IntToStr(t);

	ofstream filestr;
	filestr.open(filename.c_str());

	int k1,k2;
	k1=0;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;
		

	filestr << c.p.size() << endl;
	filestr << " " << endl;

	for (int i = 0; i<c.p.size(); i++){

		 struct de_stru d1;

		 d1.reduced_stress.f11 =   c.piola_one[i].f11;
		 d1.reduced_stress.f22 =   c.piola_one[i].f22;
		 d1.reduced_stress.f12 =   c.piola_one[i].f12;
		 d1.reduced_stress.f21 =   c.piola_one[i].f21;
		 double dpda  =  contraction_with_shear(d1,BLK1::theta);

		filestr << std::scientific << std::setprecision(16)
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< (c.non_reduced[i].c11 - c.non_reduced[i].c22)/sqrt(2.) << " "
			<< c.non_reduced[i].c12 << " "
			<< c.reduced[i].c12 << " "
			<< c.reduced[i].c11 << " "
			<< c.reduced[i].c22 << " "
			<< c.non_reduced[i].c12 << " "
			<< c.non_reduced[i].c11 << " "
			<< c.non_reduced[i].c22 << " "
			<< c.pitteri[i].c12 << " "
			<< c.pitteri[i].c11 << " "
			<< c.pitteri[i].c22 << " "
			<< c.volume[i] << " "
			<< dpda/2. << " "
			<< c.energy_local[i] << " "
			<< BLK1::load
			<< endl;
	}

 	filestr.close();




	ofstream cauchy;
	cauchy.open(filename2.c_str());
	cauchy << c.p.size() << endl;
	cauchy << " " << endl;
	for (int i = 0; i<c.p.size(); i++){
	

		cauchy << std::scientific << std::setprecision(16)
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< c.cauchy[i].f12 << " "
			<< c.cauchy[i].f11 << " "
			<< c.cauchy[i].f22 << " "
			<< c.pitteri[i].c12 << " "
			<< c.pitteri[i].c11 << " "
			<< c.pitteri[i].c22 << " "
			<< BLK1::load
			<< endl;
	}

	cauchy.close();






}

void write_to_a_ovito_file(struct conf_stru& c, int t)
{


	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ofstream metrics, disp;
	string filename, filename2, wheel,wheel2,wheel3,wheel4;



	filename = "./dir_xyz/for_ovito_" + IntToStr(t) + ".xyz";
	filename2 = "./dir_xyz2/for_ovito_" + IntToStr(t) + ".xyz";
	wheel = "./dir_xyz2/all_triangles_metrics" + IntToStr(t) + ".xyz";
	wheel2= "./dir_xyz2/all_triangles_red_metrics" + IntToStr(t) + ".xyz";
	wheel3= "./dir_xyz2/def_gradient" + IntToStr(t) + ".xyz";
// 	wheel4= "./dir_xyz2/displacement" + IntToStr(t) + ".xyz";

	ofstream filestr,filestr2,filestr3,filestr4;
	filestr.open(filename.c_str());

	filestr << c.p.size() << endl;
	filestr << " " << endl;

	for (int i = 0; i<c.p.size(); i++){

		 struct de_stru d1;

		 d1.reduced_stress.f11 =   c.piola_one[i].f11;
		 d1.reduced_stress.f22 =   c.piola_one[i].f22;
		 d1.reduced_stress.f12 =   c.piola_one[i].f12;
		 d1.reduced_stress.f21 =   c.piola_one[i].f21;
		 double dpda  =  contraction_with_shear(d1,BLK1::theta);

		filestr << std::scientific << std::setprecision(16)
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< (c.non_reduced[i].c11 - c.non_reduced[i].c22)/sqrt(2.) << " "
			<< c.non_reduced[i].c12 << " "
			<< c.reduced[i].c12 << " "
			<< c.reduced[i].c11 << " "
			<< c.reduced[i].c22 << " "
			<< c.non_reduced[i].c12 << " "
			<< c.non_reduced[i].c11 << " "
			<< c.non_reduced[i].c22 << " "
			<< c.pitteri[i].c12 << " "
			<< c.pitteri[i].c11 << " "
			<< c.pitteri[i].c22 << " "
			<< c.volume[i] << " "
			<< dpda/2. << " "
			<< c.energy_local[i] << " "
			<< c.load
			<< endl;
	}

 	filestr.close();

//****************************************************************************************

	//used for memory reading; do not change
	string filename3 ="./dir_cauchy/cauchy_"   + IntToStr(t);

	ofstream cauchy;
	cauchy.open(filename3.c_str());
	cauchy << c.p.size() << endl;
	cauchy << " " << endl;
	for (int i = 0; i<c.p.size(); i++){
	

		cauchy << std::scientific << std::setprecision(16)
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< c.cauchy[i].f12 << " "
			<< c.cauchy[i].f11 << " "
			<< c.cauchy[i].f22 << " "
			<< c.load
			<< endl;
	}

	cauchy.close();

//******************************************************************************************
//all_triangles_metrics and volumes
	filestr3.open(wheel.c_str());
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc)
		for (int i = 0; i<c.p.size(); i++){
			if(!check(c.bc_cond[i][k],2))
				continue;				
			double c11 = c.metric_full[i][k].c11;
			double c22 = c.metric_full[i][k].c22;
			double c12 = c.metric_full[i][k].c12;
			double volume_triangle = sqrt(c11*c22-c12*c12);			

			filestr3 << std::scientific << std::setprecision(16)<<
			i<< " "<< k <<" "<< c.metric_full[i][k].c12 << " "<< c.metric_full[i][k].c11  << " " <<c.metric_full[i][k].c22 << " "<< volume_triangle<< endl;
	}
						
	filestr3.close();
	
//******************************************************************************************
//all_triangles_reduced_metrics and volumes
	
	filestr3.open(wheel2.c_str());
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc)
		for (int i = 0; i<c.p.size(); i++){
			if(!check(c.bc_cond[i][k],2))
				continue;				
			double c11 = c.metricr_full[i][k].c11;
			double c22 = c.metricr_full[i][k].c22;
			double c12 = c.metricr_full[i][k].c12;
			double volume_triangle = sqrt(c11*c22-c12*c12);			


			filestr3 << std::scientific << std::setprecision(16)<<
			i<< " "<< k <<" "<< c.metricr_full[i][k].c12 << " "<< c.metricr_full[i][k].c11  << " " <<c.metricr_full[i][k].c22 << " "<< volume_triangle<< endl;

	}
						
	filestr3.close();
	
	
//******************************************************************************************
//all_triangles_full_metrics and deformation gradients

	filestr3.open(wheel3.c_str());
// 	filestr4.open(wheel4.c_str());

// 	filestr3 << c.p.size() << endl;
// 	filestr3 << " " << endl;
	for (int k=0; k<dimensions::triangle_limit; k=k+c.inc)
		for (int i = 0; i<c.p.size(); i++){
			
			if(!check(c.bc_cond[i][k],2))
				continue;	

			int iy = i / dimensions::nx;
			int ix = i % dimensions::nx;

				filestr3 << std::scientific << std::setprecision(16)
				<< c.m_field_full[i][k].m11    << 
				" " <<c.m_field_full[i][k].m22 << 
				" " <<c.m_field_full[i][k].m12 << 
				" " <<c.m_field_full[i][k].m21 <<
				" " <<c.metric_full[i][k].c12  << 
				" " <<c.metric_full[i][k].c11  << 
				" " <<c.metric_full[i][k].c22  << endl;
				
// 			if(k==2 && ix != dimensions::nx-1 && iy != dimensions::ny-1)				
// 
// 				filestr4 << std::scientific << std::setprecision(16)
// 				<< c.m_field_full[i][k].m11 << " "<< c.m_field_full[i][k].m22  << " " <<c.m_field_full[i][k].m12 << 
// 				" " <<c.m_field_full[i][k].m21 <<" "<<c.rfx[i]<<" "<<c.rfy[i]<<endl;


		}
						
	filestr3.close();
// 	filestr4.close();
	

}

void write_to_a_ovito_file_eigenvectors(struct conf_stru& c, int t, int k)
{


	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n = c.p.size();

	ofstream metrics, disp;
	string filename;

	int k1,k2;
	k1=0;
	if(BLK1::crystal_symmetry.compare("hex") == 0)
		k2=3;
	if(BLK1::crystal_symmetry.compare("square") == 0)
		k2=2;
		
	int line_number=0;
	//count the number of non fixed atoms
	for (int i = 0; i<c.p.size(); i++)
		if(check(c.bc_cond[i][k1],2) && check(c.bc_cond[i][k2],2) )
			line_number++;


	filename ="./eigenvectors_" + IntToStr(t) + "_" + IntToStr(k) +  ".xyz";



	ofstream cauchy;
	cauchy.open(filename.c_str());
	cauchy << line_number << endl;
	cauchy << " " << endl;
	
	for (int i = 0; i<c.p.size(); i++){		
		if(check(c.bc_cond[i][k1],2) && check(c.bc_cond[i][k2],2)){
			cauchy << std::scientific << std::setprecision(9)
				<< c.p[i].x << " "
				<< c.p[i].y << " "

				<< c.reduced[i].c12 << " "
				<< c.reduced[i].c11 << " "
				<< c.reduced[i].c22 << " "
				<< c.non_reduced[i].c12 << " "
				<< c.non_reduced[i].c11 << " "
				<< c.non_reduced[i].c22 << " "
				<< c.pitteri[i].c12 << " "
				<< c.pitteri[i].c11 << " "
				<< c.pitteri[i].c22 << " "
				<< c.volume[i] << " "
				<< c.energy_local[i] 
				<< endl;
		}
	}




	cauchy.close();


}

// void write_to_a_ovito_file_special(struct conf_stru& c, int t)
// {
// 
// 
// 	int nx = dimensions::nx;
// 	int ny = dimensions::ny;
// 	int n = c.p.size();
// 
// 	ofstream metrics, disp;
// 	string filename, filename2, wheel,wheel2;
// 
// 	int k1,k2;
// 	k1=0;
// 	if(BLK1::crystal_symmetry.compare("hex") == 0)
// 		k2=3;
// 	if(BLK1::crystal_symmetry.compare("square") == 0)
// 		k2=2;
// 		
// 	filename = "./dir_special/for_ovito_" + IntToStr(t) + ".xyz";
// 	int line_number=0;
// 	//count the number of non fixed atoms
// 	for (int i = 0; i<c.p.size(); i++)
// 		if(check(c.bc_cond[i][k1],2) && check(c.bc_cond[i][k2],2) )
// 			line_number++;
// 
// 		
// 
// 	ofstream filestr,filestr2,filestr3;
// 	filestr.open(filename.c_str());
// 
// 	filestr << line_number << endl;
// 	filestr << " " << endl;
// 
// 
// 
// 	for (int i = 0; i<c.p.size(); i++){		
// 		if(check(c.bc_cond[i][k1],2) && check(c.bc_cond[i][k2],2)){
// 			 struct de_stru d1;
// 
// 			 d1.reduced_stress.f11 =   c.piola_one[i].f11;
// 			 d1.reduced_stress.f22 =   c.piola_one[i].f22;
// 			 d1.reduced_stress.f12 =   c.piola_one[i].f12;
// 			 d1.reduced_stress.f21 =   c.piola_one[i].f21;
// 			 double dpda  =  contraction_with_shear(d1,BLK1::theta);
// 
// 			filestr << std::scientific << std::setprecision(9)
// 				<< c.p[i].x << " "
// 				<< c.p[i].y << " "
// 		     	<< (c.non_reduced[i].c11 - c.non_reduced[i].c22)/sqrt(2.) << " "
// 				<< c.non_reduced[i].c12 << " "
// 				<< c.reduced[i].c12 << " "
// 				<< c.reduced[i].c11 << " "
// 				<< c.reduced[i].c22 << " "
// 				<< c.non_reduced[i].c12 << " "
// 				<< c.non_reduced[i].c11 << " "
// 				<< c.non_reduced[i].c22 << " "
// 				<< c.pitteri[i].c12 << " "
// 				<< c.pitteri[i].c11 << " "
// 				<< c.pitteri[i].c22 << " "
// 				<< c.volume[i] << " "
// 				<< dpda/2. << " "
// 				<< c.energy_local[i] << " "
// 				<< BLK1::load
// 				<< endl;
// 		}
// 	}
// 
// 
// 
// }

void write_to_a_ovito_file_remove_Fmacro(struct conf_stru& c, int t, double load)
{

	ofstream pk_one;
	string filename4 ="./dir_piola_one/pk1_"      + IntToStr(t);

	pk_one.open(filename4.c_str());
	pk_one << c.p.size() << endl;
	pk_one << " " << endl;

	remove_macro_def(c);


	for (int i = 0; i<c.p.size(); i++){
		pk_one << std::scientific << std::setprecision(7)
 			<< c.rfx[i] << " "
 			<< c.rfy[i] << " "
			<< c.pcons[i].x << " "
			<< c.pcons[i].y << " "
			<< c.p[i].x << " "
			<< c.p[i].y << " "
			<< c.piola_one[i].f12 << " "
			<< c.piola_one[i].f21 << " "
			<< c.piola_one[i].f11 << " "
			<< c.piola_one[i].f22 << " "
			<< c.cauchy[i].f12 << " "
			<< c.cauchy[i].f11 << " "
			<< c.cauchy[i].f22 << " "					
			<< BLK1::load

			<< endl;
	}
	
		pk_one.close();


}

void read_input_file()
{
	std::ifstream file("./lagrange_input_cpp");
	string str;
	// nx,ny,nz
	getline(file, str, '?');
	file >> str;
	int nx, ny;
	file >> nx >> ny;
	dimensions::setNum(nx, ny);
	cout << "verification system sizes -->" << endl;
	cout << "nx= " << dimensions::nx << endl;
	cout << "ny= " << dimensions::ny << endl;

	getline(file, str, '?');
	file >> str;
	string method;
	file >> method;
	cout << "verification of method new  -->" << endl;
	BLK1::setMethod(method);
	cout << "method= " << BLK1::model << endl;


	getline(file, str, '?');
	file >> str;
	int n_threads;
	file >> n_threads;
	cout << "verification of thread number  -->" << endl;
	BLK1::setThread(n_threads);
	cout << "thread number= " << BLK1::num_threads << endl;

	getline(file, str, '?');
	file >> str;
	double v1, v2, v2_2;
	file >> v1 >> v2 >> v2_2;
	cout << "verification of beta  and theta values  -->" << endl;
	BLK1::setBeta(v1);
	BLK1::setTheta(v2,v2_2);
	cout << "beta=  " << BLK1::beta_Var << endl;
	cout << "theta= " << BLK1::theta << endl;
	cout << "theta_init= " << BLK1::theta_init << endl;


	getline(file, str, '?');
	file >> str;
	double dt;
	file >> dt;
	cout << "verification of  saving freq   -->" << endl;
	BLK1::setTimestep(dt);
	cout << "dt= " << BLK1::dt << endl;

	//  		
	getline(file, str, '?');
	file >> str;
	string analysis;
	file >> analysis;
	cout << "verification of  analysis   -->" << endl;
	BLK1::setAnalysis(analysis);
	cout << "dt= " << BLK1::method << endl;

	getline(file, str, '?');
	file >> str;
	string v3, v4;
	file >> v3 >> v4;
	cout << "verification of  BC and IC   -->" << endl;
	BLK1::setBCandIC(v3, v4);
	cout << "BC= " << BLK1::BC << endl;
	cout << "IC= " << BLK1::IC << endl;

	getline(file, str, '?');
	file >> str;
	double is, sr, is2;
	file >> is >> sr >> is2;
	cout << "verification of  IS and SR   -->" << endl;
	BLK1::setISandSR(is, sr,is2);
	cout << "IS= " << BLK1::initial_strain << endl;
	cout << "SR= " << BLK1::strain_rate << endl;
	cout << "IS2= " << BLK1::initial_strain2 << endl;


	getline(file, str, '?');
	file >> str;
	int total_iteration;
	file >> total_iteration;
	cout << "total_iteration number  -->" << endl;
	BLK1::setITER(total_iteration);
	cout << "total_iteration= " << BLK1::total_iteration << endl;

	getline(file, str, '?');
	file >> str;
	string macro_def;
	file >> macro_def;
	cout << "macroscopic BC  -->" << endl;
	BLK1::setMacro_def(macro_def);
	cout << "macro_def= " << BLK1::macro_def << endl;


	getline(file, str, '?');
	file >> str;
	string crystal_symmetry;
	file >> crystal_symmetry;
	cout << "crystal_symmetry   -->" << endl;
	BLK1::setCrystal_symmetry(crystal_symmetry);
	cout << "setCrystal_symmetry= " << BLK1::crystal_symmetry << endl;


	getline(file, str, '?');
	file >> str;
	double loading_limit;
	file >> loading_limit;
	cout << "loading_limit   -->" << endl;
	BLK1::setloading_limit(loading_limit);
	cout << "loading_limit= " << BLK1::loading_limit << endl;


	getline(file, str, '?');
	file >> str;
	string bc_x, bc_y, bc_mode;
	file >> bc_x >> bc_y >> bc_mode;
	cout << "verification of setBCnew   -->" << endl;
	BLK1::setBCnew(bc_x, bc_y, bc_mode);
	cout << "bc_x:    " << BLK1::bc_x << endl;
	cout << "bc_y:    " << BLK1::bc_y << endl;
	cout << "bc_mode: " << BLK1::bc_mode << endl;

	getline(file, str, '?');
	file >> str;
	string first;
	file >> first;
	cout << "verification of setBCnew   -->" << endl;
	BLK1::setPoly(first);
	cout << "poly:    " << BLK1::first << endl;


	getline(file, str, '?');
	file >> str;
	string  config, config_u;
	int memory;
	int memory_number;
	file >> memory >> memory_number;
	getline(file, str, '?');
	file >> str;
	file >> config;
	getline(file, str, '?');
	file >> str;
	file >> config_u;

	cout << "memory options   -->" << endl;
	BLK1::setMemory(memory, memory_number, config, config_u);
	cout << "setMemory= " << BLK1::memory << endl;
	cout << "setMemory= " << BLK1::memory_number << endl;
	cout << "setMemory= " << BLK1::config << endl;
	cout << "setMemory= " << BLK1::config_u << endl;
	
	getline(file, str, '?');
	file >> str;
	string lattice;
	file >> lattice  ;
	cout<<"verification of grid creator   -->"<<endl;
	BLK1::setGrid_creator(lattice);
	cout<<"grid_creator:    "<<BLK1::grid_creator<<endl;
	
	getline(file, str, '?');
	file >> str;
	int cg_save,cg_freq;
	file >> cg_save >> cg_freq  ;
	cout<<"verification of cg_save   -->"<<endl;
	BLK1::setCg_save(cg_save,cg_freq);
	cout<<"cg_save:    "<<BLK1::cg_save<<endl;	
	cout<<"cg_freq:    "<<BLK1::cg_freq<<endl;	

	getline(file, str, '?');
	file >> str;
	string scalar;
	file >> scalar  ;
	cout<<"verification scalar   -->"<<endl;
	BLK1::setScalar(scalar);
	cout<<"scalar:    "<<BLK1::scalar<<endl;	


	getline(file, str, '?');
	file >> str;
	double cutting_angle;
	file >> cutting_angle  ;
	cout<<"cutting_angle   -->"<<endl;
	BLK1::setCutting_angle(cutting_angle);
	cout<<"cutting angle:    "<<BLK1::cutting_angle<<endl;	

	getline(file, str, '?');
	file >> str;
	string constraint_min;
	file >> constraint_min  ;
	cout<<"verification constraint_min   -->"<<endl;
	BLK1::setConstraint_min(constraint_min);
	cout<<"constraint_min:    "<<BLK1::constraint_min<<endl;	


	getline(file, str, '?');
	file >> str;
	double scale;
	file >> scale  ;
	cout<<"verification scale   -->"<<endl;
	BLK1::setScale(scale);
	cout<<"scale:    "<<BLK1::scale<<endl;	


	getline(file, str, '?');
	file >> str;
	int sub_nx, sub_ny;
	file >> sub_nx >> sub_ny  ;
	cout<<"sub scale   -->"<<endl;
	dimensions::setNumsub(sub_nx, sub_ny);
	cout<<"sub scale nx:    "<<dimensions::sub_nx<<endl;	
	cout<<"sub scale ny:    "<<dimensions::sub_ny<<endl;	


	getline(file, str, '?');
	file >> str;
	double cut_off,r_eq;
	file >> cut_off >> r_eq ;
	cout<<"verification cut_off   -->"<<endl;
	atomistic::setNum(cut_off,r_eq);
	cout<<"cut_off:    "<<atomistic::cut_off<<endl;	
	cout<<"r_eq:    "<<atomistic::r_eq<<endl;	

	getline(file, str, '?');
	file >> str;
	double precision;
	file >> precision ;
	cout<<"verification precison   -->"<<endl;
	BLK1::setPrecision(precision);
	cout<<"precision:    "<<BLK1::precision<<endl;	


	getline(file, str, '?');
	file >> str;
	int triangle_limit;
	double dh;
	file >> triangle_limit >> dh  ;
	cout<<"triangle and dh   -->"<<endl;
	dimensions::setNumparam(triangle_limit, dh);
	cout<<"triangle_limit:    "<<dimensions::triangle_limit<<endl;	
	cout<<"dh:    "<<dimensions::dh<<endl;	


	getline(file, str, '?');
	file >> str;
	double d1,d2,d3;
	file >> d1 >> d2  >> d3 ;
	cout<<"d1, d2 and d3 for disorder   -->"<<endl;
	BLK1::setDisorder(d1,d2,d3);
	cout<<"d1:    "<<BLK1::d1<<endl;	
	cout<<"d2:    "<<BLK1::d2<<endl;	
	cout<<"d2:    "<<BLK1::d3<<endl;	


	getline(file, str, '?');
	file >> str;
	string guess_method;
	file >> guess_method  ;
	cout<<"guess method   -->"<<endl;
	BLK1::setGuess_method(guess_method);
	cout<<"guess method: "<<BLK1::guess_method<<endl;	


	getline(file, str, '?');
	file >> str;
	int req;
	file >> req  ;
	cout<<"eigenvalue requested   -->"<<endl;
	BLK1::setEigenvalues(req);
	cout<<"eigenvalue requested: "<<BLK1::req<<endl;	



}


void ps_file(struct conf_stru& c, int t, string answer)
{

	cout<<"ps_file_v1"<<endl;

	string_to_integer bset;

	bset.convert_new(BLK1::bc_x,BLK1::bc_y);
    


	ofstream metrics;
	string filename;

	filename="dir_images2/config_" + IntToStr(t) + ".ps" ;

	cout<< filename << "  \n";
	metrics.open(filename.c_str());


	double fact_multiplicatif = 5 * 72;
	
	double x_min = c.pfix[0].x-20;
	double y_min = c.pfix[0].y-20;
	
	double x_max = c.pfix[c.p.size()-1].x+20;
	double y_max = c.pfix[c.p.size()-1].y+20;

// 	double x_min = c.p[0].x;
// 	double y_min = c.p[0].y;
// 	
// 	double x_max = c.p[c.p.size()-1].x;
// 	double y_max = c.p[c.p.size()-1].y;
    
//  double range = max( x_max - x_min, y_max - y_min )

    double range = x_max - x_min;
//     cout<<"range of scaling= "<<range<<endl;

	std::vector<double> x_trace,y_trace,ener;
	x_trace.resize(c.p.size());
	y_trace.resize(c.p.size());
	ener.resize(c.p.size());
	
	
			
		
	for(int i=0;i<c.p.size();i++)
	 ener[i] = 	c.energy_local[i];//c.cauchy[i].f12;
	
	double maxval_ener = maxval(ener);
	double minval_ener = minval(ener);

	if(maxval_ener == 0)
		maxval_ener=1;
		
	
	for(int i=0;i<c.p.size();i++)
	{
		x_trace[i] = ( c.p[i].x - x_min) / range;
		y_trace[i] = ( c.p[i].y - y_min) / range;
		ener[i]    = ( ener[i] - minval_ener) / maxval_ener;

// 		x_trace[i] = ( c[i].x  ) / range;
// 		y_trace[i] = ( c[i].y  ) / range;

    }
    
    cout<<"maxval_ener: "<<maxval(ener)<<endl;
    cout<<"minval_ener: "<<minval(ener)<<endl;

	double linewidth = 0.06;//(nx/256)      ; //  ! epaisseur des liens (en inch)
	double rayon     = 0.002;///(nx/256)    ; //  ! rayon des cercles (en inch)
	double dec_x = 0.05;
	double dec_y = 0.05;

// !------- creation du fichiers Postscript ---------------------

  
    double bx_min = 0;
    double by_min = 0;
    double bx_max = 550;
    double by_max = bx_max;
    
// 	write(50,2000)
// 	write(50,2010) bx_min, by_min, bx_max,by_max
// 
// 	2000 format('%!PS-Adobe-2.0 EPSF-1.2')
// 	2010 format('%%BoundingBox: ',4(1x,i4))
	metrics <<"%!PS-Adobe-2.0 EPSF-1.2"<<endl;
	metrics <<"%%BoundingBox: "<<setw(4)<<bx_min<<setw(4)<<by_min<<setw(4)<<bx_max<<setw(4)<<by_max<<endl;
    metrics <<"/Times-Roman findfont   % Get the basic font"<<endl;
    metrics <<"20 scalefont            % Scale the font to 20 points"<<endl;
    metrics <<"setfont"<<endl;      
    metrics <<"/inch { "<<fact_multiplicatif<<" mul} def  % Convert inches->points (1/72 inch)"<<endl;
    metrics <<linewidth<<"  setlinewidth "<<endl;
	
// 	write(50,*)'/Times-Roman findfont   % Get the basic font'
// 	write(50,*)'20 scalefont            % Scale the font to 20 points'
// 	write(50,*)'setfont'  
// 	write(50,*)'/inch {', fact_multiplicatif ,' mul} def      % Convert inches->points (1/72 inch)'
// 	write(50,*) linewidth,'  setlinewidth '





	double red   = 0;
	double green = 0;
	double blue  = 0;
    metrics<<"newpath"<<endl;
	//defines the color
	metrics<<std::scientific << std::setprecision(2)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;


	
	
	int iy,ix;
	int limit,inc;
// !########### creation of lines  ##############################
// !##############################################################
// !##############################################################
	if(answer.compare("l") == 0)
	{
		if(BLK1::crystal_symmetry.compare("hex") == 0){
			limit = 2;
			inc=1;
		
		}
		
		if(BLK1::crystal_symmetry.compare("square") == 0){
			limit = 4;
			inc=2;
		
		}		
		cout<<"ps_file_v2"<<endl;

		for(int k=0;k<limit;k=k+inc)
		for(int n=0;n<c.p.size();n++)
		{
			
			//fix			
			if(!check(c.bc_cond[n][k],2))	
				continue;
		
			//pbc
			if(bset.x ==2 ||  bset.y ==2){	
				iy = n / dimensions::nx;
				ix = n % dimensions::nx;
				if(ix==0 || ix==dimensions::nx-1)
					continue;
				if(iy==0 || iy==dimensions::ny-1)	
					continue;		
			}
			//zoom	
// 			if(ix>=dimensions::nx/2 || iy>=dimensions::nx/2)
// 				continue;
		
// 			int east = (iy)*nx       + ix + 1; //EAST
// 			int north =   (iy+1)*nx + ix    ; //NORTH    
// 			int west = (iy)*nx       + ix -1 ; //WEST
// 			int south = (iy-1)*nx    + ix    ; //SOUTH     
// 			int north_east =   (iy+1)*nx + ix+1    ; //NORTH    
			
			int east  = c.p[n].nn[k]; //EAST
			int north =   c.p[n].nn[k+1];    ; //NORTH    
// 			int west = (iy)*nx       + ix -1 ; //WEST
// 			int south = (iy-1)*nx    + ix    ; //SOUTH     
// 			int north_east =   (iy+1)*nx + ix+1    ; //NORTH    			
			
// 			if(iy % 2 == 0 )
// 			{	
// 				 east = (iy+1)*nx+ ix ;
// 				 north = (iy+1)*nx+ ix -1;
// 			}
// 
// 			if(iy % 2 != 0 )
// 			{
// 				 east = (iy+1)*nx+ ix +1;
// 				 north = (iy+1)*nx+ ix  ;
// 			}
			 metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<"  inch moveto"<<endl;
			 metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[east]<<" inch "<<dec_y+y_trace[east]<<"  inch lineto"<<endl;
			 metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[north]<<" inch "<<dec_y+y_trace[north]<<"  inch lineto"<<endl;
			 metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<"  inch lineto"<<endl;
			 metrics<<"  closepath"<<endl;
// 			 cout<<"energy: "<<ener[n]<<endl;
	// 		 metrics<<std::scientific << std::setprecision(7)<<" "<<ener[n]<<"  setgray"<<endl;
			 if(ener[n]>.12){
				 red   = 1-ener[n];
				 green = 1-ener[n];
				 blue  = 1;
				 metrics<<std::scientific << std::setprecision(7)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;
				 metrics<<"fill"<<endl;
			 }
			 
			 else{
				green = 0;
				blue  = 0;
				red=0;
				//defines the color
				metrics<<std::scientific << std::setprecision(2)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;
			 }
		 

			metrics <<"stroke "<<endl;

		}
	

  
// !########### creation of boundary circles for the answer line ##############################
// 
// 		//circles           
// 		red   = 0;
// 		green = 0;
// 		blue  = 1;
// 
// 		metrics<<std::scientific << std::setprecision(2)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;
// 
// 		for(int n=0;n<c.p.size();n++)
// 		{
// 			int iy = n / nx;
// 			int ix = n % nx;
// 			if((ix == 0 || ix == nx-1 || iy == 0 || iy == ny-1))
// 			{
// 				metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<"  inch moveto"<<endl;
// 				metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<" inch "<<rayon<<"  inch  0 360 arc"<<endl;
// 				metrics<<"fill"<<endl;
// 			}
// 
// 		}
// 		//     metrics <<"fill "<<endl;
// 		metrics <<"stroke "<<endl;
	}	

// !##############################################################
// !##############################################################
// !##############################################################

/*
// !########### creation of ONLY circles  ##############################
// 
// 	if(answer.compare("c") == 0)
// 	{
// 	
// 	
		metrics<<std::scientific << std::setprecision(2)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;

		for(int n=0;n<c.p.size();n++)
		{
// 			int east  =   c.p[n].nn[k];       //EAST
// 			int north =   c.p[n].nn[k+1];    ; //NORTH    

		
			
			metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<"  inch moveto"<<endl;
			metrics<<std::scientific << std::setprecision(7)<<" "<<dec_x+x_trace[n]<<" inch "<<dec_y+y_trace[n]<<" inch "<<rayon<<"  inch  0 360 arc"<<endl;
			
			red   = (1-ener[n])*.3;
			green = (1-ener[n]);
			blue  = ener[n]   ;
			metrics<<std::scientific << std::setprecision(2)<<" "<<red<<" "<<green<<" "<<blue<<" setrgbcolor"<<endl;
// 			metrics<<std::scientific << std::setprecision(7)<<" "<<ener[n]<<"  setgray"<<endl;

			metrics<<"fill"<<endl;
			
		}
	metrics <<"stroke "<<endl;
// 
// 	
// 	
// 	}
*/
// !##############################################################

// 100 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,'moveto')
// 101 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,e14.7,'  inch  0 360 arc')
// 110 format(e14.7,1x,'inch',1x,e14.7,1x,'inch',1x,'lineto')
// 120 format(1x,f4.2,1x,f4.2,1x,f4.2,3x,'setrgbcolor')
// 
// 
// 
// close(50)
// 

}









