#include "dynamic_static.h"
#include "dynamic_static_GB.h"

#include "namespaces.h"
#include "dlib.h"

#include "custom_inline_functions.h"
#include "common.h"
#include "boundary_conditions.h"
#include "calculations.h"
#include "result_memorize.h"
#include "input_output.h"
#include "vector_functions.h"
#include "avalanche_calculations.h"
#include "plot.h"
#include "assign_solution.h"
#include "slip_calculation.h"
#include "utilities.h"
#include "init.h"

std::pair<double, double> get_XY_for_rotated_square1(double x, double y, double angle, double interface,double keep_slip) {
    double nx=dimensions::nx;
    double ny=dimensions::ny;
    double cx = interface-1;
    double cy = keep_slip-1;
	
	double temp = (x-cx)*cos(angle) - (y-cy)*sin(angle) + cx;  
	          y = (x-cx)*sin(angle) + (y-cy)*cos(angle) + cy;
    x=temp;


	return std::pair<double, double>(x, y);
}


std::pair<double, double> get_XY_for_rotated_square2(double x, double y, double angle, double interface,double keep_slip) {
    double nx=dimensions::nx;
    double ny=dimensions::ny;
    double cx = interface;
    double cy = keep_slip-1;
	
	double temp = (x-cx)*cos(angle) - (y-cy)*sin(angle) + cx;  
	          y = (x-cx)*sin(angle) + (y-cy)*cos(angle) + cy ;
    x=temp;

	return std::pair<double, double>(x, y);
}


void measurements_GB(struct conf_stru& c, double theta )
{

		int nx = dimensions::nx;
	    int ny = dimensions::ny;
		fstream filestr;
		string filename2,filename3,filename4,filename5,filename6,filename7;
		string filename2_2="alpha_energy_GB.dat" ;

		double load = BLK1::load;
		double sum=0;
		int n=c.p.size();
		
		for(int i=0;i<n;++i){ 
			sum+= c.energy_local[i];
		}


  		filestr.open (filename2_2, fstream::in | fstream::out | fstream::app);
		filestr << std::scientific << std::setprecision(7)  <<" "<<theta << " "<<sum<<endl ;
				
  		filestr.close();


}

void statique_GB(struct conf_stru& c)
{
   
   
   	int nx = dimensions::nx;
	int ny = dimensions::ny;
	int n=c.p.size();
	double load=BLK1::load;
	double initial_strain  =BLK1::load;
	double temperature = BLK1::beta_Var;
	double theta_cr=BLK1::theta	;
	double interface=nx/2-1;
	double keep_slip,keep_slip2;
	double anglee,anglee2;
	double theta_cr2=17.6;

// 	initial_strain = initial_strain/(double)ny;

	int t=0;
	static int n_direction = 2;
// 	if(BLK1::strain_rate <0)
// 		n_direction = 3;
		
	
	const char* filename = "roule";

	ofstream myfile;
	myfile.open ("roule");
	
	//SHEARING ANGLE
 	double theta=BLK1::theta;

	int temp_counter=0;
	//for(int k=0;k<2;k=k+c.inc)
	for(int k=0;k<dimensions::triangle_limit;k=k+c.inc)
	for(int i=0;i<n;i++)
	{
		if(c.bc_cond[i][k] == 2)
			temp_counter++;
	}  
	
	c.total_triangular_number = temp_counter;
	cout<<"c.total_triangular_number= "<<c.total_triangular_number<<endl;


   //IF FRAME DEFORMATION ACTIVATED, C_PFIX STARTS AS A DEFORMED CONFIGURATION
		boundary_conditions set0,setold;
		set0.setParameters(theta,load);

	if(BLK1::bc_mode.compare("frame") ==0)
		set0.def_frame(force_energy::calculation::getInstance().c,set0);
	else
		set0.macro_shear();
	setold=set0;
   //These are displacements needed in CG

	for(int i=0;i<n;i++)
	{
		c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		c.disp[i].y  = c.p[i].y -c.pfix[i].y ;   

	}  

	////dipole v

 



    column_vector starting_point,sp_max,sp_min,starting_point2;
	starting_point.set_size(2*n);
	starting_point2.set_size(2*n);

	sp_max.set_size(2*n);
	sp_min.set_size(2*n);

	dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
		starting_point(i)     = c.disp[i].x  ;
		starting_point(i+n)   = c.disp[i].y  ;   	
	});

//to make the model scalar

	if(BLK1::scalar.compare("yes") ==0){
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0. ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = 0.  ;   	


		});  
	} 


	else if(BLK1::scalar.compare("semi") ==0){
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     = 1000  ;
			sp_max(i+n)   = 0.3 ;   	
			sp_min(i)     = -1000  ;
			sp_min(i+n)   = -0.3  ;   	


		});  
	} 





	else if(BLK1::scalar.compare("no") ==0){


std::vector<int> jump_index;
int jj=0;
int counter=0;
int li=0;

 for (int iy = 0; iy<ny; ++iy){
int i= iy*nx + 0;
int hh = round(c.pfix[i].y*2.*sin(0.5*theta_cr*dlib::pi/180.));
// cout<<"round= "<<hh<<" at "<<jj<<endl;
// cout<<"j= "<<c.pfix[i].y<<endl;

	if(hh	 >2*jj ){
// 		cout<<"adding dislocation at: "<< counter<<endl;
		jump_index.push_back(i);
		for (int iyy = iy; iyy<ny; ++iyy)
		for (int ix = 0; ix<nx; ++ix){
			int i= iyy*nx + ix;
// 			cout<<"adding dislocation"<<endl;
			if(c.pfix[i].y> jj){
// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
				if(c.pfix[i].x<interface ){
					starting_point(i)   =  -1*(jj+1);
					li=i;	
				}	
				
// 				if(c.pfix[i].x>=interface	){
// 					starting_point(i)   =  1.0*(jj+1);
// 				}
			}
		}
		if(jj==0){
				keep_slip	=c.pfix[i].y;
		}

		jj++;
		counter++;
	}

}

// for (int iy =0; iy<jump_index.size(); ++iy){
// 	cout<<"-----"<<endl;
// 	cout<<jump_index[iy]<<endl;
// 	cout<<c.pfix[jump_index[1]].y<<endl;
// 	cout<<c.pfix[jump_index[0]].y<<endl;
// }	

double phase = (c.pfix[jump_index[1]].y-c.pfix[jump_index[0]].y);
	cout<<phase/2<<endl;


jj=0;
for (int iy = 0; iy<ny; ++iy){
int i= iy*nx + 0;
int hh = round((c.pfix[i].y-phase/2.)*2.*sin(0.5*theta_cr*dlib::pi/180.));
// cout<<"round= "<<hh<<" at "<<jj<<endl;
// cout<<"j= "<<c.pfix[i].y<<endl;

	if(hh	 >2*jj	){
// 		cout<<"adding dislocation at: "<< counter<<endl;
		for (int iyy = iy; iyy<ny; ++iyy)
		for (int ix = 0; ix<nx; ++ix){
			int i= iyy*nx + ix;
// 			cout<<"adding dislocation"<<endl;
			if(c.pfix[i].y> jj){
// 				cout<<"c.pfix[i].y= "<<c.pfix[i].y<<" at "<<interface + tan(15.*dlib::pi/180.)*iyy<<endl;
// 				if(c.pfix[i].x<interface ){
// 					starting_point(i)   =  -1.0*(jj+1);
// 					li=i;	
// 				}	
				
				if(c.pfix[i].x>=interface	){
					starting_point(i)   = 1*(jj+1);
				}
			}
		}

		if(jj==0){
				keep_slip2	=c.pfix[i].y;
		}
		jj++;
		counter++;
	}

}


		cout<<"keep_slip= "<<keep_slip2<<endl;
		double yy = c.p[(ny-1)*nx+interface-1].y;
// 		double xx = (interface-1.)-(c.pfix[(ny-1)*nx+interface-1].x + starting_point((ny-1)*nx+interface-1) );
		double xx =-starting_point((ny-1)*nx+interface-1)+0.25;

		anglee = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
		cout<<"yy= "<<yy<<endl;
		cout<<"xx= "<<xx<<endl;
		cout<<"li= "<<li<<endl;
		
		cout<<"angle= "<<anglee<<endl;



		 yy = c.p[(ny-1)*nx+interface].y;
		 xx =-starting_point((ny-1)*nx+interface)-0.25;

		anglee2 = asin( xx / sqrt(xx*xx+yy*yy)  )*180./pi;
		cout<<"yy= "<<yy<<endl;
		cout<<"xx= "<<xx<<endl;
		cout<<"li= "<<li<<endl;
		
		cout<<"angle2= "<<anglee2<<endl;




		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i){
			sp_max(i)     =  1000.  ;
			sp_max(i+n)   =  1000. ;   	
			sp_min(i)     = -1000.  ;
			sp_min(i+n)   = -1000.  ;  
			if(c.pfix[i].y ==ny-1  || c.pfix[i].y ==0	){
// 			sp_max(i)   = 1.;  
			sp_max(i+n) = 0.1;    
// 			sp_min(i)   = -1;  
			sp_min(i+n) = -0.1;     
			
			}		 	
// 
// 				double h=1;	
// 				starting_point(i)     =  0*(1.1*v3[i] - 1.1*v4[i])	;
// 				starting_point(i+n)   = +0*1.1*(1.1*v1[i] - 1.1*v2[i]) ;   	
// 
// 				 int iy = i / nx;
// 				 int ix = i % nx;
// //  				 if(iy==ny/2-1 && (ix>nx/8 && ix< nx- nx/8)  )
// //  				 	starting_point(i) = .5001;
// //  				 if(iy==ny/2 && (ix>nx/8 && ix< nx- nx/8)  )
// //  				 	starting_point(i) = -.5001;
// 
// 
// // 				 if(iy==ny/2 && ix>nx/4 )
// // 				 	starting_point(i) = .35;


				c.disp[i].x +=starting_point(i) ;
				c.disp[i].y +=starting_point(i+n)   ;   	


		});  	
		
	} 
	
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
//	if(BLK1::IC.compare("wiener") ==0 || BLK1::IC.compare("wienerc") ==0 || BLK1::IC.compare("volume") ==0){
	if(BLK1::IC.compare("none") !=0 ){

		std::vector<double > u_dis = wiener( 0.,BLK1::d3,c.p.size()  );    
		std::vector<double > v_dis = wiener( 0.,BLK1::d3,c.p.size()  );  
		for(int i=0;i<n;i++){
			  c.disp[i].x +=u_dis[i];
			  c.disp[i].y +=v_dis[i];
		}
	}


	if(BLK1::IC.compare("volume") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(1., BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(1., BLK1::d2,c.p.size());
	  
		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;


		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][2] =dv3[i];


	}
	
	
	if(BLK1::IC.compare("alloying") ==0 ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 std::vector<double > dv1 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv2 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	 std::vector<double > dv3 =   wiener(BLK1::beta_Var,BLK1::d2,c.p.size());
	  
		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
				c.alloying[i][k]  = BLK1::beta_Var;


		for(int i=0;i<n;i++)
			  c.alloying[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.alloying[i][2] =dv3[i];


	}	
	
	
	if(BLK1::IC.compare("precip") ==0 || BLK1::IC.compare("precipx") ==0  ){
	  //std::vector<double > v1 = uniform_volumetric(1.,0.01,1.,c.p.size()  );    
	 double amp=1.2;
	 amp=BLK1::d1;
	 std::vector<double > dv1,dv2,dv3;
	  
	  dv1 =   perturba_precipate(amp,c.p.size());
	  dv2 =   perturba_precipate(amp,c.p.size());
	  dv3 =   perturba_precipate(amp,c.p.size());

		for(int k=0;k<3;k++)
			for(int i=0;i<n;i++)
			  c.disorder[i][k] =1;
		for(int i=0;i<n;i++)
			  c.disorder[i][0] =dv1[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv2[i];
		for(int i=0;i<n;i++)
			  c.disorder[i][1] =dv3[i];
		
	}	
	
	
	for(int i=0;i<n;i++)
	{

		c.p[i].x  = c.disp[i].x + c.pfix[i].x ; 
		c.p[i].y  = c.disp[i].y + c.pfix[i].y ;
	} 

	std::vector<punto_stru>  temp_coo;
	temp_coo.resize(c.p.size());
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c, 909);
// 	anglee = theta_cr/2.;
//     anglee2 = anglee2;
	for(int i=0;i<n;i++)
	{
		int iy = i / nx;
		int ix = i % nx;
		double x;
		double y;

		if(c.pfix[i].x<interface){
			auto real_coordinates = get_XY_for_rotated_square1(c.p[i].x, c.p[i].y, -(anglee)*dlib::pi/180.,interface,keep_slip);
			x = real_coordinates.first;
			y = real_coordinates.second;
			c.p[i].x  = x ; 
			c.p[i].y  = y ; 
		}
 
		if(c.pfix[i].x>= interface){
			auto real_coordinates = get_XY_for_rotated_square2(c.p[i].x, c.p[i].y, -(anglee2)*dlib::pi/180.,interface,keep_slip2);
			x = real_coordinates.first;
			y = real_coordinates.second;
			c.p[i].x  = x ; 
			c.p[i].y  = y ; 
		}
		
		
		
	} 
	
		cout<<"writing"<<endl;
	   write_to_a_ovito_file(c, 100);
	   
	   struct punto_stru p = c.p[(ny/2)*nx+(nx/2)+5];
	   struct de_stru d;
	   d.p[0] = p;
	   d.p[1] = c.p[p.nn[0]];
	   d.p[2] = c.p[p.nn[0+1]];
	   struct base_stru vv1;
	   	vv1.e1[0]= 0 ;
		vv1.e1[1]= 1 ;
		vv1.e2[0]= (d.p[2].x-d.p[0].x) ;
		vv1.e2[1]= (d.p[2].y-d.p[0].y) ;
		
	struct cella_stru cc;

	cc.c11 = sqrt(vv1.e1[0] * vv1.e1[0] + vv1.e1[1] * vv1.e1[1]);
	cc.c22 = sqrt(vv1.e2[0] * vv1.e2[0] + vv1.e2[1] * vv1.e2[1]);
	cc.c12 = vv1.e1[0] * vv1.e2[0] + vv1.e1[1] * vv1.e2[1];
		
	double costheta= (cc.c12)/(cc.c11*cc.c22);
	 cout<<"angle rotation= "<<acos(costheta)*180/dlib::pi<<endl;;	


	double distance = +100;
	double distance_max = -100;

	double vx,vx2;
	double vy,vy2;
	int tiy=ny/2-1;
	vx =c.p[tiy*nx+interface+5].x -c.p[tiy*nx+interface+4].x;
	vy =c.p[tiy*nx+interface+5].y -c.p[tiy*nx+interface+4].y;


	vx2 =c.p[tiy*nx+interface-5].x -c.p[tiy*nx+interface-6].x;
	vy2 =c.p[tiy*nx+interface-5].y -c.p[tiy*nx+interface-6].y;

	


	for(int iy=0;iy<ny;iy++){
		 double temp_distance = sqrt(pow(c.p[iy*nx+interface].x -c.p[iy*nx+interface-1].x,2.) +
							   pow(c.p[iy*nx+interface].y -c.p[iy*nx+interface-1].y,2.) );



// 		cout<<"distance= "<<temp_distance<<endl;
// 		cout<<"vx= "<<vx<<endl;
// 		cout<<"vy= "<<vy<<endl;
		
		if(temp_distance < distance)
			distance= temp_distance;
		if(temp_distance > distance_max)
			distance_max= temp_distance;


	}

			cout<<"final distance min= "<<distance<<endl;
			cout<<"final distance max= "<<distance_max<<endl;

// 		if(distance >1){
// 			c.p[iy*nx+interface].x -= vx*(distance-1.01);
// 			c.p[iy*nx+interface].y -= vy*(distance-1.01);
// 
// 		}
// 
// 		if(distance <0.5)
// 			c.p[iy*nx+interface].x += vx*(-distance+1.01);
// 			c.p[iy*nx+interface].y += vy*(-distance+1.01);

	if(BLK1::theta > 4.){

	for(int i=0;i<n;i++){

		if(c.pfix[i].x>= interface && distance_max > 1.8){
			c.p[i].x -= vx*(.12);
			c.p[i].y -= vy*(.12);
		}	

		if(c.pfix[i].x < interface && distance_max > 1.8){
			c.p[i].x += vx2*(.12);
			c.p[i].y += vy2*(.12);
		}	



		if(c.pfix[i].x>= interface && distance < 0.6){
			c.p[i].x += vx2*(1-distance)*0.5;
			c.p[i].y += vy2*(1-distance)*0.5;
		}	
		
		if(c.pfix[i].x< interface && distance < 0.6){
			c.p[i].x -= vx2*(1-distance)*0.5;
			c.p[i].y -= vy2*(1-distance)*0.5;
		}	
		

			
	} 
	}
	

	for(int i=0;i<n;i++){
		c.pfix[i].x  = c.p[i].x  ; 
		c.pfix[i].y  = c.p[i].y  ; 	
		c.pold[i].x  = c.p[i].x  ; 
		c.pold[i].y  = c.p[i].y  ; 		
		temp_coo[i].x  = c.p[i].x;
		temp_coo[i].y  = c.p[i].y;
		starting_point2(i)   = starting_point(i)=0;
		starting_point2(i+n) = starting_point(i+n)=0;
	}




	 
	for(int i=0;i<n;i++)
	{
		starting_point(i)    = c.disp[i].x  = c.p[i].x -c.pfix[i].x ; 
		starting_point(i+n)  =  c.disp[i].y = c.p[i].y -c.pfix[i].y ;   
		starting_point2(i)   = starting_point(i);
		starting_point2(i+n) = starting_point(i+n);

	
	} 
	cout<<"writing"<<endl;
	write_to_a_ovito_file(c, 99);

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////

	
	string dissipation,av_sizes,av_sizes2,av_sizes3,size_vs_energy,strain_vs_energy,slip;
	string  av_sizespitteri = "int_total_slip_vs_stress_drop.dat";
	string half_stress =  "half_stress_drop.dat";
	string full_stress =  "full_stress_drop.dat";
	string piola_stress =  "piola_stress_drop.dat";
	string piola_stress_full =  "piola_stress_drop_full.dat";

	dissipation="dissipation.dat" ;
	slip="slip_integer.dat" ;

	av_sizes="avalanches.dat" ;
	av_sizes2="stress_drop.dat" ;
	av_sizes3="strain_jump.dat" ;
	size_vs_energy="int_total_slip_vs_energy.dat" ;
	strain_vs_energy="total_slip_vs_energy.dat" ;

	string slip_grad  = "total_slip.dat";

	
	fstream filestr,filestrav,filestrav2,filestrav3,filestrse;
	
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	std::vector <std::vector<matrix_stru> > m_field;
	m_field.resize(n);
	for (int i = 0; i < n; ++i)
		m_field[i].resize(4);
                    
                



	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////


	
		dlib::parallel_for(BLK1::num_threads, 0,n, [&](int i)
		{
			starting_point(i)     = c.disp[i].x  ;
			starting_point(i+n)   = c.disp[i].y  ;   	
		});  
		
		double drop =0;
		double drop3 =0;

		double stress_initial=0;
		double stress_final =0;
		double strain_initial =initial_strain;
		double strain_final   =initial_strain;
		double energy_initial =0;
		double energy_final   =0;
		double slip_initial =0;
		double slip_final =0;
		double stress_final_pitteri=0;
		double stress_initial_pitteri=0;
		double total_slip =0;
		double stress_initial_full=0;
		double stress_final_full=0;
		double stress_initial_piola=0;
		double stress_final_piola=0;
		double stress_initial_piola_full=0;
		double stress_final_piola_full=0;


		c.load = load;

	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	
    for(int iter=0;iter<BLK1::total_iteration;++iter){	
		

// 	    if(iter>475)
// 	    	BLK1::setCg_save(1,5);

	    result_memorize(c);
	    if(iter%1 == 0){
	    	string answer = "l";
	    	//ps_file(c,t, answer);
	    }

		if(BLK1::constraint_min.compare("yes") ==0)
			apply_BC_box(c,sp_min,sp_max); 
		
// 		double energy_initial = energy_for_avalanches(c,set0,setold);
		//recall here that is is the energy and stress of loaded but not relaxed system
		energy_initial = rosen(starting_point)*(1.0);
		stress_initial = avalanche_sizes(c);
		stress_initial_pitteri = avalanche_sizes_half(c);
		stress_initial_full = avalanche_sizes_full(c);
		stress_initial_piola = avalanche_sizes_piola(c);
		stress_initial_piola_full = avalanche_sizes_piola_full(c);

		cout<<"conjugate gradient starts"<<endl;
		
		double wall0 = get_wall_time();
		double cpu0  = get_cpu_time();



		if(BLK1::constraint_min.compare("no") ==0){
		find_min(lbfgs_search_strategy(14),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, -n);
		}



		if(BLK1::constraint_min.compare("yes") ==0){
			find_min_box_constrained(lbfgs_search_strategy(14),  // The 10 here is basically a measure of how much memory L-BFGS will use.
			objective_delta_stop_strategy(BLK1::precision),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
			rosen, rosen_derivative, starting_point, sp_min,sp_max);
		}
		

		cout<<"conjugate gradient ends"<<endl;



		double wall1 = get_wall_time();
		double cpu1  = get_cpu_time();

		cout << "Wall Time = " << wall1 - wall0 << endl;
		cout << "CPU Time  = " << cpu1  - cpu0  << endl;
		
//  		cout<<rosen(starting_point);
		double sizes=0;
		energy_final = rosen(starting_point)*(1.0);
		
		
// 		for(int i=0;i<n;i++)
// 		 { 
// 			 double xx =  starting_point(i);
// 			 double yy =  starting_point(i+n); 
// 			 sizes +=  abs((c.pfix[i].x/BLK1::scale + xx)-temp_coo[i].x) ;
// 			 sizes +=  abs((c.pfix[i].y/BLK1::scale + yy)-temp_coo[i].y) ;
// 
// 		 }


		 //it updates the  solution for measurements and keeps the old solution
		 for(int i=0;i<n;i++)
		 { 
			 double xx =  starting_point(i);
			 double yy =  starting_point(i+n); 



			 c.p[i].x = force_energy::calculation::getInstance().c.pfix[i].x/BLK1::scale + xx;
			 c.p[i].y = force_energy::calculation::getInstance().c.pfix[i].y/BLK1::scale + yy;

		 }

		 

		 result_memorize(c);
		 measurements(c);
		 double total_slips = slip_calculations(c);
		 int total_slips_int = slip_calculations_integer(c);

		// cout <<"total_slips= "<<total_slips<<endl;
		// cout <<"total_slips_integer= "<<total_slips_int<<endl;




		 stress_final = avalanche_sizes(c);
		 stress_final_pitteri = avalanche_sizes_half(c);
		 stress_final_full = avalanche_sizes_full(c);
		 stress_final_piola = avalanche_sizes_piola(c);
		 stress_final_piola_full = avalanche_sizes_piola_full(c);
		 



		 //EULERIAN OR LAGRANGIAN
		 assign_solution(c,starting_point);	 
		 
		//keep the solution against any change
// 		for (int i=0; i<n; ++i){	
// 	  	  starting_point2(i)     = starting_point(i);
// 	 	  starting_point2(i+n)   = starting_point(i+n);			 
// 		}



		 //drop of stress
		 drop = (stress_initial-stress_final)*pow(-1.,n_direction);
		 //drop of  half stress
		 double drop4 = (stress_initial_pitteri-stress_final_pitteri)*pow(-1.,n_direction);
		 double dropfull = (stress_initial_full-stress_final_full)*pow(-1.,n_direction);
		 double droppiola = (stress_initial_piola-stress_final_piola)*pow(-1.,n_direction);
		 double droppiolafull = (stress_initial_piola_full-stress_final_piola_full)*pow(-1.,n_direction);


		if(iter>1 && total_slips_int > 1){
			//cout<< energy_initial-energy_final << endl;
			
			//energy dissipation
			filestr.open (dissipation, fstream::in | fstream::out | fstream::app);
			filestr << std::scientific << std::setprecision(7)   << " "<<energy_initial-energy_final<<endl ;
			filestr.close();

			//total slip
			filestrav.open (av_sizes, fstream::in | fstream::out | fstream::app);
			filestrav << std::scientific << std::setprecision(7)   << " "<<total_slips<<endl ;
			filestrav.close();

			//stress drop
			filestrav2.open (av_sizes2, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<drop<<endl ;
			filestrav2.close();

			//half stress drop
			filestrav2.open (half_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<<load<<" "<<drop4<<endl ;
			filestrav2.close();

			//full stress drop
			filestrav2.open (full_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<dropfull<<endl ;
			filestrav2.close();
			//piola stress drop
			filestrav2.open (piola_stress, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<droppiola<<endl ;
			filestrav2.close();

			//piola stress drop
			filestrav2.open (piola_stress_full, fstream::in | fstream::out | fstream::app);
			filestrav2 << std::scientific << std::setprecision(7)   
			<< " "<<droppiolafull<<endl ;
			filestrav2.close();



			//distance to jumps
			double drop2  = abs(load - strain_initial);
			strain_initial = load;

			filestrav3.open (av_sizes3, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(drop2)<<endl ;
			filestrav3.close();

			//total_slips_int versus strain
			filestrav3.open (av_sizespitteri, fstream::in | fstream::out | fstream::app);
			filestrav3 << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips_int)<< " "<<abs(drop) <<endl ;
			filestrav3.close();

			
			//total_slip_integer versus energy dissipation
			filestrse.open (size_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips_int)<< " "<<energy_initial-energy_final <<endl ;
			filestrse.close();
			
			//total_slip versus energy dissipation
			filestrse.open (strain_vs_energy, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<<abs(total_slips)<< " "<<energy_initial-energy_final <<endl ;
			filestrse.close();			
			
			//total_slip integer
			filestrse.open (slip, fstream::in | fstream::out | fstream::app);
			filestrse << std::scientific << std::setprecision(7)   
			<< " "<< " "<<total_slips_int <<endl ;
			filestrse.close();			


		}

		
		 
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
		boundary_conditions set1;
		set1.setParameters(theta,load);
		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
// 		if(BLK1::macro_def.compare("macro_shear_rhombic") ==0)
// 			set1.uni_macro_compression();

		
		
		//keep the previous solution
		for(int i =0;i<c.p.size();++i){
			temp_coo[i].x = c.p[i].x   ;				
			temp_coo[i].y = c.p[i].y   ;
			c.pold[i].x = c.p[i].x ;
			c.pold[i].y = c.p[i].y ;

		}

// 		def_grad temp;	
// 		temp= set1.f.multiply(setold.f.inverse(setold.f));
	// 
	// 	//guess the next solution incrementally		

		int inc = force_energy::calculation::getInstance().c.inc ;	
	

			//output operations
// 			ps_file(c,t); 
		//if(iter == 0 || iter%1==0){
		if(iter == 0 || (iter>1 && total_slips_int  > 200*nx/128)|| iter%1==0){
			//slicer(c,t);
			write_to_a_ovito_file(c,t);
			write_to_a_file(c,t++);
			write_to_a_ovito_file_remove_Fmacro(c,t,load);
			string filename = "last_load.dat";
			single_data_to_a_file(load,filename);
			if(BLK1::IC.compare("precipx") ==0 && iter==0){
				for(int k=0;k<3;k++)
					for(int i=0;i<n;i++)
			 		 c.disorder[i][k] =1;

			}
		//	do_delaunay(force_energy::calculation::getInstance().c,t,theta,load);
 		}
		




	


		 if(fexists(filename) == false){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }
				 
 	


	////////////*****************///////////////////////////
	////////////*****************///////////////////////////
	////////////C is changing here due to the loading///////////////////////////
//         apply_BC_generale(c,set1);
//  	apply_BC(c,load);
	////////////*****************///////////////////////////
	////////////*****************///////////////////////////




		if(n_direction % 2 == 0 && load>   BLK1::loading_limit )
			n_direction++;
		if(n_direction % 2 != 0 && load< - BLK1::loading_limit )
			n_direction++;

		c.load = load;
		initial_strain+=BLK1::strain_rate*pow(-1.,n_direction);
// 		temperature+=0.01;
		load  = initial_strain;
		BLK1::setNum(load);
// 		BLK1::setBeta(temperature);
 		
   		set1.setParameters(theta,load);
   		choose_bc(set1);
// 		if(BLK1::macro_def.compare("shear") ==0)
// 			set1.macro_shear();
// 		if(BLK1::macro_def.compare("tension") ==0)
// 			set1.macro_compression();
// 		if(BLK1::macro_def.compare("uni_tension") ==0)
// 			set1.uni_macro_compression();
// 
// 		if(BLK1::bc_mode.compare("frame") ==0)
// 			set1.def_frame(force_energy::calculation::getInstance().c,set1);
			
		setold=set0;
		set0=set1;
        
        apply_BC_generale(c,set1);
 		apply_BC(c,load);
 		
       struct conf_stru guess;





	for (int k=0; k<dimensions::triangle_limit; k=k+inc) 
		for (int i=0; i<n; ++i){
			
			//fix boundary condition
			if(check(c.bc_cond[i][k],0)){
				 //u_min and v_min
	  	      	starting_point(i)     = c.p[i].x - c.pfix[i].x;
	 	 		starting_point(i+n)   = c.p[i].y - c.pfix[i].y;
				 
			}

// 				starting_point2(i)     = c.p[i].x - c.pfix[i].x;
// 	 	 		starting_point2(i+n)   = c.p[i].y - c.pfix[i].y;
// 				if(BLK1::BC.compare("eulerian") == 0){
// 				  starting_point2(i)    = -c.p[i].x + c.p[i].x * temp.f11 + c.p[i].y * temp.f12  ;				
// 				  starting_point2(i+n)  = -c.p[i].y + c.p[i].x * temp.f21 + c.p[i].y * temp.f22  ;
// 				}
// 
// 
// 				if(BLK1::BC.compare("eulerian") != 0){
// 				  starting_point2(i)    = -c.pfix[i].x + c.p[i].x * temp.f11 + c.p[i].y * temp.f12  ;				
// 				  starting_point2(i+n)  = -c.pfix[i].y + c.p[i].x * temp.f21 + c.p[i].y * temp.f22  ;
// 				}


		 }


		 if(fexists(filename) == false){ 
		 	cout<<"CODE HAS BEEN STOPPED AT THE ITERATION  "<<iter<<"\n";break; 
		 }

 	}
// 	double wall1 = get_wall_time();
// 	double cpu1  = get_cpu_time();
// 
// 	cout << "Wall Time = " << wall1 - wall0 << endl;
// 	cout << "CPU Time  = " << cpu1  - cpu0  << endl;
	
	
		     p = c.p[(ny-1)*nx+(nx/2)-10];
	   d.p[0] = p;
	   d.p[1] = c.p[p.nn[0]];
	   d.p[2] = c.p[p.nn[0+1]];
	   	vv1.e1[0]= 1. ;
		vv1.e1[1]= 0. ;
		vv1.e2[0]= (d.p[1].x-d.p[0].x) ;
		vv1.e2[1]= (d.p[1].y-d.p[0].y) ;
		

	cc.c11 = sqrt(vv1.e1[0] * vv1.e1[0] + vv1.e1[1] * vv1.e1[1]);
	cc.c22 = sqrt(vv1.e2[0] * vv1.e2[0] + vv1.e2[1] * vv1.e2[1]);
	cc.c12 = vv1.e1[0] * vv1.e2[0] + vv1.e1[1] * vv1.e2[1];
		
	 costheta= (cc.c12)/(cc.c11*cc.c22);
	 cout<<"angle rotation= "<<acos(costheta)*180/dlib::pi<<endl;;	


		     p = c.p[(ny-1)*nx+(nx/2)+10];
	   d.p[0] = p;
	   d.p[1] = c.p[p.nn[0]];
	   d.p[2] = c.p[p.nn[0+1]];
	   	vv1.e1[0]= 1 ;
		vv1.e1[1]= 0 ;
		vv1.e2[0]= (d.p[1].x-d.p[0].x) ;
		vv1.e2[1]= (d.p[1].y-d.p[0].y) ;
		

	cc.c11 = sqrt(vv1.e1[0] * vv1.e1[0] + vv1.e1[1] * vv1.e1[1]);
	cc.c22 = sqrt(vv1.e2[0] * vv1.e2[0] + vv1.e2[1] * vv1.e2[1]);
	cc.c12 = vv1.e1[0] * vv1.e2[0] + vv1.e1[1] * vv1.e2[1];
		
	 double costheta2 = (cc.c12)/(cc.c11*cc.c22);
	 cout<<"angle rotation= "<<acos(costheta2)*180/dlib::pi<<endl;	
	 double angle_final  = acos(costheta2)*180/dlib::pi + acos(costheta)*180/dlib::pi;
	 result_memorize(c);
	  measurements_GB(c,angle_final);

 	
}




	