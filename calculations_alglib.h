#include "common.h"
#include "dlib.h"
#include "string_to_integer.h"
#include "structures.h"
#include "r2c_c2r.h"
#include "utilities.h"
#include "namespaces.h"

#include <iostream>
#include <map>
#define ARMA_ALLOW_FAKE_GCC 
#define ARMA_DONT_USE_WRAPPER

#define ARMA_DONT_USE_HDF5

#include "ap.h"

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "optimization.h"

using std::cout;
using std::endl;


void rosen_alglib(const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void *ptr);
void rosen_alglib_2(const alglib::real_1d_array &m, double &func, alglib::real_1d_array &grad, void *ptr);
/*
This function computes what is known as Rosenbrock's function.  It is
a function of two input variables and has a global minimum at (1,1).
So when we use this function to test out the optimization algorithms
we will see that the minimum found is indeed at the point (1,1).
*/


void apply_BC_box_alglib(struct conf_stru& c,alglib::real_1d_array& sp_min, alglib::real_1d_array& sp_max);
