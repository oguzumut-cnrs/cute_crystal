#include "namespaces.h"
#include "structures.h"


#include <fstream>
#include <iostream>
using namespace std;


	void read_input_file()
	{
		cout<<"input_file_reading"<<endl;
		std::ifstream file("./lagrange_input_cpp");
		string str;
		// nx,ny,nz
		getline(file, str, '?');
		file >> str;
		int nx, ny;
		file >> nx >> ny;
		dimensions::setNum(nx,ny);
		cout<<"verification system sizes -->"<<endl;
		cout<<"nx= "<<dimensions::nx<<endl;
		cout<<"ny= "<<dimensions::ny<<endl;
		
		getline(file, str, '?');
		file >> str;
		string method;
		file >> method;
		cout<<"verification of method  -->"<<endl;
		BLK1::setMethod(method);
		cout<<"method= "<<BLK1::method<<endl;

		
		getline(file, str, '?');
		file >> str;
		int n_threads;
		file >> n_threads;
		cout<<"verification of thread number  -->"<<endl;
		BLK1::setThread(n_threads);
		cout<<"thread number= "<<BLK1::num_threads<<endl;

		getline(file, str, '?');
		file >> str;
		double v1,v2;
		file >> v1 >> v2;
		cout<<"verification of beta  and theta values  -->"<<endl;
		BLK1::setBeta(v1);
		BLK1::setTheta(v2);
		cout<<"beta=  "<<BLK1::beta_Var<<endl;
		cout<<"theta= "<<BLK1::theta<<endl;

	
		getline(file, str, '?');
		file >> str;
		double dt;
		file >> dt;
		cout<<"verification of  time step   -->"<<endl;
		BLK1::setTimestep(dt);
		cout<<"dt= "<<BLK1::dt<<endl;
		
//  		
		getline(file, str, '?');
		file >> str;
		string analysis;
		file >> analysis;
		cout<<"verification of  analysis   -->"<<endl;
		BLK1::setAnalysis(analysis);
		cout<<"dt= "<<BLK1::method<<endl;

		getline(file, str, '?');
		file >> str;
		string v3,v4;
		file >> v3 >> v4;
		cout<<"verification of  BC and IC   -->"<<endl;
		BLK1::setBCandIC(v3,v4);
		cout<<"BC= "<<BLK1::BC<<endl;
		cout<<"IC= "<<BLK1::IC<<endl;

		getline(file, str, '?');
		file >> str;
		double is,sr;
		file >> is >> sr;
		cout<<"verification of  IS and SR   -->"<<endl;
		BLK1::setISandSR(is,sr);
		cout<<"IS= "<<BLK1::initial_strain<<endl;
		cout<<"SR= "<<BLK1::strain_rate<<endl;


		getline(file, str, '?');
		file >> str;
		int total_iteration;
		file >> total_iteration;
		cout<<"total_iteration number  -->"<<endl;
		BLK1::setITER(total_iteration);
		cout<<"total_iteration= "<<BLK1::total_iteration<<endl;

		getline(file, str, '?');
		file >> str;
		string macro_def;
		file >> macro_def;
		cout<<"macroscopic BC  -->"<<endl;
		BLK1::setMacro_def(macro_def);
		cout<<"macro_def= "<<BLK1::macro_def<<endl;


		getline(file, str, '?');
		file >> str;
		string crystal_symmetry;
		file >> crystal_symmetry;
		cout<<"crystal_symmetry   -->"<<endl;
		BLK1::setCrystal_symmetry(crystal_symmetry);
		cout<<"setCrystal_symmetry= "<<BLK1::crystal_symmetry<<endl;


		getline(file, str, '?');
		file >> str;
		double loading_limit;
		file >> loading_limit;
		cout<<"loading_limit   -->"<<endl;
		BLK1::setloading_limit(loading_limit);
		cout<<"loading_limit= "<<BLK1::loading_limit<<endl;


		getline(file, str, '?');
		file >> str;
		string bc_x,bc_y,bc_mode;
		file >> bc_x >> bc_y >> bc_mode;
		cout<<"verification of setBCnew   -->"<<endl;
		BLK1::setBCnew(bc_x,bc_y,bc_mode);
		cout<<"bc_x:    "<<BLK1::bc_x<<endl;
		cout<<"bc_y:    "<<BLK1::bc_y<<endl;
		cout<<"bc_mode: "<<BLK1::bc_mode<<endl;

		getline(file, str, '?');
		file >> str;
		string first;
		file >> first;
		cout<<"verification of setBCnew   -->"<<endl;
		BLK1::setPoly(first);
		cout<<"poly:    "<<BLK1::first<<endl;


		getline(file, str, '?');
		file >> str;
		string memory,config,config_u;
		file >> memory;
		getline(file, str, '?');
		file >> str;
		file >> config;
		getline(file, str, '?');
		file >> str;
		file >> config_u;
	
		cout<<"memory options   -->"<<endl;
		BLK1::setMemory(memory,config,config_u);
		cout<<"setMemory= "<<BLK1::memory<<endl;
		cout<<"setMemory= "<<BLK1::config<<endl;
		cout<<"setMemory= "<<BLK1::config_u<<endl;


		getline(file, str, '?');
		file >> str;
		string lattice;
		file >> lattice  ;
		cout<<"verification of grid creator   -->"<<endl;
		BLK1::setGrid_creator(lattice);
		cout<<"grid_creator:    "<<BLK1::grid_creator<<endl;




// 		
//  
//   		getline(file, str, '?');
// 		file >> str;
// 		string name_lengts;
// 		file >> name_lengts;
// 		
// 
//   		getline(file, str, '?');
// 		file >> str;
// 		string name_graddef;
// 		file >> name_graddef;
// 		
// 		
// 
//  		memory_file::setName(memory_answer,name,name_lengts,name_graddef);
//  		cout<<"memory_answer= "<<memory_answer<<endl;
// 		cout<<"memory_name= "<<name<<endl;
//  		cout<<"memory_lengts= "<<name_lengts<<endl;
// 		cout<<"memory_name_graddef= "<<name_graddef<<endl;
// 
// 
//  		getline(file, str, '?');
// 		file >> str;
// 		string numerical_method2;
// 		file >> numerical_method2;
// 
// 
//  		getline(file, str, '?');
// 		file >> str;
// 		string numerical_method;
// 		file >> numerical_method;
// 		
// 		numerical_method::setName(numerical_method,numerical_method2);
// 		cout<<"numerical_method= "<<numerical_method<<endl;
// 		cout<<"numerical_method2= "<<numerical_method2<<endl;
// 	 	
// 	 	
// 	 	getline(file, str, '?');
// 		file >> str;
// 		double skin;
// 		file >> skin;
// 		info_2D::setNum(skin);
// 		cout<<"skin= "<<info_2D::r_skin<<endl;
	
	
		
	}	